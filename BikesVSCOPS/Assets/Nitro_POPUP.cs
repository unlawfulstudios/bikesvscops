﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nitro_POPUP : Info_POPUPWindow {

    public Text nitro_amount;
    protected override void PopUp_Window_Open()
    {
        base.PopUp_Window_Open();

        nitro_amount.text = "YOU CURRENTLY HAVE " + GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value + " HITS OF NITROUS(NITROUS GIVES YOU A SECOND BOOST";
    }

}
