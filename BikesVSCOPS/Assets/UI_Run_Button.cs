﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



[System.Serializable]
public struct RUN_DATA
{
    public int RUNID ;
    public int CItyID;
    public RUN Rundata;
}

[System.Serializable]
public struct RUN_Button_Iamges
{
    public Image Lock_Image;
    public Image Check_Mark;
    public Image Button_BackGround;
}

public class UI_Run_Button : MainMenuButton {


    public RUN_Button_Iamges rUN_Button_Iamges;
    public RUN_DATA rUN_DATA;
    public Color locked_color;
    public Text RunText;
    public int Winamount;    
	protected override void Awake()
	{
		base.Awake();
		SceneName = "Run_"+rUN_DATA.RUNID;


	}

	protected override void Start()
    {

        if (GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.CAREER_MODE)
        {
            rUN_DATA.Rundata = GlobaleManagers.current_UserProfile.CAREER_Citys[rUN_DATA.CItyID - 1].Runs[rUN_DATA.RUNID - 1];
            if (rUN_DATA.Rundata.id != 1)
                rUN_DATA.Rundata.Unlocked.Value = GlobaleManagers.current_UserProfile.CAREER_Citys[rUN_DATA.CItyID - 1].Runs[rUN_DATA.RUNID - 1].Unlocked.Value;
            else rUN_DATA.Rundata.Unlocked.value = true;

        }
        else if(GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE) 
        {
            rUN_DATA.Rundata = GlobaleManagers.current_UserProfile.CAREER_Citys[rUN_DATA.CItyID - 1].Runs[rUN_DATA.RUNID - 1];
            if (rUN_DATA.Rundata.id != 1)
                rUN_DATA.Rundata.Unlocked.Value = GlobaleManagers.current_UserProfile.CAREER_Citys[rUN_DATA.CItyID - 1].Runs[rUN_DATA.RUNID - 1].Unlocked.Value;
            else rUN_DATA.Rundata.Unlocked.value = true;
            
        }
        else if(GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.invalid_MODE)
        {
            Debug.LogError("OPPS");
        }


        if (!rUN_DATA.Rundata.Unlocked.value)
            LocK_Run();
        else Unlock_Run();

        rUN_Button_Iamges.Check_Mark.gameObject.SetActive(rUN_DATA.Rundata.WinAmmount.value > 1);
    }

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        RunText.color = colors.pressedColor;

        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run = rUN_DATA.Rundata;
        if (rUN_DATA.Rundata.id != 8)
        {
            GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.next_Run = GlobaleManagers.current_UserProfile.CAREER_Citys[0].Runs[rUN_DATA.Rundata.id];
        }
        GlobaleManagers.loading_Manager.SET(true);

 
        Debug.Log(GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run.id);
    }

    
    public void LocK_Run()
    {
        interactable = false;
        toggle_Lock(true);
    }
    public void Unlock_Run()
    {
        CompleteRun();
        interactable = true;
        toggle_Lock(false);
    }
    private void toggle_Lock(bool toggole)
    {
        rUN_Button_Iamges.Lock_Image.gameObject.SetActive(toggole);      
    }
    public void CompleteRun()
    {
        rUN_Button_Iamges.Check_Mark.gameObject.SetActive(true);
    }

    public override void OnButton_Up()
    {
        base.OnButton_Up();
        RunText.color = colors.normalColor;
    }
    public override void OnButton_Down()
    {
        base.OnButton_Down();
        RunText.color = colors.pressedColor;
    }
    /*protected override void Reset()
    {
        rUN_Button_Iamges.Lock_Image = transform.GetChild(1).GetComponent<Image>();
        rUN_Button_Iamges.Check_Mark = transform.GetChild(2).GetComponent<Image>();
        rUN_Button_Iamges.Button_BackGround = GetComponent<Image>();
        targetGraphic = rUN_Button_Iamges.Button_BackGround;
    }*/

}
