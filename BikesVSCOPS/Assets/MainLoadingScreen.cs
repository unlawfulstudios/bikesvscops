﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLoadingScreen : MonoBehaviour {

    public float cut_time = 1;
    public void Start() { Invoke("CutOut",cut_time); }
    public void CutOut() { gameObject.SetActive(false); }
}
