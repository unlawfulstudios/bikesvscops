﻿
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
public class UserProfileDataManager : GloableBehaviors
{

    const int kMaxLogSize = 16382;
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    private bool addScorePressed;
    public bool loadProfile;

    public delegate void FirbaseListerners(Type listenerType);

    FirbaseListerners firbaseListerners;

    private string UserID;



    IEnumerator Start()
    {

        yield return new WaitUntil(() => GlobaleManagers.current_UserProfile.UserID != null);
        UserID = GlobaleManagers.current_UserProfile.UserID;

        addScorePressed = true;


        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });


    }

    // Initialize the Firebase database:
    protected virtual void InitializeFirebase()
    {
        FirebaseApp app = FirebaseApp.DefaultInstance;
        // NOTE: You'll need to replace this url with your Firebase App's database
        // path in order for the database connection to work correctly in editor.
        app.SetEditorDatabaseUrl("https://bikesvscops-198e7.firebaseio.com/");
        if (app.Options.DatabaseUrl != null) app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);
        StartListener();

        loadProfile = true;

    }


    protected void StartListener()
    {

      

    }

   
    public void ChildInArray(System.Object[] array ,string ArrayName, string valueChange)
    {

        System.Type userProfileType = array.GetType();

        for (int i = 0; i < array.Length ; i++)
        {
            FirebaseDatabase.DefaultInstance
          .GetReference("users").Child(UserID).Child(i.ToString()).Child(ArrayName).Child(valueChange)
          .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
           {
               if (e2.DatabaseError != null)
               {
                   Debug.LogError(e2.DatabaseError.Message + "!!!!!!!!!!!!!!!!!!");
                   return;

               }
               else
               {
                   Debug.Log(e2.Snapshot.Value + "!!!!!!!!!!!!!!!!!!!!!!!");
               }

               //if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
               //{
               //    foreach (var childSnapshot in e2.Snapshot.Children)
               //    {
               //        if (childSnapshot.Child(UserID).Child(array.GetType().ToString()) == null)
               //        {
               //            Debug.LogError("Bad data in sample.  Did you forget to call SetEditorDatabaseUrl with your project id?");
               //            break;
               //        }
               //        else
               //        {
               //            Debug.Log("Leaders entry : " +

               //   childSnapshot.ToString()+"!!!!!!!!!!!!!!!!!!");



               //        }

               //    }
               //}
           };
        }


    }
    public static string GetVariableName<T>(Expression<Func<T>> expr)
    {
        var body = (MemberExpression)expr.Body;

        return body.Member.Name;
    }
    public static string GetParameterName<T>(T item) where T : class
    {
        if (item == null)
            return string.Empty;

        return typeof(T).GetProperties()[0].Name;
    }
    private FieldInfo[] GetFieldInfoArray(System.Type userProfileType)
    {
        return userProfileType.GetFields(
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
    }
    protected void ShowField(FieldInfo fieldInfo, object containingClass)
    {
        ProfileKey profileKey = null;
        string fieldName = fieldInfo.Name;
        if (fieldInfo.FieldType.BaseType == typeof(ProfileKey))
        {
            profileKey = (ProfileKey)GetFieldValue<object>(containingClass, fieldName);
            Type sysType = profileKey.MySystemType;
            Type valueType = profileKey.MyValueType;
            System.TypeCode valueTypeCode = System.Type.GetTypeCode(valueType);
            System.TypeCode typeCode = System.Type.GetTypeCode(sysType);
        }
    }

    protected static T GetFieldValue<T>(object obj, string fieldName)
    {
        if (obj == null)
        {
            throw new ArgumentNullException("obj");
        }

        var field = obj.GetType().GetField(fieldName,
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        if (field == null)
        {
            throw new ArgumentException("fieldName", "No such field was found.");
        }

        if (!typeof(T).IsAssignableFrom(field.FieldType))
        {
            throw new InvalidOperationException("Field type and requested type are not compatible.");
        }

        return (T)field.GetValue(obj);
    }
}
