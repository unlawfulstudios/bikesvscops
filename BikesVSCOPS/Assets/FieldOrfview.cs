﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOrfview : MonoBehaviour
{

    public float viewRadius;

    [Range(0, 360)]
    public float viewAngle;

    public Color viewColor;

    public LayerMask Player_Mask;
    public LayerMask obstacleMask;

 
    public Vector3 front_SensorPostion = new Vector3(0, 0.2f, 0.5f);
    public float front_SideSensorPostion = 0.5f;
    public float front_SensorAngle = 30f;



    public List<Transform> visibleTargets = new List<Transform>();

    public Cop_AI Cop_Controller { get { return this.GetComponent<Cop_AI>(); } }

    public float meshRez;

    Transform target;

    private void Start()
    {
        StartCoroutine(FindDelay(0.2f));

      
    }


    IEnumerator FindDelay(float dealay)
    {
        while (true)
        {
            yield return new WaitForSeconds(dealay);
            FindTarget();
        }
    }


    public void FindTarget()
    {

        Find_Player();

        if (target)
            return;
    }

    void Find_Player()
    {
        visibleTargets.Clear();
     
        Collider[] targetsInView = Physics.OverlapSphere(transform.position, viewRadius, Player_Mask);

        for (int i = 0; i < targetsInView.Length; i++)
        {
            target = targetsInView[i].transform;
            Vector3 dirtoTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirtoTarget) < viewAngle / 2)
            {
                float disttoTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, dirtoTarget, disttoTarget, obstacleMask))
                {
                    visibleTargets.Add(target);

                    GetComponent<Cop_AI>().chase = true;

             
                    //if (GetComponent<Cop_AI>() != null && GetComponent<Cop_AI>().isintraffic)
                    //    GetComponent<TrafficSystemVehicle>().enabled = false;

                    if (target.gameObject.GetComponent<CopDistanceController>() != null)
                        target.gameObject.GetComponent<CopDistanceController>().StartChase();


                    //GetComponent<Cop_AI>().Set_COlliders(true);


                }
            }
        }

       

    }

 

    public Vector3 Dir_FromAngle(float angelsInDegrees, bool angleIsgloable)
    {
        if (!angleIsgloable)
        {
            angelsInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angelsInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angelsInDegrees * Mathf.Deg2Rad));

    }



}
