﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public enum CopType
{
    invailed,
    wait,
    chase
}

public class Cop_AI : InGameBehaviors
{
    public float agrassrionLevel = 1;

    public bool avoiding;

    public Renderer Body;

    public bool boost;

    public bool chase;

    public float checkDistance = 0.5f;
    public CopType copType;

    public float currentSpeed;

    public float front_SensorAngle = 30f;

    public Vector3 front_SensorPostion = new Vector3(0, 0.2f, 0.5f);

    public float front_SideSensorPostion = 0.5f;

    public bool hittingPlayer = false;

    public bool is_Breaking;


    public bool isintraffic = false;

    public GameObject Light_Objects;

    public float LookAtDamping = 1f;

    public Transform lookAtPoint;

    public LayerMask mask = -1;

    public float max_BrakeTourq = 0.2f;

    public float max_MotorTorque = 50f;

    public float max_SteerAngle = 40f;

    public float maxPlayerDistance = 50;

    public float MaxSpeed = 100;

    public float min_MotorTorque = 0f;

    public Transform player_Target;
    public float respawnCounter;

    public float respawnWait = 5f;

    public float reversCounter;

    public bool reversing;

    public float revFor = 1.5f;

    public float Sensor_Length = 5f;
    private AudioSource SirensAudioSource;

    public GameObject skids1;

    public GameObject skids2;

    public AudioSource skidsource;

    public Transform startPoint;

    public float targetStreerAngle;

    public float TurnSpeed = 3;

    public float waittoRevers = 2.0f;

    public WheelCollider wheel_BL;

    public WheelCollider wheel_BR;

    public WheelCollider wheel_FL;

    public WheelCollider wheel_FR;


    private WheelHit wheelHit;


    public bool Is_GetAwayMode()
    {
        return GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE;
    }

    public Rigidbody rigidbody;
    private void Awake()
    {
        Light_Objects.SetActive(false);
        InGameManagers.object_Manager.AddCops(this);
        SirensAudioSource = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        if (copType == CopType.chase)
            chase = true;
        else
            chase = false;

        Set_Skids(false);
        respawnCounter = 1;


        respawnCounter = respawnWait;
    }

    private void Start()
    {
        Set_Up_Cop(InGameManagers.Racer_Manager.Player_BikeControl);
    }

    public void Set_Up_Cop(Player_BikeController playerTarget)
    {
        player_Target = playerTarget.transform;

        if (Is_GetAwayMode())
        {
            max_MotorTorque = 40 * InGameManagers.Racer_Manager.Player_BikeControl.bikeSetting.bikeAttributes.MaxSpeed;
            Debug.Log("is getawaymode");
        }
    }

    public void Set_COlliders(bool toggle)
    {
        if (!isintraffic)
            return;

        wheel_FL.enabled = toggle;

        wheel_FR.enabled = toggle;

        wheel_BL.enabled = toggle;

        wheel_BR.enabled = toggle;
    }


    private void Update()
    {
        if (Loading_Manager.isLoading) GetComponent<AudioSource>().enabled = false;

        Sensors();
    }

    private void FixedUpdate()
    {
        if (!InGameEventManager.Active) return;


        if (!isintraffic)
        {
            DriveUpdate();
        }
        else
        {
            if (chase) DriveUpdate();
        }

        Set_Skids(is_Breaking);
    }

    private void DriveUpdate()
    {
        ApplySteer();
        Drive();
        CheckwaypointDidstanice();
        Breaking();
        lerptoStearAngel();
        Respawn();
    }

    private void ApplySteer()
    {
        if (avoiding)
            return;

        if (player_Target == null)
            return;

        var relativeVector = transform.InverseTransformPoint(player_Target.position);

        relativeVector /= relativeVector.magnitude;


        var new_Steer = relativeVector.x / relativeVector.magnitude * max_SteerAngle;
        targetStreerAngle = new_Steer;
    }

    private void CheckwaypointDidstanice()
    {
        if (player_Target == null)
            return;
        if (!chase)
            return;


        if (Vector3.Distance(transform.position, player_Target.position) < checkDistance)
        {
            boost = false;

            var distPlayer = player_Target.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(distPlayer),
                Time.deltaTime * LookAtDamping);
            var zAngle = transform.eulerAngles.z;
            if (zAngle > 0 && zAngle <= 180)
                zAngle = 0;
            else if (zAngle < 340 && zAngle > 180) zAngle = 0;
        }
        else
        {
            boost = true;
        }
    }

    private void Drive()
    {
        if (!chase)
            return;

        if (player_Target == null)
            return;

        currentSpeed = 2 * Mathf.PI * wheel_FL.radius * wheel_FL.rpm * 60 / 1000;

        if (chase)
            Play_Sirens();


        if (currentSpeed < MaxSpeed && !is_Breaking)
        {
            if (!reversing)
                Set_motorTorque(max_MotorTorque);
            else
                Set_motorTorque(-max_MotorTorque);
        }
        else
        {
            Set_motorTorque(0);
        }
    }

    public void Play_Sirens()
    {
        if (!SirensAudioSource)
            SirensAudioSource.Play();

        Light_Objects.SetActive(true);
    }

    public void Stop_Sirens()
    {
        Light_Objects.SetActive(false);
        SirensAudioSource.Stop();
    }


    private void Breaking()
    {
        if (is_Breaking && chase)
        {
            Set_BrakeTorque(max_BrakeTourq);
            PlayBrakeSound();
        }
        else if (is_Breaking && !chase)
        {
            Set_BrakeTorque(max_BrakeTourq);
        }
        else
        {
            Set_BrakeTorque(0);
            StopBreaksound();
        }

        // Debug.Log("ISbreaking" + is_Breaking);
    }

    public void PlayBrakeSound()
    {
        //Play the clip.
        if (!skidsource.isPlaying)
            skidsource.Play();
    }

    public void StopBreaksound()
    {
        if (skidsource.isPlaying)
            skidsource.Stop();
    }

    private void lerptoStearAngel()
    {
        wheel_FR.steerAngle = Mathf.Lerp(wheel_FR.steerAngle, targetStreerAngle, Time.deltaTime * TurnSpeed);
        wheel_FL.steerAngle = Mathf.Lerp(wheel_FL.steerAngle, targetStreerAngle, Time.deltaTime * TurnSpeed);

        wheel_FR.GetGroundHit(out wheelHit);


        if (Math.Abs(wheelHit.sidewaysSlip) > 0.4f) is_Breaking = true;

        if (Math.Abs(wheelHit.sidewaysSlip) <= 0.25f) is_Breaking = false;


        // Debug.Log(is_Breaking);
    }

    private void Set_motorTorque(float amount)
    {
        wheel_FL.motorTorque = amount;
        wheel_FR.motorTorque = amount;
    }

    private void Set_BrakeTorque(float amount)
    {
        wheel_FL.brakeTorque = amount;
        wheel_FR.brakeTorque = amount;
    }

    public void Set_Skids(bool on)
    {
        skids1.SetActive(on);
        skids2.SetActive(on);
    }

    public bool Stoped()
    {
        return rigidbody.velocity.magnitude < 0.5f && !reversing && currentSpeed < 1 ? false : true;
    }

    public bool PlayerOutOfRange()
    {
        return DistanceComparer.distanceRange(gameObject, player_Target.gameObject) > maxPlayerDistance ? true : false;
    }

    private void Respawn()
    {
        if (player_Target == null)
            return;

        if (hittingPlayer)
            return;

        if (!chase)
            return;

        if (!Body.isVisible && respawnCounter >= 0) respawnCounter -= respawnWait;

        if (JoeMath.isEqual(respawnCounter, 0)) Spawn();
    }

    public void Spawn()
    {
        if (hittingPlayer)
            return;

        if (!chase)
            return;

        var closeNode = InGameManagers.object_Manager.Get_Closest_Node(player_Target, Random.Range(2, 3));

        if (closeNode.CanSpawn())
        {
            transform.position = closeNode.transform.position;
            transform.rotation = closeNode.SpawnRotation;
            //transform.LookAt(player_Target);
            respawnCounter = 1;
            Debug.Log("Respawn");
            respawnCounter = 0;
            Stop_Sirens();
            is_Breaking = false;
            reversing = false;
        }
    }

    private void Sensors()
    {
        RaycastHit hit;

        var sensor_StartingPOS = transform.position;
        sensor_StartingPOS += front_SensorPostion * front_SensorPostion.z;
        sensor_StartingPOS += transform.up * front_SensorPostion.y;

        float avoidMultiplier = 0;


        avoiding = false;

        // front right Senesor
        sensor_StartingPOS += transform.right * front_SideSensorPostion;

        if (Physics.Raycast(sensor_StartingPOS, transform.forward, out hit, Sensor_Length, mask))
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1, false, false);
        // front right Senesor angle
        else if (Physics.Raycast(sensor_StartingPOS,
            Quaternion.AngleAxis(front_SensorAngle, transform.up) * transform.forward, out hit, Sensor_Length, mask))
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 0.5f, false, false);
        // front right Senesor
        sensor_StartingPOS -= transform.right * front_SideSensorPostion * 2;

        if (Physics.Raycast(sensor_StartingPOS, transform.forward * 2, out hit, Sensor_Length, mask))
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1f, true, false);
        // front left Senesor angle
        else if (Physics.Raycast(sensor_StartingPOS,
            Quaternion.AngleAxis(-front_SensorAngle, transform.up) * transform.forward, out hit, Sensor_Length, mask))
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 0.5f, true, false);


        if (avoidMultiplier == 0)
            if (Physics.Raycast(sensor_StartingPOS, transform.forward, out hit, Sensor_Length, mask))
                Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1f, true, true);
        if (avoiding)
        {
            targetStreerAngle = max_SteerAngle * avoidMultiplier;

            reversCounter -= waittoRevers * Time.deltaTime;

            if (reversCounter <= 0)
            {
                reversing = true;
                reversCounter = 0;
            }
            else
            {
                reversing = false;
            }
        }
        else
        {
            reversing = false;
            reversCounter = 0.2f;
        }
    }

    public void Sense(Vector3 startPoint, RaycastHit hit, out float avoid_Multiplier, float movement_Amount, bool add,
        bool equals_Zero)
    {
        avoid_Multiplier = 0;

        var dist = Vector3.Distance(startPoint, hit.transform.position);

        Debug.DrawLine(startPoint, hit.point);
        avoiding = true;
        //avoid_Multiplier -= 1f;

        if (!equals_Zero)
        {
            if (!add)
                avoid_Multiplier -= movement_Amount;
            else
                avoid_Multiplier += movement_Amount;
        }
        else
        {
            if (hit.normal.x < 0)
                avoid_Multiplier = -movement_Amount;
            else
                avoid_Multiplier = movement_Amount;
        }
    }
}