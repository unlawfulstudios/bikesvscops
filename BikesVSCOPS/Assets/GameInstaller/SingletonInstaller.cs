using UnityEngine;
using Zenject;
using System;

public class SingletonInstaller<T,castT> : Installer<SingletonInstaller<T,castT>> 
{
    public castT cast;
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo(typeof(T)).AsSingle().WithArguments(cast).NonLazy();
          
    }
    public SingletonInstaller(castT castt)
    {
        
    }
}

