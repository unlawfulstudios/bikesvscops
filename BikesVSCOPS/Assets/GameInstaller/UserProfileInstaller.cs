using UnityEngine;
using Zenject;

public class UserProfileInstaller : MonoInstaller<UserProfileInstaller>
{
    public override void InstallBindings()
    {
        User_ProfileInstaller.Install(Container);
      
    }
}