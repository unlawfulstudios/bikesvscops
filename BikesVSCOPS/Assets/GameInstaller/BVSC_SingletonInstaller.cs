using UnityEngine;
using Zenject;
using System;
using System.IO;

public class BVSC_SingletonInstaller : MonoInstaller<BVSC_SingletonInstaller>
{

    public Garage_ManagerSettings gm;
    public SoundManagerSettings MainMenuSoundSettings;

    public override void InstallBindings()
    {
      
        Container.BindInterfacesAndSelfTo<Garage_Manager>().AsSingle().WithArguments(gm).NonLazy();
        Container.BindInterfacesAndSelfTo<MainMenuSoundManager>().AsSingle().WithArguments(MainMenuSoundSettings).NonLazy();
        Container.BindInterfacesAndSelfTo<MainMenu_Managers>().AsSingle().NonLazy();


        Debug.Log("MOOOOOOOOOOOOOOOOOOOOP");
       

    }




}
