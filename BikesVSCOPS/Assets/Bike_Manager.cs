﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bike_Manager : InGameBehaviors {

    public bool debug_mode = false;
    public int debug_BikeId = 0;

    public Player[] Player_Bikes;

    public void Start()
    {
        if (!debug_mode)
        {
            Set_Bike(GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike.attributes.BikeID - 1);
        }
        else
        {
            GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike = GlobaleManagers.current_UserProfile.BikeList[debug_BikeId];
            Set_Bike(debug_BikeId);
        }
    }

    private void Set_Bike(int id)
    {
        Player_Bikes[id].transform.SetParent(null);
        Player_Bikes[id].gameObject.SetActive(true);
        player = Player_Bikes[id];

    }
}
