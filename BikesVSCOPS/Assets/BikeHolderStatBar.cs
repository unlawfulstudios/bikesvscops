﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BikeHolderStatBar : MonoBehaviour {

    public Image[] bars;

#if UNITY_EDITOR

    private void Reset()
    {
        bars = transform.GetComponentsInChildren<Image>();
    }

#endif
}
