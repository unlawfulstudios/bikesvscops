﻿using UnityEngine;


[System.Serializable]
public class BikeAttributes
{
    public int BikeID = 0;
    public float MaxSpeed = 0;
    public float Acceleration = 0;
    public float Handling = 0;
    public float MaxTiltAngle = 0;
    public float purchase_Price = 0;

}

[System.Serializable]
public class Bike : UserProfileObject
{

    public override string Name
    {
        get
        {
            return "Bikes";
        }
    }
    public BoolData UNLOCKED = null;
    public BikeAttributes attributes;


    public override void SyncData()
    {
        UNLOCKED.SyncData();
        base.SyncData();
    }

    public override void Reset()
    {
        UNLOCKED.Reset();
        base.Reset();
    }
    protected override bool m_DataSynced()
    {
        return UNLOCKED.DataSynced;
    }
    public void Set_Up(out BikeAttributes _attributes)
    {
        _attributes = attributes;
    }


    public Bike(int id)
    {
        m_index = id;


        attributes = bikeInventoryItemList.bike_Data_list[id].bikeAttributes;

        UNLOCKED = new BoolData("UNLOCKED" + m_index, false);

        if (m_index == 0)
        {
            UNLOCKED.value = true;
        }

    }

    public Bike()
    {

    }

    public string Json_Value()
    {
        return JsonUtility.ToJson(this);
    }
    private int m_index;


    
}
