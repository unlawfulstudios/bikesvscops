﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Run_Manager : MonoSingleton<Run_Manager> {

    public Transform runPlath;

    public Transform[] Playerbikes;
    public Transform Player_Start_Point;

    public Transform Ai;
    public Transform AI_StartPoint;

    public Transform runPath_Position;

    public Transform BikeManager;

    public EndPoint EndPoint;
    public AIPath current_AIPAth;


    public bool Set_Rotaition = false;
    public bool Set_Length = false;

    private Vector3 current_Angle;



    [Range(0 ,360)]
    public float Bike_Angle = 0;
    [Range(-100, 100)]
    public float Hight = 0;
    [Range(-100, 100)] 
    public float Length = 0;


    public bool Move_Bike;

    protected override void Init()
    {
        Debug.Log(" Run_Manager Initialize");
    }

   

    private void Update()
    {
#if UNITY_EDITOR
        if (Set_Rotaition)
        {
            current_Angle = new Vector3(0, Bike_Angle);

            for (int i = 0; i < Playerbikes.Length; i++)
            {
                Playerbikes[i].localEulerAngles = current_Angle;
            }

            Ai.transform.localEulerAngles = current_Angle;

            if(!Move_Bike)
            BikeManager.eulerAngles = current_Angle;


        }
        if (Set_Length)
        {


            for (int i = 0; i < Playerbikes.Length; i++)
            {
                Playerbikes[i].localPosition = new Vector3(0, Hight, Length);
            }

            Ai.localPosition = new Vector3(0, Hight, -Length);
        }

#endif
    }



    public void Set_Player()
    {
        runPlath.position = runPath_Position.transform.position;
        BikeManager.position = runPlath.position;

        for (int i = 0; i < Playerbikes.Length; i++)
        {
            Playerbikes[i].position = Player_Start_Point.position;
        }
   
        Ai.position = AI_StartPoint.position;
        Vector3 movetoPoint = new Vector3(AI_StartPoint.transform.position.x, AI_StartPoint.transform.position.y + 10, AI_StartPoint.transform.position.z);
    }


}
