This Emergency Signal Flare comes complete with animated flames, smoke, flickering light and audio

Simply load the example scene - Emergency_Signal_Flare.unity - or drag the Emergency_Signal_Flare prefabs from the Prefabs folder into the Hierarchy window.

The effect includes audio, however if you wish to disable this then expand out the effect in the Hierarchy window (click the grey triangle to left of its name), select the audio file, then untick it in the Inspector window (the top left of the window).

You may wish to change the size of the flames, in which case simply scale the flames mesh; expand out the effect in the Hierarchy window, select the flames mesh (Flare_Flame), press the "r" key, then drag the manipulator.

Have fun!

If you have any questions then please get in touch at bendurrant@rivermillstudios.com


