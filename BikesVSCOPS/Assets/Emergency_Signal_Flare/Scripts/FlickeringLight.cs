using UnityEngine;
using System.Collections;

public class FlickeringLight : MonoBehaviour {

public Light fuseLight;
private float fuseLightIntensity = 0.4f;

void Start (){

}

void Update (){

    fuseLightIntensity = (Random.Range (0.6f, 0.7f));
    fuseLight.intensity = fuseLightIntensity;

}
}