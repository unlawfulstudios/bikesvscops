﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class JoeMath
{

    public static bool isApproximatelyEqual(float a, float b)
    {
        if (Mathf.Approximately(a, b))
            return true;
        else
            return false;
 
    }
    public static bool isApproximatelyEqual(int a, int b)
    {
        if (Mathf.Approximately(a, b))
            return true;
        else
            return false;
    }

    public static bool isEqual(float a, float b)
    {
        if (a >= b - Mathf.Epsilon && a <= b + Mathf.Epsilon)
            return true;
        else
            return false;
    }
    public static bool isEqual(int a, int b)
    {
        if (a >= b - Mathf.Epsilon && a <= b + Mathf.Epsilon)
            return true;
        else
            return false;
    }
}

