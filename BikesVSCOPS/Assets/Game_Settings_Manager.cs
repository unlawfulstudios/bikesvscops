﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Game_Settings_Manager : UserProfileObject
{
    public override string Name
    {
        get
        {
            return "Game_Settings";
        }
    }

    public UserProfileKey<int, bool> Has_Sound = new UserProfileKey<int, bool>("Has Sound ", true);
    public UserProfileKey<int, bool> Cam1stPersonView = new UserProfileKey<int, bool>("Cam1stPersonView ", false);
    public UserProfileKey<int, bool> DebugMode = new UserProfileKey<int, bool>("Debug Mode", true);
    public UserProfileKey<int, bool> ForceFPS60 = new UserProfileKey<int, bool>("ForceFPS60", true);


}

                    


