﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AI_Engine : MonoBehaviour
{

    public Transform path;

    public List<Node> nodes;

    private int current_Node = 0;

    public float max_SteerAngle = 40f;

    //public WheelCollider wheel_FC;

    public float checkDistance = 0.5f;

    public float maxMotorTorque = 1f;

    public float currentSpeed = 0;
    public Bike_Controller Bike_Controller;
    private float MaxSpeed() { return Bike_Controller.bikeSetting.bikeAttributes.Acceleration;}

    public bool is_Breaking = false;
    //public WheelCollider wheel_FR;

    //public controlHub hub;

    public bool Finshed;
    public Quaternion startRotaion;

    public Node currentNode;
 
    [Header("Sensors")]

    public float Sensor_Length = 5f;

    public Vector3 front_SensorPostion = new Vector3(0, 0.2f, 0.5f);
    public float front_SideSensorPostion = 0.5f;
    public float front_SensorAngle = 30f;

    //public float front_SideSensorPostion = 0.5f;

    public Transform pos;

    public EventManager event_Manager;


    private float targetStreerAngle = 0;

    private bool avoiding = false;

    public float TurnSpeed = 3;

    public Transform startPoint;

    public bool reversing = false;

    public float reversCounter = 0.0f;

    public float waittoRevers = 2.0f;

    public float revFor = 1.5f;


    public Rigidbody Current_Rigidbody;
    public LayerMask mask;

    private void Awake()
    {
        startRotaion = transform.rotation;
    }

    void Start()
    {
        Node[] path_transform = path.GetComponentsInChildren<Node>();
        nodes = new List<Node>();

        for (int i = 0; i < path_transform.Length; i++)
        {
            if (path_transform[i] != path.transform)
            {
                nodes.Add(path_transform[i]);
            }
        }
    }

    public void ReSet()
    {
        this.transform.position = startPoint.position;
        current_Node = 0;
        transform.rotation = startRotaion;
        is_Breaking = true;
    }

    private void FixedUpdate()
    {
        if (!InGameEventManager.Active)
            return;

        Sensors();
        ApplySteer();
        Drive();
        CheckwaypointDidstanice();
        Breaking();
        lerptoStearAngel();
        Respawn();

    }



    private void Sensors()
    {
        RaycastHit hit;

        Vector3 sensor_StartingPOS = transform.position;
        sensor_StartingPOS += front_SensorPostion * front_SensorPostion.z;
        sensor_StartingPOS += transform.up * front_SensorPostion.y;

        float avoidMultiplier = 0;


        avoiding = false;
        is_Breaking = false;

        // front right Senesor
        sensor_StartingPOS += transform.right * front_SideSensorPostion;

        if (Physics.Raycast(sensor_StartingPOS, transform.forward, out hit, Sensor_Length, mask))
        {
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1, false, false);
        }
        // front right Senesor angle
        else if (Physics.Raycast(sensor_StartingPOS, Quaternion.AngleAxis(front_SensorAngle, transform.up) * transform.forward, out hit, Sensor_Length, mask))
        {
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 0.5f, false, false);
        }
        // front right Senesor
        sensor_StartingPOS -= transform.right * front_SideSensorPostion * 2;

        if (Physics.Raycast(sensor_StartingPOS, transform.forward * 2, out hit, Sensor_Length, mask))
        {
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1f, true, false);
        }
        // front left Senesor angle
        else if (Physics.Raycast(sensor_StartingPOS, Quaternion.AngleAxis(-front_SensorAngle, transform.up) * transform.forward, out hit, Sensor_Length, mask))
        {
            Sense(sensor_StartingPOS, hit, out avoidMultiplier, 0.5f, true, false);
        }


        if (JoeMath.isEqual(avoidMultiplier, 0))
        {
            // front center sensor 
            if (Physics.Raycast(sensor_StartingPOS, transform.forward, out hit, Sensor_Length, mask))
            {
                is_Breaking = true;
                Sense(sensor_StartingPOS, hit, out avoidMultiplier, 1f, true, true);
            }

        }
        if (avoiding)
        {
            targetStreerAngle = max_SteerAngle * avoidMultiplier;


            reversCounter -= Time.deltaTime * waittoRevers;

            if (JoeMath.isEqual(respawnCounter,0))
            {
                reversing = true;
                reversCounter = 0;
            }
            else
            {
                reversing = false;
            }
        }
        else
        {
            reversCounter = 100;
        }

    }


    public void Sense(Vector3 startPoint, RaycastHit hit, out float avoid_Multiplier, float movement_Amount, bool add, bool equals_Zero)
    {
        avoid_Multiplier = 0;

        float dist = Vector3.Distance(startPoint, hit.transform.position);

            Debug.DrawLine(startPoint, hit.point);
            avoiding = true;
            //avoid_Multiplier -= 1f;

            if (!equals_Zero)
            {
                if (!add)
                    avoid_Multiplier -= movement_Amount;
                else
                    avoid_Multiplier += movement_Amount;
            }
            else
            {

                if (hit.normal.x < 0)
                {
                    avoid_Multiplier = -movement_Amount;
                }
                else
                    avoid_Multiplier = movement_Amount;
            }
        
    }


    private void ApplySteer()
    {
        if (avoiding)
            return;

        Vector3 relativeVector = transform.InverseTransformPoint(nodes[current_Node].transform.position);

        relativeVector /= relativeVector.magnitude;


        float new_Steer = relativeVector.x / relativeVector.magnitude * max_SteerAngle;
        targetStreerAngle = new_Steer;

    }


    public IEnumerator BrakeOnTurn(float brake_time)
    {
        Handle_Break(true);
        yield return new WaitForSeconds(brake_time);
        Handle_Break(false);
    }

    public void Handle_Break(bool toogle)
    {
        Bike_Controller.brake = toogle;
    }
    private void CheckwaypointDidstanice()
    {

        if (Vector3.Distance(transform.position, nodes[current_Node].transform.position) < checkDistance)
        {
            if (JoeMath.isEqual(current_Node,nodes.Count - 1))
            {
                is_Breaking = true;
                Finshed = true;

            }
            else
            {
                current_Node += 1;


            }

        }
    }

    
    public float accrate = 0.1f;
    public float brakepower = -1;

    public float brakeatTurnTime = 0.1f;


    private void Drive()
    {

        currentSpeed = Current_Rigidbody.velocity.magnitude * 2.7f;

   
        if (currentSpeed < MaxSpeed() && !is_Breaking)
        {
            if (nodes.Count > current_Node)
            {
                if (!nodes[current_Node].SlowDownOnApproch())
                {
                    Bike_Controller.accelFwd = accrate;
                }
                else
                {
                    Bike_Controller.accelFwd = nodes[current_Node].slowDownspeed;
                }
                if (nodes[current_Node].brakeAtNode)
                {
                    StartCoroutine(brakeatnode(nodes[current_Node].BreakTime));
                }


            }
            else
            {
                Bike_Controller.accelFwd = 0f;
            }

        }
        else
        {
            Bike_Controller.accelFwd = 0;

        }


        currentNode = nodes[current_Node];

    }

    IEnumerator brakeatnode(float break_time)
    {
        is_Breaking = true;
        yield return new WaitForSeconds(break_time);
        is_Breaking = false;
        StopCoroutine("brakeatnode");

    }

    private void Breaking()
    {
        Bike_Controller.brake = is_Breaking;
    }

    private void lerptoStearAngel()
    {
        Bike_Controller.steer = Mathf.Lerp(Bike_Controller.steer, targetStreerAngle, Time.deltaTime * TurnSpeed);
    }

    Transform GetClosestWayPoint(List<Transform> waypoints, Transform fromThis)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = fromThis.position;
        foreach (Transform potentialTarget in waypoints)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }
    public Transform RespwanPoints;
    public float respawnWait = 5f;
    public float respawnCounter = 0.0f;

    private void Respawn()
    {

        if (currentNode != null)
            return;
        if (nodes.Count < 0)
            return;
        if (currentNode != nodes[nodes.Count])
            return;

       

        if (Current_Rigidbody.velocity.magnitude < 1)
        {
            respawnCounter += Time.deltaTime;

            if (respawnCounter >= respawnWait)
            {
                if (nodes[current_Node - 1])
                {
                    transform.position = nodes[current_Node - 1].transform.position;
                }
                transform.LookAt(nodes[current_Node].transform.position);
                respawnCounter = 0;
            }
        }

    }
   


}
