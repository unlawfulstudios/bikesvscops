﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CityGeneratorAsset))]
[ExecuteInEditMode]
[CanEditMultipleObjects]
public class CityGeneratorAssetEditor : Editor 
{
	CityGeneratorAsset CityGeneratorAsset;
	CityGenerator      CityGenerator;

	void Awake()
	{
		CityGeneratorAsset = (CityGeneratorAsset)target;

//		if(!CityGeneratorAsset.m_renderer)
//		{
//			Debug.LogError("CityGeneratorAsset Error -> " + CityGeneratorAsset.name + " must have a \"renderer\" assigned in the Inspector ... assigning one now if one can be found"); 
//
//			GameObject obj = GameUtils.FindItem( CityGeneratorAsset.gameObject, GameUtils.GameObjectItem.RENDERER );
//			if(obj && obj.renderer)
//				CityGeneratorAsset.m_renderer = obj.renderer;
//		}
	}
	
	public override void OnInspectorGUI () 
	{
		DrawDefaultInspector();
		
		if(!CityGeneratorAsset)
			return;
		
		if(!CityGenerator)
		{
			GameObject obj = GameObject.Find ("CityGenerator");
			if(obj && obj.GetComponent<CityGenerator>())
				CityGenerator = obj.GetComponent<CityGenerator>();
		}
	
	}
}
