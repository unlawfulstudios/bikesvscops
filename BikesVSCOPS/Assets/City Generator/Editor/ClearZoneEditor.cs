﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ClearZone))]
[ExecuteInEditMode]
[CanEditMultipleObjects]
public class ClearZoneEditor : Editor 
{
	ClearZone ClearZone;

	void Awake()
	{
		ClearZone = (ClearZone)target;
	}
	
	public override void OnInspectorGUI () 
	{
		DrawDefaultInspector();
		
		if(!ClearZone)
			return;
	}
}
