﻿// only enable this if you have purchased the "Road & Traffic System" package from the Unity Asset Store and installed it into your project.
 #define ROAD_AND_TRAFFIC_SYSTEM

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(CityGenerator))]
public class CityGeneratorEditor : Editor 
{
	public  string           DirActiveAssets                        =    "Assets/City Generator/Prefabs/Resources/Active/";
	
	private CityGenerator    CityGenerator                          { get; set; }
	
	private string[]         m_cityGeneratorPiecesLocation;
	
	private List<CityGeneratorAsset>  m_allAssets                   = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_lowAssets                   = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_mediumAssets                = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_highAssets                  = new List<CityGeneratorAsset>();
	
	void Awake()
	{
		CityGenerator = (CityGenerator)target;
		
		if(!CityGenerator)
			Debug.LogError("City Generator Error -> \"City Generator\" script must be in the scene. It is missing. Drop in the \"CityGenerator\" Prefab to fix this.");
		
		string sDataPath  = Application.dataPath + "/City Generator/Prefabs/Resources/Active/";
		
		// get the system file paths of all the files in the asset folder
		string[] aFilePaths = Directory.GetFiles(sDataPath);
		
		// enumerate through the list of files loading the assets they represent and getting their type
		
		int count = 0;
		foreach (string sFilePath in aFilePaths) 
		{
			if(sFilePath.Length <= 6)
				continue;
			
			string sAssetPath = sFilePath.Substring(sFilePath.Length - 6, 6);
			
			if(sAssetPath == "prefab")
				count++;
		}
		
		m_cityGeneratorPiecesLocation = new string[count];
		
		m_highAssets.Clear();
		
		count = 0;
		foreach (string sFilePath in aFilePaths) 
		{
			if(sFilePath.Length <= 6)
				continue;
			
			string sAssetPath = sFilePath.Substring(sFilePath.Length - 6, 6);
			
			if(sAssetPath == "prefab")
			{
				string file = sFilePath.Substring(sDataPath.Length);
				m_cityGeneratorPiecesLocation[count] = DirActiveAssets + file;
				//Debug.Log(m_cityGeneratorPiecesLocation[count]);
				
				CityGeneratorAsset asset = AssetDatabase.LoadAssetAtPath(m_cityGeneratorPiecesLocation[count], typeof(CityGeneratorAsset)) as CityGeneratorAsset;
				
				if(asset)
				{
					if(asset.m_assetFrequency == CityGenerator.AssetFrequency.HIGH)
						m_highAssets.Add(asset);
					else if(asset.m_assetFrequency == CityGenerator.AssetFrequency.MEDIUM)
						m_mediumAssets.Add(asset);
					else if(asset.m_assetFrequency == CityGenerator.AssetFrequency.LOW)
						m_lowAssets.Add(asset);

					m_allAssets.Add(asset);
				}
				
				count++;
			}
		}
	}
	
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();
		
		if(!CityGenerator || !CityGenerator.Instance)
			return;
		
		if(CityGenerator.Instance.m_groundObj)
			CityGenerator.Instance.m_groundObj.localScale = new Vector3(CityGenerator.Instance.m_cityWidth / 10, CityGenerator.Instance.m_groundObj.transform.localScale.y, CityGenerator.Instance.m_cityLength / 10);
		
		if(CityGenerator.Instance.m_terrain && CityGenerator.Instance.m_terrain.gameObject.activeSelf && CityGenerator.Instance.m_terrainHeightMapBackup.Length > 0)
		{
			GUILayout.BeginVertical("box");
			if(GUILayout.Button("Restore Previous Terrain\n* WARNING!! This only works for the previously generated terrain!\nOnce you press play, this restore is cleared."))
			{
				CityGenerator.Instance.m_terrain.terrainData.SetHeights(0, 0, CityGenerator.Instance.m_terrainHeightMapBackup);
				CityGenerator.Instance.m_terrainHeightMapBackup = new float[0,0];
			}
			GUILayout.EndVertical();
		}
		GUILayout.BeginVertical("box");
		if(GUILayout.Button("Generate City"))
		{
			if(m_lowAssets.Count + m_mediumAssets.Count + m_highAssets.Count <= 0)
			{
				Debug.LogError("No assets with the CityGeneratorAsset.cs script attached found in \"Assets\\City Generator\\Resources\\Active\". City Generation Cancelled.");
				return;
			}

			ClearZone[] allClearZones = FindObjectsOfType(typeof(ClearZone)) as ClearZone[];

			#if ROAD_AND_TRAFFIC_SYSTEM
			TrafficSystemPiece[] roadPieces = null;
			if(CityGenerator.Instance.m_useRoadsAsClearZones)
				roadPieces = FindObjectsOfType(typeof(TrafficSystemPiece)) as TrafficSystemPiece[];
			#endif
			
			float cityGround      = CityGenerator.Instance.GetGroundPos();
			Vector3 cityCenterPos = CityGenerator.Instance.GetCenterPos();
			Vector3 cityStartPos  = CityGenerator.Instance.GetStartPos();
			
			GameObject cityParent              = new GameObject("CityParent");
			cityParent.transform.position      = cityCenterPos;
			cityParent.transform.localRotation = Quaternion.identity;
			cityParent.transform.localScale    = Vector3.one;

			float cityWidth  = cityStartPos.x;
			float cityLength = cityStartPos.z;
			float maxLengthForRow = 0.0f;

			if(CityGenerator.Instance.m_terrain && CityGenerator.Instance.m_terrain.gameObject.activeSelf)
				CityGenerator.Instance.m_terrainHeightMapBackup = CityGenerator.Instance.m_terrain.terrainData.GetHeights(0, 0, CityGenerator.Instance.m_terrain.terrainData.heightmapWidth, CityGenerator.Instance.m_terrain.terrainData.heightmapHeight);

			int lengthLoop = 0;
			while(cityLength < CityGenerator.Instance.GetCityLengthLimit())
			{
				bool streetComingUp = false;
				if((lengthLoop + 1) % 2 == 0)
					streetComingUp = true;
				
				float blockWidth = cityWidth + CityGenerator.Instance.GetBlockWidth();
				
				int randomIndexLowPrevious    = -1;
				int randomIndexMediumPrevious = -1;
				int randomIndexHighPrevious   = -1;
				int randomIndexAllPrevious    = -1;
				bool bypassPreviousCheck      = false;
				if(m_lowAssets.Count + m_mediumAssets.Count + m_highAssets.Count <= 1)
					bypassPreviousCheck = true;

				List<CityGeneratorAsset> cutdownListForAllAssets = new List<CityGeneratorAsset>();

				for(int aIndex = 0; aIndex < m_allAssets.Count; aIndex++)
				{
					CityGeneratorAsset asset = m_allAssets[aIndex];
					cutdownListForAllAssets.Add(asset);
				}
			
				bool spawnedAsset = true;

				while(cityWidth < CityGenerator.Instance.GetCityWidthLimit())
				{
					int randomPercentage = Random.Range(0, 100);
					CityGeneratorAsset asset = null;

					if(!spawnedAsset && CityGenerator.Instance.m_enableAdvancedPacking)
					{
						if(cutdownListForAllAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, cutdownListForAllAssets.Count);

							if(cutdownListForAllAssets.Count > 1)
							{
								while(randomIndex == randomIndexAllPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, cutdownListForAllAssets.Count);
							}

							randomIndexAllPrevious = randomIndex;
							asset = cutdownListForAllAssets[randomIndex];
							cutdownListForAllAssets.Remove(asset);
						}
						else
						{
							cityWidth        += 1.0f;

							if(cityWidth > blockWidth && CityGenerator.Instance.m_blockWidthMin > 0.0f)
							{
								cityWidth += CityGenerator.Instance.m_streetWidth;
								blockWidth = cityWidth + CityGenerator.Instance.GetBlockWidth();
							}

							spawnedAsset = true;
							continue;
						}
					}
					else
					{
						if(randomPercentage <= CityGenerator.Instance.m_assetFrequencyLowPercentage && m_lowAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, m_lowAssets.Count);
							
							if(m_lowAssets.Count > 1)
							{
								while(randomIndex == randomIndexLowPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, m_lowAssets.Count);
							}
							
							randomIndexLowPrevious = randomIndex;
							asset = m_lowAssets[randomIndex];
						}
						else if(randomPercentage <= CityGenerator.Instance.m_assetFrequencyMediumPercentage && m_mediumAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, m_mediumAssets.Count);
							
							if(m_mediumAssets.Count > 1)
							{
								while(randomIndex == randomIndexMediumPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, m_mediumAssets.Count);
							}
							
							randomIndexMediumPrevious = randomIndex;
							asset = m_mediumAssets[randomIndex];
						}
						else if(m_highAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, m_highAssets.Count);
							
							if(m_highAssets.Count > 1)
							{
								while(randomIndex == randomIndexHighPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, m_highAssets.Count);
							}
							
							randomIndexHighPrevious = randomIndex;
							asset = m_highAssets[randomIndex];
						}
					}

					if(!asset)
					{
						if(m_highAssets.Count <= 0)
						{
							if(m_mediumAssets.Count > 0)
							{
								int randomIndex = Random.Range(0, m_mediumAssets.Count);
								
								if(m_mediumAssets.Count > 1)
								{
									while(randomIndex == randomIndexMediumPrevious && !bypassPreviousCheck)
										randomIndex = Random.Range(0, m_mediumAssets.Count);
								}
								
								randomIndexMediumPrevious = randomIndex;
								asset = m_mediumAssets[randomIndex];
							}
							else if(m_lowAssets.Count > 0)
							{
								int randomIndex = Random.Range(0, m_lowAssets.Count);
								
								if(m_lowAssets.Count > 1)
								{
									while(randomIndex == randomIndexLowPrevious && !bypassPreviousCheck)
										randomIndex = Random.Range(0, m_lowAssets.Count);
								}
								
								randomIndexLowPrevious = randomIndex;
								asset = m_lowAssets[randomIndex];
							}
						}
					}
					
					if(asset)
					{
						CityGeneratorAsset assetClone = asset;

						Bounds renderBounds = CityGenerator.Instance.BoundsDefault;
						
						if(assetClone.m_collider)
							renderBounds = assetClone.m_collider.bounds;
						else
							renderBounds = CityGenerator.Instance.FindAssetBounds(assetClone);
						
						float extentY = 0.0f;
						if(!assetClone.m_pivotIsOnGround)
						{
							if(assetClone.m_pivotPosY > 0.0f)
								extentY = assetClone.m_pivotPosY;
							else
								extentY = renderBounds.extents.y;
						}
						
						if(assetClone.m_flipXZ)
							assetClone.transform.position = new Vector3(cityWidth + renderBounds.extents.z, cityGround + extentY, cityLength + renderBounds.extents.x); 
						else
							assetClone.transform.position = new Vector3(cityWidth + renderBounds.extents.x, cityGround + extentY, cityLength + renderBounds.extents.z); 
						
						Vector3 eulerAngleToUse = assetClone.transform.eulerAngles;
						
						if(assetClone.m_randomRotationOnSpawn)
						{
							Vector3 randomRot   = new Vector3( Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f) );
							if(assetClone.m_randomRotationAxis.x != 0.0f)
								eulerAngleToUse.x = randomRot.x;
							if(assetClone.m_randomRotationAxis.y != 0.0f)
								eulerAngleToUse.y = randomRot.y;
							if(assetClone.m_randomRotationAxis.z != 0.0f)
								eulerAngleToUse.z = randomRot.z;
						}
						else
						{
							eulerAngleToUse = assetClone.m_spawnRotation;
						}
						
						if(streetComingUp && assetClone.m_flipOnOppositeStreets)
							eulerAngleToUse -= assetClone.m_oppositeSpawnRotSubtraction;
						
						assetClone.transform.eulerAngles = eulerAngleToUse;
						
						if(CityGenerator.Instance.m_terrain && CityGenerator.Instance.m_terrain.gameObject.activeSelf && CityGenerator.Instance.m_alignAssetRotWithTerrain)
						{
							RaycastHit hit;
							if(Physics.Raycast(assetClone.transform.position, -Vector3.up, out hit)) 
								assetClone.transform.rotation = Quaternion.FromToRotation(assetClone.transform.rotation.eulerAngles, hit.normal);
						}

						float tmpCityWidth  = cityWidth;
						float tmpBlockWidth = blockWidth;

						if(assetClone.m_flipXZ)
						{
							tmpCityWidth  += renderBounds.extents.z * 2;
							if((renderBounds.extents.x * 2) > maxLengthForRow)
								maxLengthForRow = renderBounds.extents.x * 2;
						}
						else
						{
							tmpCityWidth  += renderBounds.extents.x * 2;
							if((renderBounds.extents.z * 2) > maxLengthForRow)
								maxLengthForRow = renderBounds.extents.z * 2;
						}
						
						if(tmpCityWidth > tmpBlockWidth && CityGenerator.Instance.m_blockWidthMin > 0.0f)
						{
							tmpCityWidth += CityGenerator.Instance.m_streetWidth;
							tmpBlockWidth = tmpCityWidth + CityGenerator.Instance.GetBlockWidth();
						}

						if(streetComingUp)
						{
							if(assetClone)
							{
								Vector3 pos              = assetClone.transform.position;
								pos.z                    = (cityLength) + (maxLengthForRow - renderBounds.extents.z);
								assetClone.transform.position = pos;
							}
						}

						bool assetCulled = false;
						for(int cIndex = 0; cIndex < allClearZones.Length; cIndex++)
						{
							ClearZone clearZone = allClearZones[cIndex];
							
							if(clearZone.GetComponent<Collider>().bounds.Intersects(assetClone.GetBounds()))
							{
								assetCulled = true;
								break;
							}
						}
						
						#if ROAD_AND_TRAFFIC_SYSTEM
						if(CityGenerator.Instance.m_useRoadsAsClearZones && roadPieces != null)
						{
							for(int cIndex = 0; cIndex < roadPieces.Length; cIndex++)
							{
								TrafficSystemPiece roadPiece = roadPieces[cIndex];
								
								if(roadPiece && roadPiece.m_renderer)
								{
									Bounds newBounds = roadPiece.GetRenderBounds();
									
									Vector3 size     = newBounds.size;
									size.x          += CityGenerator.Instance.m_roadClearZoneOffset.x;
									size.y          += CityGenerator.Instance.m_roadClearZoneOffset.y;
									size.z          += CityGenerator.Instance.m_roadClearZoneOffset.z;
									newBounds.size   = size;
									
									if(newBounds.Intersects(assetClone.GetBounds()))
									{
										assetCulled = true;
										break;
									}
								}
							}
						}
						#endif
						
						if(!assetCulled)
						{
							if(CityGenerator.Instance.m_terrain && CityGenerator.Instance.m_terrain.gameObject.activeSelf)
							{
								float height = CityGenerator.Instance.m_terrain.SampleHeight(assetClone.transform.position);
								Vector3 pos = assetClone.transform.position;
								pos.y = height;
								assetClone.transform.position = pos;

								if(CityGenerator.Instance.m_deformTerrain && assetClone.m_deformTerrain)
									CityGenerator.Instance.DeformTerrain( assetClone.transform.position, (int)renderBounds.size.x, (int)renderBounds.size.z, CityGenerator.Instance.m_terrain, Mathf.CeilToInt(assetClone.transform.position.y - extentY) );
							}

							assetClone = PrefabUtility.InstantiatePrefab(assetClone) as CityGeneratorAsset;
							assetClone.SetParent( cityParent.transform );
						}

						if(CityGenerator.Instance.m_enableAdvancedPacking)
						{
							spawnedAsset = !assetCulled;

							if(!assetCulled)
							{
								cityWidth  = tmpCityWidth;
								blockWidth = tmpBlockWidth;
							}
						}
						else
						{
							cityWidth  = tmpCityWidth;
							blockWidth = tmpBlockWidth;
						}
					}
					else
					{
						Debug.LogError("Cancel City Generation as we should never get stuck finding an asset to spawn! This can be caused by not having any CityGeneratorAssets in the \"Assets\\City Generator\\Prefabs\\Active\" folder...");
						break;
					}
				}

				if(spawnedAsset)
				{
					cityWidth        = cityStartPos.x;
					cityLength      += maxLengthForRow;
					maxLengthForRow  = 0.0f;
					
					lengthLoop++;
					
					if(streetComingUp)
						cityLength += CityGenerator.Instance.m_streetWidth;
				}
			}
		}
//		GUILayout.Space(10.0f);
		GUILayout.EndVertical();
		
		if(GUI.changed)
			EditorUtility.SetDirty(CityGenerator);
	}
	
	/*
	Bounds FindAssetBounds( CityGeneratorAsset a_asset )
	{
		// get the size of the prefab and add it to the width and height of the city
		Bounds renderBounds = CityGenerator.Instance.BoundsDefault;
		if(a_asset.m_renderer)
			renderBounds = a_asset.m_renderer.bounds;
		else
		{
//			Debug.LogError("CityGeneratorAsset Error -> " + a_asset.name + " must have a \"renderer\" assigned in the Inspector ... trying to find one to use");
			
			GameObject obj = GameUtils.FindItem( a_asset.gameObject, GameUtils.GameObjectItem.RENDERER );
			if(obj && obj.renderer)
			{
				a_asset.m_renderer = obj.renderer;
				renderBounds = a_asset.m_renderer.bounds;
			}
		}

		return renderBounds;
	}
	*/

	//http://mothernaturedemonstration.googlecode.com/svn/trunk/NatureTest4/Assets/Resources/TerrainDeformer.cs
//	void DeformTerrain(Vector3 pos, int sizeX, int sizeZ, Terrain terr, int a_height)
//	{
//		int hmWidth  = terr.terrainData.heightmapWidth;
//		int hmHeight = terr.terrainData.heightmapHeight;
//
//		sizeX += 1;
//		sizeZ += 1;
//
//		//get the heights only once keep it and reuse, precalculate as much as possible
//		Vector3 terrainPos = GetRelativeTerrainPositionFromPos(pos,terr,hmWidth,hmHeight);//terr.terrainData.heightmapResolution/terr.terrainData.heightmapWidth
//
//		int heightMapCraterWidth  = (int)(sizeX * (hmWidth / terr.terrainData.size.x));
//		int heightMapCraterLength = (int)(sizeZ * (hmHeight / terr.terrainData.size.z));
//		int heightMapStartPosX    = (int)(terrainPos.x - (heightMapCraterWidth / 2));
//		int heightMapStartPosZ    = (int)(terrainPos.z - (heightMapCraterLength / 2));
//		
////		Debug.Log("********************************************");
////		Debug.Log("heightMapCraterWidth: " + heightMapCraterWidth);
////		Debug.Log("heightMapCraterLength: " + heightMapCraterLength);
////		Debug.Log("heightMapStartPosX: " + heightMapStartPosX);
////		Debug.Log("heightMapStartPosZ: " + heightMapStartPosZ);
////		Debug.Log("hmWidth: " + hmWidth);
////		Debug.Log("hmHeight: " + hmHeight);
//
////		heightMapStartPosX    -= 1;
////		heightMapStartPosZ    -= 1;
////		heightMapCraterWidth  += 1;
////		heightMapCraterLength += 1;
//
//		if(heightMapStartPosX < 0 || (heightMapStartPosX + heightMapCraterWidth) > hmWidth)
//			return;
//		if(heightMapStartPosZ < 0 || (heightMapStartPosZ + heightMapCraterLength) > hmHeight)
//			return;
//
//		float[,] heights = terr.terrainData.GetHeights(heightMapStartPosX, heightMapStartPosZ, heightMapCraterWidth, heightMapCraterLength);
//
//		float desiredHeight = a_height / terr.terrainData.size.y;
//
//		// we set each sample of the terrain in the size to the desired height
//		for (int i = 0; i < heightMapCraterLength; i++) //width
//		{
//			for (int j = 0; j < heightMapCraterWidth; j++) //height
//			{
//				heights[i, j] = desiredHeight;
//			}
//		}
//		
//		terr.terrainData.SetHeights(heightMapStartPosX, heightMapStartPosZ, heights);
//	}
//
//	Vector3 GetNormalizedPositionRelativeToTerrain(Vector3 pos, Terrain terrain)
//	{
//		//code based on: http://answers.unity3d.com/questions/3633/modifying-terrain-height-under-a-gameobject-at-runtime
//		// get the normalized position of this game object relative to the terrain
//		Vector3 tempCoord = (pos - terrain.gameObject.transform.position);
//		Vector3 coord;
//		coord.x = tempCoord.x / terrain.terrainData.size.x;
//		coord.y = tempCoord.y / terrain.terrainData.size.y;
//		coord.z = tempCoord.z / terrain.terrainData.size.z;
//		
//		return coord;
//	}
//
//	Vector3 GetRelativeTerrainPositionFromPos(Vector3 pos,Terrain terrain, int mapWidth, int mapHeight)
//	{
//		Vector3 coord = GetNormalizedPositionRelativeToTerrain(pos, terrain);
//		// get the position of the terrain heightmap where this game object is
//		return new Vector3((coord.x * mapWidth), 0, (coord.z * mapHeight));
//	}  
}
