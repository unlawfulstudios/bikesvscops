﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CityGeneratorAsset : MonoBehaviour 
{
	public  CityGenerator.AssetFrequency    m_assetFrequency              = CityGenerator.AssetFrequency.HIGH;
	public  Collider                        m_collider                    = null;
	public  Renderer                        m_renderer                    = null;
	public  bool                            m_pivotIsOnGround             = true;
	public  float                           m_pivotPosY                   = 0.0f;
	public  bool                            m_randomRotationOnSpawn       = false;
	public  Vector3                         m_randomRotationAxis          = new Vector3(0.0f, 1.0f, 0.0f);
	public  Vector3                         m_spawnRotation               = Vector3.zero;
	public  bool                            m_flipXZ                      = false;
	public  bool                            m_flipOnOppositeStreets       = false;
	public  Vector3                         m_oppositeSpawnRotSubtraction = new Vector3(0.0f, 180.0f, 0.0f);
	public  bool                            m_deformTerrain               = true;

	private Transform                       m_parent                      = null;


	void Awake()
	{
		if(!m_renderer)
		{
//			Debug.LogError("CityGeneratorAsset Error -> " + name + " must have a \"renderer\" assigned in the Inspector ... assigning one now if one can be found"); 
			
			GameObject obj = CityGeneratorGameUtils.FindItem( gameObject, CityGeneratorGameUtils.GameObjectItem.RENDERER );
			if(obj && obj.GetComponent<Renderer>())
				m_renderer = obj.GetComponent<Renderer>();
		}
	}

	void Start()
	{
		if(m_parent)
			transform.parent = m_parent;
	}

	public void SetParent( Transform a_parent )
	{
		transform.parent = a_parent;
		m_parent = a_parent;
	}

	public void Kill()
	{
		DestroyImmediate(gameObject);
	}

	public Vector3 GetBoundMinPosition()
	{
		if(m_collider)
			return m_collider.bounds.min;
		else if(m_renderer)
			return m_renderer.bounds.min;

		return transform.position;
	}

	public Vector3 GetBoundMaxPosition()
	{
		if(m_collider)
			return m_collider.bounds.max;
		else if(m_renderer)
			return m_renderer.bounds.max;
		
		return transform.position;
	}

	public Bounds GetBounds()
	{
		if(m_collider)
			return m_collider.bounds;
		else if(m_renderer)
			return m_renderer.bounds;
		
		return new Bounds( transform.position, new Vector3(1.0f, 1.0f, 1.0f) );
	}
}
