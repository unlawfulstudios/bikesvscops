﻿// only enable this if you have purchased the "Road & Traffic System" package from the Unity Asset Store and installed it into your project.
 #define ROAD_AND_TRAFFIC_SYSTEM

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
[ExecuteInEditMode]
public class CityGenerator : MonoBehaviour 
{
	public  static CityGenerator Instance                    { get; set; }

	public  float                m_cityWidth                 = 0.0f;
	public  float                m_cityLength                = 0.0f;
//	public  float                m_cityHeight                = -1.0f;
	public  float                m_cityGround                = 0.0f;
	public  float                m_blockWidthMin             = 0.0f;
	public  float                m_blockWidthMax             = 0.0f;
	public  float                m_streetWidth               = 0.0f;
	public  Bounds               BoundsDefault               { get; set; }

	[ Range(0, 100) ]
	public  float                m_assetFrequencyLowPercentage = 5;

	[ Range(0, 100) ]
	public  float                m_assetFrequencyMediumPercentage = 30;

	public  string               DirActiveAssets                    { get; set; }

	private List<CityGeneratorAsset>  m_allAssets                   = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_lowAssets                   = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_mediumAssets                = new List<CityGeneratorAsset>();
	private List<CityGeneratorAsset>  m_highAssets                  = new List<CityGeneratorAsset>();

	private List<GameObject>     AllCityParents = new List<GameObject>();

	public  bool                 m_enableGUI                 = true;
	public  bool                 m_enableAdvancedPacking     = false;
	public  Transform            m_groundObj                 = null;
	public  Terrain              m_terrain                   = null;
	public  bool                 m_alignAssetRotWithTerrain  = false;
	public  bool                 m_deformTerrain             = false;

	public  float[,]             m_terrainHeightMapBackup = new float[0,0];
	public  float[,]             m_terrainHeightMapBackupOriginal = new float[0,0];

	#if ROAD_AND_TRAFFIC_SYSTEM
	public  bool   m_useRoadsAsClearZones = true; // only works when the unity package "Road & Traffic System" is installed.
	public Vector3 m_roadClearZoneOffset  = new Vector3(0.0f, 10.0f, 0.0f);
	#endif

	public enum AssetFrequency
	{
		HIGH    = 0,
		MEDIUM  = 1,
		LOW     = 2
	}

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(this);
			return;
		}
		
		Instance = this;

		BoundsDefault   = new Bounds( Vector3.zero, new Vector3(5.0f, 5.0f, 5.0f) );
		//DirActiveAssets = Application.dataPath + "/City Generator/Prefabs/Resources";
		DirActiveAssets = "Active";
	}
	
	void Start () 
	{
		if(Instance != this)
			return;

		if(m_terrain)
			m_terrainHeightMapBackupOriginal = m_terrain.terrainData.GetHeights(0, 0, m_terrain.terrainData.heightmapWidth, m_terrain.terrainData.heightmapHeight);
	}

	void OnApplicationQuit()
	{
		if(m_terrain)
			m_terrain.terrainData.SetHeights(0, 0, m_terrainHeightMapBackupOriginal);
	}
	
	void OnGUI()
	{
		if(!m_enableGUI)
			return;

//		if(!Application.isPlaying)
//			return;

		Vector2 startPos   = new Vector2(10, 10);
		Vector2 labelSize  = new Vector2(100, 20);
		float   spacerY    = 10.0f;
		float   spacerX    = 100.0f;

		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "City Generator");
		startPos.y += labelSize.y + spacerY;

		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "City Width: ");
		startPos.x += spacerX;

		string text = GUI.TextField(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), m_cityWidth.ToString()); 
		int temp = 0; 
		if (int.TryParse(text, out temp)) 
			m_cityWidth = temp;
		else if(string.IsNullOrEmpty(text))
			m_cityWidth = 0;
		startPos.y += labelSize.y + spacerY;
		startPos.x -= spacerX;

		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "City Length: ");
		startPos.x += spacerX;

		text = GUI.TextField(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), m_cityLength.ToString()); 
		temp = 0; 
		if (int.TryParse(text, out temp)) 
			m_cityLength = temp;
		else if(string.IsNullOrEmpty(text))
			m_blockWidthMin = 0;
		startPos.y += labelSize.y + spacerY;
		startPos.x -= spacerX;
		
		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "Block Width Min: ");
		startPos.x += spacerX;
		
		text = GUI.TextField(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), m_blockWidthMin.ToString()); 
		temp = 0; 
		if (int.TryParse(text, out temp)) 
			m_blockWidthMin = temp;
		else if(string.IsNullOrEmpty(text))
			m_blockWidthMin = 0;
		startPos.y += labelSize.y + spacerY;
		startPos.x -= spacerX;
		
		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x + 5.0f, labelSize.y), "Block Width Max: ");
		startPos.x += spacerX;
		
		text = GUI.TextField(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), m_blockWidthMax.ToString()); 
		temp = 0; 
		if (int.TryParse(text, out temp)) 
			m_blockWidthMax = temp;
		else if(string.IsNullOrEmpty(text))
			m_blockWidthMax = 0;
		startPos.y += labelSize.y + spacerY;
		startPos.x -= spacerX;
		
		GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "Street Width: ");
		startPos.x += spacerX;
		
		text = GUI.TextField(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), m_streetWidth.ToString()); 
		temp = 0; 
		if (int.TryParse(text, out temp)) 
			m_streetWidth = temp;
		else if(string.IsNullOrEmpty(text))
			m_streetWidth = 0;
		startPos.y += labelSize.y + spacerY;

		float btnSpacer = 0.0f;
		if(m_terrain)
		{
			startPos.x -= spacerX;
			btnSpacer = 100.0f;
			GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "Use Terrain: ");
			startPos.x += spacerX;

			if(GUI.Button(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y + 20.0f), m_terrain.gameObject.activeSelf.ToString()))
		    {
				if(m_groundObj)
				{
					m_terrain.gameObject.SetActive(!m_terrain.gameObject.activeSelf);

					if(m_terrain.gameObject.activeSelf)
						m_groundObj.gameObject.SetActive(false);
					else
						m_groundObj.gameObject.SetActive(true);
				}
			}
			startPos.y += labelSize.y + spacerY + 20.0f;

			startPos.x -= spacerX;
			btnSpacer = 100.0f;
			GUI.Label(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y), "Deform Terrain: ");
			startPos.x += spacerX;

			if(GUI.Button(new Rect(startPos.x, startPos.y, labelSize.x, labelSize.y + 20.0f), m_deformTerrain.ToString()))
			{
				m_deformTerrain = !m_deformTerrain;
			}
			startPos.y += labelSize.y + spacerY + 20.0f;

			if(m_terrain && m_terrain.gameObject.activeSelf && m_terrainHeightMapBackup.Length > 0)
			{
				if(GUI.Button( new Rect(startPos.x - btnSpacer, startPos.y, labelSize.x + btnSpacer, labelSize.y * 2), "Restore Previous Terrain" ))
				{
					m_terrain.terrainData.SetHeights(0, 0, m_terrainHeightMapBackup);
					m_terrainHeightMapBackup = new float[0,0];
				}
				startPos.y += labelSize.y + spacerY + 20.0f;
			}
		}

		if(GUI.Button( new Rect(startPos.x - btnSpacer, startPos.y, labelSize.x + btnSpacer, labelSize.y * 3), "Generate City" ))
		{
			DeleteAllCityParents();

			if(m_terrain && m_terrain.gameObject.activeSelf && m_terrainHeightMapBackup.Length > 0)
			{
				m_terrain.terrainData.SetHeights(0, 0, m_terrainHeightMapBackup);
				m_terrainHeightMapBackup = new float[0,0];
			}

			GenerateCityAtRuntime();
		}

		#if UNITY_ANDROID
//		GUI.Label(new Rect(Screen.width / 2 - 150.0f, Screen.height - 30.0f, 300.0f, 20.0f), "Rotate with finger and Pinch to zoom in and out");

		if(Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
		#else
//		GUI.Label(new Rect(Screen.width / 2 - 150.0f, Screen.height - 30.0f, 300.0f, 20.0f), "Rotate by dragging and zoom with mouse wheel");
		#endif

		if(m_groundObj)
			m_groundObj.localScale = new Vector3(m_cityWidth / 10, m_groundObj.transform.localScale.y, m_cityLength / 10);
	}

	public Vector3 GetCenterPos()
	{
		Vector3 pos = transform.position;
		pos.y      += m_cityGround;
		return pos;
	}

	public float GetGroundPos()
	{
		return transform.position.y + m_cityGround;
	}

	public Vector3 GetStartPos()
	{
		Vector3 pos = transform.position;
		pos.x      -= m_cityWidth / 2;
		pos.y       = GetGroundPos();
		pos.z      -= m_cityLength / 2;
		return pos;
	}

	public float GetCityWidthLimit()
	{
		return transform.position.x + (m_cityWidth / 2);
	}

	public float GetCityLengthLimit()
	{
		return transform.position.z + (m_cityLength / 2);
	}

	public float GetBlockWidth()
	{
		return Random.Range(m_blockWidthMin, m_blockWidthMax);
	}
	
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = new Color(1, 0, 0, 0.5F);
		Vector3 pos  = GetCenterPos();

		float height = 0.25f;
//		float height = m_cityHeight;
//		if(height > 0.0f)
//			pos.y += height / 2;
//		else
//			height = 0.25f;

		Gizmos.DrawCube( pos, new Vector3( m_cityWidth, height, m_cityLength ) );
	}

	public void DeleteAllCityParents()
	{
		for(int cIndex = 0; cIndex < AllCityParents.Count; cIndex++)
		{
			Destroy (AllCityParents[cIndex]);
		}

		AllCityParents.Clear();
	}

	public void GenerateCityAtRuntime()
	{
		m_highAssets.Clear();
		m_mediumAssets.Clear();
		m_lowAssets.Clear();
		m_allAssets.Clear();

		CityGeneratorAsset[] assets = Resources.LoadAll<CityGeneratorAsset>(DirActiveAssets);

		if(assets.Length <= 0)
		{
			Debug.LogError("No assets with the CityGeneratorAsset.cs script attached found in \"Assets\\City Generator\\Resources\\Active\". City Generation Cancelled.");
			return;
		}

		for(int aIndex = 0; aIndex < assets.Length; aIndex++)
		{
			CityGeneratorAsset asset = assets[aIndex];

			if(asset)
			{
				if(asset.m_assetFrequency == CityGenerator.AssetFrequency.HIGH)
					m_highAssets.Add(asset);
				else if(asset.m_assetFrequency == CityGenerator.AssetFrequency.MEDIUM)
					m_mediumAssets.Add(asset);
				else if(asset.m_assetFrequency == CityGenerator.AssetFrequency.LOW)
					m_lowAssets.Add(asset);

				m_allAssets.Add(asset);
			}
		}

		ClearZone[] allClearZones = FindObjectsOfType(typeof(ClearZone)) as ClearZone[];

		#if ROAD_AND_TRAFFIC_SYSTEM
		TrafficSystemPiece[] roadPieces = FindObjectsOfType(typeof(TrafficSystemPiece)) as TrafficSystemPiece[];
		#endif

		float cityGround      = GetGroundPos();
		Vector3 cityCenterPos = GetCenterPos();
		Vector3 cityStartPos  = GetStartPos();
		
		GameObject cityParent              = new GameObject("CityParent");
		cityParent.transform.position      = cityCenterPos;
		cityParent.transform.localRotation = Quaternion.identity;
		cityParent.transform.localScale    = Vector3.one;
		AllCityParents.Add(cityParent);
		
		float cityWidth  = cityStartPos.x;
		float cityLength = cityStartPos.z;
		float maxLengthForRow = 0.0f;

		if(m_terrain && m_terrain.gameObject.activeSelf)
			m_terrainHeightMapBackup = m_terrain.terrainData.GetHeights(0, 0, m_terrain.terrainData.heightmapWidth, m_terrain.terrainData.heightmapHeight);

		int lengthLoop = 0;
		List<CityGeneratorAsset> generatedAssets = new List<CityGeneratorAsset>();
		while(cityLength < GetCityLengthLimit())
		{
			bool streetComingUp = false;
			if((lengthLoop + 1) % 2 == 0)
				streetComingUp = true;
			
			generatedAssets.Clear();
			
			float blockWidth = cityWidth + GetBlockWidth();
			
			int randomIndexLowPrevious    = -1;
			int randomIndexMediumPrevious = -1;
			int randomIndexHighPrevious   = -1;
			int randomIndexAllPrevious    = -1;
			bool bypassPreviousCheck      = false;
			if(m_lowAssets.Count + m_mediumAssets.Count + m_highAssets.Count <= 1)
				bypassPreviousCheck = true;

			List<CityGeneratorAsset> cutdownListForAllAssets = new List<CityGeneratorAsset>();
			
			for(int aIndex = 0; aIndex < m_allAssets.Count; aIndex++)
			{
				CityGeneratorAsset asset = m_allAssets[aIndex];
				cutdownListForAllAssets.Add(asset);
			}
			
			bool spawnedAsset = true;

			while(cityWidth < GetCityWidthLimit())
			{
				int randomPercentage = Random.Range(0, 100);
				CityGeneratorAsset asset = null;
				
				if(!spawnedAsset && m_enableAdvancedPacking)
				{
					if(cutdownListForAllAssets.Count > 0)
					{
						int randomIndex = Random.Range(0, cutdownListForAllAssets.Count);
						
						if(cutdownListForAllAssets.Count > 1)
						{
							while(randomIndex == randomIndexAllPrevious && !bypassPreviousCheck)
								randomIndex = Random.Range(0, cutdownListForAllAssets.Count);
						}
						
						randomIndexAllPrevious = randomIndex;
						asset = cutdownListForAllAssets[randomIndex];
						cutdownListForAllAssets.Remove(asset);
					}
					else
					{
						cityWidth        += 1.0f;
						
						if(cityWidth > blockWidth && m_blockWidthMin > 0.0f)
						{
							cityWidth += m_streetWidth;
							blockWidth = cityWidth + GetBlockWidth();
						}
						
						spawnedAsset = true;
						continue;
					}
				}
				else
				{
					if(randomPercentage <= m_assetFrequencyLowPercentage && m_lowAssets.Count > 0)
					{
						int randomIndex = Random.Range(0, m_lowAssets.Count);
						
						if(m_lowAssets.Count > 1)
						{
							while(randomIndex == randomIndexLowPrevious && !bypassPreviousCheck)
								randomIndex = Random.Range(0, m_lowAssets.Count);
						}
						
						randomIndexLowPrevious = randomIndex;
						asset = m_lowAssets[randomIndex];
					}
					else if(randomPercentage <= m_assetFrequencyMediumPercentage && m_mediumAssets.Count > 0)
					{
						int randomIndex = Random.Range(0, m_mediumAssets.Count);
						
						if(m_mediumAssets.Count > 1)
						{
							while(randomIndex == randomIndexMediumPrevious && !bypassPreviousCheck)
								randomIndex = Random.Range(0, m_mediumAssets.Count);
						}
						
						randomIndexMediumPrevious = randomIndex;
						asset = m_mediumAssets[randomIndex];
					}
					else if(m_highAssets.Count > 0)
					{
						int randomIndex = Random.Range(0, m_highAssets.Count);
						
						if(m_highAssets.Count > 1)
						{
							while(randomIndex == randomIndexHighPrevious && !bypassPreviousCheck)
								randomIndex = Random.Range(0, m_highAssets.Count);
						}
						
						randomIndexHighPrevious = randomIndex;
						asset = m_highAssets[randomIndex];
					}
				}
				
				if(!asset)
				{
					if(m_highAssets.Count <= 0)
					{
						if(m_mediumAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, m_mediumAssets.Count);
							
							if(m_mediumAssets.Count > 1)
							{
								while(randomIndex == randomIndexMediumPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, m_mediumAssets.Count);
							}
							
							randomIndexMediumPrevious = randomIndex;
							asset = m_mediumAssets[randomIndex];
						}
						else if(m_lowAssets.Count > 0)
						{
							int randomIndex = Random.Range(0, m_lowAssets.Count);
							
							if(m_lowAssets.Count > 1)
							{
								while(randomIndex == randomIndexLowPrevious && !bypassPreviousCheck)
									randomIndex = Random.Range(0, m_lowAssets.Count);
							}
							
							randomIndexLowPrevious = randomIndex;
							asset = m_lowAssets[randomIndex];
						}
					}
				}
				
				if(asset)
				{
					CityGeneratorAsset assetClone = asset;
					
					Bounds renderBounds = BoundsDefault;
					
					if(assetClone.m_collider)
						renderBounds = assetClone.m_collider.bounds;
					else
						renderBounds = FindAssetBounds(assetClone);
					
					float extentY = 0.0f;
					if(!assetClone.m_pivotIsOnGround)
					{
						if(assetClone.m_pivotPosY > 0.0f)
							extentY = assetClone.m_pivotPosY;
						else
							extentY = renderBounds.extents.y;
					}
					
					if(assetClone.m_flipXZ)
						assetClone.transform.position = new Vector3(cityWidth + renderBounds.extents.z, cityGround + extentY, cityLength + renderBounds.extents.x); 
					else
						assetClone.transform.position = new Vector3(cityWidth + renderBounds.extents.x, cityGround + extentY, cityLength + renderBounds.extents.z); 
					
					Vector3 eulerAngleToUse = assetClone.transform.eulerAngles;
					
					if(assetClone.m_randomRotationOnSpawn)
					{
						Vector3 randomRot   = new Vector3( Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f) );
						if(assetClone.m_randomRotationAxis.x != 0.0f)
							eulerAngleToUse.x = randomRot.x;
						if(assetClone.m_randomRotationAxis.y != 0.0f)
							eulerAngleToUse.y = randomRot.y;
						if(assetClone.m_randomRotationAxis.z != 0.0f)
							eulerAngleToUse.z = randomRot.z;
					}
					else
					{
						eulerAngleToUse = assetClone.m_spawnRotation;
					}
					
					if(streetComingUp && assetClone.m_flipOnOppositeStreets)
						eulerAngleToUse -= assetClone.m_oppositeSpawnRotSubtraction;
					
					assetClone.transform.eulerAngles = eulerAngleToUse;
					
					if(m_terrain && m_terrain.gameObject.activeSelf && m_alignAssetRotWithTerrain)
					{
						RaycastHit hit;
						if(Physics.Raycast(assetClone.transform.position, -Vector3.up, out hit)) 
							assetClone.transform.rotation = Quaternion.FromToRotation(assetClone.transform.rotation.eulerAngles, hit.normal);
					}
					
					float tmpCityWidth  = cityWidth;
					float tmpBlockWidth = blockWidth;
					
					if(assetClone.m_flipXZ)
					{
						tmpCityWidth  += renderBounds.extents.z * 2;
						if((renderBounds.extents.x * 2) > maxLengthForRow)
							maxLengthForRow = renderBounds.extents.x * 2;
					}
					else
					{
						tmpCityWidth  += renderBounds.extents.x * 2;
						if((renderBounds.extents.z * 2) > maxLengthForRow)
							maxLengthForRow = renderBounds.extents.z * 2;
					}
					
					if(tmpCityWidth > tmpBlockWidth && m_blockWidthMin > 0.0f)
					{
						tmpCityWidth += m_streetWidth;
						tmpBlockWidth = tmpCityWidth + GetBlockWidth();
					}
					
					if(streetComingUp)
					{
						if(assetClone)
						{
							Vector3 pos              = assetClone.transform.position;
							pos.z                    = (cityLength) + (maxLengthForRow - renderBounds.extents.z);
							assetClone.transform.position = pos;
						}
					}
					
					bool assetCulled = false;
					for(int cIndex = 0; cIndex < allClearZones.Length; cIndex++)
					{
						ClearZone clearZone = allClearZones[cIndex];
						
						if(clearZone.GetComponent<Collider>() && clearZone.GetComponent<Collider>().bounds.Intersects(assetClone.GetBounds()))
						{
							assetCulled = true;
							break;
						}
					}
					
					#if ROAD_AND_TRAFFIC_SYSTEM
					if(m_useRoadsAsClearZones && roadPieces != null)
					{
						for(int cIndex = 0; cIndex < roadPieces.Length; cIndex++)
						{
							TrafficSystemPiece roadPiece = roadPieces[cIndex];
							
							if(roadPiece && roadPiece.m_renderer)
							{
								Bounds newBounds = roadPiece.GetRenderBounds();
								
								Vector3 size     = newBounds.size;
								size.x          += m_roadClearZoneOffset.x;
								size.y          += m_roadClearZoneOffset.y;
								size.z          += m_roadClearZoneOffset.z;
								newBounds.size   = size;
								
								if(newBounds.Intersects(assetClone.GetBounds()))
								{
									assetCulled = true;
									break;
								}
							}
						}
					}
					#endif
					
					if(!assetCulled)
					{
						if(m_terrain && m_terrain.gameObject.activeSelf)
						{
							float height = m_terrain.SampleHeight(assetClone.transform.position);
							Vector3 pos = assetClone.transform.position;
							pos.y = height;
							assetClone.transform.position = pos;

							if(m_deformTerrain && assetClone.m_deformTerrain)
								DeformTerrain( assetClone.transform.position, (int)renderBounds.size.x, (int)renderBounds.size.z, m_terrain, Mathf.CeilToInt(assetClone.transform.position.y - extentY) );
						}

						assetClone = Instantiate(assetClone) as CityGeneratorAsset;
						assetClone.SetParent( cityParent.transform );
					}
					
					if(m_enableAdvancedPacking)
					{
						spawnedAsset = !assetCulled;
						
						if(!assetCulled)
						{
							cityWidth  = tmpCityWidth;
							blockWidth = tmpBlockWidth;
						}
					}
					else
					{
						cityWidth  = tmpCityWidth;
						blockWidth = tmpBlockWidth;
					}
				}
				else
				{
					Debug.LogError("Cancel City Generation as we should never get stuck finding an asset to spawn! This can be caused by not having any CityGeneratorAssets in the \"Assets\\City Generator\\Prefabs\\Active\" folder...");
					break;
				}
			}
			
			if(spawnedAsset)
			{
				cityWidth        = cityStartPos.x;
				cityLength      += maxLengthForRow;
				maxLengthForRow  = 0.0f;
				
				lengthLoop++;
				
				if(streetComingUp)
					cityLength += m_streetWidth;
			}
		}
	}

	public Bounds FindAssetBounds( CityGeneratorAsset a_asset )
	{
		// get the size of the prefab and add it to the width and height of the city
		Bounds renderBounds = BoundsDefault;
		if(a_asset.m_renderer)
			renderBounds = a_asset.m_renderer.bounds;
		else
		{
			//			Debug.LogError("CityGeneratorAsset Error -> " + a_asset.name + " must have a \"renderer\" assigned in the Inspector ... trying to find one to use");
			
			GameObject obj = CityGeneratorGameUtils.FindItem( a_asset.gameObject, CityGeneratorGameUtils.GameObjectItem.RENDERER );
			if(obj && obj.GetComponent<Renderer>() )
			{
				a_asset.m_renderer = obj.GetComponent<Renderer>();
				renderBounds = a_asset.m_renderer.bounds;
			}
		}
		
		return renderBounds;
	}

	//http://mothernaturedemonstration.googlecode.com/svn/trunk/NatureTest4/Assets/Resources/TerrainDeformer.cs
	public void DeformTerrain(Vector3 pos, int sizeX, int sizeZ, Terrain terr, int a_height)
	{
		int hmWidth  = terr.terrainData.heightmapWidth;
		int hmHeight = terr.terrainData.heightmapHeight;
		
		sizeX += 1;
		sizeZ += 1;
		
		//get the heights only once keep it and reuse, precalculate as much as possible
		Vector3 terrainPos = GetRelativeTerrainPositionFromPos(pos,terr,hmWidth,hmHeight);//terr.terrainData.heightmapResolution/terr.terrainData.heightmapWidth
		
		int heightMapCraterWidth  = (int)(sizeX * (hmWidth / terr.terrainData.size.x));
		int heightMapCraterLength = (int)(sizeZ * (hmHeight / terr.terrainData.size.z));
		int heightMapStartPosX    = (int)(terrainPos.x - (heightMapCraterWidth / 2));
		int heightMapStartPosZ    = (int)(terrainPos.z - (heightMapCraterLength / 2));
		
		//		Debug.Log("********************************************");
		//		Debug.Log("heightMapCraterWidth: " + heightMapCraterWidth);
		//		Debug.Log("heightMapCraterLength: " + heightMapCraterLength);
		//		Debug.Log("heightMapStartPosX: " + heightMapStartPosX);
		//		Debug.Log("heightMapStartPosZ: " + heightMapStartPosZ);
		//		Debug.Log("hmWidth: " + hmWidth);
		//		Debug.Log("hmHeight: " + hmHeight);
		
		//		heightMapStartPosX    -= 1;
		//		heightMapStartPosZ    -= 1;
		//		heightMapCraterWidth  += 1;
		//		heightMapCraterLength += 1;
		
		if(heightMapStartPosX < 0 || (heightMapStartPosX + heightMapCraterWidth) > hmWidth)
			return;
		if(heightMapStartPosZ < 0 || (heightMapStartPosZ + heightMapCraterLength) > hmHeight)
			return;
		
		float[,] heights = terr.terrainData.GetHeights(heightMapStartPosX, heightMapStartPosZ, heightMapCraterWidth, heightMapCraterLength);
		
		float desiredHeight = a_height / terr.terrainData.size.y;
		
		// we set each sample of the terrain in the size to the desired height
		for (int i = 0; i < heightMapCraterLength; i++) //width
		{
			for (int j = 0; j < heightMapCraterWidth; j++) //height
			{
				heights[i, j] = desiredHeight;
			}
		}
		
		terr.terrainData.SetHeights(heightMapStartPosX, heightMapStartPosZ, heights);
	}
	
	Vector3 GetNormalizedPositionRelativeToTerrain(Vector3 pos, Terrain terrain)
	{
		//code based on: http://answers.unity3d.com/questions/3633/modifying-terrain-height-under-a-gameobject-at-runtime
		// get the normalized position of this game object relative to the terrain
		Vector3 tempCoord = (pos - terrain.gameObject.transform.position);
		Vector3 coord;
		coord.x = tempCoord.x / terrain.terrainData.size.x;
		coord.y = tempCoord.y / terrain.terrainData.size.y;
		coord.z = tempCoord.z / terrain.terrainData.size.z;
		
		return coord;
	}
	
	Vector3 GetRelativeTerrainPositionFromPos(Vector3 pos,Terrain terrain, int mapWidth, int mapHeight)
	{
		Vector3 coord = GetNormalizedPositionRelativeToTerrain(pos, terrain);
		// get the position of the terrain heightmap where this game object is
		return new Vector3((coord.x * mapWidth), 0, (coord.z * mapHeight));
	}  

}
