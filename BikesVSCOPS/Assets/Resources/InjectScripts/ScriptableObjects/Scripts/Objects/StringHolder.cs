﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StringHolder", menuName = "JS/ScriptableObjects/StringHolder")]
public class StringHolder : ScriptableObject
{
    public List<string> GlobalInjectTypeNames;
    public List<string> GlobalMangerInjectNames;
    public List<string> GlobalMangerSeetingsNames;
    public List<string> GlobalScriptableObjectsNames;

}
