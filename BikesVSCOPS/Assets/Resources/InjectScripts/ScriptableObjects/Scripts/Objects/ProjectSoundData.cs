﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ProjectSoundData", menuName = "JS/ScriptableObjects/ProjectSoundData")]
public class ProjectSoundData : ScriptableObject
{
    public List<AudioClip>  All_Music;
    public List<AudioClip> All_ButtonSounds;
   

}
