using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "MainMenuInjectInstallers", menuName = "Installers/MainMenuInjectInstallers")]
public class MainMenuInjectInstallers : ScriptableObjectInstaller<MainMenuInjectInstallers>
{
    public override void InstallBindings()
    {
       // Container.BindInterfacesAndSelfTo<MainMenu_Managers>().AsSingle().NonLazy();

    }
}