using UnityEngine;
using Zenject;
using System;

[CreateAssetMenu(fileName = "ProjectScriptableObjectsInstaller", menuName = "Installers/ProjectScriptableObjectsInstaller")]
public class ProjectScriptableObjectsInstaller : ScriptableObjectInstaller<ProjectScriptableObjectsInstaller>
{

    private const string ScriptableObjectPath = "InjectScripts/ScriptableObjects/Objects";

    public StringHolder data;

    public override void InstallBindings()
    {


        Container.Bind<StringHolder>().FromScriptableObjectResource(ScriptableObjectPath).AsSingle().NonLazy();
     
        //Container.Bind<BikeInventoryItemList>().FromScriptableObjectResource(ScriptableObjectPath).AsSingle().NonLazy();
        //Container.Bind<City_Data_Manager>().FromScriptableObjectResource(ScriptableObjectPath).AsSingle().NonLazy();
        Bind_Gloabla_ScriptableObject();


        Debug.Log("HELLELELEO");
    }


    public void Bind_Gloabla_ScriptableObject()
    {
        for (int i = 0; i < data.GlobalScriptableObjectsNames.Count; i++)
        {
            Container.Bind(Type.GetType(data.GlobalScriptableObjectsNames[i])).FromScriptableObjectResource(ScriptableObjectPath).AsSingle().NonLazy();
        }

    }
}