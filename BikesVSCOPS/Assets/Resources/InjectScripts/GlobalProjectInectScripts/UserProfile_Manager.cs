﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Auth;
using System;
using Zenject;
using JS.BVSC.BVSC_Behavior.FireBase;

public class FirBaseManagers : GlobaleManagers
{
    public delegate void ON_FireBaseConnected();

    public ON_FireBaseConnected oN_FireBaseConnected;

    public virtual IEnumerator ConnectToDataBase()
    {
        yield return new WaitUntil(() => FirebaseDatabase.DefaultInstance != null);
        OnConnectToDataBase();
    }



    private void OnConnectToDataBase()
    {
        ConnectedToFireBase();
        oN_FireBaseConnected += FireBaseConnectedUpdate;
    }

    public virtual void ConnectedToFireBase() { }
    public virtual void FireBaseConnectedUpdate() { }
    public virtual void DisconnectFromFirBase() { }

    public void DestroyManager()
    {
        oN_FireBaseConnected -= FireBaseConnectedUpdate;
    }


}

public class UserProfile_Manager : FirBaseManagers, IInitializable
{

    public bool Load = false;
    private bool hasPlayed { get { return current_UserProfile.bikesVSCopsProfileInfo.HasPlayedBefore.Value; } }
    private bool serverSynced { get { return isSynced; } }


    public void Initialize()
    {
   
        AppManager.AppProcesser.StartCoroutine(ConnectToDataBase());

        loading_Manager.Toogle_SeverLodingScreen(true);
        
    }
    public override void ConnectedToFireBase()
    {

        if (hasPlayed)
        {

            authManager.SigninAsync();
            Load = true;
        }
        else
        {
            RestUserProfile();
            authManager.OpenAuthManager();
        }


        Debug.Log(hasPlayed + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }


    public override void FireBaseConnectedUpdate()
    {
     

        if (Load && !serverSynced)
        {
            if (current_UserProfile != null)
                LoadUserProfile(FirebaseAuth.DefaultInstance.CurrentUser.UserId);

        }
        else
        {
           loading_Manager.Toogle_SeverLodingScreen(false);
        }

    }

    public void LoadUserProfile(string id)
    { 
       current_UserProfile.bikesVSCopsProfileInfo.current_City = new City(0);
        current_UserProfile.bikesVSCopsProfileInfo.current_Run = new RUN(0, 0);

        FirebaseDatabase.DefaultInstance
   .GetReference("users").Child(id)
   .GetValueAsync().ContinueWith(task =>
   {
       if (task.IsFaulted)
       {
           Debug.Log(task.IsFaulted + "!!!!!!!!!!!!!!!!!!");
       }
       else if (task.IsCompleted)
       {
           DataSnapshot snapshot = task.Result;
           UserProfile.Instnace.Set(snapshot);

           current_UserProfile.bikesVSCopsProfileInfo.SyncData();

           for (int i = 0; i < current_UserProfile.BikeList.Length; i++)
           {
               current_UserProfile.BikeList[i].SyncData();
           }
           for (int i = 0; i < current_UserProfile.CAREER_Citys.Length; i++)
           {
              current_UserProfile.CAREER_Citys[i].SyncData();
           }

           isSynced = Synced();

           if (isSynced)
           {
               Debug.Log("Is Synced");
           }
           else Debug.Log("Not Synced");

       }
   });
    }
    public void writeNewUser(string userId, string name, string email, string hash)
    {
        current_UserProfile.bikesVSCopsProfileInfo.UserName.value = name;
        current_UserProfile.bikesVSCopsProfileInfo.Email.value = email;
        current_UserProfile.bikesVSCopsProfileInfo.hash.Value = hash;
        current_UserProfile.UserID = userId;
        FirebaseDatabase.DefaultInstance.GetReference("users").Child(userId).SetRawJsonValueAsync(current_UserProfile.Json_Value());
        current_UserProfile.bikesVSCopsProfileInfo.HasPlayedBefore.Value = true;


        Debug.Log(current_UserProfile.bikesVSCopsProfileInfo.HasPlayedBefore.Value);
    }

    public void UpdateProfile()
    {
        FirebaseDatabase.DefaultInstance.GetReference("users").Child(current_UserProfile.UserID).SetRawJsonValueAsync(current_UserProfile.Json_Value());
        Debug.Log(current_UserProfile.Json_Value());
    }

    public bool isSynced = false;

    public bool Synced()
    {
        return ProfileObjectsArSynced(current_UserProfile.BikeList) && ProfileObjectsArSynced(current_UserProfile.CAREER_Citys)
            && current_UserProfile.bikesVSCopsProfileInfo.DataSynced ? true : false;
    }



    public void RestUserProfile()
    {
        current_UserProfile.bikesVSCopsProfileInfo.Reset();

        for (int i = 0; i < current_UserProfile.BikeList.Length; i++)
        {
           current_UserProfile.BikeList[i].Reset();
        }
        for (int i = 0; i < current_UserProfile.CAREER_Citys.Length; i++)
        {
            current_UserProfile.CAREER_Citys[i].Reset();
        }

    }


    private bool ProfileObjectsArSynced(UserProfileObject[] obejcts)
    {

        for (int i = 0; i < obejcts.Length; ++i)
        {
            if (obejcts[i].DataSynced == false)
            {
                return false;
            }
        }

        return true;

    }

  
}





