﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class Scene_Manager : GlobaleManagers,ILateDisposable
{
  
    public float AppTimeScale { get { return Time.timeScale; } set { Time.timeScale = value; } }

    public static bool in_Game()
    {
        if (SceneManager.GetActiveScene().name == "Main")
            return false;
        else
            return true;
    }

    public static void Primitive_Load(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void Load_Scene(string levelName,bool goBack,int id,bool set_current)
    {
        AppManager.AppProcesser.StartCoroutine(Asyncload(levelName, goBack,id, set_current));    
    }
   

    private IEnumerator Asyncload(string level_Name,bool goBack, int id,bool set_current)
    {
        if (!goBack)
        {
            loading_Manager.loading_ManagerSettings.Current_LoadLevelUI 
                = loading_Manager.loading_ManagerSettings.loadLevel_UI1;
        }
        else
        {
              loading_Manager.loading_ManagerSettings.Current_LoadLevelUI
               = loading_Manager.loading_ManagerSettings.loadLevel_UI2;
        }
        Loading_Manager.isLoading = true;


        loading_Manager.SET(set_current);



        loading_Manager.loading_ManagerSettings.Current_LoadLevelUI.SetActive(true);
    
        //Bike_HolderManager.Instance.StartRotat();

        yield return new WaitForSeconds(5f);

        m_async = SceneManager.LoadSceneAsync(level_Name);
        loading_Manager.SET(set_current);

        while (!m_async.isDone)
        {
            loading_Manager.SET(set_current);
            Debug.Log("Loading");
            yield return null;
        }
        loading_Manager.loading_ManagerSettings.Current_LoadLevelUI.gameObject.SetActive(false);  
        yield return new WaitUntil(() => !loading_Manager.loading_ManagerSettings.Current_LoadLevelUI.gameObject.activeInHierarchy);
        Loading_Manager.isLoading = false;
        Goback(id,goBack);
        loading_Manager.loading_ManagerSettings.StopCoroutine("Asyncload");

    }

    public void Update()
    {
        if (!Loading_Manager.isLoading)
            return;

        UserProfile_Manager.UpdateProfile();
    }
    public void Goback(int id,bool goBack)
    {
  
        if (goBack)
        {
            switch (id)
            {
                case 0:
                    UI_Manager.Instance.GoBackHome();
                    return;
                case 1:
                    UI_Manager.Instance.GoToGarage();
                    return;
                case 2:
                    UI_Manager.Instance.GoTo_Settings();
                    return;

                default:
                    UI_Manager.Instance.GoBackHome();
                    return;
            }
        }
    }

    private static AsyncOperation m_async;


    public void LateDispose()
    {
        Loading_Manager.isLoading = false;
    }
}
