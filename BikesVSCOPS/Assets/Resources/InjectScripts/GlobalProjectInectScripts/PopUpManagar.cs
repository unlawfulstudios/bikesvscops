﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class PopUpManagar : GlobaleManagers , IInitializable
{
    public int openID;


    public PopUpManagerSetteings popUpManagerSetteings;


    public PopUpManagar(PopUpManagerSetteings _popUpManagerSetteings )
    {
        popUpManagerSetteings = _popUpManagerSetteings;
    }

    public void Initialize()
    {
        CLOSE();
    }

    public void OpenInfo(int id)
    {
        openID = id;
      
        popUpManagerSetteings.info[id].Toggle(true);
        popUpManagerSetteings.POPUPWINDOW.SetActive(true);
        for (int i = 0; i < popUpManagerSetteings.info.Count; i++)
        {
            if (popUpManagerSetteings.info[i] != popUpManagerSetteings.info[id])
            {
                popUpManagerSetteings.info[i].Toggle(false);
            }
        }

    }
    public void CLOSE()
    {
        popUpManagerSetteings.POPUPWINDOW.SetActive(false);             
    }

    public void ShowPopUpAtEnd(UnlockType unlockType, int unlockID)
    {
        if (unlockType == UnlockType.Bike)
        {
            if ( current_UserProfile.BikeList[unlockID].UNLOCKED.value != true)
                ShowUnlockedBike(unlockID);
        }
        else if (unlockType == UnlockType.City)
        {
            if (current_UserProfile.CAREER_Citys[unlockID].Complete.value != true)
                ShowCompleteCity(unlockID);
        }
    }
    private void ShowUnlockedBike(int unlockID)
    {
        OpenInfo(2);
        current_UserProfile.BikeList[unlockID].UNLOCKED.value = true;
    }
    private void ShowCompleteCity(int UnlockID)
    {
        OpenInfo(3);
        current_UserProfile.CAREER_Citys[UnlockID].Complete.value = true;
    }
    
}
