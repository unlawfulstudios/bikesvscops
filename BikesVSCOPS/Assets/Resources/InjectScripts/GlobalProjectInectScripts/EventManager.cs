﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;


public enum Run_Event_States
{
    inactive,
    CamState,
    Ready,
    Start,
    Busted,
    End
}


public class EventManager : IInitializable
{
    private Dictionary<string, UnityEvent> event_Dictionary;
    public delegate void Event_Change();
    public  Event_Change event_Change;


    public void Initialize()
    {
        if (event_Dictionary == null)
        {
            event_Dictionary = new Dictionary<string, UnityEvent>();
        }
   
    }

    public void Start_Listening(string eventName,UnityAction unityAction)
    {
        UnityEvent this_Event = null;

        if (event_Dictionary.TryGetValue(eventName, out this_Event))
        {
            this_Event.AddListener(unityAction);
        }
        else
        {
            this_Event = new UnityEvent();
            this_Event.AddListener(unityAction);
           event_Dictionary.Add(eventName, this_Event);

        }
    }
    public  void Stop_Listening(string eventName, UnityAction unityAction)
    {
        UnityEvent this_Event = null;

        if (event_Dictionary.TryGetValue(eventName, out this_Event))
        {
            this_Event.RemoveListener(unityAction);
        }

    }
    public  void Trigger_Event(string sceneEvent)
    {
        UnityEvent this_Event = null;

        if (event_Dictionary.TryGetValue(sceneEvent, out this_Event))
        {
            this_Event.Invoke();
        }
    }


}
