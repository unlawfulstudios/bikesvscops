using UnityEngine;
using UnityEditor;
using Zenject;

public class ProjectManagerWindow : ZenjectEditorWindow
{
    [MenuItem("Window/ProjectManagerWindow")]
    public static ProjectManagerWindow GetOrCreateWindow()
    {
        return  CustomWindoweEditorTools.GetOrCreateWindow<ProjectManagerWindow>() as ProjectManagerWindow; ;
    }

    public override void InstallBindings()
    {
      
    }
}