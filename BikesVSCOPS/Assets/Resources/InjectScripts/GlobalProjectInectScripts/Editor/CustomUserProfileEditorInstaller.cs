using UnityEngine;
using Zenject;

public class CustomUserProfileEditorInstaller : Installer<CustomUserProfileEditorInstaller>
{
    public override void InstallBindings()
    {
        CustomEditorInstaller.Install(Container);
        Container.BindInterfacesAndSelfTo<EditorUserprofileManager>().AsSingle().NonLazy();
    }
}