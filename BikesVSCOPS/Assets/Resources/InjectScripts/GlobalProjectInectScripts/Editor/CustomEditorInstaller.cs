using UnityEngine;
using Zenject;

public class CustomEditorInstaller : Installer<CustomEditorInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<UserProfile>().AsSingle().NonLazy();
        Container.Bind<BikeInventoryItemList>().FromScriptableObjectResource("InjectScripts/ScriptableObjects/Objects/").AsSingle().NonLazy();
        Container.Bind<City_Data_Manager>().FromScriptableObjectResource("InjectScripts/ScriptableObjects/Objects/").AsSingle().NonLazy();
    }
}

public class CityDataManagerEditorInstaller : Installer<CityDataManagerEditorInstaller>
{
    public override void InstallBindings()
    {
        CustomEditorInstaller.Install(Container);
        Container.BindInterfacesAndSelfTo<CityManager>().AsSingle().NonLazy();
    }
}
