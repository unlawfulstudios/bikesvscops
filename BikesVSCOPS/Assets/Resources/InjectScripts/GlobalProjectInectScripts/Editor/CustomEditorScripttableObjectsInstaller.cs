using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "CustomEditorScripttableObjectsInstaller", menuName = "Installers/CustomEditorScripttableObjectsInstaller")]
public class CustomEditorScripttableObjectsInstaller : ScriptableObjectInstaller<CustomEditorScripttableObjectsInstaller>
{
    public override void InstallBindings()
    {
        CustomEditorInstaller.Install(Container);
    }
}