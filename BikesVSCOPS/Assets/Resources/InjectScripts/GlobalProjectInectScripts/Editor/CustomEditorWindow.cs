﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEditor;
using System;

public class CustomEditorWindow : ZenjectEditorWindow
{
    public UserProfile UserProfile
    {
        get
        {
            if (m_UserProfile == null)
            {
                m_UserProfile = UserProfile.Instnace;
                return m_UserProfile;
            }
            else
            {
                return m_UserProfile;
            }
        }
        
    }
    private UserProfile m_UserProfile;

    public BikeInventoryItemList inventoryItemList;
    public City_Data_Manager city_Data_Manager;

    [Inject]
    public void Construct(
   BikeInventoryItemList _inventoryItemList,
   City_Data_Manager _city_Data_Manager
   )
    {
        inventoryItemList = _inventoryItemList;
        city_Data_Manager = _city_Data_Manager;
    }

    public override void InstallBindings()
    {
        throw new NotImplementedException();
    }


}