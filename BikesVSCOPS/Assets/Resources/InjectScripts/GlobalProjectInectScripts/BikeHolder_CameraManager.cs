﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BikeHolder_CameraManager : Scene_CameraManagers, IInitializable ,IFixedTickable{

    public BikeHolder bikeHolder;
    public Bike currentSlectedBike;
    private bool Move = false;
    private float currentid;
    private float times = 0.1f;

    public  bool ON = false;

    public  BikeHolder_CameraManager(BikeHolder _bikeHolder)
    {
        bikeHolder = _bikeHolder;
    }

    public void Initialize()
    {
        Set_SceneCam(bikeHolder.BikeHolderCam);
        Move_SceneCam_Isnatnt(bikeHolder.BikeTransfroms[current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].transform.position);
        bikeHolder.StartRotate();
        currentid = 160;

    }

    public void Move_Right()
    {
        if (current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value < bikeHolder.BikeTransfroms.Length - 1 )

        {
            current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value++;
            Set_Bike(current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value);
            Move = true;
        }
  

    }
    public void Move_Left()
    {
        if (current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value > 0)
        {
            current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value--;
            Set_Bike(current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value);
            Move = true;
        }
    }
    public void Set_Bike(int id)
    {
      
        currentSlectedBike = current_UserProfile.BikeList[id];
        bikeHolder.StartRotate();
        currentid = 160;

    }
    float roatspeed = 20;
    public void FixedTick()
    {
        if (!ON)
            return;

        if (Move)
        {
            MoveCam_Lerp(bikeHolder.BikeTransfroms[current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].transform.position);
        }
        roatspeed = Mathf.Lerp(roatspeed, 1, bikeHolder.rotationSmothtime);


        if (bikeHolder.RotateLeft && !Mathf.Approximately(currentid, 280))
            currentid += times * roatspeed;


        if (bikeHolder.RotateRight && !Mathf.Approximately(currentid, 70))
            currentid -= times * roatspeed;

        bikeHolder.BikeTransfroms[current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].transform.GetChild(0).localRotation 
            = new Quaternion(0, bikeHolder.BikeTransfroms[current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].transform.GetChild(0).localRotation.y + currentid, 0, 180);

    }

    


}
