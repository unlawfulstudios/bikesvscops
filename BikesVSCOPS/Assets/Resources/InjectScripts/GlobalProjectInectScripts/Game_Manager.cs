﻿
using UnityEngine;
using Zenject;


public class Game_Manager 
{
    public GameMode_Type current_GameMode_Type;
    public GameController_Type current_Controller_Type {
        get
        { 
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
           return GameController_Type.pc_KeyBoard;
#elif UNITY_ANDROID || UNITY_IPHONE
            return GameController_Type.moblie_Touch;
#endif


        }
    }

}
