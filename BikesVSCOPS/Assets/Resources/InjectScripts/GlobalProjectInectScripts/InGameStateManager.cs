﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InGameStateManager : InGameManagers  {

    public bool isPaused = false;

    public bool HasAiInScene()
    {
        if (Game_Manager.current_GameMode_Type == GameMode_Type.FREERIDE_MODE || Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE)
            return false;
        else return true;
    }

    public void RestartRun()
    {
        EventManager.Trigger_Event("Restart");
    }
    public void pause_Game()
    {
        ingameSoundManager.Pause_Sound();
        EventManager.Trigger_Event("Pause");
        // EventManager.Trigger_Event("Pause");
    }
    public void waitBefore_UnPause()
    {
        EventManager.Trigger_Event("wait");
    }
    public void resume_Game()
    {
        ingameSoundManager.Resuem_Sound();
        EventManager.Trigger_Event("Resume");
    }

  
}
