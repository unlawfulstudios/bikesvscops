using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using UnityEditor;
using Zenject;
using System;

public class UserProfile
{
    private static UserProfile instance = null;

    public static UserProfile Instnace
    {

        get
        {

            if (instance == null)
            {
                instance = new UserProfile();
            }
            return instance;

        }
    }

    #region Public Global Access

    #endregion

    public BikesVSCopsInfo bikesVSCopsProfileInfo = new BikesVSCopsInfo();
    public Game_Settings_Manager game_Settings_Manager = new Game_Settings_Manager();
    public string UserID;

    public City[] CAREER_Citys =
    {
        new City(0),
        //new City(),
        //new City(),

    };

    public Bike[] BikeList =
    {
        new Bike(0),
        new Bike(1),
        new Bike(2),
        new Bike(3),
        new Bike(4),
        new Bike(5),
        new Bike(6),
    };

    public string Json_Value()
    {
        return JsonUtility.ToJson(this);
    }


    public UserProfile()
    {

    }

    public void Set(DataSnapshot dataSnapshot)
    {
        instance = JsonUtility.FromJson<UserProfile>(dataSnapshot.GetRawJsonValue());
    }

}
