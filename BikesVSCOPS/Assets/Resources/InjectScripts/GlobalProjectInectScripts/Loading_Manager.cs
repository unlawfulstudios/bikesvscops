﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


public class Loading_Manager : GlobaleManagers, IInitializable {

    public static bool isLoading = false;

    public Loading_ManagerSettings loading_ManagerSettings;

    public void Initialize()
    {
     
        loading_ManagerSettings.loadLevel_UI1.SetActive(false);
        loading_ManagerSettings.loadLevel_UI2.SetActive(false);
        isLoading = false;
    }

    public Loading_Manager(Loading_ManagerSettings _loading_ManagerSettings)
    {
        loading_ManagerSettings = _loading_ManagerSettings;
    }

    public void Toogle_SeverLodingScreen(bool toggole)
    {
        loading_ManagerSettings.SeverLoadUI.SetActive(toggole);
    }
    public GameMode_Type current_GameModeType() { return Game_Manager.current_GameMode_Type; }
    public void SET(bool current)
    {
        Set_Type_Text();

        if (current_GameModeType() == GameMode_Type.GET_AWAY_MODE)
        {
            Set_TryAomunt_Text();
          
        }
        else
        {
            loading_ManagerSettings.TriesAmountText.text = "";
            Set_Run_Text(current);

        }


    }
    public void Set_Type_Text()
    {
        loading_ManagerSettings.TypeText.text = current_GameModeType().ToString().Replace("_"," ");
    }
    public void Set_Run_Text(bool showCurrent)
    {

        loading_ManagerSettings.TriesAmountText.text = "";

       
            if (showCurrent)
            {
                if ( current_UserProfile.bikesVSCopsProfileInfo.current_Run != null)
                 {
                Debug.Log("Show Current ");
                loading_ManagerSettings.Run_Info_text.text = "RUN : " + current_UserProfile.bikesVSCopsProfileInfo.current_Run.id;
                 }
            }
            else
            {
            loading_ManagerSettings. Run_Info_text.text = "RUN : " + current_UserProfile.bikesVSCopsProfileInfo.next_Run.id;
            }

        loading_ManagerSettings.city_Info_text.text = "CITY : " + current_UserProfile.bikesVSCopsProfileInfo.current_City.CityName.Value;
        Debug.Log(current_UserProfile.bikesVSCopsProfileInfo.current_City.CityName.Value);
    }

    public void Set_TryAomunt_Text()
    {
        loading_ManagerSettings.Run_Info_text.text = "";
        loading_ManagerSettings.city_Info_text.text = "";
        loading_ManagerSettings.TriesAmountText.text = "TRY :  " + loading_ManagerSettings.Try_Amount();
    }

    
}
