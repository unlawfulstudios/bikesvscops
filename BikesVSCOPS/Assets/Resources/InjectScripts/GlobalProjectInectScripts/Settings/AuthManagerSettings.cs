﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using JS.BVSC.BVSC_Behavior;

public class AuthManagerSettings : BVSC_BehaviorBase
{
    public InputField nameField;
    public InputField passwordFiled;
    public InputField emailFiled;
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;


    public override void Process_Start()
    {
        Debug.Log("StarT");
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                GlobaleManagers.authManager.InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

  
}
