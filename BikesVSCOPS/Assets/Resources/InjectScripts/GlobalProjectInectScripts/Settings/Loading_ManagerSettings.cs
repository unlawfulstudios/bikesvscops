﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JS.BVSC.BVSC_Behavior;

public class Loading_ManagerSettings : BVSC_BehaviorBase
{

    public Text TriesAmountText;
    public Text TypeText;
    public Text Run_Info_text;
    public Text city_Info_text;

    public string Try_Amount() { return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value.ToString(); }

    [HideInInspector]public GameObject Current_LoadLevelUI;
    public GameObject loadLevel_UI1;
    public GameObject loadLevel_UI2;
    public GameObject SeverLoadUI;




}

