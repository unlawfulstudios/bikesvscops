using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "S", menuName = "Installers/S")]
public class S : ScriptableObjectInstaller<S>
{
    public override void InstallBindings()
    {
        Container.Bind<Object_Manager>().FromScriptableObjectResource("Object_Manager");

    }

  
}