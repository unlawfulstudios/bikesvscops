﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(VideoCaptureManager))]
public class VideoCaptuerManagerEditor : Editor {

    // Use this for initializatio
    VideoCaptureNode captureNode;
    SceneView sceneView { get { return SceneView.lastActiveSceneView; } }
    int camNum = 0;
    int copID = 0;
    bool moveCam = false;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        VideoCaptureManager videoCaptureManagerScript = (VideoCaptureManager)target;


        if (GUILayout.Button("Move Cam To next Node"))
        {
            videoCaptureManagerScript.setCamOnPlayer = false;
            videoCaptureManagerScript.setCamOnCop = false;
            videoCaptureManagerScript.MoveCamToNextNode(out captureNode);
            moveCam = true;
        }
        if (GUILayout.Button("Move Back a Node"))
        {
            videoCaptureManagerScript.setCamOnPlayer = false;
            videoCaptureManagerScript.setCamOnCop = false;
            videoCaptureManagerScript.MoveCamToLastNode(out captureNode);
            moveCam = true;
        }

        if (GUILayout.Button("Move Cam To next Cam Point On Node"))
        {
            videoCaptureManagerScript.setCamOnPlayer = false;
            videoCaptureManagerScript.setCamOnCop = false;
            videoCaptureManagerScript.MoveCamToNextPoint(captureNode);
            moveCam = true;
        }
        if (GUILayout.Button("Move Last Cam Point On Node"))
        {
            videoCaptureManagerScript.setCamOnPlayer = false;
            videoCaptureManagerScript.setCamOnCop = false;
            videoCaptureManagerScript.MoveCamToLastPoint(captureNode);
            moveCam = true;
        }
        if (GUILayout.Button("Reset"))
        {
            videoCaptureManagerScript.Reset_Cam();
            MoveSceneCam();
            videoCaptureManagerScript.CaptureCamera.transform.SetParent(null);
            videoCaptureManagerScript.setCamOnPlayer = false;
            videoCaptureManagerScript.setCamOnCop = false;
            camNum = 0;
            moveCam = true;
        }

        if (GUILayout.Button(" Cam On Player "))
        {
            videoCaptureManagerScript.setCamOnPlayer = true;
            moveCam = true;
        }


        if (GUILayout.Button(" Cam On Cop "))
        {
            videoCaptureManagerScript.setCamOnCop = true;
            videoCaptureManagerScript.setCamOnPlayer = false;
            moveCam = true;

        }

        if (videoCaptureManagerScript.setCamOnPlayer)
        {
            videoCaptureManagerScript.setCamOnCop = false;

            if (GUILayout.Button("Move Next a Camera"))
            {
                if (camNum < 2)
                {
                    camNum++;
                    moveCam = true;
                }
            }

            if (GUILayout.Button("Move Back a Camera"))
            {
                if (camNum > 0)
                {
                    camNum--;
                    moveCam = true;
                }

        
            }

          
            videoCaptureManagerScript.Set_Cam_OnPlayer(camNum);
        }
    

        if (videoCaptureManagerScript.setCamOnCop)
        {
            videoCaptureManagerScript.setCamOnPlayer = false;

            if (GUILayout.Button("Move Next a Cop"))
            {
                if (copID < videoCaptureManagerScript.copsNode.Length - 1)
                {
                    copID++;
                    moveCam = true;
                }
                   

              
            }

            if (GUILayout.Button("Move Back a Cop"))
            {
                if (copID > 0)
                {
                    copID--;
                    moveCam = true;
                }

         
            }

            if (GUILayout.Button("Move Next a Camera"))
            {
                if (camNum < 2)
                {
                    camNum++;
                    moveCam = true;
                }
            }

            if (GUILayout.Button("Move Back a Camera"))
            {
                if (camNum > 0)
                {
                    camNum--;
                    moveCam = true;

                }

            }
          
            videoCaptureManagerScript.Set_Cam_OnCop(copID,camNum);

      
        }



        if(moveCam)
        {
            MoveSceneCam();
            moveCam = false;

        }

    }

    public void MoveSceneCam()
    {

        if (SceneView.lastActiveSceneView != null && Selection.activeTransform != null)
        {
            Selection.activeGameObject = VideoCaptureManager.Instance.CaptureCamera.gameObject;

            sceneView.LookAt(VideoCaptureManager.Instance.CaptureCamera.gameObject.transform.position);
        }
    }

}
