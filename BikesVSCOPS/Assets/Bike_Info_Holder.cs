﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JS.BVSC.BVSC_Behavior;

public class Bike_Info_Holder : MainMenuBehaviors {

    public Text Bike_Name_Text;
    public Text TrieAmountText;
    public RawImage Bike_Image;

    public override void On_Update()
    {
        TrieAmountText.text = GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value.ToString();
        Bike_Name_Text.text = "BIKE " + GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike.attributes.BikeID;
        Bike_Image.texture = GlobaleManagers.BikeInventoryItemList.bike_Data_list[GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike.attributes.BikeID - 1].Icon_Iamge;
    }



}
