﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class helicopter : InGameBehaviors {

    // Use this for initialization

    public int speed;
    public Renderer body;
    private void Update()
    {
        if (!InGameEventManager.Active)
            return;

        if(!body.isVisible && !InGameEventManager.Active)
        {
           gameObject.SetActive(false);
        }
        transform.Translate(Vector3.forward * speed * Time.deltaTime);      
    }

}
