﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Zenject;

[System.Serializable]
public class Results_TimePanels
{
    public GameTimePanle Player_Results_TimePanel;
    public GameTimePanle Ai_Career_Results_TimePanel;
    public GameTimePanle Player_Busted_TimePanel;
    public GameObject busted_Panel;
    public GameObject career_Panel;
    public GameObject WinUiObject;
    public GameObject Lose_Ui_gameObject;
    public void Set_Player_Panel(GameMode_Type gameMode, Result_Type end_Result_Type)
    {
        career_Panel.SetActive(true);
        Player_Results_TimePanel.Set_Time_Panel(end_Result_Type, gameMode);
    }
    public void Set_Ai_Panel(GameMode_Type gameMode, Result_Type end_Result_Type)
    {
        Ai_Career_Results_TimePanel.Set_Time_Panel(end_Result_Type, gameMode);
    }
    public void Set_Busted_Panel()
    {
       
            Player_Busted_TimePanel.Set_Time_Panel(Result_Type.Busted, GameMode_Type.GET_AWAY_MODE);
        busted_Panel.SetActive(true);
    }
}


public class Results_Manager : InGameManagers, IInitializable
{
    public string ai_StoredTime;
    string player_stroedTime;
    public bool PopUpwindowAttheEndOFRun = false;
    public int UnlockID;
    public Results_Manager_Settings Results_ManagerSettings;
    public float time_tellPopUP = 0.1f;
    public UnlockType current_Unlocktype;
    private RUN current_Run() { return current_UserProfile.bikesVSCopsProfileInfo.current_Run; }
    private City currentCity() { return current_UserProfile.bikesVSCopsProfileInfo.current_City; }

    public  Results_Manager(Results_Manager_Settings _Results_ManagerSettings)
    {
        Results_ManagerSettings = _Results_ManagerSettings;
    }


    public void Initialize()
    {
        Results_ManagerSettings.results_TimePanels.busted_Panel.SetActive(false);
        Results_ManagerSettings.results_TimePanels.career_Panel.SetActive(false);
        Results_ManagerSettings.results_TimePanels.WinUiObject.SetActive(false);
        Results_ManagerSettings.results_TimePanels.Lose_Ui_gameObject.SetActive(false);
        showResualts = false;
    }
    public int tryAmounts
    {
        get
        {
            if (current_UserProfile != null)
            {
                m_tryAmounts = current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value;
                Results_ManagerSettings.Amount_Text.text = "YOU HAVE " + m_tryAmounts.ToString("00") + " TRIES REMAINING";
            }
            else
            {
                Debug.LogError(" Probblem UserProfile.Instance == null");
                m_tryAmounts = 0;
            }

            return m_tryAmounts;
        }
        set
        {
            if (current_UserProfile != null)
            {
                current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value = value;
            }
            else
            {

                Debug.LogError(" Probblem UserProfile.Instance == null");
            }
        }



    }
    private int m_tryAmounts = 0;
    private bool playerisBusted = false;
    private int winawmount
    {
        get
        {
            return current_UserProfile.bikesVSCopsProfileInfo.current_Run.WinAmmount.value;
        }
        set 
        {
           current_UserProfile.bikesVSCopsProfileInfo.current_Run.WinAmmount.value = value;
        }
    }
    public bool showResualts = false;
    public bool PlayerHasCompeletedRun1
    {
        get
        {
            return winawmount > 0 ? true : false; 
        }
    
    }
    public bool PlayerHasCompeletedRunA
    {
        get
        {
            return winawmount > 1 ? true : false; 
        }
    }
    public bool PlayerHasUnlockedNextRUn
    {
        get
        {
            return current_UserProfile.bikesVSCopsProfileInfo.next_Run.Unlocked.value;
        }
        set
        {
            current_UserProfile.bikesVSCopsProfileInfo.next_Run.Unlocked.value = value;
        }
    }
   
    public void Player_End()
    {
        player_stroedTime = GameTime.Current_Game_Time;

        Set_BestTime(GameTime.current_timeSpan, current_UserProfile.bikesVSCopsProfileInfo.current_Run.RUN_BESTTIME.best_Time(), GameMode_Type.CAREER_MODE);
        
        
        if (Game_Manager.current_GameMode_Type == GameMode_Type.CAREER_MODE)
        {
            Results_ManagerSettings.Run_Info_text.text = "CITY : " + currentCity().CityName.Value + "        " + " RUN : " + current_Run().id ;
        }
        InGameEventManager.current_Run_EventState = Run_Event_States.inactive;
    }
    public void Set_BestTime(TimeSpan timeSpan1, TimeSpan timeSpan2, GameMode_Type gameMode_Type)
    {

        if (CheckTimeISBestTime(timeSpan1, timeSpan2, false))
        {
            if (gameMode_Type == GameMode_Type.CAREER_MODE)
                current_UserProfile.CAREER_Citys[current_Run().city_id].Runs[current_Run().id].RUN_BESTTIME.Set(timeSpan1);
        }
        if (CheckTimeISBestTime(timeSpan1, timeSpan2, true))
        {
            if (gameMode_Type == GameMode_Type.GET_AWAY_MODE)
                current_UserProfile.CAREER_Citys[current_Run().city_id].GetAwayBestTime.Set(timeSpan1);
        }



    }
    public bool CheckTimeISBestTime(TimeSpan timeSpan1, TimeSpan timeSpan2, bool greater)
    {
        return check(timeSpan1, timeSpan2, greater);
    }
    bool check(TimeSpan timeSpan1, TimeSpan timeSpan2, bool greater)
    {

        bool check1;

        if (!greater)
            check1 = timeSpan1 < timeSpan2 ? true : false;
        else check1 = timeSpan1 > timeSpan2 ? true : false;

        bool check2 = timeSpan2 == new TimeSpan(0, 0, 0, 0) ? true : false;


        if (check1 || check2)
        {
            return true;
        }
        else return false;


    }
    public void Ai_End()
    {
        ai_StoredTime = GameTime.Current_Game_Time;
    }
    public void End(bool player_has_Won)
    {
        if (showResualts == false)
        {
            Results_ManagerSettings.results_TimePanels.career_Panel.SetActive(true);

            if (player_has_Won)
            {

                PlayerWin();
              
            }
            else
            {
                Ai_Win();
            }
            Results_ManagerSettings.results_Animator.SetTrigger("Finsh");
            Results_ManagerSettings.results_Animator.SetBool("win", player_has_Won);
            InGameEventManager.current_Run_EventState = Run_Event_States.inactive;
            showResualts = true;
        }



   

    }
    public void PlayerWin()
    {
        if (winawmount > 2)
        {
            winawmount += 1;
        }
        else
        {
            PlayerHasUnlockedNextRUn = true;
        }

        if (PopUpwindowAttheEndOFRun)
            PopUP();


        Results_ManagerSettings.results_TimePanels.Player_Results_TimePanel.Set_Time_Panel(player_stroedTime);
        Results_ManagerSettings.results_TimePanels.Ai_Career_Results_TimePanel.Set_Time_Panel(Result_Type.LOST, GameMode_Type.CAREER_MODE);
        Results_ManagerSettings.results_TimePanels.WinUiObject.SetActive(true);
        Results_ManagerSettings.results_TimePanels.Lose_Ui_gameObject.SetActive(false);
        Results_ManagerSettings.results_Animator.SetBool("win", true);
        InGameEventManager.current_Run_EventState = Run_Event_States.inactive;



    }
    private void PopUP()
    {
        PopUpManagar.ShowPopUpAtEnd(current_Unlocktype, UnlockID);
        Time.timeScale = 0;
    }
    public void Ai_Win()
    {

        Results_ManagerSettings.results_TimePanels.Player_Results_TimePanel.Set_Time_Panel(Result_Type.LOST, GameMode_Type.CAREER_MODE);
        Results_ManagerSettings.results_TimePanels.Ai_Career_Results_TimePanel.Set_Time_Panel(ai_StoredTime);
        Results_ManagerSettings. results_TimePanels.Lose_Ui_gameObject.SetActive(true);
        Results_ManagerSettings.results_TimePanels.WinUiObject.SetActive(false);
        Results_ManagerSettings.results_Animator.SetBool("win", false);

    }
    public void SHowcarrierResulets()
    {
        Results_ManagerSettings.results_TimePanels.career_Panel.SetActive(true);
    }
    private void Update()
    {
        if (playerisBusted &&  Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE)
            Results_ManagerSettings.results_TimePanels.Set_Busted_Panel();



        if(Input.GetKeyDown(KeyCode.O))
        {
            Debug.Log(current_Run().id);
        }
    }
    public void Player_Busted()
    {

        playerisBusted = true;

        if (showResualts != true)
        {
            if (Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE)
           {

                if (tryAmounts >= 0)
                {

                    tryAmounts -= 1;
                    current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value = tryAmounts;
                    Set_TryAomunt_Text_Tries(current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value);

                }
                else
                {
                    tryAmounts = 0;
                }

                Set_BestTime(GameTime.current_timeSpan, current_UserProfile.bikesVSCopsProfileInfo.current_City.GetAwayBestTime.best_Time(), GameMode_Type.GET_AWAY_MODE);
            }
        }


        InGameEventManager.current_Run_EventState = Run_Event_States.inactive;

    }
    public void Set_TryAomunt_Text_Tries(int Try_Amount)
    {

        Results_ManagerSettings.TriesAmountText.text = "YOU HAVE " + Try_Amount + " TRIES REMAINING";
    }

}
