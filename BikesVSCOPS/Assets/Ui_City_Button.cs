﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_City_Button : MainMenuButton
{
    public int City_ID = 0;
  
    protected override void Button_Press(int f)
    {
        Set();
        base.Button_Press(f);
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_City 
            = GlobaleManagers.current_UserProfile.CAREER_Citys[City_ID];    
       
    }

    void Set()
    {
        if ( GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.CAREER_MODE)
        {
            Go_StraightToScene(false);
        }
        else if (GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.FREERIDE_MODE)
        {
            Go_StraightToScene(GlobaleManagers.City_Data_Manager.citys[City_ID].FreeRideScenName,true);
         
        }
        else if (GlobaleManagers.Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE)
        {
            Go_StraightToScene(GlobaleManagers.City_Data_Manager.citys[City_ID].GetAwaySceneName,true);
        }
    }

    public void Go_StraightToScene(bool toggle)
    {
        Go_StraightToScene("", toggle);
    }
    public void Go_StraightToScene(string sceneName,bool toggle)
    {
        SceneName = sceneName;
        buttonAtrabutties.ChangesController = !toggle;
        buttonAtrabutties.LoadsAfter = toggle;
        buttonAtrabutties.ChangesScene = toggle;
    }
}
