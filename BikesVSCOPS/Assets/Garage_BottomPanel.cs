﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum BottomPanelType
{
    none,
    ColoredTires,
    Colors,
    Nos

}

public class Garage_BottomPanel : MonoSingleton<Garage_BottomPanel>
{

    public GameObject Bottom_Panel_Object;
    public GameObject Nos_Panel_Object;   
    public GameObject ColoredTires_Panel_object;

    public UI_Panel[] uI_Panels;
    public UI_Panel current_ui_Panel;




    public Animator panel_Anomator;
    public static BottomPanelType bottomPanelType;

    public bool PanelOpen = false;


    public bool CanCange = true;


    protected override void Init()
    {
        Toggle_BottomPanel(false,BottomPanelType.none);
        Change_Controller(0);


    }


    public void Toggle_BottomPanel(bool toggle, BottomPanelType panel_Type)
    {

        PanelOpen = toggle;
        bottomPanelType = panel_Type;

       
        panel_Anomator.SetBool("toggle", toggle);

        if (CanCange)
        {
            switch (panel_Type)
            {
                case BottomPanelType.Colors:
                    Change_Controller(0);
                    return;
                case BottomPanelType.ColoredTires:
                    Change_Controller(1);
                    return;
                case BottomPanelType.Nos:
                    Change_Controller(2);
                    return;
                case BottomPanelType.none:
                    return;
            }
        }


      
    }

 
    public void Change_Controller(int switchto_id)
    {
     
        if (uI_Panels[switchto_id] != null)
        current_ui_Panel = uI_Panels[switchto_id];

        current_ui_Panel.Set_Active_State(true);

        for (int i = 0; i < uI_Panels.Length; i++)
        {

            if (uI_Panels[i] != current_ui_Panel)
            {
                uI_Panels[i].Set_Active_State(false);
            }
        }

    }


}
