﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class UserProfileBestTime : UserProfileObject
{

    public override string Name
    {
        get
        {
            return "UserProfileBestTime";
        }


    }


    private int index = 0;
   

    public UserProfileKey<int, int> Best_min;
    public UserProfileKey<int, int> Best_Second;
    public UserProfileKey<int, int> Best_milSecond;

    public TimeSpan best_Time()
    {
        TimeSpan time = new TimeSpan(0,0,Best_min.Value, Best_Second.Value, Best_milSecond.Value);
        Value = time.ToString();
        return time;
    }

    public UserProfileBestTime(int m_index)
    {
        index = m_index;

        Best_min = new UserProfileKey<int, int>(m_index + "Best_min", 0);
        Best_Second = new UserProfileKey<int, int>(m_index+ "Best_Second", 0);
        Best_milSecond = new UserProfileKey<int, int>(m_index+ "Best_milSecond", 0);
        Value = best_Time().ToString();

    }
    public void Set(TimeSpan timeSpan)
    {
        Best_min.Value = timeSpan.Minutes;
        Best_Second.Value = timeSpan.Seconds;
        Best_milSecond.Value = timeSpan.Milliseconds;
    }

    public string Value = "NULL";


    public override void SyncData()
    {
        TimeSpan ts = TimeSpan.Parse(Value);

        Best_min.Value = ts.Minutes;
        Best_Second.Value = ts.Seconds;
        Best_milSecond.Value = ts.Milliseconds / 10;
        base.SyncData();

    }
    public override void Reset()
    {
        TimeSpan timeSpan = new TimeSpan();

        Value = timeSpan.ToString();
        Best_min.Value = 0;
        Best_Second.Value = 0;
        Best_min.Value = 0;
        base.Reset();

    }
    protected override bool m_DataSynced()
    {
        string timeText = best_Time().ToString();

    
        return Value == timeText;
    }


}
