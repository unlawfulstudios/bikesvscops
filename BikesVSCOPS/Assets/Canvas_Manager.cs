﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum CanvasRezType
{
    Default,
    ScreenSize,
    HD,
    FHD,
    QHD,
    forK
    

}

public class Canvas_Manager : CanvasScaler
{

    public CanvasRezType canvas_RezType;
    
    protected override void Awake()
    {
        base.Awake();

        Set(canvas_RezType);
    }


    void Set(CanvasRezType canvasRezType)
    {
        switch(canvasRezType)
        {
            case CanvasRezType.Default:

                return;

            case CanvasRezType.ScreenSize:
                m_ReferenceResolution.Set(Screen.currentResolution.width, Screen.currentResolution.height);
                return;
            case CanvasRezType.HD:
                m_ReferenceResolution.Set(1250,720);
                return;
            case CanvasRezType.FHD:
                m_ReferenceResolution.Set(1920, 1080);
                return;
            case CanvasRezType.QHD:
                m_ReferenceResolution.Set(2560, 1440);
                return;
            case CanvasRezType.forK:
                m_ReferenceResolution.Set(3840, 2160);
                return;
        }
    }
    //private void Awake()
    //{
    //    cs.referenceResolution.(Screen.currentResolution.width, Screen.currentResolution.height);
    //}
}
