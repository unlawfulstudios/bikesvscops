﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[DisallowMultipleComponent]
public class Player : Racer {

	public Bike current_Bike() { return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike; }
    private Player_BikeController current_BikeControl() { return GetComponent<Player_BikeController>(); }

    private void Awake()
    {
            InGameManagers.Racer_Manager.Player_BikeControl = current_BikeControl();
           

        current_Bike().Set_Up(
            out current_BikeControl().bikeSetting.bikeAttributes);



     

    }


}
