﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SizeType
{
    Custom = 0,
    Type1 = 1,
    Type2 = 2,
}
[System.Serializable]
public class Node : InGameBehaviors
{
    #region Public Variables 

    public bool StopAtNode;
    public bool SpawnAble;
    public bool is_CheckPoint;
    public bool Show;
    public bool SlowDownOnApproch()
    {
        return slowDownspeed > 1 ? true : false;
    }
    public bool brakeAtNode;

    [Range(0, 10)] public float BreakTime;
    [Range(0, 1)] public float slowDownspeed;
    [Range(0, 100)] public float CheckSpawnRange;

    public LayerMask mask = -1;

    public SizeType size_Type;

    public Quaternion SpawnRotation;

    #endregion;

    #region private Methods 


    private void Awake()
    {
        InGameManagers.object_Manager.Path_NodeList.Add(this);
    }

    



    bool spawnAble = false;

    public bool CanSpawn()
    {

        if (GetComponent<Renderer>().isVisible)
            return false;


        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, CheckSpawnRange, mask);


        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i] != null)
            {

                return false;

            }
            i++;
        }
        return true;
    }

    #endregion

    #region Public Methods 

    public void AddCheckPoint()
    {
        if (!this.GetComponent<CheckPoint>())
            this.transform.gameObject.AddComponent<CheckPoint>();

        this.GetComponent<BoxCollider>().isTrigger = true;
    }

    public void RemoveCheckPoint()
    {
        if (this.GetComponent<CheckPoint>())
            DestroyImmediate(this.GetComponent<CheckPoint>());
        else
            return;
        if (this.GetComponent<BoxCollider>())
            DestroyImmediate(this.GetComponent<BoxCollider>());
        else
            return;

    }

    #endregion

}
