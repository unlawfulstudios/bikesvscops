using UnityEngine;
using Zenject;

public class User_ProfileInstaller : Installer<User_ProfileInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<UserProfile>().AsSingle().NonLazy();
    }
}