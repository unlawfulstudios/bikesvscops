﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Top_Panel_Controller : MonoSingleton<Top_Panel_Controller> {

    protected override void Init()
    {

    }

    public void Toggle(bool toggle)
    {
        if(this.gameObject !=null)
       gameObject.SetActive(toggle);
    }
}
