using UnityEngine;

public static class StringTools
{
	public static string RemoveString(string sourceString, string splitter)
	{
		if(!sourceString.Contains(splitter))
		{
			return sourceString;
		}

		int startIndex = sourceString.IndexOf(splitter);
		string result = sourceString.Remove(startIndex, splitter.Length);

		return result;
	}


	public static string TrimStartOfAllCharacters(string sourceString, char trimed)
	{
		while(sourceString[0] == trimed)
		{
			sourceString = sourceString.TrimStart(trimed);
		}
		return sourceString;
	}


	public static string GetStringAfterChar(string sourceString, char delemenator)
	{
		while(sourceString[0] != delemenator)
		{
			sourceString = sourceString.TrimStart(sourceString[0]);
		}
		return sourceString;
	}


	public static string TrimStartCharacter(string sourceString, char trimed)
	{
		sourceString = sourceString.TrimStart(trimed);
		return sourceString;
	}


	public static string TrimStartString(string sourceString, string trimed)
	{
		char[] trimedChars = trimed.ToCharArray();
		foreach(char trimedChar in trimedChars)
			sourceString = sourceString.TrimStart(trimedChar);
		return sourceString;
	}


	public static string GetBeforeNextChar(string sourceString, char trimed)
	{
		string[] splits = sourceString.Split(trimed);
		return splits[0];
	}


	public static string GetParentPathFromString(string sourceString)
	{
		int endOfParentPath = sourceString.Length - 1;
		for(; endOfParentPath >= 0; --endOfParentPath)
		{
			if(sourceString[endOfParentPath] == '/'
				|| sourceString[endOfParentPath] == '\\')
			{
				break;
			}
		}

		//string parentPath = "";
		char[] parentPathBuffer = new char[endOfParentPath];
		sourceString.CopyTo(0, parentPathBuffer, 0, endOfParentPath);
		string parentPath = new string(parentPathBuffer);

		return parentPath;
	}

	public static string BuildReadableString(string source)
	{
		string readableString = "";
		bool lastCharWasSpace = false;
		foreach(char c in source)
		{
			if(c >= 'A' && c <= 'Z' && (!lastCharWasSpace))
			{
				readableString += " " + c;
				lastCharWasSpace = true;
				continue;
			}
			else if(c == '_')
				{
					if(!lastCharWasSpace)
					{
						readableString += " ";
					}
					lastCharWasSpace = true;
					continue;
				}
				else
				{
					readableString += c;
				}
			lastCharWasSpace = false;
		}

		return readableString;
	}
}