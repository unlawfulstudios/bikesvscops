using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HammerCat
{
	public static partial class Diagnostics
	{
	#region Public Methods

		public static string GetMemberName<T>(System.Linq.Expressions.Expression<System.Func<T>> memberExpression)
		{
			System.Linq.Expressions.MemberExpression expressionBody = (System.Linq.Expressions.MemberExpression)memberExpression.Body;
			return expressionBody.Member.Name;
		}


		public static bool IsSelectedAFolder(Object selectedObj)
		{
			#if UNITY_EDITOR

			string selectedObjPath = AssetDatabase.GetAssetPath(selectedObj);

			string dummyPath = System.IO.Path.Combine(selectedObjPath, "fake.asset");
			string assetPath = AssetDatabase.GenerateUniqueAssetPath(dummyPath);

			return assetPath != "";

			#else
			return false;
			#endif
		}
	
	#endregion

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods


	
	#endregion
	}
}