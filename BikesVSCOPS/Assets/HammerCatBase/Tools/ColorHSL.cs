using UnityEngine;

public static class ColorTools
{
	#region Public Defines

	public static readonly float MaxHue = 360;
	public static readonly float MaxSaturation = 1f;
	public static readonly float MaxValue = 1f;

	public struct ColorHSV
	{
		public float h;	// angle in degrees
		public float s;	// percent
		public float v;	// percent

		public ColorHSV(float hue, float saturation, float value)
		{
			h = hue;
			s = saturation;
			v = value;
		}


		public override string ToString()
		{
			return string.Format("({0:F1}, {1:F1}, {2:F1})", this.h, this.s * 100f, this.v * 100f);
		}

	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	public static ColorHSV Color2HSV(Color rgb)
	{
		ColorHSV hsv = new ColorHSV(0f, 0f, 0f);
		float max = 0f, min = 0f;


		if(rgb.r > rgb.g)
		{
			max = rgb.r;
		}
		else
		{
			max = rgb.g;
		}

		if(rgb.b > max)
		{
			max = rgb.b;
		}

		if(rgb.r < rgb.g)
		{
			min = rgb.r;
		}
		else
		{
			min = rgb.g;
		}

		if(rgb.b < min)
		{
			min = rgb.b;
		}


		hsv.v = max;



		hsv.s = 0f;
		if(max != 0f)
		{
			hsv.s = (max - min) / max;
		}




		if(hsv.s == 0f)
		{
			hsv.h = float.NaN;
		}
		else
		{
			float delta = max - min;
			float maxR = max - rgb.r;
			float maxG = max - rgb.g;
			float maxB = max - rgb.b;

			if(rgb.r == max)
			{
				hsv.h = (maxB - maxG) / delta;
			}
			else if(rgb.g == max)
				{
					hsv.h = 2f + (maxR - maxB) / delta;
				}
				else if(rgb.b == max)
					{
						hsv.h = 4f + (maxG - maxR) / delta;
					}

			hsv.h *= 60f;
			while(hsv.h < 0f)
			{
				hsv.h += 360f;
			}
			while(hsv.h > 360f)
			{
				hsv.h -= 360f;
			}
		}
		return hsv;
	}


	public static Color HSV2Color(ColorHSV hsv)
	{
		Color rgb = new Color(0f, 0f, 0f, 1f);
		if(hsv.s <= 0f)
		{
			if(float.IsNaN(hsv.h))
			{
				rgb.r = 0f;
				rgb.g = 0f;
				rgb.b = 0f;
				return rgb;
			}
			rgb.r = hsv.v;
			rgb.g = hsv.v;
			rgb.b = hsv.v;
			return rgb;
		}

		float hh = hsv.h;
		if(hh >= 360f)
		{
			hh = 0f;
		}
		hh /= 60f;
		long i = (long)hh;
		float ff = hh - (float)i;
		float p = hsv.v * (1f - hsv.s);
		float q = hsv.v * (1f - (hsv.s * ff));
		float t = hsv.v * (1f - (hsv.s * (1f - ff)));

		switch(i)
		{
			case 0:
				rgb.r = hsv.v;
				rgb.g = t;
				rgb.b = p;
				break;
			case 1:
				rgb.r = q;
				rgb.g = hsv.v;
				rgb.b = p;
				break;
			case 2:
				rgb.r = p;
				rgb.g = hsv.v;
				rgb.b = t;
				break;

			case 3:
				rgb.r = p;
				rgb.g = q;
				rgb.b = hsv.v;
				break;
			case 4:
				rgb.r = t;
				rgb.g = p;
				rgb.b = hsv.v;
				break;
			case 5:
			default:
				rgb.r = hsv.v;
				rgb.g = p;
				rgb.b = q;
				break;
		}
		return rgb;
	}

	#endregion

}