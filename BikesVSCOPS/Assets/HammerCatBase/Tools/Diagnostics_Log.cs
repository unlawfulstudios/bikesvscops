using UnityEngine;
using System.Collections.Generic;
using System.Reflection;

namespace HammerCat
{
	/// <summary>
	/// Wrapper class for logging.
	/// </summary>
	public static partial class Diagnostics
	{
#if UNITY_EDITOR
		#region Public Properties

		public static bool OpenFileAtLineExternal_OnError
		{
			get { return m_openFileAtLineExternal_OnError;	}
			set { m_openFileAtLineExternal_OnError = value; }
		}

		public static bool OpenFileAtLineExternal_OnWarning
		{
			get { return m_openFileAtLineExternal_OnWarning; }
			set { m_openFileAtLineExternal_OnWarning = value; }
		}

		public static bool OpenFileAtLineExternal_OnLog
		{
			get { return m_openFileAtLineExternal_OnLog; }
			set { m_openFileAtLineExternal_OnLog = value; }
		}

		#endregion

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		#region Public Methods

		/// <summary>
		/// Logs the message, will only happen if RegisterContextForLogging() was
		/// 	called on the context passed in with a setting of LogType.Log.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		/// <param name='context'>
		/// Context.
		/// </param>
		public static void Log(object message, System.Object context)
		{
			Diagnostics.Internal_Log(LogType.Log, message, context);
		}

		/// <summary>
		/// Logs the warning, will only happen if RegisterContextForLogging() was
		/// 	called on the context passed in with a setting of LogType.Warning.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		/// <param name='context'>
		/// Context.
		/// </param>
		public static void LogWarning(object message, System.Object context)
		{
			Diagnostics.Internal_Log(LogType.Warning, message, context);
		}

		/// <summary>
		/// Logs the error, will allways happen.
		/// </summary>
		/// <param name='message'>
		/// Message.
		/// </param>
		/// <param name='context'>
		/// Context.
		/// </param>
		public static void LogError(object message, System.Object context)
		{
			Diagnostics.Internal_Log(LogType.Error, message, context);
		}

		/// <summary>
		/// If a context object is registered, when it calls Diagnostics.Log...(),
		/// 	it will only show messages to the level it was set to when registered.
		/// 	This allows you too easly turn off log spam for certain objects.
		/// </summary>
		/// <param name='context'>
		/// The object that will call this log.
		/// </param>
		/// <param name='messageLevel'>
		/// Message level allowed.
		/// 	LogType.Log it will allow all.
		/// 	LogType.Warning will skip logs.
		/// 	LogType.Error will skip logs and warnings.
		/// 	Others will default to Error settings.
		/// </param>
		public static void RegisterContextForLogging(System.Object context, LogType messageLevel)
		{
			ContextRegistration registeredContext = m_registeredContexts.Find(
				delegate(ContextRegistration registered)
			{
				return registered.Context == context;
			});
			if(registeredContext == null)
			{
				m_registeredContexts.Add(new ContextRegistration(context, messageLevel));
			}
			else
			{
				registeredContext.LogType = messageLevel;
			}
		}

		public static void ClearConsoleWindow()
		{
			Assembly assembly = Assembly.GetAssembly(typeof(UnityEditor.SceneView));
		    System.Type type = assembly.GetType("UnityEditorInternal.LogEntries");
			if(type == null)
			{
				Debug.Log("Failed to open source file");
				return;
			}

			MethodInfo method = type.GetMethod("Clear");
			method.Invoke(new object(), null);
		}

		#endregion

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		#region Private Defines

		internal sealed class ContextRegistration
		{
			public ContextRegistration(System.Object context, LogType logType)
			{
				m_context = context;
				m_logType = (logType == LogType.Log || logType == LogType.Warning) ? logType : LogType.Error;
			}


			public System.Object Context
			{
				get { return this.m_context; }
			}


			public LogType LogType
			{
				get { return this.m_logType; }
				set { this.m_logType = value; }
			}


			private System.Object m_context = null;
			private LogType m_logType = LogType.Log;
		}

		#endregion

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		#region Private Memebers

		private static List<ContextRegistration> m_registeredContexts = new List<ContextRegistration>();
		private static bool m_openFileAtLineExternal_OnLog = false;
		private static bool m_openFileAtLineExternal_OnWarning = false;
		private static bool m_openFileAtLineExternal_OnError = true;

		#endregion

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		#region Private Methods

		private static bool IsContextRegistrationAllowed(LogType type, System.Object context)
		{
			ContextRegistration registeredContext = m_registeredContexts.Find(
				delegate(ContextRegistration registered)
			{
				return registered.Context == context;
			});
			if(registeredContext == null)
			{
				// The context was never registered, so only display errors.
				return type != LogType.Log && type != LogType.Warning;
			}
			else
			{
				return registeredContext.LogType >= type;
			}
		}


		private static void Internal_Log(LogType type, object message, System.Object context)
		{
			if(IsContextRegistrationAllowed(type, context))
			{
				UnityEngine.Object unityObject = null;
				try
				{
					if(unityObject is UnityEngine.Object)
					unityObject = (UnityEngine.Object)context;
				}
				finally
				{
					switch(type)
					{
						case LogType.Log:
							{
								if(OpenFileAtLineExternal_OnLog)
								{
									OpenFile();
								}
								if(unityObject)
								{
									Debug.Log(message, unityObject);
								}
								else
								{
									Debug.Log(message);
								}
								break;
							}
						case LogType.Warning:
							{
								if(OpenFileAtLineExternal_OnWarning)
								{
									OpenFile();
								}
								if(unityObject)
								{
									Debug.LogWarning(message, unityObject);
								}
								else
								{
									Debug.LogWarning(message);
								}
								break;
							}
						default:
							{
								if(OpenFileAtLineExternal_OnError)
								{
									OpenFile();
								}
								if(unityObject)
								{
									Debug.LogError(message, unityObject);
								}
								else
								{
									Debug.LogError(message);
								}
								break;
							}
					}
				}
			}
		}

		private static void OpenFile()
		{
		    Assembly assembly = Assembly.GetAssembly(typeof(UnityEditor.SceneView));
		    System.Type type = assembly.GetType("UnityEditorInternal.InternalEditorUtility");
			if(type == null)
			{
				Debug.Log("Failed to open source file");
				return;
			}

			string[] stackFrames = System.Environment.StackTrace.Split(new char[] {'\n' });
			string callingFrame = stackFrames[4];

#if UNITY_STANDALONE_WIN
			string[] splitLog = callingFrame.Split(':');
			char drive = splitLog[0][splitLog[0].Length-1];
			string filePath = splitLog[1];
			string fullDrive = drive + ":" + filePath;
			string lineInfo = splitLog[2];
#else
			string cleanedCallFrame = StringTools.GetStringAfterChar(callingFrame, '/' );
			string[] splitLog = cleanedCallFrame.Split(':');
			string fullDrive = splitLog[0];
			string lineInfo = splitLog[1];
#endif

			lineInfo = StringTools.TrimStartString(lineInfo, "line ");
			int lineNumber = int.Parse(StringTools.GetBeforeNextChar(lineInfo, '\n'));


			//Debug.Log(@fullDrive + " line #" + lineNumber );
			MethodInfo method = type.GetMethod("OpenFileAtLineExternal");
			method.Invoke(method, new object[] { @fullDrive, lineNumber });
		}

		#endregion
#else
		public static void Log(object message, System.Object context)
		{
		}


		public static void LogWarning(object message, System.Object context)
		{
		}


		public static void LogError(object message, System.Object context)
		{
		}


		public static void RegisterContextForLogging(System.Object context, LogType messageLevel)
		{
		}


		public static void ClearConsoleWindow()
		{
		}


		public static bool OpenFileAtLineExternal_OnError
		{
			get ;
			set ;
		}


		public static bool OpenFileAtLineExternal_OnLog
		{
			get ;
			set ;
		}


		public static bool OpenFileAtLineExternal_OnWarning
		{
			get ;
			set ;
		}
#endif
	}
}