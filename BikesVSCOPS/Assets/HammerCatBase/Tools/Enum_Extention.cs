using System;
using System.Linq;
using System.Collections.Generic;

public static class Enum_Extention
{
	/// <summary>
	/// Get the next value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetNextEnumVal(setting4) returns 'setting5 value(8)' and not '5 (4+1)'
	/// </summary>
	/// <returns>
	/// The next enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetNextEnumVal<T>(T currentValue) where T : struct, IConvertible, IComparable
	{
		// Get the enum list
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);

		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any())
			return skipedVal.First();

		// Other wise return the starting index.
		return enumVals.First();
	}

	/// <summary>
	/// Get the next value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetNextEnumVal(setting3, setting4) returns 'setting0 value(0)' and not '3(2+1)' or 'setting4 value(4)'
	/// </summary>
	/// <returns>
	/// The next enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <param name='endValue'>
	/// End value to loop back to the begining on.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetNextEnumVal<T>(T currentValue, T endValue) where T : struct, IConvertible, IComparable
	{
		// Get the enum list
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);

		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any()
			&& !skipedVal.First().Equals(endValue))
			return skipedVal.First();

		// Other wise return the starting index.
		return enumVals.SkipWhile(e => endValue.Equals(e)).First();
	}

	/// <summary>
	/// Get the next value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetNextEnumVal(setting3, setting4) returns 'setting0 value(0)' and not '3(2+1)' or 'setting4 value(4)'
	/// </summary>
	/// <returns>
	/// The next enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <param name='endValue'>
	/// End value to loop back to the begining on.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetNextEnumVal<T>(T currentValue, T[] invalidReturns) where T : struct, IConvertible, IComparable
	{
		// Get the enum list
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);

		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any()
			&& !invalidReturns.Contains(skipedVal.First()))
		{
			return skipedVal.First();
		}
		// Other wise return the starting index from the reversed list.
		return enumVals.SkipWhile(e => invalidReturns.Contains(e)).First();
	}

	/// <summary>
	/// Get the previous value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetPreviousEnumVal(setting5) returns 'setting4 value(4)' and not '7 (8-1)'
	/// </summary>
	/// <returns>
	/// The previous enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetPreviousEnumVal<T>(T currentValue) where T : struct, IConvertible, IComparable
	{
		// Get the enum list in reverse
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>().Reverse();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);

		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any())
			return skipedVal.First();

		// Other wise return the starting index from the reversed list.
		return enumVals.First();
	}

	/// <summary>
	/// Get the previous value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetPreviousEnumVal(setting4, setting3) returns 'setting8 value(8)' and not '3 (4-1)'
	/// </summary>
	/// <returns>
	/// The previous enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetPreviousEnumVal<T>(T currentValue, T startValue) where T : struct, IConvertible, IComparable
	{
		// Get the enum list in reverse
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>().Reverse();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);
		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any()
			&& !skipedVal.First().Equals(startValue))
			return skipedVal.First();

		// Other wise return the starting index from the reversed list.
		return enumVals.SkipWhile(e => startValue.Equals(e)).First();
	}

	/// <summary>
	/// Get the previous value of an enum even if they are not 1 apart.
	///		(i.e.) eSetting{ setting1 = 0, setting2 = 1, setting3 = 2,
	///		setting4 = 4, setting5 = 8 };
	///		GetPreviousEnumVal(setting4, setting3) returns 'setting8 value(8)' and not '3 (4-1)'
	/// </summary>
	/// <returns>
	/// The previous enum value.
	/// </returns>
	/// <param name='currentValue'>
	/// Current value.
	/// </param>
	/// <param name='invalidReturns'>
	/// Invalid returns.
	/// </param>
	/// <typeparam name='T'>
	/// The Enum type parameter
	/// </typeparam>
	public static T GetPreviousEnumVal<T>(T currentValue, T[] invalidReturns) where T : struct, IConvertible, IComparable
	{
		// Get the enum list in reverse
		var enumVals = Enum.GetValues(typeof(T)).Cast<T>().Reverse();

		// Get a enumator that does not start with the current value
		var newValArray = enumVals.SkipWhile(e => !e.Equals(currentValue));

		// Skip one past this enumator index
		var skipedVal = newValArray.Skip(1);
		// If the enumator (which should never be null here I think) has any values in it return the first one.
		if(skipedVal != null && skipedVal.Any()
			&& !invalidReturns.Contains(skipedVal.First()))
		{
			return skipedVal.First();
		}
		// Other wise return the starting index from the reversed list.
		return enumVals.SkipWhile(e => invalidReturns.Contains(e)).First();
	}

	public static int GetIndexOfEnum<T>(T currentValue) where T : struct, IConvertible, IComparable
	{
		List<T> enumList = new List<T>(Enum.GetValues(typeof(T)).Cast<T>());

		return enumList.IndexOf(currentValue);
	}

}
