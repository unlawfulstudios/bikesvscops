using System.Collections;
using UnityEngine;

public class SmoothTraverser : MonoBehaviour
{
	#region Public Defines

	public delegate void ArivedAtTargetPositionHandler();


	public delegate void StartTraversingToPositionHandler();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Member

	// Exposed
	public bool keepTracking = false;

	// Editor Exposed

	// Member
	private float m_smoothTime = 0.5f;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public event ArivedAtTargetPositionHandler ArivedAtTargetPositionEvent;
	public event StartTraversingToPositionHandler StartTraversingToPositionEvent;


	public void TraversToPosition(GameObject go, Vector3 targetOffset, bool instant)
	{
		TraversToPosition(go, targetOffset, instant, m_smoothTime);
	}


	public void TraversToPosition(GameObject go, Vector3 targetOffset, bool instant, float smoothTime)
	{
		m_smoothTime = smoothTime;

		StopCoroutine("Traverse");

		if(instant)
		{
			transform.position = go.transform.position + targetOffset;
			CallArivedAtTargetPositionEvent();
		}
		else
		{
			CallStartTraversingToPositionEvent();
			StartCoroutine("Traverse", new object[] { go, targetOffset });
		}
	}


	public void ResetEvents()
	{
		StartTraversingToPositionEvent = null;
		ArivedAtTargetPositionEvent = null;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private IEnumerator Traverse(object[] args)
	{
		GameObject go = args[0] as GameObject;
		Vector3 offset = (Vector3)args[1];

		Vector3 startPos = transform.position;

		float currentTime = 0f;
		while(currentTime != m_smoothTime)
		{
			currentTime = Mathf.Min(currentTime + Time.deltaTime, m_smoothTime);
			float lerp = currentTime / m_smoothTime;
			float smoothLerp = EaseLerp.Ease(lerp, EaseType.InOut);

			transform.position = Vector3.Lerp(startPos, go.transform.position + offset, smoothLerp);

			yield return new WaitForEndOfFrame();
		}

		CallArivedAtTargetPositionEvent();

		while(keepTracking)
		{
			transform.position = go.transform.position + offset;
			yield return new WaitForEndOfFrame();
		}
	}


	private void CallStartTraversingToPositionEvent()
	{
		if(StartTraversingToPositionEvent != null)
		{
			StartTraversingToPositionEvent();
			StartTraversingToPositionEvent = null;
		}
	}


	private void CallArivedAtTargetPositionEvent()
	{
		if(ArivedAtTargetPositionEvent != null)
		{
			ArivedAtTargetPositionEvent();
			ArivedAtTargetPositionEvent = null;
		}
	}

	#endregion
}