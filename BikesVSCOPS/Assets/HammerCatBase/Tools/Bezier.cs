using UnityEngine;

public static class Bezier
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public static Vector3 Lerp(Vector3 startOfCurve, Vector3 endOfCurve, Vector3 bezierCurveControlPoint, float lerp)
	{
		lerp = Mathf.Clamp01(lerp);

		float reverseLerp = 1f - lerp;
		float lerpSqr = lerp * lerp;
		float reverseLerpSqr = reverseLerp * reverseLerp;

		return new Vector3(
			(reverseLerpSqr * startOfCurve.x) +
				(2 * lerp * reverseLerp * bezierCurveControlPoint.x) + (lerpSqr * endOfCurve.x),
			(reverseLerpSqr * startOfCurve.y) +
				(2 * lerp * reverseLerp * bezierCurveControlPoint.y) + (lerpSqr * endOfCurve.y),
			(reverseLerpSqr * startOfCurve.z) +
				(2 * lerp * reverseLerp * bezierCurveControlPoint.z) + (lerpSqr * endOfCurve.z));
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	#endregion
}