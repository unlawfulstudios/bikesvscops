using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiPlayingSound : MonoBehaviour
{
	#region Public Properties

	public AudioSource Source
	{
		get { return this.m_source; }
		set	{ m_source = value;	}
	}


	public Transform Target
	{
		get
		{
			if(this.m_target == null)
			{
				return transform;
			}
			return this.m_target;
		}
		set	{ m_target = value; }
	}


	public float Volume
	{
		get	{ return this.m_volume;	}
		set	{ m_volume = value;	}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void Play()
	{
		AudioSource newSound = null;
		if(SoundPool.Count == 0)
		{
			newSound = DuplicateSound();
		}
		else
		{
			newSound = SoundPool.Dequeue();
		}
		newSound.volume = Volume;
		ActiveSounds.Add(newSound);
		StartCoroutine("PlayThenReturn", new object[] { newSound });
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private Queue<AudioSource> SoundPool
	{
		get
		{
			if(this.m_soundPool == null)
			{
				this.m_soundPool = new Queue<AudioSource>();
			}
			return this.m_soundPool;
		}
	}


	private List<AudioSource> ActiveSounds
	{
		get
		{
			if(this.m_activeSounds == null)
			{
				this.m_activeSounds = new List<AudioSource>();
			}
			return this.m_activeSounds;
		}
	}


	private IEnumerator PlayThenReturn(object[] args)
	{
		AudioSource audioSource = args[0] as AudioSource;

		audioSource.Play();
		while(audioSource.isPlaying)
		{
			yield return new WaitForEndOfFrame();
		}

		SoundPool.Enqueue(audioSource);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	public AudioSource DuplicateSound()
	{
		if(!Application.isPlaying
			|| Source == null)
		{
			return null;
		}

		GameObject localSound = new GameObject(
			Source.name + " (Clone) " + (SoundPool.Count + ActiveSounds.Count).ToString(), new System.Type[] { typeof(AudioSource) });

		localSound.transform.parent = Target;
		AudioSource newLocalAudioSource = localSound.GetComponent<AudioSource>();

		newLocalAudioSource.clip = Source.clip;
		newLocalAudioSource.mute = Source.mute;
		newLocalAudioSource.bypassEffects = Source.bypassEffects;
		newLocalAudioSource.playOnAwake = Source.playOnAwake;
		newLocalAudioSource.loop = Source.loop;
		newLocalAudioSource.priority = Source.priority;
		newLocalAudioSource.volume = Source.volume;
		newLocalAudioSource.pitch = Source.pitch;
		newLocalAudioSource.panStereo = Source.panStereo;

		return newLocalAudioSource;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private AudioSource m_source = null;
	[SerializeField]
	private Transform m_target = null;
	private float m_volume = 1f;
	private Queue<AudioSource> m_soundPool = null;
	private List<AudioSource> m_activeSounds = null;

	#endregion
}