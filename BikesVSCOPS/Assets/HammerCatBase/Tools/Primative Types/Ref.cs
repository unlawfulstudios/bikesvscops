/// <summary>
/// Reference object that can allow member variables of system types.
/// </summary>
public sealed class Ref<T>
{
	#region Public Methods

	public Ref(System.Func<T> getter, System.Action<T> setter)
	{
		this.getter = getter;
		this.setter = setter;
	}

	public T Value { get { return getter(); } set { setter(value); } }

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Memebers

	private readonly System.Func<T> getter;
	private readonly System.Action<T> setter;

	#endregion
}