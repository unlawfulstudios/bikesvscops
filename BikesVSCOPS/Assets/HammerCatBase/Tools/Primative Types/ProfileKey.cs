

public abstract class ProfileKey 
{
	#region Public Properties

	public abstract System.Type MyValueType { get; }

    public bool SetDefault = false;

	public abstract string DefaultValueAsString { get; }
    
    public abstract void Reset();


    public abstract bool IsDefault { get; }


	public string Name
	{
		get	{ return this.m_name; }
		protected set { m_name = value; }
	}

	public System.Type MySystemType
	{
		get	{ return this.m_myType;	}
		protected set { m_myType = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected string HashName
	{
		get	{ return this.m_hashName; }
		set { m_hashName = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private string m_hashName = "";
	private System.Type m_myType;
	private string m_name = "";
    
	#endregion
}