using System;
using UnityEngine;

public class TemporaryActiveGameObject : IDisposable
{
	#region Public Methods

	public TemporaryActiveGameObject(GameObject temporaryActivatedGameObject)
	{
		m_temporaryGO = temporaryActivatedGameObject;
		m_temporaryGO.SetActive(true);
	}

	public void Dispose()
	{
		m_temporaryGO.SetActive(false);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GameObject m_temporaryGO = null;

	#endregion
}
