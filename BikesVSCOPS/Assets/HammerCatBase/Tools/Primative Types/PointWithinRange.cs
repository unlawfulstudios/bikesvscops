using System;
using UnityEngine;

[Serializable]
public class PointWithinRange
{
	public Vector2 range = Vector2.zero;
	public float point = 0f;
}
