using System;
using UnityEngine;

[Serializable]
public class AxisToggle
{
	public bool x = true;
	public bool y = false;
}
