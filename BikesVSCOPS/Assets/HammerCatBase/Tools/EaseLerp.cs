using UnityEngine;

public enum EaseType
{
	None,
	In,
	Out,
	InOut
}


public static class EaseLerp
{
	#region Public Static Methods

	public static float Ease(float dt, EaseType easeType)
	{
		if(easeType == EaseType.None)
		{
			return dt;
		}
		else if(easeType == EaseType.In)
		{
			return Mathf.Lerp(0f, 1f, 1f - Mathf.Cos(dt * Mathf.PI * 0.5f));
		}
		else if(easeType == EaseType.Out)
		{
			return Mathf.Lerp(0f, 1f, Mathf.Sin(dt * Mathf.PI * 0.5f));
		}
		else
		{
			return Mathf.SmoothStep(0f, 1f, dt);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	#endregion
}