using UnityEngine;

namespace HammerCat
{
	/// <summary>
	/// Wrapper class for logging.
	/// </summary>
	public static partial class Diagnostics
	{
#if UNITY_EDITOR
		#region Public Methods

		public static void DrawLine(Vector3 start, Vector3 end, Color color, Object context)
		{
			if(IsContextRegistrationAllowed(LogType.Log, context))
			{
				Debug.DrawLine(start, end, color);
			}
		}


		public static void DrawRay(Vector3 start, Vector3 direction, Color color, Object context)
		{
			if(IsContextRegistrationAllowed(LogType.Log, context))
			{
				Debug.DrawRay(start, direction, color);
			}
		}


		public static void DrawRay(Vector3 start, Vector3 direction, Color color, float duration, Object context)
		{
			if(IsContextRegistrationAllowed(LogType.Log, context))
			{
				Debug.DrawRay(start, direction, color, duration);
			}
		}

		#endregion
#else
		public static void DrawLine(Vector3 start, Vector3 end, Color color, Object context)
		{
		}

		public static void DrawRay(Vector3 start, Vector3 direction, Color color, Object context)
		{
		}

		public static void DrawRay(Vector3 start, Vector3 direction, Color color, float duration, Object context)
		{
		}
#endif
	}
}