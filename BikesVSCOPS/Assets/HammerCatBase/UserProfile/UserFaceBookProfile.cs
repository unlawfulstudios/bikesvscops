﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserFaceBookProfile : UserProfileObject {

    public override string Name
    {
        get
        {
            return "User FaceBook Profile INFO";
        }
    }

    public UserProfileKey<int, bool> Connected_ToFaceBook = new UserProfileKey<int, bool>("Connected_ToFaceBook ", false);

}
