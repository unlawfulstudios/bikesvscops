using UnityEngine;
using HammerCat;
using Zenject;



public abstract class UserProfileObject
{
    #region Public Properties

    public abstract string Name { get; }

    #endregion

    public bool DataSynced = false;
    protected virtual bool m_DataSynced() { return false; }


   public BikeInventoryItemList bikeInventoryItemList { get { return Resources.FindObjectsOfTypeAll<BikeInventoryItemList>()[0]; } }
   public City_Data_Manager City_Data_Manager { get { return Resources.FindObjectsOfTypeAll<City_Data_Manager>()[0]; } }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods


    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    #endregion


    public virtual void SyncData()
    {
        DataSynced = m_DataSynced();
    }
    public virtual void Reset()
    {
        DataSynced = m_DataSynced();
    }



   
}