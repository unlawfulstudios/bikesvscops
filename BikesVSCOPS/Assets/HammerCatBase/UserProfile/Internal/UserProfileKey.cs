using System;
using UnityEngine;



[System.Serializable]
public class DataObject : UserProfileObject
{
    public override string Name
    {
        get
        {
            return valueName;
        }
    }
    public string valueName = "NULL";

    public string Value = "NULL";

  
}
[System.Serializable]
public class StringData : DataObject
{
    public override string Name
    {
        get
        {
            return valueName;
        }
    }
    UserProfileKey<string, string> stringKey = new UserProfileKey<string, string>("Default","default");
    public StringData(string Name, string Data)
    {
        stringKey = new UserProfileKey<string, string>(Name, Data);
        Value = value.ToString();
        Value = Name;
    }
    public string value {
        get
        {
            return stringKey.Value;
        }
        set {
            stringKey.Value = value;
            Value = value.ToString();
        } }


    public override void SyncData()
    {
        stringKey.Value = Value;
        base.SyncData();
    }

    protected override bool m_DataSynced()
    {
        return stringKey.Value == Value;
    }

    public override void Reset()
    {
        stringKey.Value = stringKey.DeafultValue;
        value = stringKey.DeafultValue;
    }

}
[System.Serializable]
public class BoolData : DataObject
{
    public override string Name
    {
        get
        {
            return valueName;
        }
    }

    UserProfileKey<int, bool> boolKey = new UserProfileKey<int, bool>("Default", false);

    public BoolData(string Name, bool Data)
    {
        boolKey = new UserProfileKey<int, bool>(Name, Data);
        Value = value.ToString();
        valueName = Name;
    }

    public bool value
    {
        get { return boolKey.Value; }
        set
        {
            boolKey.Value = value;
            Value = value.ToString();
        }
    }


    public override void SyncData()
    {
        bool bv = Value == "True" ? true : false;
        boolKey.Value = bv;
        base.SyncData();
    }

    protected override bool m_DataSynced()
    {
        return boolKey.Value == value;
    }

    public override void Reset()
    {
        boolKey.Value = boolKey.DeafultValue;
        value = boolKey.DeafultValue;
    }
}
[System.Serializable]
public class IntData : DataObject
{

    UserProfileKey<int, int> intKey  = new UserProfileKey<int, int>("Default", 0);

    public IntData(string Name, int Data)
    {
        intKey = new UserProfileKey<int, int>(Name, Data);
        Value = value.ToString();
        valueName = Name;
    }

    public int value
    {
        get { return intKey.Value; }
        set
        {
            intKey.Value = value;
            Value  = value.ToString();
        }
    }

    public override void SyncData()
    {
        intKey.Value = Int32.Parse(Value);
        base.SyncData();
    }

    protected override bool m_DataSynced()
    {
        return intKey.Value == Int32.Parse(Value);
    }

    public override void Reset()
    {
        intKey.Value = intKey.DeafultValue;
        value = intKey.DeafultValue;
    }
}
[System.Serializable]
public class FloatData : DataObject
{
    public override string Name
    { 
        get
        {
            return valueName;
        }
    }
    UserProfileKey<float, float> floatKey  = new UserProfileKey<float, float>("Default", 0); 

    public FloatData(string Name, float Data)
    {
        floatKey = new UserProfileKey<float, float>(Name, Data);
        Value = value.ToString();
        valueName = Name;
    }


    public float value
    {
        get { return floatKey.Value; }
        set
        {
            floatKey.Value = value;
            Value = value.ToString();
        }
    }


    public override void SyncData()
    {
        floatKey.Value = float.Parse(Value);
        base.SyncData();
    }

    protected override bool m_DataSynced()
    {
        return floatKey.Value == float.Parse(Value);
    }

    public override void Reset()
    {
        floatKey.Value = floatKey.DeafultValue;
        value = floatKey.DeafultValue;
    }
}

public class UserProfileKey<T, castT> : ProfileKey where T : IConvertible where castT : IConvertible
{
    #region Public Properties
    public castT Value
    {
        get { return this.m_value; }
        set
        {
            m_value = (castT)value;
            if (MySystemType == typeof(int))
            {
                if (typeof(castT) == typeof(bool))
                {
                    PlayerPrefs.SetInt(HashName, ((bool)(object)m_value == false) ? 0 : 1);
                }
                else
                {
                    PlayerPrefs.SetInt(HashName, (int)(object)m_value);
                }
            }
            else if (MySystemType == typeof(float))
            {
                PlayerPrefs.SetFloat(HashName, (float)(object)m_value);
            }
            else if (MySystemType == typeof(string))
            {
                PlayerPrefs.SetString(HashName, (string)(object)m_value);
            }
            else
            {
                Debug.LogError("Could not store/retrive this type of user profile key. It can only be int, float, or string.");
            }
        }
    }


    public override System.Type MyValueType
    {
        get { return typeof(castT); }
    }


    public castT DeafultValue
    {
        get { return this.m_defaultValue; }
    }


    public string CurrentValueAsString
    {
        get { return this.m_value.ToString(); }
    }

    public override string DefaultValueAsString
    {
        get { return this.m_defaultValue.ToString(); }

    }


    public override bool IsDefault
    {
        get { return m_value.ToString() == this.m_defaultValue.ToString(); }

    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Public Methods

    public UserProfileKey(string name, castT defaultValue)
    {
        MySystemType = typeof(T);
        m_defaultValue = defaultValue;

        Name = name;
        HashName = m_keyPrefix + Name;

        if (PlayerPrefs.HasKey(HashName))
        {
            if (MySystemType == typeof(int))
            {
                if (typeof(castT) == typeof(bool))
                {
                    m_value = (castT)(object)((int)(object)PlayerPrefs.GetInt(HashName) == 0 ? false : true);
                }
                else
                {
                    m_value = (castT)(object)PlayerPrefs.GetInt(HashName);
                }
            }
            else if (MySystemType == typeof(float))
            {
                m_value = (castT)(object)PlayerPrefs.GetFloat(HashName);
            }
            else if (MySystemType == typeof(string))
            {
                m_value = (castT)(object)PlayerPrefs.GetString(HashName);
            }
            else
            {
                Debug.LogError("Could not store/retrive this type of user profile key. It can only be int, float, or string.");
            }
        }
        else
        {
            Value = m_defaultValue;
        }
    }


    public override void Reset()
    {
        Value = m_defaultValue;
    }

    #endregion
    public string TESTT = "TEST";
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private static readonly string m_keyPrefix = "BiksVsCops_playerPref_";
    private castT m_value;
    private castT m_defaultValue;

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private readonly System.Func<T> getter;
    private readonly System.Action<T> setter;

    #endregion
}
