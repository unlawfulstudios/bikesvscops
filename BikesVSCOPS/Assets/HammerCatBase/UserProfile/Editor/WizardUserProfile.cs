using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Zenject;


public class WizardUserProfile : BaseWizard
{
    #region Protected Global Methods

 
    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Custom Public Methods



    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods
    Vector3 scrollPosition;
    private string Name = "NULL";
    private void OnEnable()
    {

        ShowProfile = new EditorProfileKey<int, bool>("ShowProfile", true);


    }


  
    //private void OnGUI()
    //{
    //    scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(Screen.height), GUILayout.Height(Screen.width));
    //    DisplayUserProfileObject(UserProfile.Instance, ShowProfile);
    //    GUILayout.EndScrollView();
    //}
    protected override void DisplayFields()
    {
        //return;
        DisplayUserProfileObject(UserProfile, ShowProfile);


    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods


    private void DisplayUserProfileObject(System.Object profileType, EditorProfileKey<int, bool> toggle)
    {

        System.Type userProfileType = profileType.GetType();
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            bool resetAll = false;
            using (new GUIGroupAlignment(GroupAlignment.Horizontal))
            {

                    if (!ShowToggleField(toggle, StringTools.BuildReadableString(userProfileType.Name)))
                    {
                        return;
                    }

                    if (GUILayout.Button("Reset All", GUILayout.MaxWidth(64f)))
                    {
                        resetAll = true;
                    }
                }
            
            EditorGUILayout.Space();

            FieldInfo[] fieldInfo = GetFieldInfoArray(userProfileType);

            for (int memberIt = 0; memberIt < fieldInfo.Length; ++memberIt)
            {
                ShowField(fieldInfo[memberIt], profileType, resetAll);
            }

        }
    }


    private bool ShowFloatField(FieldInfo fieldInfo, object containingClass, ref string defulaultColor)
    {
        ProfileKey profileKey = null;
        string fieldName = fieldInfo.Name;
        bool displayedValue = false;

        defulaultColor = "NaN";

        if (fieldInfo.FieldType.BaseType == typeof(ProfileKey))
        {
            profileKey = (ProfileKey)GetFieldValue<object>(containingClass, fieldName);

            //Type sysType = profileKey.MySystemType;
            Type valueType = profileKey.MyValueType;
            //System.TypeCode typeCode = System.Type.GetTypeCode(sysType);

            if (valueType == typeof(float))
            {
                ((UserProfileKey<float, float>)profileKey).Value = BaseEditor.FloatField(fieldName, ((UserProfileKey<float, float>)profileKey).Value);
                displayedValue = true;
                defulaultColor = profileKey.DefaultValueAsString;
            }
        }

        if (displayedValue
            && !profileKey.IsDefault)
        {
            return true;
        }

        return false;
    }


    protected static T GetFieldValue<T>(object obj, string fieldName)
    {
        if (obj == null)
        {
            throw new ArgumentNullException("obj");
        }

        var field = obj.GetType().GetField(fieldName,
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        if (field == null)
        {
            throw new ArgumentException("fieldName", "No such field was found.");
        }

        if (!typeof(T).IsAssignableFrom(field.FieldType))
        {
            throw new InvalidOperationException("Field type and requested type are not compatible.");
        }

        return (T)field.GetValue(obj);
    }


    private FieldInfo[] GetFieldInfoArray(System.Type userProfileType)
    {
        return userProfileType.GetFields(
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
    }


    protected void ShowField(FieldInfo fieldInfo, object containingClass, bool resetAll)
    {
        ProfileKey profileKey = null;
        string fieldName = fieldInfo.Name;
        if (fieldInfo.FieldType.BaseType == typeof(ProfileKey))
        {
            profileKey = (ProfileKey)GetFieldValue<object>(containingClass, fieldName);

            if (resetAll)
            {
                profileKey.Reset();
            }

            Type sysType = profileKey.MySystemType;
            Type valueType = profileKey.MyValueType;
            System.TypeCode valueTypeCode = System.Type.GetTypeCode(valueType);
            System.TypeCode typeCode = System.Type.GetTypeCode(sysType);

            bool displayedValue = false;

            using (new GUIGroupAlignment(GroupAlignment.Horizontal))
            {
                using (new GUIGroupAlignment(GroupAlignment.Horizontal))
                {
                    switch (typeCode)
                    {
                        case System.TypeCode.Int32:
                            {
                                if (valueType.IsEnum)
                                {
                                    if (valueType == typeof(KeyCode))
                                    {
                                        ((UserProfileKey<int, KeyCode>)profileKey).Value =
                                            (KeyCode)BaseEditor.EnumPopup(fieldName, ((UserProfileKey<int, KeyCode>)profileKey).Value);
                                        displayedValue = true;
                                    }
                                    else if (DisplayCustomEnumType(valueType, profileKey, fieldName, ref displayedValue))
                                    {
                                    }
                                    else
                                    {
                                        Debug.LogWarning("Please implement a enum profile key for the enum \"" + valueType + "\"");
                                    }
                                }
                                else if (valueTypeCode == System.TypeCode.Boolean)
                                {
                                    ((UserProfileKey<int, bool>)profileKey).Value = BaseEditor.Toggle(fieldName, ((UserProfileKey<int, bool>)profileKey).Value);
                                }
                                else
                                {
                                    ((UserProfileKey<int, int>)profileKey).Value = BaseEditor.IntField(fieldName, ((UserProfileKey<int, int>)profileKey).Value);
                                    displayedValue = true;
                                }
                                break;
                            }
                        case System.TypeCode.Single:
                            {
                                if (valueType == typeof(float))
                                {
                                    ((UserProfileKey<float, float>)profileKey).Value = BaseEditor.FloatField(fieldName, ((UserProfileKey<float, float>)profileKey).Value);
                                    displayedValue = true;
                                }
                                else
                                {
                                    Debug.LogWarning("Please implement a profile key for the float type \"" + valueType + "\"");
                                }
                                break;
                            }
                        case System.TypeCode.Double:
                            {
                                if (valueType == typeof(float))
                                {
                                    ((UserProfileKey<float, float>)profileKey).Value = BaseEditor.FloatField(fieldName, ((UserProfileKey<float, float>)profileKey).Value);
                                    displayedValue = true;
                                }
                                else
                                {
                                    Debug.LogWarning("Please implement a profile key for the float type \"" + valueType + "\"");
                                }
                                break;
                            }
                        case System.TypeCode.Boolean:
                            {
                                if (valueType == typeof(bool))
                                {
                                    ((UserProfileKey<bool, bool>)profileKey).Value = BaseEditor.Toggle(fieldName, ((UserProfileKey<bool, bool>)profileKey).Value);
                                    displayedValue = true;
                                }
                                else
                                {
                                    Debug.LogWarning("Please implement a profile key for the bool type \"" + valueType + "\"");
                                }
                                break;
                            }
                        case System.TypeCode.String:
                            {
                                if (valueType == typeof(string))
                                {

                                    if (!DisplaySpecialStringType(containingClass, profileKey, fieldName))
                                    {
                                        ((UserProfileKey<string, string>)profileKey).Value = BaseEditor.TextField(fieldName, ((UserProfileKey<string, string>)profileKey).Value);
                                    }
                                    displayedValue = true;
                                }
                                else
                                {
                                    Debug.LogWarning("Please implement a profile key for the string type \"" + valueType + "\"");
                                }
                                break;
                            }
                        default:
                            {
                                Debug.LogWarning("Please implement a profile key for the type \"" + valueType + "\"");
                                break;
                            }
                    }
                }

                //GUILayout.FlexibleSpace();


                if (displayedValue)
                {
                    using (new GUIGroupAlignment(GroupAlignment.Horizontal))
                    {
                        GUILayout.Space(32f);
                        using (new GUIColorChange(Color.white, DefaultNormalColor, profileKey.IsDefault))
                        {
                            using (new GUIColorChange(Color.white, DefaultActiveColor, !profileKey.IsDefault))
                            {
                                EditorGUILayout.LabelField("Default:" + profileKey.DefaultValueAsString);//, GUILayout.MaxWidth(42f));
                            }
                        }
                        //GUILayout.FlexibleSpace();

                        using (new GUIColorChange(ResetColor, !profileKey.IsDefault))
                        {
                            if (GUILayout.Button(!profileKey.IsDefault ? "Reset" : "", GUILayout.MaxWidth(42f)) && !profileKey.IsDefault)
                            {
                                profileKey.Reset();
                            }
                        }


                    }
                }
            }
        }
        else
        {
            if (fieldInfo.FieldType.BaseType == typeof(UserProfileObject))
            {
                //Debug.Log(fieldInfo);

                object objs = null;
                try
                {
                    objs = (object)GetFieldValue<object>(containingClass, fieldName);
                }
                catch
                {
                }

                if (objs != null && objs is UserProfileObject)
                {
                    UserProfileObject profileObj = (UserProfileObject)objs;
                    string profileName = profileObj.Name;

                    EditorProfileKey<int, bool> ShowProfileObj = new EditorProfileKey<int, bool>("ShowProfileObj" + profileName, false);
                    DisplayUserProfileObject(
                                    profileObj,
                                    ShowProfileObj);
                }
            }
            else if (fieldInfo.FieldType.BaseType == typeof(DataObject))
            {
                //Debug.Log(fieldInfo);

                object objs = null;
                try
                {
                    objs = (object)GetFieldValue<object>(containingClass, fieldName);
                }
                catch
                {
                }

                if (objs != null && objs is DataObject)
                {
                    DataObject profileObj = (DataObject)objs;
                    string profileName = profileObj.Name;

                    EditorProfileKey<int, bool> ShowProfileDatAObj = new EditorProfileKey<int, bool>("ShowProfileDatAObj" + profileName, false);
                    DisplayUserProfileObject(
                                    profileObj,
                                    ShowProfileDatAObj);
                }
            }
            else if (fieldInfo.FieldType.BaseType == typeof(System.Array))
            {
                object[] arrayObjs = null;
                try
                {
                    arrayObjs = (object[])GetFieldValue<object>(containingClass, fieldName);
                }
                catch
                {
                }


                if (arrayObjs != null && arrayObjs is UserProfileObject[])
                {
                    UserProfileObject[] profileArray = (UserProfileObject[])arrayObjs;
                    if (profileArray != null && profileArray.Length > 0)
                    {
                        //Debug.Log(fieldInfo.Name + " : " + fieldInfo.FieldType.BaseType + " ~ " + profileArray.Length);
                        string name = profileArray[0].Name;

                        EditorProfileKey<int, bool> ShowArrayField = new EditorProfileKey<int, bool>("ShowArrayField" + name, false);
                        // Robots
                        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
                        {
                            if (ShowToggleField(ShowArrayField, name))
                            {
                                for (int arrayIndex = 0; arrayIndex < profileArray.Length; ++arrayIndex)
                                {
                                    EditorProfileKey<int, bool> ShowArrayIndex = new EditorProfileKey<int, bool>("ShowArrayIndex" + name + "-" + arrayIndex, false);
                                    DisplayUserProfileObject(
                                        profileArray[arrayIndex],
                                        ShowArrayIndex);
                                }
                            }
                        }

                    }
                }



            }
            else
            {
                if (fieldInfo.Name == "Purchases")
                {
                    int x = 0;
                    ++x;
                }
                Type[] itemTypes = fieldInfo.FieldType.GetGenericArguments();
                if (itemTypes != null
                    && itemTypes.Length > 0
                    && itemTypes[0].BaseType == typeof(UserProfileObject))
                {

                    string name = fieldInfo.Name;
                    EditorProfileKey<int, bool> ShowListField = new EditorProfileKey<int, bool>("ShowListField" + name, false);

                    using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
                    {
                        if (ShowToggleField(ShowListField, name))
                        {
                            var test = GetFieldValue<object>(containingClass, fieldName);
                            System.Type testType = typeof(UserProfileObject);
                            var castItems = CastMethod.MakeGenericMethod(new Type[] { testType })
                                  .Invoke(null, new object[] { test });
                            var list = ToListMethod.MakeGenericMethod(new Type[] { testType })
                                  .Invoke(null, new object[] { castItems });

                            //							List<UserProfileKey<string, string>> listObjects = (List<UserProfileKey<string, string>>)list;
                            //							UserProfileKey<string, string>[] listArray = listObjects.ToArray();
                            List<UserProfileObject> listObjects = (List<UserProfileObject>)list;//GetFieldValue<object>(containingClass, fieldName);
                            for (int listIndex = 0; listIndex < listObjects.Count; ++listIndex)
                            {
                                //EditorProfileKey<int, bool> ShowListIndex =
                                //	new EditorProfileKey<int, bool>("ShowListIndex" + name + "-" + listIndex, true);

                                //itemTypes[listIndex]
                                DisplayUserProfileObject(listObjects[listIndex], ShowListField);
                                //ShowField(listObjects[listIndex], testType);
                            }
                        }
                    }
                    /*
					object objList = null;
					try
					{
						objList = (object)GetFieldValue<object>(containingClass, fieldName);
					}
					catch
					{
					}

					if(objList != null)
					{
						System.Type listType = objList.GetType().GetProperty("Item").PropertyType;
						Debug.Log(listType.BaseType);
						if(listType.BaseType == typeof(ProfileKey))
						{

						}
					}*/
                }

            }
        }
    }


    private void DisplayUserProfileListObject(System.Object profileType, EditorProfileKey<int, bool> toggle)
    {
        System.Type userProfileType = profileType.GetType();
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            bool resetAll = false;

            using (new GUIGroupAlignment(GroupAlignment.Horizontal))
            {
                if (!ShowToggleField(toggle, StringTools.BuildReadableString(userProfileType.ToString())))
                {
                    return;
                }

                if (GUILayout.Button("Reset All", GUILayout.MaxWidth(64f)))
                {
                    resetAll = true;
                }
            }

            EditorGUILayout.Space();

            FieldInfo[] fieldInfo = GetFieldInfoArray(userProfileType);

            for (int memberIt = 0; memberIt < fieldInfo.Length; ++memberIt)
            {
                ShowField(fieldInfo[memberIt], profileType, resetAll);
            }

        }
    }


    static readonly MethodInfo CastMethod = typeof(Enumerable).GetMethod("Cast");
    static readonly MethodInfo ToListMethod = typeof(Enumerable).GetMethod("ToList");

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private Color DefaultActiveColor
    {
        get { return this.m_defaultActiveColor; }
    }


    private Color DefaultNormalColor
    {
        get { return this.m_defaultNormalColor; }
    }


    private Color ResetColor
    {
        get { return this.m_resetColor; }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private EditorProfileKey<int, bool> ShowProfile;
    private readonly Color m_defaultNormalColor = new Color(1f, 1f, 1f, 0.8f);
    private readonly Color m_defaultActiveColor = new Color(0.65f, 1f, 0.65f, 1f);
    private readonly Color m_resetColor = new Color(0.75f, 0.75f, 0f, 1f);



    public static void SetAll()
    {

    }
    #endregion
}