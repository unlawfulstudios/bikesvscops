using HammerCat;
using System;
using UnityEditor;
using UnityEngine;
using Zenject;


public class EditorMangers
{
    
    public UserProfile UserProfile
    {
        get
        {
            if (m_UserProfile == null)
            {
                m_UserProfile = UserProfile.Instnace;
                return m_UserProfile;
            }
            else
            {
                return m_UserProfile;
            }
        }
            
           
    }

    [Inject] public BikeInventoryItemList BikeInventoryItemList;
    [Inject] public City_Data_Manager City_Data_Manager;
    private UserProfile m_UserProfile = null;
}

public class BaseWizard : EditorMangers
{
    #region Public Profile Defines
   
 
    public virtual bool DisplayCustomEnumType(Type valueType, ProfileKey profileKey, string fieldName, ref bool displayedValue)
	{
		return false;
	}


	public virtual bool DisplaySpecialStringType(object containingClass, ProfileKey profileKey, string fieldName)
	{
		return false;
	}

    #endregion


    public void Initialize()
    {
        Diagnostics.RegisterContextForLogging(this, LogType.Warning);
        Init();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Defines

    protected delegate void ShowHeaderDelegate();


	protected delegate void ShowFooterDelegate();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected ShowHeaderDelegate HeaderFunction
	{
		get	{ return this.m_headerFunction;	}
		set
		{
			m_headerFunction = null;
			m_headerFunction = value;
		}
	}


	protected ShowFooterDelegate FooterFunction
	{
		get	{ return this.m_footerFunction;	}
		set
		{
			m_footerFunction = null;
			m_footerFunction = value;
		}
	}

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods

    protected virtual void DisplayFields() { }
    protected virtual void Init() { }

	protected static bool ShowToggleField(EditorProfileKey<int, bool> toggle, string headerName)
	{
		return EditorAssistant.ShowToggleField(toggle, headerName);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	public Ref<Vector2> ScrollRef
	{
		get
		{
			if(this.m_scrollRef == null)
			{
				this.m_scrollRef = new Ref<Vector2>(() => m_scrollPosition, value => {
					m_scrollPosition = value;});

			}
			return this.m_scrollRef;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Awake()
	{
		
	}


    public void GuiRender()
    {
        if (m_headerFunction != null)
		{
			m_headerFunction();
		}

		using(new GUIGroupAlignment(GroupAlignment.Vertical))
		{
			using(new GUIGroupAlignment(GroupAlignment.Scroll, ScrollRef))
			{
				DisplayFields();
			}
		}

		if(m_footerFunction != null)
		{
			m_footerFunction();
		}
	}


    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Memebers

    private ShowHeaderDelegate m_headerFunction = null;
	private ShowFooterDelegate m_footerFunction = null;
	private Vector2 m_scrollPosition = Vector2.zero;
	private Ref<Vector2> m_scrollRef = null;

	#endregion
}