using UnityEngine;
using UnityEditor;

public class MenuGetInspectorSkin
{
//	[MenuItem("Hammer Cat/Editor/~", false, 1)]
//	public static void space()
//	{
//	}

	[MenuItem("Hammer Cat/Editor/Tools/Get Inspector GUI", false, 200)]
	public static void getGUI()
	{
		GUISkin gui = Object.Instantiate(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector)) as GUISkin;
		AssetDatabase.CreateAsset(gui, "Assets/Editor Default Resources/My Editor/DefaultInspectorGUI.guiskin");
	}
}