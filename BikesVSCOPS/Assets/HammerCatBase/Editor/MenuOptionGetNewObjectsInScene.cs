using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuOptionGetNewObjectsInScene : EditorWindow
{
    #region Public Static Access

    [MenuItem("Hammer Cat/Editor/Tools/New Objects", false, 501)]
    public static void Launch()
    {
        MenuOptionGetNewObjectsInScene window = GetWindow<MenuOptionGetNewObjectsInScene>();

        if (window != null)
        {
            window.Focus();
        }
        else
        {
            window = CreateInstance<MenuOptionGetNewObjectsInScene>();
            window.Show();
            window.Focus();
        }
        window.name = "MenuOptionGetNewObjectsInScene";
        GUIContent title = new GUIContent("New Objects");
        window.titleContent = title;
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Member

    // Exposed

    // Editor Exposed

    // Member
    private static readonly Color COLOR_NEW_OBJECT_BG = new Color(1f, 0f, 0f, 1f);
    private static readonly Color COLOR_NEW_OBJECT_FG = new Color(1f, 1f, 1f, 1f);
    private static readonly Color COLOR_COUNT_CHANGED_BG = new Color(0f, 1f, 1f, 1f);
    private static readonly Color COLOR_COUNT_CHANGED_FG = new Color(1f, 1f, 1f, 1f);
    private List<TypeAndCount> m_objectsInSceneLastTime = new List<TypeAndCount>();
    private List<TypeAndCount> m_objectsInSceneCurrent = new List<TypeAndCount>();
    private EditorProfileKey<int, bool> m_showLastObjects;
    private EditorProfileKey<int, bool> m_showDifferentCurrentObjects;
    private Ref<Vector2> m_scrollRef = null;
    private Vector2 m_scrollPosition = Vector2.zero;

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Public Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Public Methods

    public void OnGUI()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            ShowHeader();
            ShowControls();

            ShowLastObjects();
            ShowDifferentCurrentObjects();

            GUILayout.FlexibleSpace();
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Defines

    private class TypeAndCount
    {
        public readonly System.Type type;
        public int count;


        public TypeAndCount(System.Type type)
        {
            this.type = type;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Serialized Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private Ref<Vector2> scrollRef
    {
        get
        {
            if (m_scrollRef == null)
            {
                m_scrollRef = new Ref<Vector2>(() => m_scrollPosition, value =>
                {
                    m_scrollPosition = value;
                });

            }
            return m_scrollRef;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    private void OnEnable()
    {


        m_showLastObjects = new EditorProfileKey<int, bool>("m_showLastObjects", false);
        m_showDifferentCurrentObjects = new EditorProfileKey<int, bool>("m_showDifferentCurrentObjects", false);
    }


    private void ShowHeader()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            using (new GUIGroupAlignment(GroupAlignment.Horizontal))
            {
                BaseEditor.ConstIntField("Last Object Count", m_objectsInSceneLastTime.Count);
                BaseEditor.ConstIntField("Current Object Count", m_objectsInSceneCurrent.Count);
                GUILayout.FlexibleSpace();
            }
        }
    }


    private void ShowControls()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            using (new GUIGroupAlignment(GroupAlignment.Horizontal))
            {
                if (GUILayout.Button("Get Current Objects"))
                {
                    GetCurrentObjects();
                }
                GUILayout.FlexibleSpace();
            }
        }
    }


    private void GetCurrentObjects()
    {
        m_objectsInSceneLastTime.Clear();
        m_objectsInSceneLastTime.AddRange(m_objectsInSceneCurrent.ToArray());
        m_objectsInSceneCurrent.Clear();

        Object[] objectsInScene = Resources.FindObjectsOfTypeAll(typeof(Object));
        foreach (Object objectInScene in objectsInScene)
        {
            System.Type type = objectInScene.GetType();
            TypeAndCount typeAndCount = m_objectsInSceneCurrent.Find(objAndTypeIt => objAndTypeIt.type == type);
            if (typeAndCount == null)
            {
                typeAndCount = new TypeAndCount(type);
                m_objectsInSceneCurrent.Add(typeAndCount);
            }
            ++typeAndCount.count;
        }

        m_objectsInSceneCurrent.Sort((a, b) => a.type.ToString().CompareTo(b.type.ToString()));
    }


    private void ShowLastObjects()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            if (ShowLastObjectsHeader())
            {
                using (new GUIGroupAlignment(GroupAlignment.Scroll, scrollRef))
                {
                    foreach (TypeAndCount typeAndCount in m_objectsInSceneLastTime)
                    {
                        ShowTypeAndCount(typeAndCount);
                    }
                }
            }
        }
    }


    private bool ShowLastObjectsHeader()
    {
        return EditorAssistant.ShowToggleField(m_showLastObjects, "Last Objects In Scene");
    }


    private void ShowTypeAndCount(TypeAndCount typeAndCount)
    {
        using (new GUIGroupAlignment(GroupAlignment.Horizontal, GUI.skin.box))
        {
            BaseEditor.ConstIntField(typeAndCount.type.ToString(), typeAndCount.count);
        }
    }


    private void ShowDifferentCurrentObjects()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, GUI.skin.box))
        {
            if (ShowCurrentObjectsHeader())
            {
                using (new GUIGroupAlignment(GroupAlignment.Scroll, scrollRef))
                {
                    foreach (TypeAndCount typeAndCount in m_objectsInSceneCurrent)
                    {
                        ShowTypeAndCountDifferent(typeAndCount);
                    }
                }
            }
        }
    }


    private bool ShowCurrentObjectsHeader()
    {
        return EditorAssistant.ShowToggleField(m_showDifferentCurrentObjects, "Different Current Objects In Scene");
    }


    private void ShowTypeAndCountDifferent(TypeAndCount typeAndCount)
    {
        TypeAndCount prevTypeAndCount = m_objectsInSceneLastTime.Find(objIt => objIt.type == typeAndCount.type);

        bool newObject = prevTypeAndCount == null;
        int difference = typeAndCount.count;

        if (!newObject)
        {
            difference -= prevTypeAndCount.count;
        }
        bool onlyCountChanged = !newObject && difference != 0;

        if (!newObject && !onlyCountChanged)
        {
            return;
        }

        Color bg = newObject ? COLOR_NEW_OBJECT_BG : COLOR_COUNT_CHANGED_BG;
        Color fg = newObject ? COLOR_NEW_OBJECT_FG : COLOR_COUNT_CHANGED_FG;

        using (new GUIColorChange(bg, fg))
        {
            using (new GUIGroupAlignment(GroupAlignment.Horizontal, GUI.skin.box))
            {
                BaseEditor.ConstIntField(typeAndCount.type.ToString(), difference);

                GUILayout.FlexibleSpace();

                BaseEditor.Label(newObject ? "*NEW*" : "*CHANGED*");
            }
        }
    }

    #endregion
}