using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public partial class BaseEditor : Editor
{
	#region Public Properties

	public static bool ForceRepaint
	{
		get	{ return m_forceRepaint; }
		set	{ m_forceRepaint = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	#region Public Methods

	public override void OnInspectorGUI()
	{
		SerilizedTarget.Update();

		Update();
		
		if(ShowDefaultGUI)
		{
			EditorGUIUtility.LookLikeInspector();
			base.OnInspectorGUI();
			return;
		}
		GUI.skin = Skin;

		using(new GUIGroupAlignment(GroupAlignment.Vertical, GUILayout.Height(WindowHeight)))
		{
			//ScrollPosition = EditorGUILayout.BeginScrollView(ScrollPosition);
			{
				using(new GUIGroupAlignment(GroupAlignment.Vertical))
				{
					GUILayout.Space(HeaderHeight);
					ScrollPosition = new Vector2(ScrollPosition.x, ScrollPosition.y);

					DisplayFields();
				}

				float lastHeight = GUILayoutUtility.GetLastRect().height;
				//if(lastHeight > 5)
				{
					WindowHeight = lastHeight;
					if(Event.current.type == EventType.Repaint &&
						(WindowHeight != ScrollHeight || ForceRepaint))
					{
						ForceRepaint = false;
						ScrollHeight = WindowHeight;
						Repaint();
					}
				}
			}
			//EditorGUILayout.EndScrollView();
		}

		//Repaint();
		SerilizedTarget.ApplyModifiedProperties();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Accessors

	protected bool ShowDefaultGUI
	{
		get	{ return this.m_showDefaultGUI; }
		set	{ m_showDefaultGUI = value;	}
	}


	protected Object Target
	{
		get
		{
			if(this.m_target == null)
			{
				this.m_target = target;
			}
			return this.m_target;
		}
	}

	protected SerializedObject SerilizedTarget
	{
		get
		{
			if(this.m_serilizedTarget == null)
			{
				this.m_serilizedTarget = serializedObject;// new SerializedObject(Target);
			}
			return this.m_serilizedTarget;
		}
	}

	protected GUISkin Skin
	{
		get { return this.m_skin; }
		set { m_skin = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected virtual void Update()
	{

	}

	protected virtual void DisplayFields()
	{
	}


	protected bool ShowToggleField(EditorProfileKey<int, bool> toggle, string headerName)
	{
		return EditorAssistant.ShowToggleField(toggle, headerName);
	}


	protected static bool RunFunctionOnEnumChange<T>(T inputValue, System.Action<T> outputValue, string fieldLabel) where T : struct, System.IConvertible
	{
		System.Type enumType = typeof(T);
		System.Enum enumValue = (System.Enum)System.Enum.ToObject(enumType, inputValue);

		System.Enum newType = (System.Enum)System.Enum.ToObject(enumType, EditorGUILayout.EnumPopup(
			fieldLabel, enumValue));

		if(!enumValue.Equals(newType))
		{
			outputValue((T)(object)newType);
			return true;
		}

		return false;
	}

	protected SerializedProperty FindProperty(string propertyPath)
	{
		return SerilizedTarget.FindProperty(propertyPath);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private Vector2 ScrollPosition
	{
		get { return this.m_scrollPosition;	}
		set { m_scrollPosition = value;	}
	}


	public float ScrollHeight
	{
		get	{ return this.m_scrollHeight; }
		set	{ m_scrollHeight = value; }
	}


	public float WindowHeight
	{
		get	{ return this.m_windowHeight; }
		set	{ m_windowHeight = value; }
	}

	public float HeaderHeight
	{
		get { return m_headerHeight; }
	}
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private bool m_showDefaultGUI = false;
	private Object m_target = null;
	private SerializedObject m_serilizedTarget = null;
	private GUISkin m_skin = null;
	private Vector2 m_scrollPosition = new Vector2(0, 0);
	private float m_scrollHeight = 0f;
	private float m_windowHeight = 0f;
	private readonly float m_headerHeight = 15f;
	private static bool m_forceRepaint = false;

	#endregion
}


public static class BaseEditorHelper
{
	public static bool KeyPressed<T>(this T s, string controlName, KeyCode key, out T fieldValue)
	{

		fieldValue = s;
		if(GUI.GetNameOfFocusedControl() == controlName)
		{
			if((Event.current.type == EventType.KeyUp) && (Event.current.keyCode == key))
			{
				return true;
			}
			return false;
		}
		else
		{
			return false;
		}
	}
}