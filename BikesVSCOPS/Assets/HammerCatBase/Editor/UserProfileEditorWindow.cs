using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Zenject;

public class UserProfileEditorWindow : ZenjectEditorWindow
{
    [MenuItem("Bikes VS Cops/ User Profile Editor Window")]
    public static UserProfileEditorWindow GetOrCreateWindow()
    {
        return CustomWindoweEditorTools.GetOrCreateWindow<UserProfileEditorWindow>() as UserProfileEditorWindow;
    }



    public override void InstallBindings()
    {
        CustomUserProfileEditorInstaller.Install(Container);
    }
 

  

}