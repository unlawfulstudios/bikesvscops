using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public abstract class EditorDebugDisplay<T> : IDisposable where T : IConvertible, IComparable
{
	#region Public Properties

	public EditorDebugDisplay(T invalidValue, T totalValue)
	{
		m_invalidValue = invalidValue;
		m_totalValue = totalValue;

		m_debugOperation = m_invalidValue;

		UnityEditor.EditorApplication.update += Update;
		//Debug.Log("Added");
	}


	public void Dispose()
	{
		//Debug.Log("Disposed");
		UnityEditor.EditorApplication.update -= Update;
	}


	public void DisplayDebug()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			if(!EditorAssistant.ShowToggleField(DisplayDebugTools, "Debug Tools"))
			{
				EditorGUILayout.EndVertical();
				return;
			}

			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.BeginVertical("Box");
				{
					DisplayDebugButtons();
				}
				EditorGUILayout.EndVertical();

				//GUILayout.FlexibleSpace();

				if(m_displayDebugOutput != null)
				{
					EditorGUILayout.BeginVertical("Box");
					{
						m_displayDebugOutput();
					}
					EditorGUILayout.EndVertical();

				}
				else
				{
					GUILayout.FlexibleSpace();
				}
			}
			EditorGUILayout.EndHorizontal();

		}
		EditorGUILayout.EndVertical();
	}


	public void MarkDebugOperationComplete(T compleatedOperation)
	{
		if(m_debugOperation.Equals(compleatedOperation))
		{
			m_operationComplete = true;
			m_updateDebugOutput = null;
			//Debug.Log("Compleated debug operation");
		}
		else
		{
			Debug.LogWarning("We are not performing the operation " + compleatedOperation.ToString() +
				" but was told it was done when we are performing the operation " + m_debugOperation.ToString());
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Defines

	protected delegate void EnableDebugOutputMethod();


	protected delegate void DisplayDebugOutputMethod();


	protected delegate void UpdateDebugOutputMethod();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected abstract string[] DebugButtonText { get; }


	protected abstract string[] DebugButtonToolTip { get; }


	protected abstract EnableDebugOutputMethod[] DebugEnableMethods { get; }


	protected abstract DisplayDebugOutputMethod[] DebugDisplayMethods { get; }


	protected abstract UpdateDebugOutputMethod[] DebugUpdateMethods { get; }

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	public Type EnumType
	{
		get
		{
			if(this.m_enumType == null)
			{
				this.m_enumType = typeof(T);
			}
			return this.m_enumType;
		}
	}


	private T InvalidValue
	{
		get	{ return this.m_invalidValue; }
		set	{ m_invalidValue = value; }
	}


	private bool NoDebugOperationActivated
	{
		get { return DebugOperation.Equals(m_invalidValue) || m_operationComplete; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Update()
	{
		if(m_updateDebugOutput != null)
		{
			m_updateDebugOutput();
		}
	}


	private void DisplayDebugButtons()
	{
		int totalValue = (int)(object)m_totalValue;
		for(int debugOperationIt=0; debugOperationIt < totalValue; ++debugOperationIt)
		{
			DisplayDebugButton((T)(object)debugOperationIt);
		}
	}


	private void DisplayDebugButton(T debugFlag)
	{
		using(new GUIColorChange(Color.black, Color.green, DebugOperation.Equals(debugFlag) && !m_operationComplete))
		{
			if(GUILayout.Button(new GUIContent(DebugButtonText[(int)(object)debugFlag], DebugButtonToolTip[(int)(object)debugFlag])))
			{
				if(NoDebugOperationActivated)
				{
					DebugOperation = debugFlag;
				}
				else
				{
					if(!debugFlag.Equals(DebugOperation))
					{
						Debug.Log("Current debug process \"" + DebugOperation.ToString() + "\" is active, please finish that one.");
					}
				}
			}
		}
	}


	private T DebugOperation
	{
		get	{ return m_debugOperation; }
		set
		{
			if(this.m_debugOperation.Equals(m_invalidValue) || m_operationComplete)
			{
				if(value.Equals(m_invalidValue))
				{
					m_debugOperation = m_invalidValue;
					m_displayDebugOutput = null;
					m_updateDebugOutput = null;
				}
				else
				{
					if(value.CompareTo(m_totalValue) < 0)
					{
						m_debugOperation = value;
						int indexValue = (int)(object)m_debugOperation;

						if(DebugEnableMethods[indexValue] != null)
						{
							DebugEnableMethods[indexValue]();
						}
						m_displayDebugOutput = DebugDisplayMethods[indexValue];
						m_updateDebugOutput = DebugUpdateMethods[indexValue];
						m_operationComplete = false;
					}
					else
					{
						Debug.LogWarning("Debug operation requested is larger than total.");
					}
				}
			}
			else
			{
				Debug.Log("Current debug process \"" + m_debugOperation.ToString() + "\" is active, please finish that one.");
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private EditorProfileKey<int, bool> DisplayDebugTools = new EditorProfileKey<int, bool>("DisplayDebugTools", false);
	private Type m_enumType = null;
	private T m_invalidValue;
	private T m_totalValue;
	private T m_debugOperation;
	private DisplayDebugOutputMethod m_displayDebugOutput = null;
	private UpdateDebugOutputMethod m_updateDebugOutput = null;
	private bool m_operationComplete = false;

	#endregion
}