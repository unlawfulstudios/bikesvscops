using UnityEditor;
using UnityEngine;

public partial class BaseEditor : Editor
{
	#region Public Static Methods

	public static void ShowLastRectDimmed()
	{
		ShowLastRectDimmed(true);
	}


	public static void ShowLastRectDimmed(bool toggle)
	{
		if(toggle)
		{
			using(new GUIColorChange(new Color(0.5f, 0.5f, 0.5f, 0.1f)))
			{
				Rect groupRect = GUILayoutUtility.GetLastRect();
				for(int dimmedIt=0; dimmedIt < 1; ++dimmedIt)
				{
					EditorGUI.DropShadowLabel(groupRect, "", EditorStyles.objectFieldThumb);
				}
			}
		}
	}

	// Labels
	public static void Label(GUIContent label)
	{
		using(new FixedWidthLabel(label))
		{
		}
	}


	public static void Label(GUIContent label, GUIStyle style)
	{
		using(new FixedWidthLabel(label, style))
		{
		}
	}


	public static void Label(string label)
	{
		using(new FixedWidthLabel(label))
		{
		}
	}


	public static void Label(string label, GUIStyle style)
	{
		using(new FixedWidthLabel(label, style))
		{
		}
	}


	public static void LabelField(string label)
	{
		using(new FixedWidthLabel(label))
		{
		}
	}


	public static void LabelField(string label, GUIStyle style)
	{
		using(new FixedWidthLabel(label, style))
		{
		}
	}


	public static void LabelField(string label, Color textColor, GUIStyle style)
	{
		Color prevColor = GUI.color;
		GUI.color = textColor;
		using(new FixedWidthLabel(label, style))
		{
		}
		GUI.color = prevColor;
	}


	// Text Fields
	public static string TextField(string value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.TextField(value, options);
	}


	public static string TextField(string value, GUIStyle style, params GUILayoutOption[] options)
	{
		return EditorGUILayout.TextField(value, style, options);
	}


	public static string TextField(string label, string value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return TextField(value, options);
		}
	}


	public static string TextField(string label, string value, GUIStyle style, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return TextField(value, style, options);
		}
	}


	// Floats
	public static float FloatField(float value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.FloatField(value, options);
	}


	public static float FloatField(string label, float value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return FloatField(value, options);
		}
	}


	public static float FloatField(GUIContent label, float value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return FloatField(value, options);
		}
	}

	// Float Slider
	public static float Slider(float value, float leftValue, float rightValue, params GUILayoutOption[] options)
	{

		return EditorGUILayout.Slider(value, leftValue, rightValue, options);
	}


	public static float Slider(string label, float value, float leftValue, float rightValue, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return Slider(value, leftValue, rightValue, options);
		}
	}

	// Ints
	public static int IntField(int value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.IntField(value, options);
	}


	public static int IntField(string label, int value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return IntField(value, options);
		}
	}

	// UInts
	public static uint IntField(uint value, params GUILayoutOption[] options)
	{
		return (uint)Mathf.Clamp(EditorGUILayout.IntField((int)value, options), 0, int.MaxValue);
	}


	public static uint IntField(string label, uint value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return IntField(value, options);
		}
	}

	// Int Slider
	public static int IntSlider(string label, int value, int leftValue, int rightValue)
	{
		using(new FixedWidthLabel(label))
		{
			return EditorGUILayout.IntSlider(value, leftValue, rightValue);
		}
	}

	// Vector2
	public static Vector2 Vector2Field(string label, Vector2 value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.Vector2Field(label, value, options);
	}

	// Vector2 Slider
	public static Vector2 Vector2Slider(Vector2 value, Vector2 leftValue, Vector2 rightValue, params GUILayoutOption[] options)
	{
		Vector2 returnVal = value;
		using(new GUIGroupAlignment(GroupAlignment.Vertical))
		{
			returnVal = new Vector2(
				Slider(value.x, leftValue.x, rightValue.x, options),
				Slider(value.y, leftValue.y, rightValue.y, options));
		}

		return returnVal;
	}


	public static Vector2 Vector2Slider(string label, Vector2 value, Vector2 leftValue, Vector2 rightValue, params GUILayoutOption[] options)
	{
		Vector2 returnVal = value;
		using(new GUIGroupAlignment(GroupAlignment.Vertical))
		{
			Label(label);
			returnVal = Vector2Slider(value, leftValue, rightValue, options);
		}
		return returnVal;
	}


	public static Vector2 Vector2Slider(bool isConst, string label, Vector2 value, Vector2 leftValue, Vector2 rightValue, params GUILayoutOption[] options)
	{
		Vector2 tempValue = Vector2Slider(label, value, leftValue, rightValue, options);
		if(!isConst)
		{
			value = tempValue;
		}
		ShowLastRectDimmed(isConst);
		return value;
	}


	// Vector3
	public static Vector3 Vector3Field(string label, Vector3 value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.Vector3Field(label, value, options);
	}

	// Color
	public static Color ColorField(Color value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.ColorField(value, options);
	}


	public static Color ColorField(string label, Color value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return ColorField(value, options);
		}
	}

	// Toggle
	public static bool Toggle(bool value, params GUILayoutOption[] options)
	{
		return EditorGUILayout.Toggle(value, options);
	}


	public static bool Toggle(GUIContent label, bool value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return Toggle(value, options);
		}
	}


	public static bool Toggle(string label, bool value, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return Toggle(value, options);
		}
	}


	public static bool Toggle(bool value, GUIStyle style, params GUILayoutOption[] options)
	{
		return EditorGUILayout.Toggle(value, style, options);
	}


	public static bool Toggle(bool value, Texture image, params GUILayoutOption[] options)
	{
		return GUILayout.Toggle(value, image, options);
	}

	// Popup
	public static int Popup(int selectedIndex, string[] displayedOptions, params GUILayoutOption[] options)
	{
		return EditorGUILayout.Popup(selectedIndex, displayedOptions, options);
	}

 
	public static int Popup(string label, int selectedIndex, string[] displayedOptions, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return Popup(selectedIndex, displayedOptions, options);
		}
	}

	// Foldout
	public static bool Foldout(bool value, string label, GUIStyle style)
	{
		//using(new FixedWidthLabel(label, style))
		{
			return EditorGUILayout.Foldout(value, label, style);
		}
	}


	public static bool Foldout(bool value, GUIContent label, GUIStyle style)
	{
		//using(new FixedWidthLabel(label, style))
		{
			return EditorGUILayout.Foldout(value, label, style);
		}
	}


	public static bool Foldout(bool value, string label)
	{
		//using(new FixedWidthLabel(label))
		{
			return EditorGUILayout.Foldout(value, label);
		}
	}


	public static bool Foldout(bool value, GUIContent label)
	{
		//using(new FixedWidthLabel(label, style))
		{
			return EditorGUILayout.Foldout(value, label);
		}
	}

	// Enums
	public static System.Enum EnumPopup(System.Enum selected)
	{
		return EditorGUILayout.EnumPopup(selected);
	}


	public static System.Enum EnumPopup(string label, System.Enum selected)
	{
		using(new FixedWidthLabel(label))
		{
			return EditorGUILayout.EnumPopup(selected);
		}
	}

	// Mask
	public static int MaskField(int mask, string[] displayedOptions, params GUILayoutOption[] options)
	{
		return EditorGUILayout.MaskField(mask, displayedOptions, options);
	}


	public static int MaskField(string label, int mask, string[] displayedOptions, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return MaskField(mask, displayedOptions, options);
		}
	}


	public static int MaskField(GUIContent label, int mask, string[] displayedOptions, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return MaskField(mask, displayedOptions, options);
		}
	}


	// Object
	public static T ObjectField<T>(UnityEngine.Object obj, bool allowSceenObject, params GUILayoutOption[] options) where T : UnityEngine.Object
	{
		return EditorGUILayout.ObjectField(obj, typeof(T), allowSceenObject, options) as T;
	}


	public static UnityEngine.Object ObjectField(UnityEngine.Object obj, System.Type objType, bool allowSceenObject, params GUILayoutOption[] options)
	{
		return EditorGUILayout.ObjectField(obj, objType, allowSceenObject, options);
	}


	public static T ObjectField<T>(string label, Object obj, bool allowSceenObject, params GUILayoutOption[] options) where T : UnityEngine.Object
	{
		using(new FixedWidthLabel(label))
		{
			return ObjectField<T>(obj, allowSceenObject, options);
		}
	}


	public static Object ObjectField(string label, Object obj, System.Type objType, bool allowSceenObject, params GUILayoutOption[] options)
	{
		using(new FixedWidthLabel(label))
		{
			return ObjectField(obj, objType, allowSceenObject, options);
		}
	}

	// Const
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// Const Float
	public static void ConstFloatField(float value)
	{
		using(new GUIGroupAlignment(GroupAlignment.Horizontal, "textfield"))
		{
			Label(value.ToString());
		}
	}


	public static void ConstFloatField(string label, float value)
	{
		using(new FixedWidthLabel(label))
		{
			ConstFloatField(value);
		}
	}

	// Const Int
	public static void ConstIntField(int value)
	{
		using(new GUIGroupAlignment(GroupAlignment.Horizontal, "textfield"))
		{
			Label(value.ToString());
		}
	}


	public static void ConstIntField(string label, int value)
	{
		using(new FixedWidthLabel(label))
		{
			ConstIntField(value);
		}
	}

	// Const Vector3
	public static void ConstVector3Field(string label, Vector3 value)
	{
		Label(label);
		using(new GUIGroupAlignment(GroupAlignment.Horizontal))
		{
			GUILayout.Space(12f);
			ConstFloatField("X", value.x);
			ConstFloatField("Y", value.y);
			ConstFloatField("Z", value.z);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	// Generic Property
	// This can not be shared with other devices to inherited from BaseEditor.
	public bool DrawPropertyField(string propertyName, bool includeChildren)
	{
		bool result = EditorGUILayout.PropertyField(serializedObject.FindProperty(propertyName), includeChildren);

		SerilizedTarget.ApplyModifiedProperties();
		//SerilizedTarget.Update();

		return result;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	#endregion
}