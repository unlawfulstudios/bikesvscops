using UnityEditor;
using UnityEngine;

public class MenuOptionFindMissingScripts : EditorWindow
{
	#region Public static Access

	[ MenuItem( "Hammer Cat/Editor/Tools/Find Missing Scripts", false, 600 ) ]
	public static void Launch()
	{
		EditorWindow window = GetWindow(typeof(MenuOptionFindMissingScripts));
		window.name = "Missing Scripts";
		window.title = "Missing Scripts";
		window.Show();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Member

	// Exposed

	// Editor Exposed

	// Member
	private static int m_goCount = 0;
	private static int m_componentsCount = 0;
	private static int m_missingCount = 0;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void OnGUI()
	{
		if(GUILayout.Button("Find Missing Scripts in selected GameObjects"))
		{
			FindInSelected();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Serialized Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private static void FindInSelected()
	{
		GameObject[] go = Selection.gameObjects;
		m_goCount = 0;
		m_componentsCount = 0;
		m_missingCount = 0;
		foreach(GameObject g in go)
		{
			FindInGO(g);
		}
		Debug.Log(string.Format("Searched {0} GameObjects, {1} components, found {2} missing", m_goCount, m_componentsCount, m_missingCount));
	}


	private static void FindInGO(GameObject g)
	{
		++m_goCount;
		Component[] components = g.GetComponents<Component>();
		for(int i = 0; i < components.Length; i++)
		{
			++m_componentsCount;
			if(components[i] == null)
			{
				++m_missingCount;
				Debug.Log(g.name + " has an empty script attached in position: " + i, g);
			}
		}
		// Now recurse through each child GO (if there are any):
		foreach(Transform childT in g.transform)
		{
			//Debug.Log("Searching " + childT.name  + " " );
			FindInGO(childT.gameObject);
		}
	}

	#endregion
}