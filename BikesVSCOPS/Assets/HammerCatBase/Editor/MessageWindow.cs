using UnityEditor;
using UnityEngine;

public class MessageWindow : EditorWindow
{
	#region Public Static Methods

	public static void ShowMessage(string message)
	{
		ShowMessage(new GUIContent(message), MessageWindowLevel.Info, new GUIContent("OK"), null);
	}


	public static void ShowMessage(GUIContent message)
	{
		ShowMessage(message, MessageWindowLevel.Info, new GUIContent("OK"), null);
	}


	public static void ShowMessage(string message, MessageWindowLevel level)
	{
		ShowMessage(new GUIContent(message), level, new GUIContent("OK"), null);
	}


	public static void ShowMessage(GUIContent message, MessageWindowLevel level, GUIContent button1, GUIContent button2)
	{
		MessageWindow messageWindow = ScriptableObject.CreateInstance(typeof(MessageWindow)) as MessageWindow;

		messageWindow.Level = level;
		messageWindow.Message = message;
		messageWindow.Buttons[0] = button1;
		if(messageWindow.Buttons[0] != null)
		{
			messageWindow.Buttons[1] = button2;
		}

		messageWindow.position = new Rect(Screen.width / 2, Screen.height / 2, DefaultSize.x, DefaultSize.y);

		messageWindow.ShowPopup();
		messageWindow.Focus();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIContent Message
	{
		get	{ return this.m_message; }
		set	{ m_message = value; }
	}


	private MessageWindowLevel Level
	{
		get	{ return this.m_level; }
		set	{ m_level = value; }
	}


	public GUIContent[] Buttons
	{
		get	{ return this.m_buttons; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Static Methods



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void OnEnable()
	{
//		Close();
//			return;
		IconTextures[(int)MessageWindowLevel.Info] = AssetDatabase.LoadAssetAtPath("Assets/Editor Default Resources/Icons/info.png", typeof(Texture)) as Texture;
		IconTextures[(int)MessageWindowLevel.Warning] = AssetDatabase.LoadAssetAtPath("Assets/Editor Default Resources/Icons/warn.png", typeof(Texture)) as Texture;
		IconTextures[(int)MessageWindowLevel.Error] = AssetDatabase.LoadAssetAtPath("Assets/Editor Default Resources/Icons/error.png", typeof(Texture)) as Texture;
	}

//	void OnLostFocus()
//	{
//		Debug.Log("Lost focus");
//	}

	private void OnInspectorUpdate()
	{
		if(focusedWindow != this)
		{
			Close();
		}
		else
		{
			Repaint();
		}
	}


	private void OnGUI()
	{
		ForceMinSize();

		using(new GUIGroupAlignment(GroupAlignment.Vertical))
		{
			using(new GUIGroupAlignment(GroupAlignment.Horizontal))
			{
				GUILayout.FlexibleSpace();

				GUILayout.Label(new GUIContent(IconTextures[(int)Level]), GUILayout.Width(IconSize.x), GUILayout.Height(IconSize.y));
				GUILayout.FlexibleSpace();
			}

			using(new GUIGroupAlignment(GroupAlignment.Horizontal))
			{
				GUILayout.FlexibleSpace();

				GUILayout.Space(LabelSpacing);
				GUILayout.Label(Message);
				GUILayout.Space(LabelSpacing);
				GUILayout.FlexibleSpace();
			}

			GUILayout.FlexibleSpace();

			using(new GUIGroupAlignment(GroupAlignment.Horizontal))
			{
				foreach(GUIContent buttonContent in Buttons)
				{
					if(buttonContent != null)
					{
						GUILayout.FlexibleSpace();
						if(GUILayout.Button(buttonContent))
						{
							Close();
						}
					}

				}
				GUILayout.FlexibleSpace();
			}
		}

//		if(focusedWindow != this)
//			Close();
	}


	private void ForceMinSize()
	{
		Vector2 max = GUI.skin.label.CalcSize(Message);
		max.x += LabelSpacing * 2;
		max.x += IconSize.x;

		if(Buttons[0] != null)
		{
			Vector2 button1Size = GUI.skin.button.CalcSize(Buttons[0]);
			max.x = Mathf.Max(max.x, button1Size.x);
			float extraY = button1Size.y;

			if(Buttons[1] != null)
			{
				Vector2 button2Size = GUI.skin.button.CalcSize(Buttons[1]);
				max.x += button2Size.x;
				extraY = Mathf.Max(extraY, button2Size.y);
			}


			max.y = extraY;
		}

		max.x = Mathf.Max(max.x, DefaultSize.x);
		max.y = Mathf.Max(max.y, DefaultSize.y);

		if(this.maxSize != max)
		{
			this.minSize = max;
			Repaint();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	// Static
	private static readonly float LabelSpacing = 32f;
	private static readonly Vector2 DefaultSize = new Vector2(75f, 100f);
	private static readonly Vector2 IconSize = new Vector2(32f, 32f);
	private static readonly Texture[] IconTextures = new Texture[3];

	// Member
	private GUIContent m_message = null;
	private MessageWindowLevel m_level = MessageWindowLevel.Info;
	private GUIContent[] m_buttons = new GUIContent[2] { null, null };

	#endregion
}


public enum MessageWindowLevel
{
	Info,
	Warning,
	Error
}