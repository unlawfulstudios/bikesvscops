﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class HierarchyConttroller  {

    #region Public Global Access

    public static HierarchyConttroller Instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = new HierarchyConttroller();
                if (m_instance == null)
                {
                    Debug.LogError("Something bad happened");
                }
            }

            return m_instance;
        }
    }

    #endregion

    private static HierarchyConttroller m_instance;


}
