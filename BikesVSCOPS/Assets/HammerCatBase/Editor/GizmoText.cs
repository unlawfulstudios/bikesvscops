using System.Collections.Generic;
using UnityEngine;

public class GizmoText
{
	#region Public Properties

	public static GizmoText Instance { get { return GizmoText.Singleton.m_instance; } }

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	private class Singleton
	{
		static Singleton()
		{
			if(m_instance == null)
			{
				m_instance = new GizmoText();
			}
		}


		internal static readonly GizmoText m_instance;
	}

	private const int CHAR_TEXTURE_HEIGHT = 11;
	private const int CHAR_TEXTURE_WIDTH = 8;
	private const string characters = " !#%'()+,-.0123456789;=abcdefghijklmnopqrstuvwxyz_{}~\\?\":/*";

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private Dictionary<char, string> TexturePathLookup
	{
		get
		{
			if(this.m_texturePathLookup == null)
			{
				this.m_texturePathLookup = new Dictionary<char, string>();

			}
			return this.m_texturePathLookup;
		}
	}


	private Dictionary<char, string> SpecialChars
	{
		get
		{
			if(this.m_specialChars == null)
			{
				this.m_specialChars = new Dictionary<char, string>();
				this.m_specialChars.Add('\\', "backslash");

				this.m_specialChars.Add('?', "questionmark");
		
				this.m_specialChars.Add('"', "quotes");
		
				this.m_specialChars.Add(':', "colon");
		
				this.m_specialChars.Add('/', "slash");
		
				this.m_specialChars.Add('*', "star");
			}
			return this.m_specialChars;
		}
	}
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private GizmoText()
	{

	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private Dictionary<char,string> m_texturePathLookup = null;
	private Dictionary<char, string> m_specialChars = null;

	#endregion
}