using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class EditorAssistant
{
	#region Public Methods

	public static void ShowHeaderMinor(string label)
	{
		//using(new GUIColorChange(Color.white))
		{
			using(new GUIColorChange(MinorHeaderColor, Color.white))
			{
				BaseEditor.Label(label, EditorStyles.objectFieldThumb);
			}
		}
	}


	public static bool ShowToggleField(EditorProfileKey<int, bool> toggle, string headerName)
	{
		using(new GUIGroupAlignment(GroupAlignment.Horizontal))
		{
          
                toggle.Value = EditorGUILayout.Foldout(
                    toggle.Value, GUIContent.none);
            

			if(headerName != null)
			{
				using(new GUIColorChange(
					toggle.Value?ToggleGroupColor_BG_On:ToggleGroupColor_BG_Off,
					toggle.Value?ToggleGroupColor_FG_On:ToggleGroupColor_FG_Off))
					{
						BaseEditor.Label(headerName, EditorStyles.objectFieldThumb);
					}
			}

			GUILayout.FlexibleSpace();
		}

		//EditorGUILayout.Space();

		return toggle.Value;
	}


	public static System.Type ShowTypePopup(string popupLabel, System.Type currentType, System.Type[] typeArray, string[] typeNameArray)
	{
		int currentIndex = ArrayUtility.IndexOf(typeArray, currentType);

		if(currentIndex == -1)
		{
			currentIndex = typeArray.Length;
		}

		int pickedType = -1;

		using(new GUIColorChange(Color.red, Color.white, (currentIndex == typeArray.Length)))
		{
			pickedType = BaseEditor.Popup(popupLabel, currentIndex, typeNameArray);
		}

		if(pickedType != typeArray.Length)
		{
			return typeArray[pickedType];
		}

		return null;
	}


	public static bool IsGameObject(GameObject gObject)
	{
		string locationOfAsset = AssetDatabase.GetAssetPath(gObject);

		return (locationOfAsset == null || locationOfAsset == "");
	}


	public static string RootPrefabPathOfGameObject(GameObject gObject)
	{
		Object rootPrefab = PrefabUtility.GetCorrespondingObjectFromSource(gObject);
		string path = "";
		if(rootPrefab)
		{
			path = AssetDatabase.GetAssetPath(rootPrefab);
		}
		return path;
	}


	public static string RootPrefabNameOfGameObject(GameObject gObject)
	{
		Object rootPrefab = PrefabUtility.GetCorrespondingObjectFromSource(gObject);
		string name = "";
		if(rootPrefab)
		{
			name = rootPrefab.name;
		}
		return name;
	}


	public static Type[] AllClassTypes
	{
		get
		{
			if(m_classTypes == null)
			{
				List<Type> types = new List<Type>();
				foreach(Assembly assembly in ProjectAssembly)
				{
					types.AddRange(assembly.GetTypes());
				}

				m_classTypes = types.ToArray();
			}
			return m_classTypes;
		}
	}

	public static float CalcLabelWidth(GUIContent label)
	{
		return GUI.skin.label.CalcSize(label).x +
			9 * EditorGUI.indentLevel;
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private static Assembly[] ProjectAssembly
	{
		get
		{
			if(m_projectAssembly == null)
			{
				m_projectAssembly = System.AppDomain.CurrentDomain.GetAssemblies();
			}
			return m_projectAssembly;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	// Static
	private static Assembly[] m_projectAssembly = null;
	private static Type[] m_classTypes = null;
	private static readonly Color MinorHeaderColor = new Color(1f, 1f, 1f, 0.6f);
	private static readonly Color ToggleGroupColor_BG_On = new Color(0f, 1f, 0f, 1f);
	private static readonly Color ToggleGroupColor_BG_Off = new Color(1f, 1f, 0f, 0.85f);
	private static readonly Color ToggleGroupColor_FG_On = new Color(0.9f, 1f, 0.9f, 1f);
	private static readonly Color ToggleGroupColor_FG_Off = new Color(1f, 1f, 0.5f, 1f);

	#endregion
}
