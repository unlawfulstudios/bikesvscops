using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;

public class EditorProfileKey<T, castT> : ProfileKey where T : IConvertible where castT : IConvertible
{
    #region Public Properties

    public castT Value
    {
        get { return this.m_value; }
        set
        {
            m_value = (castT)value;
            if (MySystemType == typeof(int))
            {
                if (typeof(castT) == typeof(bool))
                {
                    EditorPrefs.SetInt(HashName, ((bool)(object)m_value == false) ? 0 : 1);
                }
                else
                {
                    EditorPrefs.SetInt(HashName, (int)(object)m_value);
                }
            }
            else if (MySystemType == typeof(float))
            {
                EditorPrefs.SetFloat(HashName, (float)(object)m_value);
            }
            else if (MySystemType == typeof(string))
            {
                EditorPrefs.SetString(HashName, (string)(object)m_value);
            }
            else
            {
                Debug.LogError("Could not store/retrive this type of user profile key. It can only be int, float, or string.");
            }
        }
    }


    public override System.Type MyValueType
    {
        get { return typeof(castT); }
    }


    public castT DeafultValue
    {
        get { return this.m_defaultValue; }
    }


    public override string DefaultValueAsString
    {
        get { return this.m_defaultValue.ToString(); }
    }


    public override bool IsDefault
    {
        get { return m_value.ToString() == this.m_defaultValue.ToString(); }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Public Methods

    public EditorProfileKey(string name, castT defaultValue)
    {
        MySystemType = typeof(T);
        m_defaultValue = defaultValue;

        System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
        System.Diagnostics.StackFrame frame = trace.GetFrame(1);
        MethodBase method = frame.GetMethod();
        string callingClass = method.DeclaringType.ToString();
        Name = name;
        HashName = m_keyPrefix + callingClass + Name;

        if (EditorPrefs.HasKey(HashName))
        {
            if (MySystemType == typeof(int))
            {
                if (typeof(castT) == typeof(bool))
                {
                    m_value = (castT)(object)((int)(object)EditorPrefs.GetInt(HashName) == 0 ? false : true);
                }
                else
                {
                    m_value = (castT)(object)EditorPrefs.GetInt(HashName);
                }
            }
            else if (MySystemType == typeof(float))
            {
                m_value = (castT)(object)EditorPrefs.GetFloat(HashName);
            }
            else if (MySystemType == typeof(string))
            {
                m_value = (castT)(object)EditorPrefs.GetString(HashName);
            }
            else
            {
                Debug.LogError("Could not store/retrive this type of user profile key. It can only be int, float, or string.");
            }
        }
        else
        {
            Value = m_defaultValue;
        }
    }


    public override void Reset()
    {
        Value = m_defaultValue;
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private static readonly string m_keyPrefix = "HammerCat_editorPref_";
    private castT m_value;
    private castT m_defaultValue;

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private readonly System.Func<T> getter;
    private readonly System.Action<T> setter;

    #endregion
}