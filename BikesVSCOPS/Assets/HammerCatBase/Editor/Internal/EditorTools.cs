using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;

public static class EditorTools
{
	#region Public Static Properties

	// everything below this line would normally be in other libraries I've made, but I've brought these versions of them into here for simplicity
	public static List<Object> AllAssets
	{
		get
		{
			return AllAssetsOfFileName("*.*");
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Static Methods

	public static List<Object> AllAssetsOfFileName(string fileName)
	{
		return AllAssetsOfFileName(fileName, false);
	}


	public static List<Object> AllAssetsOfFileName(string fileName, bool caseSensative)
	{
		if(!caseSensative)
		{
			fileName = fileName.ToLower();
		}
		// get every single one of the files in the Assets folder.
		List<FileInfo> files = DirSearch(new DirectoryInfo(Application.dataPath), fileName, caseSensative);

		// now make them all into Asset references.
		List<Object> assetRefs = new List<Object>();

		foreach(FileInfo fi in files)
		{
			string fiName = caseSensative ? fi.Name : fi.Name.ToLower();
			if(fiName.StartsWith(fileName))
			{
				continue;
			} // Unity ignores dotfiles.
			assetRefs.Add(AssetDatabase.LoadMainAssetAtPath(GetRelativeAssetPath(fi.FullName)));
		}
		return assetRefs;
	}

	/// Given a path to a node in the filesystem, lop off anything above
	/// the project Assets folder in the pathname so it can work with
	/// UnityEditor's built-in commands
	public static string GetRelativeAssetPath(string pathName)
	{
		// dataPath uses forward slashes on all platforms now
		return FixSlashes(pathName).Replace(Application.dataPath, "Assets");
	}

	// Given a folder and a search filter, return a list of file references
	// (in the unlikely event you have some filesystem arrangement with
	//	recursive "hard links", be aware this may not work out well for you)
	public static List<FileInfo> DirSearch(DirectoryInfo dirInfo, string searchFor, bool caseSensative)
	{
		if(caseSensative)
		{
			searchFor = searchFor.ToLower();
		}
		//string[] searchSplit = searchFor

		List<FileInfo> founditems = new List<FileInfo>();

		List<FileInfo> prevFounditems = dirInfo.GetFiles("*.*").ToList();
		foreach(FileInfo info in prevFounditems)
		{
			string infoName = caseSensative ? @info.Name : @info.Name.ToLower();
			if(infoName.EndsWith("ttf") || infoName.EndsWith("TTF"))
			{
				//Debug.Log("Found");
			}
			//string[] fileSplit = infoName.Split('.');

			if(FitsMask(infoName, searchFor))
			{
				founditems.Add(info);
			}
		}

		// Add (by recursing) subdirectory items.
		DirectoryInfo[] dirInfos = dirInfo.GetDirectories();
		foreach(DirectoryInfo dir in dirInfos)
		{
			founditems.AddRange(DirSearch(dir, searchFor, caseSensative));
		}

		return(founditems);
	}


	private static bool FitsMask(this string sFileName, string sFileMask)
	{
		Regex mask = new Regex(sFileMask.Replace(".", "[.]").Replace("*", ".*").Replace("?", "."));
		return mask.IsMatch(sFileName);
	}


	public static string FixSlashes(string s)
	{
		const string forwardSlash = "/";
		const string backSlash = "\\";
		return s.Replace(backSlash, forwardSlash);
	}

	#endregion
}