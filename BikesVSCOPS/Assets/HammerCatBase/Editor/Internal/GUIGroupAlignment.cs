using System;
using UnityEditor;
using UnityEngine;

public enum GroupAlignment
{
	Vertical,
	Horizontal,
	Scroll
}


public class GUIGroupAlignment : IDisposable
{
	#region Public Methods

	public GUIGroupAlignment(GroupAlignment alignment, params GUILayoutOption[] options)
	{
		if(alignment == GroupAlignment.Scroll)
		{
			Debug.LogError("You must pass in a \"Ref<Vector2>\" to utilize a scroll group");
			// Just setting to verticle to prevent crashing.
			alignment = GroupAlignment.Vertical;
		}
		Options = options;
		Alignment = alignment;
	}


	public GUIGroupAlignment(GroupAlignment alignment, Ref<Vector2> scroll, params GUILayoutOption[] options)
	{
		ScrollRef = scroll;
		Options = options;
		Alignment = alignment;
	}


	public GUIGroupAlignment(GroupAlignment alignment, Ref<Vector2> scroll, GUIStyle style, params GUILayoutOption[] options)
	{
		ScrollRef = scroll;
		Options = options;
		Style = style;
		Alignment = alignment;
	}


	public GUIGroupAlignment(GroupAlignment alignment, GUIStyle style, params GUILayoutOption[] options)
	{
		if(alignment == GroupAlignment.Scroll)
		{
			Debug.LogError("You must pass in a \"Ref<Vector2>\" to utilize a scroll group");
			// Just setting to verticle to prevent crashing.
			alignment = GroupAlignment.Vertical;
		}
		Options = options;
		Style = style;
		Alignment = alignment;
	}


	public void Dispose()
	{
		try
		{
			switch(Alignment)
			{
				case GroupAlignment.Vertical:
					{
	
						EditorGUILayout.EndVertical();
	
						break;
					}
				case GroupAlignment.Horizontal:
					{
						EditorGUILayout.EndHorizontal();
						break;
					}
				case GroupAlignment.Scroll:
					{
						EditorGUILayout.EndScrollView();
						break;
					}
			}
		}
		catch
		{
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUILayoutOption[] Options
	{
		get	{ return this.m_options; }
		set	{ m_options = value; }
	}


//	private Vector2 Scroll
//	{
//		get	{ return this.m_scroll;	}
//		set	{ m_scroll = value;	}
//	}

	private Ref<Vector2> ScrollRef
	{
		get	{ return this.m_scrollRef; }
		set	{ m_scrollRef = value; }
	}

	private GUIStyle Style
	{
		get	{ return this.m_style; }
		set	{ m_style = value; }
	}


	private GroupAlignment Alignment
	{
		get	{ return this.m_alignment; }
		set
		{
			m_alignment = value;
			try
			{
				switch(m_alignment)
				{
					case GroupAlignment.Vertical:
						{
							EditorGUILayout.BeginVertical(Style, Options);
							break;
						}
					case GroupAlignment.Horizontal:
						{
							EditorGUILayout.BeginHorizontal(Style, Options);
							break;
						}
					case GroupAlignment.Scroll:
						{
							ScrollRef.Value = EditorGUILayout.BeginScrollView(ScrollRef.Value, Style);
							break;
						}
				}
			}
			catch
			{
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GroupAlignment m_alignment = GroupAlignment.Vertical;
	private GUIStyle m_style = GUIStyle.none;
	private GUILayoutOption[] m_options = null;
	//private Vector2 m_scroll = Vector2.zero;
	private Ref<Vector2> m_scrollRef = null;

	#endregion
}