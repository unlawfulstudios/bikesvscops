using System;
using UnityEngine;

public class GUIColorChange : IDisposable
{
	#region Public Methods

	public GUIColorChange(Color bgColor, Color contentColor, bool useColor)
	{
		ChangedColor = useColor;
		if(ChangedColor)
		{
			SaveGUIColors();

			SetGUIColors(PrevGlobalColor, bgColor, contentColor);
		}
	}

	public GUIColorChange(Color newColor, bool useColor)
	{
		ChangedColor = useColor;
		if(ChangedColor)
		{
			SaveGUIColors();

			GUI.color = newColor;
		}
	}

	public GUIColorChange(Color bgColor, Color contentColor) : this(bgColor, contentColor, true)
	{

	}

	public GUIColorChange(Color newColor) : this(newColor, true)
	{

	}


	public void Dispose()
	{
		if(ChangedColor)
		{
			SetGUIColors(PrevGlobalColor, PrevBGColor, PrevContentColor);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private Color PrevGlobalColor
	{
		get	{ return this.m_prevGlobalColor; }
		set	{ m_prevGlobalColor = value; }
	}

	public Color PrevBGColor
	{
		get	{ return this.m_prevBGColor; }
		set	{ m_prevBGColor = value; }
	}

	private Color PrevContentColor
	{
		get	{ return this.m_prevContentColor; }
		set	{ m_prevContentColor = value; }
	}

	private bool ChangedColor
	{
		get	{ return this.m_changedColor; }
		set	{ m_changedColor = value; }
	}
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private void SaveGUIColors()
	{
		PrevGlobalColor = GUI.color;
		PrevContentColor = GUI.contentColor;
		PrevBGColor = GUI.backgroundColor;
	}

	private void SetGUIColors(Color globalColor, Color bgColor, Color contentColor)
	{
		GUI.color = globalColor;
    	GUI.backgroundColor = bgColor;
    	GUI.contentColor = contentColor;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private Color m_prevGlobalColor = Color.grey;
	private Color m_prevBGColor = Color.grey;
	private Color m_prevContentColor = Color.grey;
	private bool m_changedColor = false;

	#endregion
}