using UnityEngine;
using System;
using UnityEditor;

public class FixedWidthLabel : IDisposable
{
	#region Public Methods

	/// <summary>
	/// Contructor taking in GUIContents.
	/// </summary>
	/// <param name='label'>
	/// Label.
	/// </param>
	public FixedWidthLabel(GUIContent label)
	{
		float width = EditorAssistant.CalcLabelWidth(label);
		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.LabelField(label, GUILayout.Width(width));
			//Set following indentation to zero:
			m_indentReset = new ZeroIndent();
		}

		GUILayout.FlexibleSpace();
	}


	public FixedWidthLabel(GUIContent label, GUIStyle style)
	{
		float width = EditorAssistant.CalcLabelWidth(label);
		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.LabelField(label, style, GUILayout.Width(width));
			//Set following indentation to zero:
			m_indentReset = new ZeroIndent();
		}

		GUILayout.FlexibleSpace();
	}

	/// <summary>
	/// Alternative constructor, if we don't want to deal with GUIContents.
	/// </summary>
	/// <param name='label'>
	/// Label.
	/// </param>
	public FixedWidthLabel(string label) : this(new GUIContent(label))
	{
	}


	public FixedWidthLabel(string label, GUIStyle style) : this(new GUIContent(label), style)
	{
	}


	public void Dispose()
	{
		try
		{
			//restore indentation state:
			m_indentReset.Dispose();
			//finish horizontal group:
			EditorGUILayout.EndHorizontal();
		}
		catch(Exception e)
		{
			if(e.GetType() == typeof(Exception))
			{
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	/// <summary>
	/// Helper class to clear indentation
	/// </summary>
	private class ZeroIndent : IDisposable
	{

		public ZeroIndent()
		{
			//save original indentation
			m_originalIndent = EditorGUI.indentLevel;

			//clear indentation
			EditorGUI.indentLevel = 0;
		}

	
		public void Dispose()
		{
			//restore original indentation
			EditorGUI.indentLevel = m_originalIndent;
		}

		/// <summary>
		/// The original indentation value before we change the GUI state
		/// </summary>
		private readonly int m_originalIndent;
	}
	
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	/// <summary>
	/// Helper class to reset and restore indentation
	/// </summary>
	private readonly ZeroIndent m_indentReset;

	#endregion
}


public class ScopedIndent : IDisposable
{
	public ScopedIndent(int delta)
	{
		m_delta = delta;
		EditorGUI.indentLevel += m_delta;
	}


	public void Dispose()
	{
		EditorGUI.indentLevel -= m_delta;
	}


	private readonly int m_delta = 0;
}