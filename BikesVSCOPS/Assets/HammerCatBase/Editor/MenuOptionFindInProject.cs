using UnityEngine;
using UnityEditor;

public class MenuOptionFindInProject
{
	#region Protected Global

	[MenuItem("Hammer Cat/Editor/Tools/Find in Hierarchy", false, 301)]
	protected static void FindInHierarchy()
	{
		EditorGUIUtility.PingObject( Selection.activeGameObject );
	}

	[MenuItem("Hammer Cat/Editor/Tools/Find in Hierarchy", true)]
	protected static bool ValidateFindInHierarchy()
	{
		return Selection.activeGameObject != null;
	}

	#endregion
}
