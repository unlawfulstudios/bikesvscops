using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;

public class WizardFindComponents : ScriptableWizard
{
	#region Protected Global Methods

	// Not needed any more due to MenuOptionComponentLister
	
	//[MenuItem("Hammer Cat/Editor/Tools/Find Component in Hierarchy", false, 302)]
	protected static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<WizardFindComponents>("Find Componenets in Hierarchy");
	}


	//[MenuItem("Hammer Cat/Editor/Tools/Find Component in Hierarchy", true)]
	protected static bool ValidateFindInHierarchy()
	{
		return GameObject.FindObjectOfType(typeof(Transform)) != null;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Members

	protected List<System.Type> m_cmpTypes = new List<System.Type>();
	protected List<System.Type> m_fieldTypes = new List<System.Type>();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected void Awake()
	{
		GatherSceneTypes();
	}


	protected void OnEnable()
	{
		GatherSceneTypes();
	}


	protected void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.BeginVertical("Box");
			{
				EditorGUILayout.LabelField("Components");
				foreach(System.Type type in m_cmpTypes)
				{
					if(GUILayout.Button(type.ToString()))
					{
						Selection.objects = new Object[0];
						List<Object> selectedObjects = new List<Object>();
						//int nextPingTime = 0;
						GameObject[] allGos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];//
						//SceneView.FindSceneObjectsOfType(typeof(Component));

						//Selection.objects = allGos;
						foreach(GameObject go in allGos)
						{
							if(go == null)
							{
								continue;
							}
							//MonoBehaviour mono = go.GetComponent<MonoBehaviour>();

							//foreach(Component cmp in go.GetComponents(type))
							{
								//EditorGUIUtility.PingObject( cmp );
								//if(mono)
								//	mono.StartCoroutine(PingAllObjects(cmp, ++nextPingTime));
								//if(!selectedObjects.Contains(go))
								if(go.GetComponent(type) != null)
								{
									selectedObjects.Add(go);
								}
							}
//							foreach(Component cmp in go.GetComponentsInChildren(type))
//							{
//								//EditorGUIUtility.PingObject( cmp );
//								if(!selectedObjects.Contains(go))
//								{
//									selectedObjects.Add(go);
//								}
//							}
						}
						Selection.objects = selectedObjects.ToArray();
					}
				}
			}
			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical("Box");
			{
				EditorGUILayout.LabelField("Fields");
				foreach(System.Type type in m_fieldTypes)
				{
					if(GUILayout.Button(type.ToString()))
					{
					}
				}
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndHorizontal();
	}

//	protected IEnumerator PingAllObjects(Component cmp, int time)
//	{
//		yield return new WaitForSeconds(1);
//
//		EditorGUIUtility.PingObject( cmp );
//		Debug.Log(cmp.gameObject.name);
//	}


	protected void GatherSceneTypes()
	{
		m_cmpTypes.Clear();
		m_fieldTypes.Clear();
		GameObject[] allGos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];// SceneView.FindSceneObjectsOfType(typeof(Component));

		foreach(GameObject go in allGos)
		{
			if(go == null)
			{
				continue;
			}
			foreach(Component cmp in go.GetComponents(typeof(Component)))
			{
				if(go.name == "GameObject")
				{
					int x = 0;
					++x;
				}
				if(!m_cmpTypes.Contains(cmp.GetType()))
				{
					m_cmpTypes.Add(cmp.GetType());
				}
				GetFieldTypes(cmp);
			}
			foreach(Component cmp in go.GetComponentsInChildren(typeof(Component)))
			{
				if(!m_cmpTypes.Contains(cmp.GetType()))
				{
					m_cmpTypes.Add(cmp.GetType());
				}
				GetFieldTypes(cmp);
			}
		}

		m_cmpTypes.Sort(delegate(System.Type type1, System.Type type2)
		{
			return type1.ToString().CompareTo(type2.ToString());
		}
		);
		m_fieldTypes.Sort(delegate(System.Type type1, System.Type type2)
		{
			return type1.ToString().CompareTo(type2.ToString());
		}
		);
	}


	protected void GetFieldTypes(Component o)
	{
		System.Reflection.FieldInfo[] oMembersInfo = o.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
		for(int memberIt=0; memberIt < oMembersInfo.Length; ++memberIt)
		{
			if(!m_fieldTypes.Contains(oMembersInfo[memberIt].FieldType))
			{
				m_fieldTypes.Add(oMembersInfo[memberIt].FieldType);
			}
		}
	}

	#endregion
}