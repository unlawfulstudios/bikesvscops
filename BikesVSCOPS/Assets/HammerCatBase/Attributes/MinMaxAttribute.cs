using UnityEngine;

public class MinMaxAttribute : PropertyAttribute
{

	public readonly bool SpecifiedTypeEnum = false;
	public readonly System.Type Type = null;

	public MinMaxAttribute()
	{
		this.SpecifiedTypeEnum = false;
		this.Type = null;
	}

	public MinMaxAttribute(System.Type type)
	{
		if(type.IsEnum)
		{
			this.SpecifiedTypeEnum = true;
			this.Type = type;
		}
	}
}
