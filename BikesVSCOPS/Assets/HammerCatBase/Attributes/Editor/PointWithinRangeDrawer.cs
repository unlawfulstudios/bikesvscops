using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof(PointWithinRange))]
public class PointWithinRangeDrawer : PropertyDrawer
{
	#region Public Methods

	public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
	{
		SerializedProperty range = prop.FindPropertyRelative("range");
		SerializedProperty x = range.FindPropertyRelative("x");
		SerializedProperty y = range.FindPropertyRelative("y");
		SerializedProperty point = prop.FindPropertyRelative("point");

		EditorGUI.LabelField(
			new Rect(pos.x, pos.y, pos.width/4, pos.height),
			label);

		EditorGUI.BeginChangeCheck();

		float newRangeMin = DrawFloatField(
			new Rect(pos.x, pos.y + textHeight, pos.width/4, pos.height),
			x.floatValue,
			new GUIContent("Min"));

		if(EditorGUI.EndChangeCheck())
		{
			x.floatValue = newRangeMin;
		}

		EditorGUI.BeginChangeCheck();

		float newRangeMax = DrawFloatField(
			new Rect(pos.x + ((pos.width/3) * 1), pos.y + textHeight, pos.width/4, pos.height),
			y.floatValue,
			new GUIContent("Max"));

		if(EditorGUI.EndChangeCheck())
		{
			y.floatValue = newRangeMax;
		}

		EditorGUI.BeginChangeCheck();

		float newPoint = DrawFloatField(
			new Rect(pos.x + ((pos.width/3) * 2), pos.y + textHeight, pos.width/4, pos.height),
			point.floatValue,
			new GUIContent("Center"));

		if(EditorGUI.EndChangeCheck())
		{
			point.floatValue = newPoint;
		}
	}

	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
	{

		float extraHeight = 20.0f;  

		return base.GetPropertyHeight(prop, label) + extraHeight;

	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Static Methods

	private static float DrawFloatField(Rect position, float value, GUIContent label)
	{
		float width = EditorAssistant.CalcLabelWidth(label);

		EditorGUI.LabelField(
			new Rect(position.x, position.y, width, position.height),
			label);

		return EditorGUI.FloatField(
			new Rect(position.x + width, position.y, position.width, position.height),
			value);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private static readonly int textHeight = 16;

	#endregion
}
