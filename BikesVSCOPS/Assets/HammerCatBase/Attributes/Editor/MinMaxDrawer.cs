using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof(MinMaxAttribute))]
public class MinMaxDrawer : PropertyDrawer
{
	#region Public Static Methods

	public static void DrawMinMaxField(Rect position, SerializedProperty prop, GUIContent label)
	{
		Vector2 currentValue = prop.vector2Value;

		EditorGUI.LabelField(position, label);

		int indent = EditorGUI.indentLevel;
		++EditorGUI.indentLevel;

		EditorGUI.BeginChangeCheck();

		float newValueX = DrawFloatField(
			new Rect(position.x, position.y + textHeight, position.width / 2, textHeight),
			currentValue.x,
			new GUIContent("Min"));

		EditorGUI.indentLevel = 1;

		float newValueY = DrawFloatField(
			new Rect(position.x + position.width / 2, position.y + textHeight, position.width / 2, textHeight),
			currentValue.y,
			new GUIContent("Max"));

		if(newValueY < newValueX)
		{
			newValueY = newValueX;
		}
		Vector2 newValue = new Vector2(newValueX, newValueY);

		if(EditorGUI.EndChangeCheck())
		{
			prop.vector2Value = newValue;
		}

		EditorGUI.indentLevel = indent;
	}



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public override void OnGUI(Rect position,
                                SerializedProperty prop,
                                GUIContent label)
	{
		Rect textFieldPosition = position;
		textFieldPosition.height = textHeight;
		if(!prop.isArray)
		{
			m_count = 1;
			DrawMinMaxField(textFieldPosition, prop, label);
		}
		else
		{
			string savedLabelText = label.text;
			m_count = prop.arraySize;
			for(int arrayIt=0; arrayIt < m_count; ++arrayIt)
			{
				SerializedProperty propItem = prop.GetArrayElementAtIndex(arrayIt);

				string indexText = arrayIt.ToString();
				if(minMaxAttribute.SpecifiedTypeEnum)
				{
					indexText = System.Enum.GetName(minMaxAttribute.Type, arrayIt);
				}
				DrawMinMaxField(textFieldPosition, propItem, new GUIContent(savedLabelText + ": " + indexText));
				textFieldPosition.y += (textHeight + 4f) * 2f;
			}
		}
	}


	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
	{
		float extraHeight = textHeight + 4f;

		return (2f * extraHeight) * m_count;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private MinMaxAttribute minMaxAttribute { get { return ((MinMaxAttribute)attribute); } }

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Static Methods

	private static float DrawFloatField(Rect position, float value, GUIContent label)
	{
		float width = EditorAssistant.CalcLabelWidth(label) + 9;

		EditorGUI.LabelField(
			new Rect(position.x, position.y, width, position.height),
			label);

		return EditorGUI.FloatField(
			new Rect(position.x + width, position.y, position.width - labelWidth_MinMax, position.height),
			value);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private static readonly int textHeight = 16;
	private static readonly int labelWidth_MinMax = 50;
	private int m_count = 0;

	#endregion
}
