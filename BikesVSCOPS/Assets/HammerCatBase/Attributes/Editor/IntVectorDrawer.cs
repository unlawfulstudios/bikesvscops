using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof(IntVectorAttribute))]
public class IntVectorDrawer : PropertyDrawer
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public override void OnGUI(Rect position,
                                SerializedProperty property,
                                GUIContent label)
	{
		Rect textFieldPosition = position;
		textFieldPosition.height = textHeight;

		if(property.propertyType == SerializedPropertyType.Vector2)
		{
			DrawIntVector2Field(textFieldPosition, property, label.text);
		}
		else if(property.propertyType == SerializedPropertyType.Vector3)
		{
			DrawIntVector3Field(textFieldPosition, property, label.text);
		}
//		else if(property.propertyType == SerializedPropertyType.Vector4)
//		{
//			DrawIntVector4Field(textFieldPosition, property, label.text);
//		}
	}

	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
	{
		float extraHeight = textHeight + marginHeight;

		return (2f * extraHeight);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DrawIntVector2Field(Rect position, SerializedProperty property, string label)
	{
		Vector2 prevValue = property.vector2Value;

		EditorGUI.BeginChangeCheck();

		Vector2 newValue = EditorGUI.Vector2Field(position, label, prevValue);

		if(EditorGUI.EndChangeCheck())
		{
			newValue = new Vector2((int)newValue.x, (int)newValue.y);
			if(newValue != prevValue)
			{
				property.vector2Value = newValue;
			}
		}
	}


	private void DrawIntVector3Field(Rect position, SerializedProperty property, string label)
	{
		Vector3 prevValue = property.vector3Value;

		EditorGUI.BeginChangeCheck();

		Vector3 newValue = EditorGUI.Vector3Field(position, label, prevValue);

		if(EditorGUI.EndChangeCheck())
		{
			newValue = new Vector3((int)newValue.x, (int)newValue.y, (int)newValue.z);
			if(newValue != prevValue)
			{
				property.vector3Value = newValue;
			}
		}
	}

//	private void DrawIntVector4Field(Rect position, SerializedProperty property, string label)
//	{
//		Vector4 prevValue = property.vector4Value;
//
//		EditorGUI.BeginChangeCheck();
//
//		Vector4 newValue = EditorGUI.Vector4Field(position, label, prevValue);
//
//		if(EditorGUI.EndChangeCheck())
//		{
//			newValue = new Vector4((int)newValue.x, (int)newValue.y, (int)newValue.z, (int)newValue.w);
//			if(newValue != prevValue)
//			{
//				property.vector4Value = newValue;
//			}
//		}
//	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private static readonly int textHeight = 16;
	private static readonly int marginHeight = 4;

	#endregion
}