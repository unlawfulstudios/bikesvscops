using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof(AxisToggle))]
public class AxisToggleDrawer : PropertyDrawer
{
	#region Public Methods

	public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
	{
		SerializedProperty x = prop.FindPropertyRelative("x");
		SerializedProperty y = prop.FindPropertyRelative("y");

		EditorGUI.LabelField(
			new Rect(pos.x, pos.y, pos.width/2, pos.height),
			label);

		EditorGUI.BeginChangeCheck();

		bool newX = DrawBoolField(
			new Rect(pos.x + pos.width/2, pos.y, pos.width/4, pos.height),
			x.boolValue,
			new GUIContent("X"));

		if(EditorGUI.EndChangeCheck())
		{
			x.boolValue = newX;
		}

		EditorGUI.BeginChangeCheck();

		bool newY = DrawBoolField(
			new Rect(pos.x + pos.width/2 + pos.width/4, pos.y, pos.width/4, pos.height),
			y.boolValue,
			new GUIContent("Y"));

		if(EditorGUI.EndChangeCheck())
		{
			y.boolValue = newY;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Static Methods

	private static bool DrawBoolField(Rect position, bool value, GUIContent label)
	{
		EditorGUI.LabelField(
			new Rect(position.x, position.y, labelWidth_Axis, position.height),
			label);

		return EditorGUI.Toggle(
			new Rect(position.x + labelWidth_Axis, position.y, position.width - labelWidth_Axis, position.height),
			value);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private static readonly int labelWidth_Axis = 25;

	#endregion
}
