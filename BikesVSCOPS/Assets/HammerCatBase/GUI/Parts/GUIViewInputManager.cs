using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GUIViewInputManager : MonoSingleton<GUIViewInputManager>
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public CursorState CurrentCursorState
	{
		get
		{
			if(m_currentCursorState == null)
			{
				m_currentCursorState = new CursorState();
			}
			return m_currentCursorState;
		}
	}


	public void AddInput(GUIViewInput input)
	{
		if(!EnabledInput.Contains(input))
		{
			EnabledInput.Add(input);

			EnabledInput.Sort(delegate(GUIViewInput inputA, GUIViewInput inputB)
			{
				return (-inputA.finalLayer).CompareTo(-inputB.finalLayer);
			});
		}
	}


	public void RemoveInput(GUIViewInput input)
	{
		EnabledInput.Remove(input);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void Init()
	{
		Reset();



//		LastCursorPosition = Input.mousePosition;
//		CurrentCursorState.ActiveInput = null;
//		CurrentCursorState.InitialInput = null;

	}


	protected override void OnDestroy()
	{
		Reset();
		base.OnDestroy();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	private delegate void ProcessInputMethod();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

//	private Vector2 LastCursorPosition
//	{
//		get	{ return this.m_lastCursorPosition;	}
//		set	{ m_lastCursorPosition = value; }
//	}

	private Camera SceneCamera
	{
		get
		{
			if(this.m_sceneCamera == null)
			{
				GameObject go = GameObject.Find("Main Camera");
				if(go != null)
				{
					this.m_sceneCamera = go.GetComponent<Camera>();
				}
			}
			return this.m_sceneCamera;
		}
	}


	private List<GUIViewInput> EnabledInput
	{
		get
		{
			if(m_enabledInput == null)
			{
				GUIViewInput[] inputs = GameObject.FindObjectsOfType(typeof(GUIViewInput)) as GUIViewInput[];
				m_enabledInput = new List<GUIViewInput>(inputs);

				EnabledInput.Sort(delegate(GUIViewInput inputA, GUIViewInput inputB)
				{
					return (-inputA.finalLayer).CompareTo(-inputB.finalLayer);
				});
			}
			return m_enabledInput;
		}
	}


	public bool InputIsEnabled
	{
		get	{ return m_inputIsEnabled; }
		set { m_inputIsEnabled = value; }
	}


	public bool UsesTouchInput
	{
		get
		{
			return Application.platform == RuntimePlatform.Android
			|| Application.platform == RuntimePlatform.IPhonePlayer;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Function Pointers

	private ProcessInputMethod ProcessInput = null;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	protected void OnDisable()
	{
		Reset();
	}


	private void OnApplicationQuit()
	{
		Reset();
	}


	private void Update()
	{
		if(!Application.isPlaying)
		{
			return;
		}

		if(ProcessInput != null)
		{
			ProcessInput();
		}
	}


	private void Reset()
	{
		m_enabledInput = null;
		m_currentCursorState = null;
		ProcessInput = null;

		if(UsesTouchInput)
		{
			ProcessInput = ProcessTouch;
		}
		else if(Application.platform == RuntimePlatform.WindowsPlayer ||
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			ProcessInput = ProcessMouse;
		}
		else if(Application.platform == RuntimePlatform.WindowsEditor ||
				 Application.platform == RuntimePlatform.OSXEditor)
		{
			ProcessInput = ProcessMouse;
		}
	}


	private void ProcessMouse()
	{
		if(Input.GetMouseButtonDown(0))
		{
			CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Press,
				Input.mousePosition);
		}
		else if(Input.GetMouseButtonUp(0))
		{
			CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Released,
					Input.mousePosition);
		}
		else if(Input.GetMouseButton(0))
		{
			CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Down,
						Input.mousePosition);
		}
		else
		{
			CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.None,
						Input.mousePosition);
		}


		ProcessCursor();
	}


	private void ProcessTouch()
	{
//		int fingerCount = 0;
//		foreach(Touch touch in Input.touches)
//		{
//			if(touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
//			{
//				++fingerCount;
//			}
//
//		}
		if(Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			switch(touch.phase)
			{
				case TouchPhase.Began:
				{
					CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Press,
						touch.position);
					break;
				}
				case TouchPhase.Ended:
				{
					if(CurrentCursorState.PressedState != CursorState.PressState.None)
					{
						CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Released,
								touch.position);
					}
					else if(CurrentCursorState.PressedState != CursorState.PressState.None)
					{
						CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.None,
								touch.position);
					}
					break;
				}
				case TouchPhase.Canceled:
				{

					CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.None,
						touch.position);
					break;
				}
				case TouchPhase.Moved:
				case TouchPhase.Stationary:
				{
					CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.Down,
						touch.position);
					break;
				}
			}
		}
		else if(CurrentCursorState.PressedState != CursorState.PressState.None)
		{
			CurrentCursorState.ApplyNewStateAndPosition(CursorState.PressState.None,
								Vector2.zero);
		}
		//
		ProcessCursor();
	}


	private void ProcessCursor()
	{
//		if(m_lastCursorVel != CurrentCursorState.VelocityTotal)
//		{
//			m_lastCursorVel = CurrentCursorState.VelocityTotal;
//			Debug.Log("Total Velocity: " + CurrentCursorState.VelocityTotal);
//		}
		GUIViewInput currentInput = null;
		if(CurrentCursorState.PressedState != CursorState.PressState.None)
		{
			foreach(GUIViewInput enabledInput in EnabledInput)
			{
				if(enabledInput
					&& enabledInput.inputEnabled
					&& !enabledInput.lockInput
					&& InputIsEnabled
					&& enabledInput.ContainsPoint(CurrentCursorState.Position))
				{
					currentInput = enabledInput.gameObject.GetComponent<GUIViewInput>();
					break;
				}
			}
		}
		CurrentCursorState.ApplyNewActiveInput(currentInput);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	//private Vector2 m_lastCursorPosition = Vector2.zero;
	private static CursorState m_currentCursorState = null;
	private Camera m_sceneCamera = null;
	private static List<GUIViewInput> m_enabledInput = null;
	private static bool m_inputIsEnabled = true;
	//private static Vector2 m_lastCursorVel = Vector2.zero;

	#endregion
}