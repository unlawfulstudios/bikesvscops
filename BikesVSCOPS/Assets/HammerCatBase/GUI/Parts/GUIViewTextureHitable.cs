using UnityEngine;

[ExecuteInEditMode]
public class GUIViewTextureHitable : GUIViewButton
{
	#region Public Defines

	public delegate void OnHitDelegate(GameObject hitGameObject);

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Member

	// Exposed
	public event OnHitDelegate OnHitEvent = null;

	// Editor Exposed
	[SerializeField]
	protected bool m_sendHitInformation = false;
	[SerializeField]
	protected Camera m_observer = null;
	[SerializeField]
	protected GUIView m_viewAreaManipulating = null;
	[SerializeField]
	protected Transform m_target = null;
	[SerializeField]
	protected LayerMask m_mask = 0;
	// Hack to allow multiple sliders to controll the same thing
	[SerializeField]
	protected Vector2 m_areaManipulationSizeOffset = Vector2.zero;

	// Member

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	protected bool hasHitEvent
	{
		get { return OnHitEvent != null; }
	}


	protected bool SendHitInformation
	{
		get	{ return this.m_sendHitInformation; }
	}


	protected GUIView ViewAreaManipulating
	{
		get
		{
			if(this.m_viewAreaManipulating == null)
			{
				return this;
			}
			return this.m_viewAreaManipulating;
		}
		set { m_viewAreaManipulating = value; }
	}


	protected LayerMask Mask
	{
		get	{ return this.m_mask; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected virtual GameObject CheckForHit()
	{
		return RayInTextureHit(
			ViewAreaManipulating.viewGuiTexture.GetScreenRect(),
			inputManagerCursorState.Position,
			ViewAreaManipulating.sizeInPointSpace, //new Vector2(origionalPositionRight - origionalPositionLeft, sizeInPointSpace.y),
			m_observer,
			m_target,
			m_mask,
			ViewAreaManipulating.finalLayer);
	}


	protected override void ProcessOnDownState()
	{
		if(inputManagerCursorState == null)
		{
			return;
		}

		// Allow user to keep the cursor down even though the cursor is not above it.
		if(//inputManagerCursorState.ActiveInput != this
			//||
			inputManagerCursorState.InitialInput != this)
		{
			currentState = ButtonState.Enabled;
			return;
		}

		if(inputManagerCursorState.InitialInput == this
			&& inputManagerCursorState.PressedState == CursorState.PressState.Released)
		{
			if(SendHitInformation
				&& hasHitEvent)
			{
				currentState = GUIViewButton.ButtonState.Selected;
			}
			else
			{
				currentState = ButtonState.Enabled;
			}
			return;
		}
	}


	protected override void ProcessSelectedState()
	{
		if(inputManagerCursorState == null)
		{
			return;
		}

		GameObject hit = CheckForHit();

		if(hit != null)
		{
			OnHitEvent(hit);
		}

		currentState = ButtonState.Enabled;
	}

	#endregion
}