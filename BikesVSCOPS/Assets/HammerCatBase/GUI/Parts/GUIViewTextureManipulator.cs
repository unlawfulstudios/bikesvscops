using UnityEngine;

[ExecuteInEditMode]
public abstract class GUIViewTextureManipulator : GUIViewTextureHitable
{
	#region Public Defines

	public delegate void OnManipulateDelegate();


	public delegate void OnFinishedDelegate();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public event OnManipulateDelegate OnManipulateEvent = null;
	public event OnFinishedDelegate OnFinishedEvent = null;


	public virtual Transform Target
	{
		get	{ return this.m_target;	}
		set	{ m_target = value;	}
	}

	public Vector2 Multiplier
	{
		get	{ return this.m_multiplier; }
		set { m_multiplier = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected bool AllowAxisX
	{
		get	{ return this.m_allowAxisX;	}
		set	{ m_allowAxisX = value;	}
	}


	protected bool AllowAxisY
	{
		get	{ return this.m_allowAxisY;	}
		set	{ m_allowAxisY = value;	}
	}


	protected bool AllowOneAxisAtATime
	{
		get	{ return this.m_allowOneAxisAtATime;	}
		set	{ m_allowOneAxisAtATime = value;	}
	}


	protected bool PerformedManipulation
	{
		get	{ return this.ManipulationAxis.x == 1 || this.ManipulationAxis.y == 1; }
	}


	protected Vector2 ManipulationAxis
	{
		get	{ return this.m_manipulationAxis; }
		set	{ m_manipulationAxis = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void ProcessEnabledState()
	{
		if(inputManagerCursorState == null)
		{
			return;
		}

		if(inputManagerCursorState.ActiveInput == this
			&& inputManagerCursorState.InitialInput == this)
		{
			ManipulationAxis = Vector2.zero;
			ResetManipulationValues();
			currentState = ButtonState.OnDown;
			return;
		}
	}


	protected override void ProcessOnDownState()
	{
		if(inputManagerCursorState == null)
		{
			return;
		}

		// Allow user to keep the cursor down even though the cursor is not above it.
		if(//inputManagerCursorState.ActiveInput != this
			//||
			inputManagerCursorState.InitialInput != this)
		{
			if(OnFinishedEvent != null)
			{
				OnFinishedEvent();
			}
			currentState = ButtonState.Enabled;
			return;
		}

		if(inputManagerCursorState.InitialInput == this
			&& inputManagerCursorState.PressedState == CursorState.PressState.Released)
		{
			if(!PerformedManipulation
				&& SendHitInformation
				&& hasHitEvent)
			{
				currentState = GUIViewButton.ButtonState.Selected;
			}
			else
			{
				if(OnFinishedEvent != null)
				{
					OnFinishedEvent();
				}
				currentState = ButtonState.Enabled;
			}
			return;
		}

		if(Target != null)
		{
			bool calcX = AllowAxisX && inputManagerCursorState.Delta.x != 0f;
			bool calcY = AllowAxisY && inputManagerCursorState.Delta.y != 0f;

			if(AllowOneAxisAtATime)
			{
				if(!PerformedManipulation)
				{
					if(calcX && calcY)
					{
						if(inputManagerCursorState.TotalDelta.y > inputManagerCursorState.TotalDelta.x)
						{
							calcX = false;
						}
						else
						{
							calcY = false;
						}
					}
				}
				else
				{
					if(calcY && ManipulationAxis.x == 1)
					{
						calcY = false;
					}
					if(calcX && ManipulationAxis.y == 1)
					{
						calcX = false;
					}
				}
			}

			if((calcX || calcY) &&
				(inputManagerCursorState.ActiveInput == this || CanManipulationOutsideArea))
			{
				//PerformedManipulation = true;
				ManipulationAxis = new Vector2(calcX ? 1 : 0, calcY ? 1 : 0);

				ProcessManipulation(calcX, calcY);

				SendManipulationEvent();
			}
		}
	}


	protected void SendManipulationEvent()
	{
		if(OnManipulateEvent != null)
		{
			OnManipulateEvent();
		}
	}


	protected bool CanManipulationOutsideArea
	{
		get	{ return this.m_canManipulationOutsideArea; }
		set	{ m_canManipulationOutsideArea = value; }
	}


	protected virtual void ResetManipulationValues()
	{
	}

	protected abstract void ProcessManipulation(bool calcX, bool calcY);


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Members

	[SerializeField]
	protected bool m_allowAxisX = true;
	[SerializeField]
	protected bool m_allowAxisY = false;
	[SerializeField]
	protected bool m_allowOneAxisAtATime = false;
	[SerializeField]
	protected Vector2 m_multiplier = Vector2.one;
	[SerializeField]
	protected bool m_canManipulationOutsideArea = false;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private Vector2 m_manipulationAxis = Vector2.zero;

	#endregion
}