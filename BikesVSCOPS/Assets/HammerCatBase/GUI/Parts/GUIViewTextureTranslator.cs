using System.Collections.Generic;
using UnityEngine;

public class GUIViewTextureTranslator : GUIViewTextureManipulator
{
	#region Public Methods
	public float sliderFractionalAmountF
	{
		get
		{
			return AllowAxisX ? sliderFractionalAmount.x : sliderFractionalAmount.y;
		}
		set
		{
			Vector2 prevSliderAmount = sliderFractionalAmount;
			if(AllowAxisX)
			{
				prevSliderAmount.x = value;
			}
			if(AllowAxisY)
			{
				prevSliderAmount.y = value;
			}
			sliderFractionalAmount = prevSliderAmount;
		}
	}


	public Vector2 sliderFractionalAmount
	{
		get
		{
			Vector2 positionWithoutScale = TargetAsGUIView.localPositionInPointSpaceWithoutScaling;

			float y = 0f;
			if(AllowAxisY)
			{
				if(m_reverseY)
				{
					y = 1f - ((positionWithoutScale.y + (float)TranslationBoundsInPointSpace.height) / (float)TranslationBoundsInPointSpace.height);
				}
				else
				{
					y = positionWithoutScale.y / (float)TranslationBoundsInPointSpace.height;
				}
			}
			float x = AllowAxisX ? (positionWithoutScale.x / (float)TranslationBoundsInPointSpace.width) : 0f;

			return new Vector2(x, y);
		}
		set
		{

			Vector2 localPos = new Vector2((float)TranslationBoundsInPointSpace.x, (float)TranslationBoundsInPointSpace.y);
			if(AllowAxisX)
			{
				localPos.x = value.x * (float)TranslationBoundsInPointSpace.width;
			}
			if(AllowAxisY)
			{
				if(m_reverseY)
				{
					localPos.y = (1f - value.y) * (float)TranslationBoundsInPointSpace.height - (float)TranslationBoundsInPointSpace.height;
				}
				else
				{
					localPos.y = value.y * (float)TranslationBoundsInPointSpace.height;
				}

//			string formatedString = string.Format("{0:F4}", localPos.y);
//			Debug.Log(formatedString);
//				if(name == "sliderBar parts")
//				{
//					string debug = string.Format("FRACTIONAL VALUE: {0:F4} NEW POS: {1:F4}", value.y, localPos.y);
//					//Debug.Log(debug);
//				}
			}

			ApplyNewPosition(localPos);
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Members

	[SerializeField]
	protected bool m_reverseY = true;
	[SerializeField]
	protected bool m_translateBasedOnDelta = false;
	[SerializeField]
	protected Vector2 m_range = Vector2.up;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected GUIView TargetAsGUIView
	{
		get	{ return Target.GetComponent<GUIView>(); }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected void ApplyNewPosition(Vector2 position)
	{
		if(TargetAsGUIView.localPositionInPointSpace != position)
		{
			TargetAsGUIView.localPositionInPointSpace = position;
			TargetAsGUIView.ReCalculateNow(true);
		}
	}


	protected virtual Rect TranslationBoundsInScreenSpace
	{
		get { return ViewAreaManipulating.viewGuiTexture.GetScreenRect(); }
	}


	protected virtual Rect TranslationBoundsInPointSpace
	{
		get
		{
			return new Rect(
				ViewAreaManipulating.localPositionInPointSpace.x,
				ViewAreaManipulating.localPositionInPointSpace.y,
				ViewAreaManipulating.sizeInPointSpace.x,
				ViewAreaManipulating.sizeInPointSpace.y
				);
		}
	}


	protected override GameObject CheckForHit()
	{
		//Debug.Log("Checking for hit");
		 
		// Make a list of all the views in the scene.
		List<GUIView> viewsInScene = new List<GUIView>(GameObject.FindObjectsOfType(typeof(GUIView)) as GUIView[]);

		// Sort them by Z
		viewsInScene.Sort(delegate(GUIView viewA, GUIView viewB)
		{
			return (-viewA.finalLayer).CompareTo(-viewB.finalLayer);
		});

		foreach(GUIView viewIt in viewsInScene)
		{
			if((viewIt.gameObject.layer | Mask) == viewIt.gameObject.layer)
			{
				if(viewIt.ContainsPoint(GUIViewInputManager.Instance.CurrentCursorState.Position))
				{
					Debug.Log(viewIt.name);
					return viewIt.gameObject;
				}
			}
		}

		return base.CheckForHit();
	}


	protected override void ProcessManipulation(bool calcX, bool calcY)
	{
		Rect textureScreenRect = TranslationBoundsInScreenSpace;
		Vector2 clickPosOnTexture = inputManagerCursorState.Position - new Vector2(
			textureScreenRect.x,
			textureScreenRect.y);

		if(!m_translateBasedOnDelta)
		{
			SnapToManipulation(calcX, calcY, textureScreenRect, clickPosOnTexture);
		}
		else
		{
			TranslateToManipulation(calcX, calcY, textureScreenRect, clickPosOnTexture);
		}

	}


	protected override void ResetManipulationValues()
	{
		m_hasSetLastPosition = false;
		m_lastFractionalClickPosition = Vector2.zero;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void TranslateToManipulation(bool calcX, bool calcY, Rect textureScreenRect, Vector2 clickPosOnTexture)
	{
		float yPos = 0f;
		if(m_reverseY)
		{
			yPos = 1f - clickPosOnTexture.y / (float)textureScreenRect.height;
		}
		else
		{
			yPos = clickPosOnTexture.y / (float)textureScreenRect.height;
		}

		Vector2 fractionalClickAmount = new Vector2(
			Mathf.Clamp(clickPosOnTexture.x / (float)textureScreenRect.width, 0f, 1f),
			Mathf.Clamp(yPos, 0f, 1f));

		fractionalClickAmount.Scale(Multiplier);



		if(!m_hasSetLastPosition)
		{
			m_hasSetLastPosition = true;
			m_lastFractionalClickPosition = fractionalClickAmount;
			return;
		}

		Vector2 delta = fractionalClickAmount - m_lastFractionalClickPosition;
		delta = new Vector2(calcX ? delta.x : 0f, calcY ? delta.y : 0f);
		if(delta.x == 0f && delta.y == 0f)
		{
			return;
		}

		m_lastFractionalClickPosition = fractionalClickAmount;

//		if(name == "sliderBar parts")
//		{
//			string debug = string.Format("DELTA: {0:F4} FRAC: {1:F4} CURRENT: {2:F4}", delta.y, fractionalClickAmount.y, sliderFractionalAmount.y);
//			Debug.Log(debug);
//		}

		Vector2 newSlderAmount = sliderFractionalAmount + delta;
		newSlderAmount = new Vector2(
			Mathf.Clamp(newSlderAmount.x, m_range.x, m_range.y),
			Mathf.Clamp(newSlderAmount.y, m_range.x, m_range.y));

		sliderFractionalAmount = newSlderAmount;
	}


	private void SnapToManipulation(bool calcX, bool calcY, Rect textureScreenRect, Vector2 clickPosOnTexture)
	{
		float yPos = 0f;
		if(m_reverseY)
		{
			yPos = 1f - clickPosOnTexture.y / (float)textureScreenRect.height;
		}
		else
		{
			yPos = clickPosOnTexture.y / (float)textureScreenRect.height;
		}

		Vector2 fractionalClickAmount = new Vector2(
			Mathf.Clamp(clickPosOnTexture.x / (float)textureScreenRect.width, 0f, 1f),
			Mathf.Clamp(yPos, 0f, 1f));

		fractionalClickAmount.Scale(Multiplier);
		fractionalClickAmount = new Vector2(
			Mathf.Clamp(fractionalClickAmount.x, m_range.x, m_range.y),
			Mathf.Clamp(fractionalClickAmount.y, m_range.x, m_range.y));

		sliderFractionalAmount = fractionalClickAmount;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private Vector2 m_lastFractionalClickPosition = Vector2.zero;
	private bool m_hasSetLastPosition = false;

	#endregion
}