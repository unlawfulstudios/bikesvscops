using UnityEngine;

[ExecuteInEditMode]
public class GUIView : MonoBehaviour, GUIViewBase
{
	#region Public Defines

	public delegate bool AllowToggleDelegate();


	public delegate void PerformedRecalculationDelegate();


	public delegate void ToggleEventMethod(bool toggleValue);

	#endregion


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Members

	// Exposed

	// Editor Exposed
	[SerializeField]
	private Vector2 m_sizeInPoints = Vector2.one;
	[SerializeField]
	private Vector2 m_scale = Vector2.one;
	[SerializeField]
	private Vector2 m_localPositionInPoints = Vector2.zero;
	[SerializeField]
	private float m_layerAboveParent = 0f;
	[SerializeField]
	private GUIViewAnchor m_anchor = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
	[SerializeField]
	private GUIViewAnchor m_alignment = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
	[SerializeField]
	private bool m_scaleXWithScreenWidth = false;
	[SerializeField]
	private bool m_scaleYWithScreenWidth = false;
	[SerializeField]
	private bool m_pinnedToLeft = false;
	[SerializeField]
	private bool m_pinnedToRight = false;
	private Transform m_parent = null;
	private GUIViewBase m_parentBase = null;

	// Member
	private bool m_toggle = true;
	private Vector2 m_postionInPoints = Vector2.zero;
	private float m_finalLayer = 0f;
	private GUITexture m_viewGuiTexture = null;
	private static readonly Rect m_defaultTextureRect = new Rect(0f, 0f, 1f, 1f);
	private bool m_showDebug = false;
	private Vector2 m_midAnchor = Vector2.zero;
	private bool m_recalculatesViewInformation = true;
	private bool m_hasRecalulatAtLeastOnce = false;
	private Rect m_rectInScreenSpace = new Rect(0f, 0f, 0f, 0f);
	// Pinning variables
	private bool m_notGatheredPinnedData = true;
	private float m_childPercentLeft = 0f;
	private float m_childPercentRight = 0f;
	private float m_origionalAnchorPositionInPointSpaceX = 0f;
	private float m_origionalPositionLeft = 0f;
	private float m_origionalPositionRight = 0f;
	private float m_origionalPositionCenter = 0f;
	private bool m_applyRecalculation = true;
	private float m_pinnedWidth = 0f;


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public AllowToggleDelegate AllowToggleMethod = null;


	public event PerformedRecalculationDelegate PerformedPreRecalculationEvent = null;
	public event PerformedRecalculationDelegate PerformedRecalculationEvent = null;
	public event PerformedRecalculationDelegate PerformedRecalculationEventPostChildren = null;
	public event ToggleEventMethod ToggleEvent = null;


	public Vector2 localPositionInPointSpace
	{
		get
		{
			Vector2 scaledPosition = new Vector2(this.m_localPositionInPoints.x * scale.x, this.m_localPositionInPoints.y * scale.y);
			return scaledPosition;
		}
		set	{ this.m_localPositionInPoints = value; }
	}


	public Vector2 localPositionInPointSpaceWithoutScaling
	{
		get { return m_localPositionInPoints; }
	}


	public Vector2 positionInPointSpaceAbsolute
	{
		get	{ return this.m_postionInPoints; }
	}


	public Vector2 positionInPointSpace
	{
		get	{ return this.m_postionInPoints - localAlignmentPositionInPointSpace; }
	}


	public Vector2 sizeInPointSpace
	{
		get
		{
			Vector2 scaledSize = new Vector2(this.m_sizeInPoints.x * scale.x, this.m_sizeInPoints.y * scale.y);
			return scaledSize;
		}
		set { m_sizeInPoints = value; }
	}


	public Vector2 sizeInPointSpaceWithoutScale
	{
		get
		{
			return this.m_sizeInPoints;
		}
		set { m_sizeInPoints = value; }
	}


	public Vector2 midAnchorPoint
	{
		get	{ return this.m_midAnchor; }
		protected set { m_midAnchor = value; }
	}


	public GUIViewAnchor anchor
	{
		get	{ return this.m_anchor;	}
		set
		{
			if(m_anchor != value)
			{
				m_anchor = value;
				ReCalculate();
			}
		}
	}


	public GUIViewAnchor alignment
	{
		get	{ return this.m_alignment;	}
		set
		{
			if(m_alignment != value)
			{
				m_alignment = value;
				ReCalculate();
			}
		}
	}


	public Vector2 localScale
	{
		get	{ return this.m_scale; }
		set	{ m_scale = value; }
	}


	public Vector2 scale
	{
		get	{ return Vector2.Scale(this.m_scale, (viewParent != null ? viewParent.scale : Vector2.one)); }
		set	{ m_scale = value; }
	}


	public float finalLayer
	{
		get { return this.m_finalLayer; }
		protected set { m_finalLayer = value; }
	}


	public virtual GUIViewBase viewParent
	{
		get
		{
			if(transform.parent != m_parent)
			{
				m_parent = null;
				m_parentBase = null;

				if(transform.parent != null)
				{
					GUIViewBase parentView = transform.parent.GetComponent(typeof(GUIViewBase)) as GUIViewBase;
					if(parentView != null)
					{
						m_parent = transform.parent;
						m_parentBase = parentView;

						GUIViewController controller = m_parent.GetComponent(typeof(GUIViewController)) as GUIViewController;
						if(controller != null)
						{
							m_parent = m_parent.parent;
							m_parentBase = null;
							if(controller.transform.parent != null)
							{
								m_parentBase = controller.transform.parent.GetComponent(typeof(GUIViewBase)) as GUIViewBase;
							}
						}
					}
				}
			}

			return m_parentBase;
		}
		set
		{
			if(value == null)
			{
				transform.parent = null;
			}
			else
			{
				if(value is GUIView)
				{
					transform.parent = ((GUIView)value).gameObject.transform;
				}
				else if(value is GUIViewCore)
				{
					transform.parent = ((GUIViewCore)value).gameObject.transform;
				}
				else
				{
					Debug.LogError("Invalid parent");
				}
			}
			ReCalculate();
		}
	}

	
	public bool showDebug
	{
		get	{ return this.m_showDebug; }
		set
		{
			if(GUIViewCore.Instance.EditorDebugEnabled)
			{
				m_showDebug = value;
				if(m_showDebug)
				{
					viewGuiTexture.enabled = true;
					viewGuiTexture.texture = debugTexture;
					viewGuiTexture.color = debugColor;
				}
				else
				{
					RemoveDebugTexture();
					viewGuiTexture.color = defaultColor;
				}
			}
		}
	}


	public bool toggle
	{
		get	{ return this.m_toggle;	}
		set
		{
			if(AllowToggleMethod != null)
			{
				value = value && AllowToggleMethod();
			}
			viewGuiTexture.enabled = m_toggle = value;



			ProcessToggle();

			if(ToggleEvent != null)
			{
				ToggleEvent(m_toggle);
			}
		}
	}


	public GUITexture viewGuiTexture
	{
		get
		{
			if(m_viewGuiTexture == null)
			{
				m_viewGuiTexture = gameObject.GetComponent<GUITexture>();
				if(m_viewGuiTexture == null)
				{
					m_viewGuiTexture = gameObject.AddComponent<GUITexture>();
					m_viewGuiTexture.pixelInset = defaultTextureRect;
				}
			}

			return m_viewGuiTexture;
		}
	}


	public virtual Color defaultColor
	{
		get { return GUIViewCore.Instance.DefaultColor; }
	}


	public float layerAboveParent
	{
		get	{ return this.m_layerAboveParent; }
		set	{ m_layerAboveParent = value; }
	}


	public Rect screenSpaceRect
	{
		get { return this.viewGuiTexture.GetScreenRect(); }
	}


	public Rect rectInScreenSpace
	{
		get	{ return this.m_rectInScreenSpace; }
	}


	public float origionalPositionLeft
	{
		get	{ return this.m_origionalPositionLeft; }
		private set { m_origionalPositionLeft = value; }
	}


	public float origionalPositionRight
	{
		get	{ return this.m_origionalPositionRight; }
		private set { m_origionalPositionRight = value; }
	}


	public float origionalPositionCenter
	{
		get	{ return this.m_origionalPositionCenter; }
		private set { m_origionalPositionCenter = value; }
	}


	public bool hasRunInitialCalculate
	{
		get { return m_hasRecalulatAtLeastOnce || !recalculatesViewInformation; }
	}


	public bool pinnedToRight
	{
		get	{ return this.m_pinnedToRight; }
		set	{ m_pinnedToRight = value; }
	}


	public bool pinnedToLeft
	{
		get	{ return this.m_pinnedToLeft; }
		set	{ m_pinnedToLeft = value; }
	}


	public float pinnedWidth
	{
		get { return this.m_pinnedWidth; }
	}


	public virtual Color color
	{
		get { return debugColor; }
		set { ; }
	}


	public virtual float alpha
	{
		get { return 0f; }
		set { ; }
	}


	public bool recalculatesViewInformation
	{
		get	{ return this.m_recalculatesViewInformation; }
		set	{ m_recalculatesViewInformation = value; }
	}

	
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public bool ContainsPoint(Vector2 point)
	{
		if(!gameObject.activeInHierarchy)
		{
			return false;
		}
		return viewGuiTexture.GetScreenRect().Contains(point);
	}


	public void ReCalculate()
	{
		if(GUIViewCore.Instance == null)
		{
			return;
		}
		if(!GUIViewCore.Instance.EditModeEnabled)
		{
			applyRecalculation = true;
		}
		else
		{
			ReCalculateNow(true);
		}
	}


	public void ReCalculateNow(bool processChildren)
	{
		ProcessRecalculation(processChildren);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected Vector2 localAnchorPositionInPointSpace
	{
		get
		{
			Vector2 adjustedPositionAcordingToParentSize =
				new Vector2(
					viewParent.midAnchorPoint.x * (float)anchor.Horizontal,
					viewParent.midAnchorPoint.y * ((float)anchor.Vertical));
			return adjustedPositionAcordingToParentSize + viewParent.positionInPointSpaceAbsolute;
		}
	}


	protected Vector2 localAlignmentPositionInPointSpace
	{
		get
		{
			Vector2 adjustedPositionAcordingToSize =
				new Vector2(
					midAnchorPoint.x + (-midAnchorPoint.x * (1f + (float)alignment.Horizontal)),
					midAnchorPoint.y + (-midAnchorPoint.y * (1f + (float)alignment.Vertical)));

			//Debug.Log("Alignment: " + adjustedPositionAcordingToParentSize);
			return adjustedPositionAcordingToSize;
		}
	}


	protected virtual Texture debugTexture
	{
		get	{ return GUIViewCore.Instance.DebugTexture; }
	}


	protected static Rect defaultTextureRect
	{
		get	{ return m_defaultTextureRect; }
	}


	protected virtual Color debugColor
	{
		get	{ return GUIViewCore.Instance.DebugColor; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Static Methods

	protected static Vector3 RayInTextureDelta(Rect textureScreenRect,
		Vector2 posInScreenSpace, Vector2 deltaInScreenSpace, ref Vector3 prevRayPoint, Vector2 sizeInPointSpace, Camera renderingCamera, Vector3 targetPosition,
		float layer)
	{
		Vector3 currentPoint = RayInTextureEndPoint(
			textureScreenRect,
			posInScreenSpace,
			sizeInPointSpace,
			renderingCamera,
			targetPosition,
			layer);

		if(prevRayPoint.x == -Mathf.Infinity)
		{
			prevRayPoint = currentPoint;
			return Vector3.zero;
		}

		Vector3 prevPoint = RayInTextureEndPoint(
			textureScreenRect,
			posInScreenSpace - deltaInScreenSpace,
			sizeInPointSpace,
			renderingCamera,
			targetPosition,
			layer);

		//Debug.Log(string.Format("WORLD POINT: Current {0} prev {1}", currentPoint, prevRayPoint));
		//Debug.DrawLine(renderingCamera.transform.position, currentPoint, Color.red, 3f);
		//Debug.DrawLine(renderingCamera.transform.position, prevPoint, Color.blue, 3f);

		Vector3 toCurrentPoint = currentPoint - prevPoint;

		prevRayPoint = currentPoint;

		return toCurrentPoint;
	}


	protected static Vector3 RayInTextureEndPoint(Rect textureScreenRect,
		Vector2 posInScreenSpace, Vector2 sizeInPointSpace, Camera renderingCamera, Vector3 targetPosition, float layer)
	{
//		Vector3 diffOfScreenAndCameraRect = new Vector3(
//			renderingCamera.pixelRect.x - textureScreenRect.x,
//			renderingCamera.pixelRect.y - textureScreenRect.y,
//			renderingCamera.transform.position.z - layer);
//
//		Vector3 posIn3D = new Vector3(posInScreenSpace.x, posInScreenSpace.y, 0f);

		//Vector3 posAdjustedToCurrentCamera = posIn3D + diffOfScreenAndCameraRect;

		//float dist = Vector3.Distance(renderingCamera.transform.position, targetPosition);

		Ray ray = renderingCamera.ScreenPointToRay(
			posInScreenSpace);

		ray.direction.Normalize();

		//float dist = Vector3.Distance(renderingCamera.transform.position, targetPosition);

		Vector3 positionAlongRay = ray.origin + ray.direction;

		return positionAlongRay;
		//float distanceAlongRay = -ray.origin.y / ray.direction.y;
		//return ray.GetPoint(distanceAlongRay);
	}


	protected static GameObject RayInTextureHit(Rect textureScreenRect, Vector2 posInScreenSpace,
		Vector2 sizeInPointSpace, Camera renderingCamera, Transform target,
		LayerMask mask, float layer)
	{
		Ray actualCast = RayInTexture(
			textureScreenRect,
			posInScreenSpace,
			sizeInPointSpace,
			renderingCamera,
			layer);

		float dist = (target != null) ? Vector3.Distance(renderingCamera.transform.position, target.position) : Mathf.Infinity;

		//Ray distanceRay = actualCast.origin + actualCast.direction * dist;

		Debug.DrawRay(actualCast.origin, actualCast.direction * dist, Color.white, 2f);

		RaycastHit hit;
		if(Physics.Raycast(actualCast, out hit, dist, 1 << mask))
		{
			if(hit.collider.gameObject)
			{
				return hit.collider.gameObject;
			}
		}

		return null;
	}


	protected static Ray RayInTexture(Rect textureScreenRect,
		Vector2 posInScreenSpace, Vector2 sizeInPointSpace, Camera renderingCamera, float layer)
	{
		Vector3 diffOfScreenAndCameraRect = new Vector3(
			renderingCamera.pixelRect.x - textureScreenRect.x,
			renderingCamera.pixelRect.y - textureScreenRect.y,
			0f);

		Vector3 posIn3D = new Vector3(posInScreenSpace.x, posInScreenSpace.y, 0f);

		Vector3 posAdjustedToCurrentCamera = posIn3D + diffOfScreenAndCameraRect;

		//Debug.Log(posAdjustedToCurrentCamera);

		Ray fixedPosition = renderingCamera.ScreenPointToRay(posAdjustedToCurrentCamera);

		//Debug.DrawRay(fixedPosition.origin, fixedPosition.direction * 100f, Color.white, 2f);

		//Debug.DrawRay(fixedPosition.origin, fixedPosition.direction, Color.green, 30f);

		return fixedPosition;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected void Start()
	{
		viewGuiTexture.enabled = toggle;
	}


	protected virtual void OnEnable()
	{
		transform.localScale = Vector3.forward;
		applyRecalculation = true;
	}


	protected virtual void LateUpdate()
	{
		if(applyRecalculation)
		{
			ProcessRecalculation(true);
		}
	}


	protected virtual void RemoveDebugTexture()
	{
		viewGuiTexture.texture = null;
		viewGuiTexture.enabled = false;
	}


	protected void UpdateChildren(bool immediate)
	{
		GUIView[] childGUIObjects = GetComponentsInChildren<GUIView>();
		foreach(GUIView view in childGUIObjects)
		{
			if(view.transform.parent == transform)
			{
				if(immediate)
				{
					view.ReCalculateNow(true);
				}
				else
				{
					view.ReCalculate();
				}
			}
		}
	}


	protected virtual void ProcessToggle()
	{
		BasicToggle();
	}


	protected void BasicToggle()
	{
		ReCalculate();
		GUIView[] childView = GetComponentsInChildren<GUIView>();
		foreach(GUIView view in childView)
		{
			// Only toggling immediate childs.
			if(view.transform.parent == transform)
			{
				view.toggle = m_toggle;
				view.ReCalculate();
			}
		}
	}


	protected virtual void SetPreAdditionalSettings()
	{
		if(PerformedPreRecalculationEvent != null)
		{
			PerformedPreRecalculationEvent();
		}
	}


	protected virtual void SetAdditionalSettings()
	{
		if(PerformedRecalculationEvent != null)
		{
			PerformedRecalculationEvent();
		}
	}


	protected virtual void SetAdditionalSettingsPostChildren()
	{
		if(PerformedRecalculationEventPostChildren != null)
		{
			PerformedRecalculationEventPostChildren();
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private bool scaleXWithScreenWidth
	{
		get	{ return this.m_scaleXWithScreenWidth; }
	}


	private bool scaleYWithScreenWidth
	{
		get	{ return this.m_scaleYWithScreenWidth; }
	}


	private float parentLeft
	{
		get	{ return viewParent.midAnchorPoint.x * (float)GUIViewAnchorX.Left; }
	}


	private float parentRight
	{
		get	{ return viewParent.midAnchorPoint.x * (float)GUIViewAnchorX.Right; }
	}


	private float ParentsCurrentWidth
	{
		get	{ return parentRight - parentLeft; }
	}

	
	private float origionalAnchorPositionInPointSpaceX
	{
		get { return m_origionalAnchorPositionInPointSpaceX; }
		set { m_origionalAnchorPositionInPointSpaceX = value; }
	}


	private bool applyRecalculation
	{
		get	{ return this.m_applyRecalculation; }
		set	{ m_applyRecalculation = value;	}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void ProcessRecalculation(bool runChildrenImmediatly)
	{
//		if(name == "Bar")
//		{
//			Debug.Log("running");
//		}
		if(recalculatesViewInformation)
		{
			// If moving or duplicating gui objects, we might not have a parent for a moment.
			if(viewParent == null || !viewParent.hasRunInitialCalculate)
			{
				return;
			}

			SetPreAdditionalSettings();

			if(name == "view build robot")
			{
				int x = 0;
				++x;
			}
			if(name == "view robot")
			{
				int x = 0;
				++x;
			}

			{
				// Reset the transfrom and only set the z.
				m_finalLayer = viewParent.finalLayer + layerAboveParent;
				transform.localPosition = Vector3.forward * m_finalLayer;
			}

			midAnchorPoint = (sizeInPointSpace * 0.5f);
			m_postionInPoints = localPositionInPointSpace + localAnchorPositionInPointSpace + localAlignmentPositionInPointSpace;

			{
				Vector2 sizeInScreenSpace = GUIViewCore.Instance.GetSizeInScreenSpace(sizeInPointSpace);
				m_rectInScreenSpace.width = sizeInScreenSpace.x;
				m_rectInScreenSpace.height = sizeInScreenSpace.y;
			}

			ReCalculatePinning();

			{
				Vector2 posInScreenSpace = GUIViewCore.Instance.GetPositionInScreenSpace(m_postionInPoints);
				m_rectInScreenSpace.x = posInScreenSpace.x;
				m_rectInScreenSpace.y = posInScreenSpace.y;
			}

			ReCalculateZoomScale();

			viewGuiTexture.pixelInset = rectInScreenSpace;

			m_hasRecalulatAtLeastOnce = true;
		}

		SetAdditionalSettings();

		UpdateChildren(runChildrenImmediatly);

		SetAdditionalSettingsPostChildren();

		applyRecalculation = false;
	}


	private void ReCalculatePinning()
	{
		if(m_notGatheredPinnedData)
		{
			float parentWidth = viewParent.origionalPositionRight - viewParent.origionalPositionLeft;

			float adjustedPositionAcordingToParentSizeX = (parentWidth * 0.5f) * (float)anchor.Horizontal
					+ viewParent.origionalPositionLeft;

			origionalAnchorPositionInPointSpaceX = localPositionInPointSpace.x + adjustedPositionAcordingToParentSizeX + localAlignmentPositionInPointSpace.x;

			// Get pinned information
			origionalPositionCenter = origionalAnchorPositionInPointSpaceX + sizeInPointSpace.x * 0.5f;

			origionalPositionLeft = origionalAnchorPositionInPointSpaceX;
			origionalPositionRight = origionalAnchorPositionInPointSpaceX + sizeInPointSpace.x;

			// Child percents (Calculate only at start)
			m_childPercentLeft = (origionalPositionLeft - viewParent.origionalPositionLeft) / parentWidth;
			m_childPercentRight = (origionalPositionRight - viewParent.origionalPositionLeft) / parentWidth;

			m_notGatheredPinnedData = false;
		}

		if(pinnedToLeft || pinnedToRight)
		{
			float parentWidthCurrent = ParentsCurrentWidth;

			float currentLeftPoint = 0f;
			float currentRightPoint = 0f;

			if(pinnedToLeft && !pinnedToRight)
			{
				currentLeftPoint = viewParent.positionInPointSpaceAbsolute.x + parentWidthCurrent * m_childPercentLeft;
				currentRightPoint = m_postionInPoints.x + sizeInPointSpace.x;

			}
			else if(!pinnedToLeft && pinnedToRight)
			{
				currentRightPoint = viewParent.positionInPointSpaceAbsolute.x + parentWidthCurrent * m_childPercentRight;
				currentLeftPoint = m_postionInPoints.x;
			}
			else
			{
				currentLeftPoint = viewParent.positionInPointSpaceAbsolute.x + parentWidthCurrent * m_childPercentLeft;
				currentRightPoint = viewParent.positionInPointSpaceAbsolute.x + parentWidthCurrent * m_childPercentRight;
			}

			m_pinnedWidth = currentRightPoint - currentLeftPoint;
			midAnchorPoint = new Vector2(m_pinnedWidth * 0.5f, midAnchorPoint.y);

			m_postionInPoints.x = currentLeftPoint;

			Vector2 pinnedsizeInScreenSpace = GUIViewCore.Instance.GetSizeInScreenSpace(new Vector2(m_pinnedWidth, sizeInPointSpace.y));
			m_rectInScreenSpace.width = pinnedsizeInScreenSpace.x;
			m_rectInScreenSpace.height = pinnedsizeInScreenSpace.y;
		}
	}


	private void ReCalculateZoomScale()
	{
		if(scaleXWithScreenWidth || scaleYWithScreenWidth)
		{
			float zoomScale = GUIViewCore.Instance.ZoomScale.x;
			if((scaleXWithScreenWidth
					&& scaleYWithScreenWidth
					&& zoomScale < 1f)
					|| (!scaleYWithScreenWidth && scaleYWithScreenWidth))
			{
				zoomScale = GUIViewCore.Instance.ZoomScale.y;
			}
			m_rectInScreenSpace.width *= zoomScale;
			m_rectInScreenSpace.height *= zoomScale;

			midAnchorPoint *= zoomScale;
		}
	}

	#endregion
}