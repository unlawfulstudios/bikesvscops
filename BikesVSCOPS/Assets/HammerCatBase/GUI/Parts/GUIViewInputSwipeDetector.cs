using UnityEngine;

public class GUIViewInputSwipeDetector : GUIViewTextureManipulator
{
	#region Public Defines

	public delegate void SwipeDetectedDelegate(Vector2 swipeVelocites);

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Events

	public event SwipeDetectedDelegate SwipeDetectedEvent;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void ProcessToggle()
	{
		base.ProcessToggle();
		if(toggle)
		{
			SetInputUp();
		}
	}


	protected override GameObject CheckForHit()
	{
		return null;
	}


	protected override void ProcessManipulation(bool calcX, bool calcY)
	{
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	public float CursorVelocityNeededToFastSwipe
	{
		get	{ return this.m_cursorVelocityNeededToFastSwipe; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void SetInputUp()
	{
		{
			OnFinishedEvent -= DetectSwipes;
			OnFinishedEvent -= DetectSwipes;
			OnFinishedEvent -= DetectSwipes;
		}
		OnFinishedEvent += DetectSwipes;
	}


	private void DetectSwipes()
	{
		if(SwipeDetectedEvent == null)
		{
			return;
		}

		Vector2 swipedVelocity = GUIViewInputManager.Instance.CurrentCursorState.VelocityTotal;

		float swipeXAbsVelocity = Mathf.Abs(swipedVelocity.x);
		if(swipeXAbsVelocity < CursorVelocityNeededToFastSwipe
			|| !AllowAxisX)
		{
			swipedVelocity.x = 0f;
		}
		float swipeYAbsVelocity = Mathf.Abs(swipedVelocity.y);
		if(swipeYAbsVelocity < CursorVelocityNeededToFastSwipe
			|| !AllowAxisY)
		{
			swipedVelocity.y = 0f;
		}

		if(swipedVelocity.x != 0f || swipedVelocity.y != 0f)
		{
			SwipeDetectedEvent(swipedVelocity);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private float m_cursorVelocityNeededToFastSwipe = 400f;

	#endregion
}