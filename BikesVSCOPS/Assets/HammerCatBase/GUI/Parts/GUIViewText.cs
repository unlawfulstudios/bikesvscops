using UnityEngine;

[RequireComponent(typeof(GUIText))]
[ExecuteInEditMode]
public class GUIViewText : GUIView
{
	#region Members

	// Editor Exposed
	[SerializeField]
	private string m_displayText = "";
	[SerializeField]
	private Color m_color = Color.white;
	[SerializeField]
	private int m_fontSize = 0;
	[SerializeField]
	private Font m_textFont = null;
	[SerializeField]
	private Texture m_fontTexture = null;
	[SerializeField]
	private bool m_allowEditingFontSizeInRuntime = false;
	[SerializeField]
	private bool m_shadow = false;
	[SerializeField]
	private Color m_shadowColor = Color.black;
	[SerializeField]
	private Vector2 m_shadowOffset = new Vector2(0f, 2f);

	// Member
	private static readonly string SHADER_FONT_SHADOW_PATH = "Shaders/Font-Shadow";
	private Material m_shadowMaterial = null;
	private static Shader m_shaderFontShadow = null;
	private GUIText m_text = null;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public string displayText
	{
		get	{ return m_displayText; }
		set
		{
			if(value != m_displayText)
			{
				text.text = m_displayText = value;
				SetTextPosition();
				ReCalculate();
			}
		}
	}


	public override Color color
	{
		get	{ return m_color; }
		set	{ text.color = m_color = value; }
	}


	public override float alpha
	{
		get { return text.color.a; }
		set
		{
			Color prevColor = color;
			prevColor.a = value;
			color = prevColor;
		}
	}


	public Font textFont
	{
		get	{ return m_textFont; }
	}


	public Texture fontTexture
	{
		get	{ return m_fontTexture; }
	}


	public bool shadow
	{
		get { return m_shadow; }
		set
		{
			if(m_shadow != value)
			{
				m_shadow = value;
				RebuildText();
			}
		}
	}


	public Color shadowColor
	{
		get	{ return m_shadowColor; }
		set
		{
			if(!m_shadow
				|| m_shadowColor == value)
			{
				return;
			}

			if(m_shadowMaterial == null)
			{
				Debug.LogError("Missing material");
			}

			m_shadowColor = value;
			m_shadowMaterial.SetColor("_ShadowColor", m_shadowColor);
		}
	}


	public float shadowAlpha
	{
		get { return shadowColor.a; }
		set
		{
			Color prevColor = shadowColor;
			prevColor.a = value;
			shadowColor = prevColor;
		}
	}


	public Vector2 shadowOffset
	{
		get	{ return m_shadowOffset; }
		set
		{
			if(!m_shadow
				|| m_shadowOffset == value)
			{
				return;
			}

			if(m_shadowMaterial == null)
			{
				Debug.LogError("Missing material");
			}

			m_shadowOffset = value;
			m_shadowMaterial.SetFloat("_ShadowOffsetX", m_shadowOffset.x);
			m_shadowMaterial.SetFloat("_ShadowOffsetY", m_shadowOffset.y);
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void RebuildText()
	{
		text.font = textFont;

		text.alignment = alignment.ConvertToUnityAlignment();
		text.anchor = TextAnchor.MiddleLeft;

		text.fontSize = (int)(GUIViewCore.Instance.GetFontSizeInScreenSpace((float)fontSize) * scale.x);
		SetMaterials();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void OnEnable()
	{
		base.OnEnable();

		SetMaterials();
	}


	protected void OnDisable()
	{
		DestroyMaterial();
	}


	protected override void SetAdditionalSettings()
	{
		base.SetAdditionalSettings();

		SetTextPosition();
	}


	protected override void ProcessToggle()
	{
		base.ProcessToggle();
		text.enabled = toggle;

		if(toggle)
		{
			RebuildText();
		}
		text.text = displayText;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIText text
	{
		get
		{
			if(m_text == null)
			{
				m_text = gameObject.GetComponent<GUIText>();
				if(m_text == null)
				{
					m_text = gameObject.AddComponent<GUIText>();
					m_text.anchor = TextAnchor.MiddleCenter;
					m_text.alignment = TextAlignment.Center;
				}
			}
			return m_text;
		}
	}


	private int fontSize
	{
		get	{ return m_fontSize; }
	}


	private bool allowEditingFontSizeInRuntime
	{
		get	{ return m_allowEditingFontSizeInRuntime; }
	}


	private static Shader shaderFontShadow
	{
		get
		{
			if(m_shaderFontShadow == null)
			{
				m_shaderFontShadow = Resources.Load(SHADER_FONT_SHADOW_PATH) as Shader;
			}
			return m_shaderFontShadow;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DestroyMaterial()
	{
		if(m_shadowMaterial != null)
		{
			Object.DestroyImmediate(m_shadowMaterial);
			m_shadowMaterial = null;
		}
	}


	private void SetTextPosition()
	{
		if(allowEditingFontSizeInRuntime)
		{
			text.fontSize = (int)(GUIViewCore.Instance.GetFontSizeInScreenSpace((float)fontSize * scale.x));
		}

		Rect screenSpaceRect = text.GetScreenRect();

		Vector2 size = new Vector2(screenSpaceRect.width / scale.x, screenSpaceRect.height / scale.y);

		float alignmentOffset = size.y * scale.y;
		if(alignment.Vertical == GUIViewAnchorY.Middle)
		{
			alignmentOffset *= 0.5f;
		}
		else if(alignment.Vertical == GUIViewAnchorY.Top)
		{
			alignmentOffset = 0f;
		}

		text.pixelOffset = new Vector2(
			viewGuiTexture.pixelInset.x,
			viewGuiTexture.pixelInset.y + alignmentOffset);

		sizeInPointSpace = GUIViewCore.Instance.GetScreenSpaceSizeInPointSpace(size);
	}


	private void SetMaterials()
	{
		DestroyMaterial();

		if(textFont != null)
		{
			if(!m_shadow)
			{
				text.material = textFont.material;
			}
			else
			{
				//Debug.Log("Rebuilding");
				m_shadowMaterial = Object.Instantiate(textFont.material as Object) as Material;
				m_shadowMaterial.shader = shaderFontShadow;
				text.material = m_shadowMaterial;

				m_shadowMaterial.SetColor("_ShadowColor", m_shadowColor);
				m_shadowMaterial.SetFloat("_ShadowOffsetX", m_shadowOffset.x);
				m_shadowMaterial.SetFloat("_ShadowOffsetY", m_shadowOffset.y);

				//Debug.Log(text.material.shader.name);
			}
			//Debug.Log(text.material.shader.name);
		}
		text.color = m_color;
	}

	#endregion
}