using UnityEngine;

[ExecuteInEditMode]
public interface GUIViewBase
{
	#region Public Properties

	Vector2 localPositionInPointSpace { get; }


	Vector2 positionInPointSpaceAbsolute { get; }


	Vector2 positionInPointSpace { get; }


	Vector2 sizeInPointSpace { get; }


	Vector2 midAnchorPoint { get; }


	Vector2 scale { get; }


	float finalLayer { get; }


	GUIViewAnchor anchor { get; }


	GUIViewAnchor alignment { get; }


	GUIViewBase viewParent { get; }


	bool showDebug { get; set; }


	Rect screenSpaceRect { get; }


	float origionalPositionLeft { get; }


	float origionalPositionRight { get; }


	float origionalPositionCenter { get; }


	bool hasRunInitialCalculate { get; }

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	void ReCalculate();

	#endregion
}