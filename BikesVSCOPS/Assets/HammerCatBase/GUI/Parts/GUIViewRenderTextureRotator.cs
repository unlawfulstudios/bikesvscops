using UnityEngine;

[ExecuteInEditMode]
public class GUIViewRenderTextureRotator : GUIViewRenderTextureManipulator
{
	#region Protected Methods

	protected override void ApplyManipulation(Vector3 rotation)
	{
		if(AllowAxisX)
		{
			//Debug.Log(string.Format("ROTATE: {0} {1}%", Target.name, rotation.x));
			Target.localEulerAngles += Vector3.up * rotation.x * Mathf.PI * 2f;
		}
		if(AllowAxisY)
		{
			Target.localEulerAngles += Vector3.right * rotation.y * Mathf.PI * 2f;
		}
	}

	#endregion
}