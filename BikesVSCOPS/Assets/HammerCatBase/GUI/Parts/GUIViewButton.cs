using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

[ExecuteInEditMode]
public class GUIViewButton : GUIViewInput
{
	#region Public Defines

	public enum ButtonState
	{
		Enabled,	// Showing
		OnDown,		// Pushing on button
		Selected,	// In an activated state
		Disabled,	// Showing, but not selectable

		Total,

		DeActive = Total,	// Not showing
	};


	public delegate void RunTimeAddedButtonEvent(GUIViewButton sender);

	#endregion


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Members

	// Editor Exposed
	[SerializeField]
	private List<GUIView> m_stateGuiObjects_Enabled = new List<GUIView>();
	[SerializeField]
	private List<GUIView> m_stateGuiObjects_OnDown = new List<GUIView>();
	[SerializeField]
	private List<GUIView> m_stateGuiObjects_Selected = new List<GUIView>();
	[SerializeField]
	private List<GUIView> m_stateGuiObjects_Disabled = new List<GUIView>();
	[SerializeField]
	protected List<string> m_stateEvents_Enabled = new List<string>();
	[SerializeField]
	protected List<string> m_stateEvents_OnDown = new List<string>();
	[SerializeField]
	protected List<string> m_stateEvents_Selected = new List<string>();
	[SerializeField]
	protected List<string> m_stateEvents_Disabled = new List<string>();

	// Members
	private List<RunTimeAddedButtonEvent> m_runTimeSetEvents_Enabled = new List<RunTimeAddedButtonEvent>();
	private List<RunTimeAddedButtonEvent> m_runTimeSetEvents_OnDown = new List<RunTimeAddedButtonEvent>();
	private List<RunTimeAddedButtonEvent> m_runTimeSetEvents_Selected = new List<RunTimeAddedButtonEvent>();
	private List<RunTimeAddedButtonEvent> m_runTimeSetEvents_Disabled = new List<RunTimeAddedButtonEvent>();
	private List<RunTimeAddedButtonEvent>[] m_runTimeSetEvents = null;
	// Helper Containers
	private List<GUIView>[] m_stateGuiObjects = null;
	private List<string>[] m_allStateEvents = null;
	// Current values
	protected ButtonState m_currentState = ButtonState.DeActive;
	private GUIViewController m_parentController = null;
	private bool m_returnToSelectedState = false;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public ButtonState currentState
	{
		get	{ return m_currentState; }
		set
		{
			m_currentState = value;
			ProcessStateUpdate = null;
			switch(m_currentState)
			{
				case ButtonState.Enabled:
				{
					if(!toggle)
					{
						toggle = true;
					}

					m_returnToSelectedState = false;

					SwitchStateObjects(m_currentState);
					ReCalculateNow(true);
					CallStateEvent(m_currentState);

					inputEnabled = true;
					if(m_currentState == ButtonState.Enabled)
					{
						ProcessStateUpdate = ProcessEnabledState;
					}
					break;
				}
				case ButtonState.OnDown:
				{
					SwitchStateObjects(m_currentState);
					CallStateEvent(m_currentState);
					if(m_currentState == ButtonState.OnDown)
					{
						ProcessStateUpdate = ProcessOnDownState;
					}
					break;
				}
				case ButtonState.Selected:
				{
					m_returnToSelectedState = false;
					CallStateEvent(m_currentState);
					if(toggle)
					{
						if(!SwitchStateObjects(m_currentState))
						{
							currentState = ButtonState.Enabled;
						}
						else
						{
							if(m_currentState == ButtonState.Selected)
							{
								ProcessStateUpdate = ProcessSelectedState;
							}
						}
					}
					break;
				}
				case ButtonState.Disabled:
				{
					m_returnToSelectedState = false;
					SwitchStateObjects(m_currentState);
					inputEnabled = false;
					CallStateEvent(m_currentState);
					if(m_currentState == ButtonState.Disabled)
					{
						ProcessStateUpdate = null;
					}

					break;
				}
				case ButtonState.DeActive:
				{
					m_returnToSelectedState = false;
					inputEnabled = false;
					SwitchStateObjects(m_currentState);
					if(m_currentState == ButtonState.DeActive)
					{
						ProcessStateUpdate = null;
					}

					if(toggle)
					{
						toggle = false;
					}
					break;
				}
			}
		}
	}


	public List<GUIView>[] stateGuiObjects
	{
		get
		{
			if(m_stateGuiObjects == null)
			{
				m_stateGuiObjects = new List<GUIView>[(int)ButtonState.Total] {
					m_stateGuiObjects_Enabled,
					m_stateGuiObjects_OnDown,
					m_stateGuiObjects_Selected,
					m_stateGuiObjects_Disabled
				};
			}
			return m_stateGuiObjects;
		}
	}


	public bool forceSelect
	{
		set
		{
			if(!value && currentState == ButtonState.Selected)
			{
				currentState = ButtonState.Enabled;
			}
			else if(value)
			{
				currentState = ButtonState.Selected;
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void AddRuntimeEvent(ButtonState state, RunTimeAddedButtonEvent buttonEvent)
	{
		if(!runTimeSetEvents[(int)state].Contains(buttonEvent))
		{
			runTimeSetEvents[(int)state].Add(buttonEvent);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected List<string>[] allStateEvents
	{
		get
		{
			if(m_allStateEvents == null)
			{
				m_allStateEvents = new List<string>[(int)ButtonState.Total] {
					m_stateEvents_Enabled,
					m_stateEvents_OnDown,
					m_stateEvents_Selected,
					m_stateEvents_Disabled
				};
			}
			return m_allStateEvents;
		}
	}


	protected GUIViewController parentController
	{
		get
		{
			if(m_parentController == null)
			{
				GUIView parentIt = this;
				while(parentIt != null
					&& m_parentController == null)
				{
					parentIt = parentIt.transform.parent.GetComponent<GUIView>();
					if(parentIt != null)
					{
						m_parentController = parentIt.GetComponent<GUIViewController>();
					}
				}
			}
			return m_parentController;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void ProcessToggle()
	{
		if(toggle && !lockInput)
		{
			currentState = ButtonState.Enabled;
		}
		else if(!toggle)
		{
			currentState = ButtonState.DeActive;
		}
		else
		{
			currentState = ButtonState.Disabled;
		}
	}


	protected virtual void ProcessEnabledState()
	{
		if(!GUIViewInputManager.Instance.UsesTouchInput || !multiTouchInput)
		{
			if(inputManagerCursorState == null)
			{
				return;
			}

			if(inputManagerCursorState.ActiveInput != null
				&& inputManagerCursorState.ActiveInput.gameObject == gameObject
				&& (inputManagerCursorState.InitialInput != null
				&& inputManagerCursorState.InitialInput.gameObject == gameObject))
			{
				currentState = ButtonState.OnDown;
				return;
			}
		}
		else if(multiTouchInput && inputEnabled)
		{
			if(Input.touchCount > 0)
			{
				foreach(Touch touch in Input.touches)
				{
					if(touch.phase == TouchPhase.Began
						|| touch.phase == TouchPhase.Moved
						|| touch.phase == TouchPhase.Stationary)
					{
						if(ContainsPoint(touch.position))
						{
							currentState = ButtonState.OnDown;
							return;
						}
					}
				}
			}
		}
	}


	protected virtual void ProcessOnDownState()
	{
		if(!GUIViewInputManager.Instance.UsesTouchInput || !multiTouchInput)
		{
			if(inputManagerCursorState == null)
			{
				return;
			}

			if(inputManagerCursorState.ActiveInput != null)
			{
				if(inputManagerCursorState.ActiveInput.gameObject != gameObject
					|| inputManagerCursorState.InitialInput.gameObject != gameObject)
				{
					if(m_returnToSelectedState)
					{
						currentState = ButtonState.Selected;
					}
					else
					{
						currentState = ButtonState.Enabled;
					}
					return;
				}
			}
			else
			{
				if(m_returnToSelectedState)
				{
					currentState = ButtonState.Selected;
				}
				else
				{
					currentState = ButtonState.Enabled;
				}
				return;
			}


			if(inputManagerCursorState.InitialInput != null
			&& inputManagerCursorState.InitialInput.gameObject == gameObject
			&& inputManagerCursorState.PressedState == CursorState.PressState.Released)
			{
				currentState = ButtonState.Selected;
				return;
			}
		}
		else if(multiTouchInput && inputEnabled)
		{
			if(Input.touchCount > 0)
			{
				bool noTouchOnMe = true;
				foreach(Touch touch in Input.touches)
				{
					if(ContainsPoint(touch.position))
					{
						noTouchOnMe = false;
						break;
					}
				}
				if(noTouchOnMe)
				{
					currentState = ButtonState.Selected;
				}
			}
			else
			{
				currentState = ButtonState.Selected;
				return;
			}
		}

	}


	protected virtual void ProcessSelectedState()
	{
		if(!GUIViewInputManager.Instance.UsesTouchInput || !multiTouchInput)
		{
			if(inputManagerCursorState == null)
			{
				return;
			}

			if(inputManagerCursorState.ActiveInput == this
				&& inputManagerCursorState.PressedState == CursorState.PressState.Press)
			{
				// This is a repress after it has been selected,
				//	from here the event can test the state of the cursor,
				//	If it is in the press state it is being reselected
				//CallStateEvent(m_currentState);
				m_returnToSelectedState = true;
				
				currentState = ButtonState.OnDown;
			}
		}
		else if(multiTouchInput && inputEnabled)
		{
			// TODO
		}
	}


	protected bool SwitchStateObjects(ButtonState buttonState)
	{
		bool foundAtLeastOneObjectForThisState = false;

		// Loop through each state.
		for(int buttonStateIt=0; buttonStateIt < (int)ButtonState.Total; ++buttonStateIt)
		{
			foreach(GUIView view in stateGuiObjects[buttonStateIt])
			{
				if(view == null)
				{
					continue;
				}
				// Tell ones that should be on to toggle on.
				if(buttonStateIt == (int)buttonState)
				{
					if(!view.toggle)
					{
						view.toggle = true;
					}
					foundAtLeastOneObjectForThisState = true;
				}
				else
				{

					if(buttonState == ButtonState.DeActive ||
						stateGuiObjects[(int)buttonState].Find(
						delegate(GUIView viewIt)
					{
						return viewIt == view;
					}) == null)
					{
						if(view.toggle)
						{
							view.toggle = false;
						}
					}
				}
			}
		}

		ReCalculateNow(true);

		return foundAtLeastOneObjectForThisState;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private List<RunTimeAddedButtonEvent>[] runTimeSetEvents
	{
		get
		{
			if(m_runTimeSetEvents == null)
			{
				m_runTimeSetEvents = new List<RunTimeAddedButtonEvent>[(int)ButtonState.Total]{
					m_runTimeSetEvents_Enabled,
					m_runTimeSetEvents_OnDown,
					m_runTimeSetEvents_Selected,
					m_runTimeSetEvents_Disabled
				};

			}
			return m_runTimeSetEvents;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private bool CallStateEvent(ButtonState buttonState)
	{
		if(!Application.isPlaying)
		{
			return false;
		}
		if(runTimeSetEvents[(int)buttonState].Count > 0)
		{
			foreach(RunTimeAddedButtonEvent buttonEvent in runTimeSetEvents[(int)buttonState])
			{
				if(buttonEvent != null)
				{
					buttonEvent(this);
				}
			}
		}
		if(allStateEvents[(int)buttonState].Count > 0)
		{
			bool calledEvents = true;
			foreach(string buttonEventMethodName in allStateEvents[(int)buttonState])
			{

				MethodInfo buttonEventMethod = parentController.EventList.GetType().GetMethod(buttonEventMethodName,
					BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetProperty);

				if(buttonEventMethod != null)
				{
					buttonEventMethod.Invoke(parentController.EventList, new object[] { this });
				}
				else
				{
					calledEvents = false;
					string parentControllerName = parentController != null ? parentController.gameObject.name : "Null";
					Debug.Log("Button \"" + gameObject.name + "\" could not find the event \"" +
						buttonEventMethodName + "\" in its parent controller \"" + parentControllerName + "\"");
				}
			}
			return calledEvents;
		}
		return false;
	}

	#endregion
}