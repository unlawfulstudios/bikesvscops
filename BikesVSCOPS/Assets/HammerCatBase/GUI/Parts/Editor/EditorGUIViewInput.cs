using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewInput))]
public abstract class EditorGUIViewInput : EditorGUIView
{
    #region Protected Methods

    protected override void DisplayFields()
    {
        base.DisplayFields();


        ShowInputFields = new EditorProfileKey<int, bool>("ShowInputFields", false);

        DisplayInputFields();
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    private void DisplayInputFields()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            if (!ShowToggleField(ShowInputFields, "Input"))
            {
                EditorGUILayout.Space();
                return;
            }

            EditorGUI.BeginChangeCheck();

            bool newValue = Toggle(new GUIContent("Multi-Touch Input", "Allow entering on down and selected states even if it is not the initial input"),
            FindProperty("m_multiTouchInput").boolValue);

            if (EditorGUI.EndChangeCheck())
            {
                if (newValue != FindProperty("m_multiTouchInput").boolValue)
                {
                    FindProperty("m_multiTouchInput").boolValue = newValue;
                }
            }
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private EditorProfileKey<int, bool> ShowInputFields;

    #endregion
}