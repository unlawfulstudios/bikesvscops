using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewController))]
public class EditorGUIViewController : EditorGUIView
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		base.DisplayFields();

        ShowTranisitonEvents = new EditorProfileKey<int, bool>("ShowTranisitonEvents", true);
        ShowMaterEventList = new EditorProfileKey<int, bool>("ShowMaterEventList", true);


        DisplayTransitionEvents();
		DisplayEvents();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewController TargetGUIViewController
	{
		get
		{
			if(this.m_targetGUIViewController == null)
			{
				this.m_targetGUIViewController = Target as GUIViewController;
			}
			return this.m_targetGUIViewController;
		}
	}


	public List<string> MasterEventNames
	{
		get
		{
			if(this.m_masterEventNames == null)
			{
				this.m_masterEventNames = new List<string>();

				System.Type targetClassType = TargetGUIViewController.GetType();

//				MethodInfo getDerivedClassButtonEventsMethod = targetClassType.BaseType.GetMethod("GetDerivedClassButtonEvents", BindingFlags.Instance | BindingFlags.NonPublic);
//				getDerivedClassButtonEventsMethod.Invoke(TargetGUIViewController, null);

				MethodInfo getMasterListMethod = targetClassType.GetMethod("get_MasterEventList", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
				List<MethodInfo> masterEventMethods = (List<MethodInfo>)getMasterListMethod.Invoke(TargetGUIViewController, null);

				foreach(MethodInfo methodInfo in masterEventMethods)
				{
					this.m_masterEventNames.Add(methodInfo.Name);
				}
			}

			return this.m_masterEventNames;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayTransitionEvents()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			if(!ShowToggleField(ShowTranisitonEvents, "Tranisiton Events"))
			{
				EditorGUILayout.EndVertical();
				return;
			}

			EditorGUILayout.BeginVertical("Box");
			{
				DrawPropertyField("m_introTransitions", true);
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical("Box");
			{
				DrawPropertyField("m_exitTransitions", true);
			}
			EditorGUILayout.EndVertical();

//			EditorGUILayout.BeginVertical("Box");
//			{
//				GUIViewController.TransitionEvent prevEvent = (GUIViewController.TransitionEvent)FindProperty("m_introTransitionEvent").enumValueIndex;
//				GUIViewController.TransitionEvent newEvent = (GUIViewController.TransitionEvent)EnumPopup("Intro Transition", prevEvent);
//
//				if(newEvent != prevEvent
//	 			&& newEvent != GUIViewController.TransitionEvent.Total)
//				{
//					FindProperty("m_introTransitionEvent").enumValueIndex = (int)newEvent;
//					serializedObject.ApplyModifiedProperties();
//				}
//
//				if(newEvent == GUIViewController.TransitionEvent.FadeOutTranslate)
//				{
//					{
//						Vector2 prevStart = FindProperty("m_startTranslation").vector2Value;
//						Vector2 newStart = Vector2Field("Start Translation", prevStart);
//						if(newStart != prevStart)
//						{
//							FindProperty("m_startTranslation").vector2Value = newStart;
//							serializedObject.ApplyModifiedProperties();
//						}
//					}
//					{
//						Vector2 prevEnd = FindProperty("m_endTranslation").vector2Value;
//						Vector2 newEnd = Vector2Field("End Translation", prevEnd);
//						if(newEnd != prevEnd)
//						{
//							FindProperty("m_endTranslation").vector2Value = newEnd;
//							serializedObject.ApplyModifiedProperties();
//						}
//					}
//				}
//			}
//			EditorGUILayout.EndVertical();

//			EditorGUILayout.BeginVertical("Box");
//			{
//				GUIViewController.TransitionEvent prevEvent = (GUIViewController.TransitionEvent)FindProperty("m_exitTransitionEvent").enumValueIndex;
//				GUIViewController.TransitionEvent newEvent = (GUIViewController.TransitionEvent)EnumPopup("Exit Transition", prevEvent);
//
//				if(newEvent != prevEvent
//	 			&& newEvent != GUIViewController.TransitionEvent.Total)
//				{
//					FindProperty("m_exitTransitionEvent").enumValueIndex = (int)newEvent;
//					serializedObject.ApplyModifiedProperties();
//				}
//
//				if(newEvent == GUIViewController.TransitionEvent.FadeOutTranslate)
//				{
//					{
//						Vector2 prevStart = FindProperty("m_startExitTranslation").vector2Value;
//						Vector2 newStart = Vector2Field("Start Translation", prevStart);
//						if(newStart != prevStart)
//						{
//							FindProperty("m_startExitTranslation").vector2Value = newStart;
//							serializedObject.ApplyModifiedProperties();
//						}
//					}
//					{
//						Vector2 prevEnd = FindProperty("m_endExitTranslation").vector2Value;
//						Vector2 newEnd = Vector2Field("End Translation", prevEnd);
//						if(newEnd != prevEnd)
//						{
//							FindProperty("m_endExitTranslation").vector2Value = newEnd;
//							serializedObject.ApplyModifiedProperties();
//						}
//					}
//				}
//			}
//			EditorGUILayout.EndVertical();

		}
		EditorGUILayout.EndVertical();
	}


	private void DisplayEvents()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			if(!ShowToggleField(ShowMaterEventList, "Master Event List"))
			{
				EditorGUILayout.EndVertical();
				return;
			}

			{
				MethodInfo getMasterListObject = TargetGUIViewController.GetType().GetMethod("get_EventList", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);

				GUIViewButtonEventList currentEventList = (GUIViewButtonEventList)getMasterListObject.Invoke(TargetGUIViewController, null);

				GUIViewButtonEventList newEventList = ObjectField("Event List Source", currentEventList, typeof(GUIViewButtonEventList), true) as GUIViewButtonEventList;
				if(newEventList != currentEventList)
				{
					MethodInfo setMasterListObject = TargetGUIViewController.GetType().GetMethod("set_EventList", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);

					setMasterListObject.Invoke(TargetGUIViewController, new object[] { newEventList });
					this.m_masterEventNames = null;
					serializedObject.ApplyModifiedProperties();
				}
			}

			//Master list

			string eventNames = "";
			foreach(string eventName in MasterEventNames)
			{
				eventNames += eventName + "\n";

			}
			EditorGUILayout.TextArea(eventNames);
		}
		EditorGUILayout.EndVertical();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIViewController m_targetGUIViewController = null;
	private List<string> m_masterEventNames = null;
	private EditorProfileKey<int, bool> ShowTranisitonEvents;
	private EditorProfileKey<int, bool> ShowMaterEventList;

	#endregion
}