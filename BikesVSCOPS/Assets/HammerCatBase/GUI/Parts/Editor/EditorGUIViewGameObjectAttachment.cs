using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewGameObjectAttachment))]
public class EditorGUIViewGameObjectAttachment : EditorGUIView
{
    #region Public Properties

    #endregion

    private GUIViewGameObjectAttachment m_targetGUIViewGameObjectAttachment = null;
    private EditorProfileKey<int, bool> ShowViewFileds;
    private EditorProfileKey<int, bool> ShowAttachmentFileds;

    private void OnEnable()
    {


        ShowViewFileds = new EditorProfileKey<int, bool>("ShowViewFileds", true);
        ShowAttachmentFileds = new EditorProfileKey<int, bool>("ShowAttachmentFileds", true);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods

    protected override void DisplayFields()
    {
        DisplayBasicFields();
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private GUIViewGameObjectAttachment TargetGUIViewGameObjectAttachment
    {
        get
        {
            if (this.m_targetGUIViewGameObjectAttachment == null)
            {
                this.m_targetGUIViewGameObjectAttachment = Target as GUIViewGameObjectAttachment;
            }
            return this.m_targetGUIViewGameObjectAttachment;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    private void DisplayBasicFields()
    {
        EditorGUILayout.BeginVertical("Box");
        {
            if (ShowToggleField(ShowViewFileds, "Base Settings"))
            {
                DisplayToggle();

                DisplayPositionFields();

                DisplaySizeFields();

                DisplayLayerFields();

                //DrawAnchorFields();
            }

            EditorGUILayout.Space();
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical("Box");
        {
            if (ShowToggleField(ShowAttachmentFileds, "Attachment Settings"))
            {
                DisplayAttachedGameObjectFields();
            }

            EditorGUILayout.Space();
        }
        EditorGUILayout.EndVertical();


    }


    private void DisplaySizeFields()
    {
        Vector2Field("Size In Points", TargetGUIViewGameObjectAttachment.sizeInPointSpace);
        ShowLastRectDimmed();
    }


    private void DisplayPositionFields()
    {
        Vector2Field("Local Position In Points", TargetGUIViewGameObjectAttachment.localPositionInPointSpace);
        ShowLastRectDimmed();
    }


    private void DisplayAttachedGameObjectFields()
    {
        TargetGUIViewGameObjectAttachment.AttachedGameObject = ObjectField<GameObject>("AttachedGameObject",
            TargetGUIViewGameObjectAttachment.AttachedGameObject, true);
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members



    #endregion
}