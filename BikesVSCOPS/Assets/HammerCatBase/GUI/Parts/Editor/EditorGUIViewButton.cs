using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewButton))]
public class EditorGUIViewButton : EditorGUIViewInput
{
    #region Protected Methods
    private GUIViewButton m_targetGUIViewButton = null;
    private EditorProfileKey<int, bool> ShowButtonStateObjects ;
    private EditorProfileKey<int, int> CurrentEditingButtonState ;
    private List<string>[] m_allStateEventsMethods = null;
    private List<MethodInfo> m_masterEventList = null;
    private GUIViewController m_parentController = null;



    protected override void DisplayFields()
	{
		base.DisplayFields();

        ShowButtonStateObjects = new EditorProfileKey<int, bool>("ShowButtonStateObjects", false);
        CurrentEditingButtonState = new EditorProfileKey<int, int>("CurrentEditingButtonState", 1);

        DisplayButtonStateObjects();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewButton TargetGUIViewButton
	{
		get
		{
			if(this.m_targetGUIViewButton == null)
			{
				this.m_targetGUIViewButton = Target as GUIViewButton;
			}
			return this.m_targetGUIViewButton;
		}
	}


	private GUIViewController ParentController
	{
		get
		{
			if(this.m_parentController == null)
			{
				MethodInfo getParentMethod = TargetGUIViewButton.GetType().GetMethod("get_parentController", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
				this.m_parentController = (GUIViewController)getParentMethod.Invoke(TargetGUIViewButton, null);
			}
			return this.m_parentController;
		}
	}


	private List<MethodInfo> MasterEventList
	{
		get
		{
			if(this.m_masterEventList == null)
			{

				if(ParentController != null)
				{
					MethodInfo getMasterListMethod = ParentController.GetType().GetMethod("get_MasterEventList", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
					this.m_masterEventList = (List<MethodInfo>)getMasterListMethod.Invoke(ParentController, null);
				}
				else
				{
					this.m_masterEventList = new List<MethodInfo>();
				}

				//this.m_allStateEventsMethods = (List<MethodInfo>[])getAllStateEventsMethod.Invoke(TargetGUIViewButton, null);

				//if(TargetGUIViewButton.ParentController != null

				//this.m_materEventList
			}
			return this.m_masterEventList;
		}
	}


	private List<string>[] AllStateEventsMethods
	{
		get
		{
			if(this.m_allStateEventsMethods == null)
			{
				MethodInfo getAllStateEventsMethod = TargetGUIViewButton.GetType().GetMethod("get_allStateEvents", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
				this.m_allStateEventsMethods = (List<string>[])getAllStateEventsMethod.Invoke(TargetGUIViewButton, null);
			}
			return this.m_allStateEventsMethods;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayButtonStateObjects()
	{
		EditorGUILayout.BeginVertical("Box");
		if(!ShowToggleField(ShowButtonStateObjects, "Button States"))
		{
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			return;
		}

		EditorGUILayout.BeginVertical();
		{
			Color currentColor = GUI.color;
			GUI.color = Color.cyan;
			FieldInfo currentTargetStateFieldInfo = target.GetType().GetField("m_currentState", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
			GUIViewButton.ButtonState currentTargetButtonState = (GUIViewButton.ButtonState)currentTargetStateFieldInfo.GetValue(target);
			string[] buttonStateNames = new string[1] { System.Enum.GetName(typeof(GUIViewButton.ButtonState), currentTargetButtonState) };
			Popup("Current Active State", 0, buttonStateNames);
			GUI.color = currentColor;
		}
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical();
		{
			EditorGUILayout.BeginVertical("Box");
			Label("Choose a state to edit.");
			EditorGUILayout.EndVertical();
			// Buttons to select state
			EditorGUILayout.BeginVertical("Box");
			{
				Label("\"State Name\": \"# Of GUI Objects\" ~ \"# Of Events\"");
				EditorGUILayout.BeginHorizontal();
				{
					// Using reflections to get list rather than breaking encapsulation.
					MethodInfo getStateGuiObjectsMethod = TargetGUIViewButton.GetType().GetMethod("get_stateGuiObjects",
						BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);
					List<GUIView>[] stateGUIObjects = (List<GUIView>[])getStateGuiObjectsMethod.Invoke(TargetGUIViewButton, new object[] {});

					// Split up buttons for room (1st 3 then last 2)
					int numberOfColumns = ((stateGUIObjects.Length % 2) == 0) ? (stateGUIObjects.Length / 2) : ((stateGUIObjects.Length + 1) / 2);
					for(int columnIt=0; columnIt < numberOfColumns; ++columnIt)
					{
						EditorGUILayout.BeginVertical();
						for(int rowIt=0; rowIt < 2; ++rowIt)
						{
							// Index we are on
							int stateIndex = numberOfColumns * rowIt + columnIt;
							if(stateIndex >= stateGUIObjects.Length)
							{
								continue;
							}

							// Create a button name that will show the state, plus the number of gui objects it uses.
							int numberOfGUIObjects = stateGUIObjects[stateIndex].Count;
							int numberOfEvents = AllStateEventsMethods[stateIndex].Count;
							string enumName = System.Enum.GetName(typeof(GUIViewButton.ButtonState), stateIndex);
							string enumLabel = StringTools.BuildReadableString(enumName) + ": " + numberOfGUIObjects.ToString() + " ~ " + numberOfEvents.ToString();

							Color currentGUIColor = GUI.color;

							// If the state we are editing is this one, make it green as selected.
							if(CurrentEditingButtonState.Value == stateIndex)
							{
								GUI.color = Color.green;
							}
							if(GUILayout.Button(enumLabel))
							{
								// Save the state we are editing so if we switch buttons we will see similar setups when editing buttons,
								//	i.e. some one has to change all the sounds on button up.
								CurrentEditingButtonState.Value = stateIndex;

								// Switch the state in the editor (using reflections to prevent breaking encapsulation).
								MethodInfo switchStateObjectsMethod = target.GetType().GetMethod("SwitchStateObjects", BindingFlags.Instance | BindingFlags.NonPublic);
								switchStateObjectsMethod.Invoke(target, new object[] { stateIndex });
							}

							GUI.color = currentGUIColor;
						}
						EditorGUILayout.EndVertical();
						//GUILayout.Space(25f);
					}
					//

				}
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical("Box");
			{
				string selectedStateName = System.Enum.GetName(typeof(GUIViewButton.ButtonState), CurrentEditingButtonState.Value);

				{
					EditorGUILayout.Space();
					int enabledEvents = GetEventMask(AllStateEventsMethods[(int)CurrentEditingButtonState.Value], MasterEventList);
					EditorGUILayout.BeginHorizontal();
					{
						Label(selectedStateName + " Events");

						// Get a list of names for the mask field
						List<string> methodNames = new List<string>();
						foreach(MethodInfo methodInfo in MasterEventList)
						{
							methodNames.Add(methodInfo.Name);
						}
						int newEnabledEvents = EditorGUILayout.MaskField(enabledEvents, methodNames.ToArray());

						if(newEnabledEvents != enabledEvents)
						{
							FieldInfo currentTargetStateEventListFieldInfo = TargetGUIViewButton.GetType().GetField("m_stateEvents_" + selectedStateName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
							List<string> newEventList = AddEnabledEventsToList(newEnabledEvents, MasterEventList);
							currentTargetStateEventListFieldInfo.SetValue(TargetGUIViewButton, newEventList);
							serializedObject.ApplyModifiedProperties();
							serializedObject.Update();

							// Reseting the event list
							FieldInfo allStateEvents = TargetGUIViewButton.GetType().GetField("m_allStateEvents", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
							allStateEvents.SetValue(TargetGUIViewButton, null);
							m_allStateEventsMethods = null;
						}
						//AllStateEventsMethods[(int)CurrentEditingButtonState.Value] = AddEnabledEventsToList(enabledEvents, MasterEventList);
					}
					EditorGUILayout.EndHorizontal();
				}


				{
					//List<GUIView> currentStateObjects = TargetGUIViewButton.StateGuiObjects[stateIndex];
					SerializedProperty buttonStateObjects = SerilizedTarget.FindProperty("m_stateGuiObjects_" + selectedStateName);
					if(buttonStateObjects != null)
					{
						//SerializedProperty currentList = buttonStateObjects.GetArrayElementAtIndex(CurrentEditingButtonState.Value);

						string userFriendlyListName = StringTools.BuildReadableString(selectedStateName) + " GUI Objects";
						EditorGUILayout.PropertyField(buttonStateObjects, new GUIContent(userFriendlyListName), true);
					}
					else
					{
						Debug.LogWarning("Could not find the serilized list.");
					}
				}
				//EditorGUILayout.MaskField("", enabledEvents, sourceEventNames);



			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.EndVertical();


		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
	}


	private int GetEventMask(List<string> eventList, List<MethodInfo> masterEventList)
	{
		int eventsMask = 0;
		if(eventList == null
			|| eventList.Count <= 0)
		{
			return 0;
		}

		int masterIndex = 0;
		foreach(MethodInfo masterEvent in masterEventList)
		{
			foreach(string enabledEvent in eventList)
			{
				if(masterEvent.Name == enabledEvent)
				{
					eventsMask |= 1 << masterIndex;
					break;
				}
			}
			++masterIndex;
		}

		return eventsMask;
	}


	protected List<string> AddEnabledEventsToList(int enabledEvents, List<MethodInfo> masterEventList)
	{
		List<string> enabledEventList = new List<string>();
		for(int mEventIt=0; mEventIt < masterEventList.Count; ++mEventIt)
		{
			if((enabledEvents & (1 << mEventIt)) == 1 << mEventIt)
			{
				enabledEventList.Add(masterEventList[mEventIt].Name);
			}
		}

		return enabledEventList;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

    #endregion
}