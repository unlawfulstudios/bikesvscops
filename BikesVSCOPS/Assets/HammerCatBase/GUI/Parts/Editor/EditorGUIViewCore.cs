using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewCore))]
public class EditorGUIViewCore : EditorGUIViewBase
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected override bool AllowEditingCore
	{
		get { return true; }
	}

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods


    private GUIViewCore m_targetGUICore = null;
    private EditorProfileKey<int, bool> ShowControllerFileds;
    private List<UnityEngine.Object> m_currentControllers = null;
    private Color currentColor;


    protected override void DisplayFields()
	{
		base.DisplayFields();

        ShowControllerFileds = new EditorProfileKey<int, bool>("ShowControllerFileds", true);
        DisplayControllerSettings();
	}


	protected override void ApplyUpdateAndRecalculateGUI()
	{
		//serializedObject.ApplyModifiedProperties();
		SerilizedTarget.ApplyModifiedProperties();
		EditorUtility.SetDirty(TargetGUICore);
		GUIViewCore.Instance.ReCalculate();
		SerilizedTarget.UpdateIfDirtyOrScript();
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewCore TargetGUICore
	{
		get
		{
			if(this.m_targetGUICore == null)
			{
				this.m_targetGUICore = Target as GUIViewCore;
			}
			return this.m_targetGUICore;
		}
	}


	public List<UnityEngine.Object> CurrentControllers
	{
		get
		{
			if(this.m_currentControllers == null)
			{
				this.m_currentControllers = new List<UnityEngine.Object>();
			}
			return this.m_currentControllers;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayControllerSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			if(ShowToggleField(ShowControllerFileds, "Controller Settings"))
			{
				DisplayControllerList();

				DisplayStartingController();

				DisplayDebugControllerChager();
			}
			EditorGUILayout.Space();
		}
		EditorGUILayout.EndVertical();
	}


	private void DisplayControllerList()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			//List<GUIViewController> prevControllers = FindProperty("m_controllers");

			DrawPropertyField("m_controllers", true);
			//List<GUIViewController> newControllers = FindProperty("m_controllers");
//			bool listChanged = prevControllers.Count != newControllers.Count;
//			for(int controllerIt=0; !listChanged && controllerIt < prevControllers.Count; ++controllerIt)
//			{
//				if(prevControllers[controllerIt] != newControllers[controllerIt])
//				{
//					listChanged = true;
//				}
//			}
//
//			if(listChanged)
//			{
				ApplyUpdateAndRecalculateGUI();
//			}
		}
		EditorGUILayout.EndVertical();
	}

	/// <summary>
	/// While keeping the members private, we display a popup with only valid controllers,
	/// 	plus a "None" at the end.
	/// To achive this we must make a list of valid controllers and index properly into the
	/// 	array.
	/// </summary>
	private void DisplayStartingController()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			CurrentControllers.Clear();
			SerializedProperty startController = FindProperty("m_startController");

			int indexOfStartController = -1;
			SerializedProperty controllers = FindProperty("m_controllers");

			// Loop through all the controllers.
			for(int controllerIt = 0, validControllerCount = 0; controllerIt < controllers.arraySize; ++controllerIt)
			{
				// Get access to the value saved in the controller list.
				SerializedProperty child = controllers.GetArrayElementAtIndex(controllerIt);

				// If it null continue.
				if(child.objectReferenceValue != null)
				{
					// Save the names of the controllers from the list.
					CurrentControllers.Add(child.objectReferenceValue);

					// If we find the starting controller in the list, save the index for the popup later.
					if(startController.objectReferenceValue == child.objectReferenceValue)
					{
						indexOfStartController = validControllerCount;
					}
					++validControllerCount;
				}
			}

			// Add the invalid controller to allow the core to use no controller at start up.
			CurrentControllers.Add(null);

			// If we could not match the starting controller with any of the other controllers
			if(indexOfStartController == -1)
			{
				indexOfStartController = (CurrentControllers.Count > 0) ? (CurrentControllers.Count - 1) : 0;
			}

			// Get a list of names while validating.
			List<string> controllerNames = new List<string>();
			foreach(UnityEngine.Object controller in CurrentControllers)
			{
				string controllerName = (controller != null) ? controller.name : GUIViewCore.InvalidControllerName;
				if(controllerNames.Contains(controllerName))
				{
					Debug.LogWarning("Malutipile controllers in core have the same name: " + controllerName + "\nPlease change the name of one of them.");
				}
				controllerNames.Add(controllerName);
			}
			// Show the popup and save the new index choosen.
			int newIndexOfStartController = Popup("Starting Controller", indexOfStartController, controllerNames.ToArray());
			if(newIndexOfStartController != indexOfStartController)
			{
				// Get the name of the new controller.
				UnityEngine.Object controllerSelected = CurrentControllers[newIndexOfStartController];

				// If it is invalid, remove the start controller.
				if(controllerSelected == null)
				{
					startController.objectReferenceValue = null;
				}
				else
				{
					// Loop through the controllers again.
					for(int controllerIt = 0; controllerIt < controllers.arraySize; ++controllerIt)
					{
						// If it is valid, and the name matches the selected name from the popup.
						if(controllers.GetArrayElementAtIndex(controllerIt) != null
						&& controllers.GetArrayElementAtIndex(controllerIt).objectReferenceValue != null
						&& controllerSelected == controllers.GetArrayElementAtIndex(controllerIt).objectReferenceValue)
						{
							// Set the start controller.
							startController.objectReferenceValue = controllers.GetArrayElementAtIndex(controllerIt).objectReferenceValue;
							break;
						}
					}
				}
				//serializedObject.ApplyModifiedProperties();
				ApplyUpdateAndRecalculateGUI();
			}
		}
		EditorGUILayout.EndVertical();
	}


	private void DisplayDebugControllerChager()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			Label("Debug: Display controller");

			// Go through each controller.
			for(int controllerIt = 0; controllerIt < CurrentControllers.Count; ++controllerIt)
			{
                // Save the gui color to restore it later.
                 currentColor = GUI.color;

				{
					// Make 2 buttons per row to save on room.
					{


						// Get an accsessor for the object and its name.
						UnityEngine.Object currentController = CurrentControllers[controllerIt];
						string currentControllerName = (currentController != null) ? currentController.name : GUIViewCore.InvalidControllerName;
						GUIViewController controller = (currentController != null) ? (currentController as GUIViewController) : null;;

						bool noControllerActive_onControllerInvalid = GUIViewCore.Instance.ActiveController == null && currentController == null;
						bool controllerActive_onSameControllerAsActive = GUIViewCore.Instance.ActiveController != null && currentController == GUIViewCore.Instance.ActiveController;
						// Setting the button color to identify the current active controller.
						if(noControllerActive_onControllerInvalid || controllerActive_onSameControllerAsActive)
						{
							GUI.color = Color.green;
						}
						if(GUILayout.Button(currentControllerName))
						{
							// If pressed, switch controller.
							//GUIViewCore.Instance.SwitchController(currentControllerName);
							GUIViewCore.Instance.ActivateController(controller);
						}
						// Restore gui colors.
						GUI.color = currentColor;
					}
				}
			}
		}
		EditorGUILayout.EndVertical();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members


    #endregion
}