using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewPanel))]
public class EditorGUIViewPanel : EditorGUIView
{
    #region Protected Methods
    private void OnEnable()
    {

    }
    protected override void DisplayFields()
    {


        ShowPanelSettings = new EditorProfileKey<int, bool>("ShowPanelSettings", true);
        DisplayPanelSettings();
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Defines

    private enum BackGroundPiece
    {
        Left,
        Center,
        Right,

        Total
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private GUIViewPanel TargetGUIViewPanel
    {
        get
        {
            if (this.m_targetGUIViewPanel == null)
            {
                this.m_targetGUIViewPanel = Target as GUIViewPanel;
            }
            return this.m_targetGUIViewPanel;
        }
    }


    private Texture[] TextureBackground
    {
        set
        {
            MethodInfo setMethod = typeof(GUIViewPanel).GetMethod("set_TextureBackground", BindingFlags.Instance |
                BindingFlags.NonPublic | BindingFlags.SetProperty);

            setMethod.Invoke(TargetGUIViewPanel, new object[] { value });
        }
        get
        {
            MethodInfo getMethod = typeof(GUIViewPanel).GetMethod("get_TextureBackground", BindingFlags.Instance |
                BindingFlags.NonPublic | BindingFlags.GetProperty);

            return getMethod.Invoke(TargetGUIViewPanel, null) as Texture[];
        }
    }


    private void SetBGViewsValues()
    {
        MethodInfo method = typeof(GUIViewPanel).GetMethod("SetBGViewsValues", BindingFlags.Instance |
                BindingFlags.NonPublic);

        method.Invoke(TargetGUIViewPanel, null);
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    private void DisplayPanelSettings()
    {
        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            if (!ShowToggleField(ShowPanelSettings, "Panel Settings"))
            {
                EditorGUILayout.Space();
                return;
            }

            DisplayTextures();
        }
        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            DisplayColor();
        }
        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            DisplayClip("Open Animation", "m_clipOn");
            DisplayClip("Close Animation", "m_clipOff");
        }

        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            DisplaySound("Open Sound", "m_soundOpen");
            DisplaySound("Close Sound", "m_soundClose");
        }

        using (new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
        {
            DisplayAutoOpen();
        }
    }


    private void DisplayTextures()
    {
        Texture[] backGroundTextures = TextureBackground;
        for (BackGroundPiece bgIt = (BackGroundPiece)0; bgIt < BackGroundPiece.Total; ++bgIt)
        {
            EditorGUI.BeginChangeCheck();

            Texture newTexture = ObjectField<Texture>("Texture " + bgIt.ToString(), backGroundTextures[(int)bgIt], false);

            if (EditorGUI.EndChangeCheck())
            {
                if (newTexture != backGroundTextures[(int)bgIt])
                {
                    backGroundTextures[(int)bgIt] = newTexture;
                    TextureBackground = backGroundTextures;

                    SetBGViewsValues();
                }
            }
        }
    }


    private void DisplayColor()
    {
        Color newColor = ColorField("Back Ground Color", TargetGUIViewPanel.TextureBackGroundColor);
        if (newColor != TargetGUIViewPanel.TextureBackGroundColor)
        {
            TargetGUIViewPanel.TextureBackGroundColor = newColor;
            SetBGViewsValues();
        }
    }


    private void DisplayClip(string labelName, string clipMemberName)
    {
        AnimationClip newClip = ObjectField<AnimationClip>(labelName, FindProperty(clipMemberName).objectReferenceValue, false);
        if (newClip != FindProperty(clipMemberName).objectReferenceValue)
        {
            FindProperty(clipMemberName).objectReferenceValue = newClip;
        }
    }


    private void DisplaySound(string labelName, string soundMemberName)
    {
        AudioSource newSound = ObjectField<AudioSource>(labelName, FindProperty(soundMemberName).objectReferenceValue, true);
        if (newSound != FindProperty(soundMemberName).objectReferenceValue)
        {
            FindProperty(soundMemberName).objectReferenceValue = newSound;
        }
    }

    private void DisplayAutoOpen()
    {
        bool newValue = Toggle("Auto Open Buttons", FindProperty("m_autoOpenButtons").boolValue);
        if (newValue != FindProperty("m_autoOpenButtons").boolValue)
        {
            FindProperty("m_autoOpenButtons").boolValue = newValue;
        }
    }


    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private GUIViewPanel m_targetGUIViewPanel = null;
    private EditorProfileKey<int, bool> ShowPanelSettings;

    #endregion
}