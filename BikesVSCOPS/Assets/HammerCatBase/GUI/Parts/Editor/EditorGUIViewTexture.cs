using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewTexture))]
public class EditorGUIViewTexture : EditorGUIView
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion
    
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		base.DisplayFields();


       ShowTextureFields = new EditorProfileKey<int, bool>("ShowTextureFields", true);


    EditorGUILayout.BeginVertical("Box");
		{
			if(ShowToggleField(ShowTextureFields, "Texture Settings"))
			{
				Label("Texture");
				Texture prevTexture = null;
				if(FindProperty("m_actualViewTexture") != null)
				{
					prevTexture = FindProperty("m_actualViewTexture").objectReferenceValue as Texture;
				}
				Rect textureRect = GUILayoutUtility.GetLastRect();
				float baseHeight = 70f;
				float width = (prevTexture != null) ? ((baseHeight * prevTexture.width) / prevTexture.height) : baseHeight;
				textureRect.width = width;
				textureRect.height = baseHeight;
				textureRect.x = 55f;
				Texture newTexture =
					EditorGUI.ObjectField(
						textureRect,
						prevTexture,
						typeof(Texture),
						false) as Texture;
				GUILayout.Space(baseHeight);
				if(newTexture != prevTexture)
				{
					FindProperty("m_actualViewTexture").objectReferenceValue = newTexture;
					GUITexture guiTexture = TargetGUIViewTexture.GetComponent<GUITexture>();
					if(guiTexture == null)
					{
						guiTexture = TargetGUIViewTexture.gameObject.AddComponent<GUITexture>();
					}
					guiTexture.texture = newTexture;
				}
				Color newColor =
					ColorField("Texture Color", FindProperty("m_viewTextureColor").colorValue);
				if(newColor != FindProperty("m_viewTextureColor").colorValue)
				{
					FindProperty("m_viewTextureColor").colorValue = newColor;
					GUITexture guiTexture = TargetGUIViewTexture.GetComponent<GUITexture>();
					if(guiTexture == null)
					{
						guiTexture = TargetGUIViewTexture.gameObject.AddComponent<GUITexture>();
					}
					guiTexture.color = FindProperty("m_viewTextureColor").colorValue;
				}
				EditorGUILayout.Space();
			}
		}
		EditorGUILayout.EndVertical();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewTexture TargetGUIViewTexture
	{
		get
		{
			if(this.m_targetGUIViewTexture == null)
			{
				this.m_targetGUIViewTexture = Target as GUIViewTexture;
			}
			return this.m_targetGUIViewTexture;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIViewTexture m_targetGUIViewTexture = null;
	private EditorProfileKey<int, bool> ShowTextureFields;

	#endregion
}