using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewTextureHitable))]
public class EditorGUIViewTextureHitable : EditorGUIViewButton
{
	#region Member

	// Exposed

	// Editor Exposed

	// Member
	private EditorProfileKey<int, bool> ShowHitableSettings = new EditorProfileKey<int, bool>("ShowHitableSettings", true);

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		base.DisplayFields();

		DisplayHitResponse();
	}


	protected void DisplayHitResponse()
	{
		using(new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
		{
			if(!ShowToggleField(ShowHitableSettings, "Hitable Settings"))
			{
				EditorGUILayout.Space();
				return;
			}

			DisplayTarget();

			bool prevHitValue = FindProperty("m_sendHitInformation").boolValue;
			bool newHitValue = Toggle("Respond Hit Information on Events", prevHitValue);
			if(prevHitValue != newHitValue)
			{
				FindProperty("m_sendHitInformation").boolValue = newHitValue;
				SerilizedTarget.ApplyModifiedProperties();
			}

			if(newHitValue)
			{
				LayerMask prevMaskValue = FindProperty("m_mask").intValue;

				List<string> layerNames = new List<string>();
				for(int layerIt=0; layerIt < 32; ++layerIt)
				{
					layerNames.Add(LayerMask.LayerToName(layerIt));
				}

				LayerMask newMaskValue = Popup("Target Layer To Respond", prevMaskValue, layerNames.ToArray());//IntField("Respond Hit Information on Events", prevMaskValue);
				if(prevMaskValue != newMaskValue)
				{
					FindProperty("m_mask").intValue = newMaskValue;
					SerilizedTarget.ApplyModifiedProperties();
				}
			}

			DisplayRenderingCamera();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Serialized Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayRenderingCamera()
	{
		Camera prevCam = FindProperty("m_observer").objectReferenceValue as Camera;

		Camera newCam = null;
		using(new FixedWidthLabel("Rendering Camera"))
		{
			Color color = GUI.color;

			if(prevCam == null)
			{
				GUI.color = Color.red;
			}
			newCam = ObjectField<Camera>(prevCam, true);
			GUI.color = color;
		}

		if(newCam != prevCam)
		{
			FindProperty("m_observer").objectReferenceValue = newCam;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}

	private void DisplayTarget()
	{
		Transform prevTarget = FindProperty("m_target").objectReferenceValue as Transform;

		Transform newTarget = null;
		using(new FixedWidthLabel("Target"))
		{
			Color color = GUI.color;

			if(prevTarget == null)
			{
				GUI.color = Color.red;
			}
			newTarget = ObjectField<Transform>(prevTarget, true);
			GUI.color = color;
		}

		if(newTarget != prevTarget)
		{
			FindProperty("m_target").objectReferenceValue = newTarget;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}

	#endregion
}