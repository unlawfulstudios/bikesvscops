using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewScrollView))]
public class EditorGUIViewScrollView : EditorGUIViewRenderTextureTranslator
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		DisplayDebugFields();

		DisplayBasicSettings();

		DisplayScrollViewSettings();
	}



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewScrollView TargetGUIViewScrollView
	{
		get
		{
			if(this.m_targetGUIViewScrollView == null)
			{
				this.m_targetGUIViewScrollView = Target as GUIViewScrollView;
			}
			return this.m_targetGUIViewScrollView;
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayScrollViewSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		if(!ShowToggleField(ShowScrollViewSettings, "Scroll View Settings"))
		{
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			return;
		}

		DisplayAxisEnabled();

		{
			bool value = FindProperty("m_respondToSwipesOnDisabledAxis").boolValue;
			bool newValue = Toggle(new GUIContent("Use Swipe To Control Disabled Axis",
				"Will look for swiping velocity on axis that are disabled and adjust the scrollview in response."), value);
			if(newValue != value)
			{
				FindProperty("m_respondToSwipesOnDisabledAxis").boolValue = newValue;
				ApplyUpdateAndRecalculateGUI();
			}
		}

		DisplayAllowOutside();

		DisplayHitResponse();

		DisplayCorrectPosition();

		DisplayAutoScrollSpeed();

		DisplayRanges();

		EditorGUILayout.EndVertical();
	}


	private void DisplayCorrectPosition()
	{
		bool value = FindProperty("m_correctPositionOnUp").boolValue;
		bool newValue = Toggle(new GUIContent("Correct Position On Up",
			"Will allow the views to be centered when starting and releasing the scroll view."), value);
		if(newValue != value)
		{
			FindProperty("m_correctPositionOnUp").boolValue = newValue;
			ApplyUpdateAndRecalculateGUI();
		}
	}


	private void DisplayAutoScrollSpeed()
	{
		{
			float value = FindProperty("m_autoScrollSeconds_Normal").floatValue;
			float newValue = FloatField(new GUIContent("Speed to Correct Position (Points per Second)",
				"How many seconds it takes to either corect itself and snap into position, or perform the AutoScrollCenterView method."), value);
			if(newValue != value)
			{
				FindProperty("m_autoScrollSeconds_Normal").floatValue = newValue;
				ApplyUpdateAndRecalculateGUI();
			}
		}
		{
			float value = FindProperty("m_autoScrollSeconds_Fast").floatValue;
			float newValue = FloatField(new GUIContent("Time to Perform Swipes (Time in Seconds)",
				"How many seconds it takes when user quickly swipes across the ViewScroll."), value);
			if(newValue != value)
			{
				FindProperty("m_autoScrollSeconds_Fast").floatValue = newValue;
				ApplyUpdateAndRecalculateGUI();
			}
		}
		{
			float value = FindProperty("m_cursorVelocityNeededToFastSwipe").floatValue;
			float newValue = FloatField(new GUIContent("Cursor Velocity Needed To Fast Swipe",
				"Takes into acount the total movment of the press over the time."), value);
			if(newValue != value)
			{
				FindProperty("m_cursorVelocityNeededToFastSwipe").floatValue = newValue;
				ApplyUpdateAndRecalculateGUI();
			}
		}
	}


	private void DisplayRanges()
	{
		DrawPropertyField("m_rangeX", true);
		DrawPropertyField("m_rangeY", true);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private EditorProfileKey<int, bool> ShowScrollViewSettings = new EditorProfileKey<int, bool>("ShowScrollViewSettings", true);
	private GUIViewScrollView m_targetGUIViewScrollView = null;

	#endregion
}