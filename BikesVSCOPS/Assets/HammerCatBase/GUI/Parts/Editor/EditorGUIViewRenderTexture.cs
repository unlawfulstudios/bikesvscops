using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewRenderTexture))]
public class EditorGUIViewRenderTexture : EditorGUIView
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		Vector2 prevSize = FindProperty("m_sizeInPoints").vector2Value;

		base.DisplayFields();

		EditorGUILayout.BeginVertical("Box");
		{
			if(ShowToggleField(ShowRenderTextureFields, "Render-Texture Settings"))
			{
				DisplayObserver();

				int prevDepth = FindProperty("m_depth").intValue;
				int newDepth = IntField("Texture Depth", prevDepth);
				if(prevDepth != newDepth)
				{
					FindProperty("m_depth").intValue = newDepth;
					RebuildRenderTexture();
				}

				RenderTextureFormat prevFormat = (RenderTextureFormat)FindProperty("m_renderTextureFormat").enumValueIndex;
				RenderTextureFormat newFormat = (RenderTextureFormat)EnumPopup("Render-Texture Format", prevFormat);
				if(prevFormat != newFormat)
				{
					Debug.Log(newFormat);
					FindProperty("m_renderTextureFormat").enumValueIndex = (int)newFormat;

					RebuildRenderTexture();
				}

				Vector2 newSize = FindProperty("m_sizeInPoints").vector2Value;
				if(prevSize != newSize)
				{
					RebuildRenderTexture();
				}


				Color newColor =
					ColorField("Texture Color", FindProperty("m_viewTextureColor").colorValue);
				if(newColor != FindProperty("m_viewTextureColor").colorValue)
				{
					FindProperty("m_viewTextureColor").colorValue = newColor;
					GUITexture guiTexture = TargetGUIViewRenderTexture.GetComponent<GUITexture>();
					if(guiTexture == null)
					{
						guiTexture = TargetGUIViewRenderTexture.gameObject.AddComponent<GUITexture>();
					}
					guiTexture.color = FindProperty("m_viewTextureColor").colorValue;
				}
				EditorGUILayout.Space();
			}
		}
		EditorGUILayout.EndVertical();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewRenderTexture TargetGUIViewRenderTexture
	{
		get
		{
			if(this.m_targetGUIViewRenderTexture == null)
			{
				this.m_targetGUIViewRenderTexture = Target as GUIViewRenderTexture;
			}
			return this.m_targetGUIViewRenderTexture;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void RebuildRenderTexture()
	{
		MethodInfo buildRenderTextureMethod = typeof(GUIViewRenderTexture).GetMethod("BuildRenderTexture", BindingFlags.Instance | BindingFlags.Public);
		if(buildRenderTextureMethod != null)
		{
			buildRenderTextureMethod.Invoke(TargetGUIViewRenderTexture, null);
		}
	}


	private void DisplayObserver()
	{
		Color currentColor = GUI.color;
		Camera camera = FindProperty("m_observer").objectReferenceValue as Camera;
		if(camera == null)
		{
			GUI.color = Color.red;
		}

		Camera newCamera = ObjectField<Camera>("Observer Camera", camera, true);
		GUI.color = currentColor;

		if(newCamera != camera)
		{
			FindProperty("m_observer").objectReferenceValue = newCamera;

			ApplyUpdateAndRecalculateGUI();

			MethodInfo setObserverMethod = typeof(GUIViewRenderTexture).GetMethod("SetObserverCamera",
				BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			setObserverMethod.Invoke(TargetGUIViewRenderTexture, null);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIViewRenderTexture m_targetGUIViewRenderTexture = null;
	private EditorProfileKey<int, bool> ShowRenderTextureFields = new EditorProfileKey<int, bool>("ShowRenderTextureFields", true);

	#endregion
}