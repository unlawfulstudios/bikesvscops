using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewSliderBar))]
public class EditorGUIViewSliderBar : EditorGUIViewTextureTranslator
{
	#region Members

	// Members
	private EditorProfileKey<int, bool> ShowSliderSettings = new EditorProfileKey<int, bool>("ShowSliderSettings", true);
	private GUIViewSliderBar m_targetGUIViewSliderBar = null;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		DisplayDebugFields();

		DisplayBasicSettings();

		ShowBarSettings();

		DisplayHitResponse();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewSliderBar TargetGUIViewSliderBar
	{
		get
		{
			if(this.m_targetGUIViewSliderBar == null)
			{
				this.m_targetGUIViewSliderBar = Target as GUIViewSliderBar;
			}
			return this.m_targetGUIViewSliderBar;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void ShowBarSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		if(!ShowToggleField(ShowSliderSettings, "Slider Settings"))
		{
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			return;
		}

		bool xAxis = FindProperty("m_xAxis").boolValue;
		bool newXAxis = Toggle("X Axis", xAxis);
		if(xAxis != newXAxis)
		{
			FindProperty("m_xAxis").boolValue = newXAxis;
			ApplyUpdateAndRecalculateGUI();
		}

		if(!newXAxis)
		{
			DisplayYAxisDirection();
		}

		Vector2 range = FindProperty("m_range").vector2Value;
		Vector2 newRange = Vector2Field("Range", range);

		if(range != newRange)
		{
			FindProperty("m_range").vector2Value = newRange;
			ApplyUpdateAndRecalculateGUI();
		}

		FloatField("Current Value", TargetGUIViewSliderBar.sliderFractionalAmountF);
		ShowLastRectDimmed();


		ShowSliderTextureField("Bar Texture", TargetGUIViewSliderBar.Bar.viewTexture);
		ShowSliderTextureField("Thumb Texture", TargetGUIViewSliderBar.thumb.viewTexture);


		EditorGUILayout.EndVertical();
	}


	private void ShowSliderTextureField(string label, Texture texture)
	{
		Texture prevTexture = texture;


		Label(label);
		Rect textureRect = GUILayoutUtility.GetLastRect();
		float baseHeight = 70f;
		float width = (prevTexture != null) ? ((baseHeight * prevTexture.width) / prevTexture.height) : baseHeight;
		textureRect.width = width;
		textureRect.height = baseHeight;
		//textureRect.x = 0f;
		textureRect.y += 20f;

		Texture newTexture =
			EditorGUI.ObjectField(
				textureRect,
				prevTexture,
				typeof(Texture),
				false) as Texture;

		if(newTexture != prevTexture)
		{
			texture = newTexture;
			ApplyUpdateAndRecalculateGUI();
		}

		GUILayout.Space(textureRect.height + 20f);
	}

	#endregion
}