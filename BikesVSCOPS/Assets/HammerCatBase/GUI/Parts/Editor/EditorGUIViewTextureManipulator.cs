using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewRenderTextureManipulator))]
public abstract class EditorGUIViewTextureManipulator : EditorGUIViewTextureHitable
{
	#region Protected Methods

	protected override void DisplayFields()
	{
		base.DisplayFields();

		DisplayManipulationSettings();
	}


	protected void DisplayAxisEnabled()
	{
		using(new FixedWidthLabel("Axis Allowed: "))
		{
			EditorGUILayout.BeginHorizontal();

			{
				bool prevX = FindProperty("m_allowAxisX").boolValue;
				bool newX = Toggle("x", prevX);
				if(prevX != newX)
				{
					FindProperty("m_allowAxisX").boolValue = newX;
					SerilizedTarget.ApplyModifiedProperties();
				}
			}
			{
				bool prevY = FindProperty("m_allowAxisY").boolValue;
				bool newY = Toggle("y", prevY);
				if(prevY != newY)
				{
					FindProperty("m_allowAxisY").boolValue = newY;
					SerilizedTarget.ApplyModifiedProperties();
				}
			}

			EditorGUILayout.EndHorizontal();
		}
	}


	protected void DisplayAllowOutside()
	{
		bool prevValue = FindProperty("m_canManipulationOutsideArea").boolValue;
		bool newValue = Toggle("Can Manipulation Outside Area", prevValue);
		if(prevValue != newValue)
		{
			FindProperty("m_canManipulationOutsideArea").boolValue = newValue;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}




	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayManipulationSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		if(!ShowToggleField(ShowManipulationSettings, "Manipulation Settings"))
		{
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			return;
		}

		DisplayRenderingTexture();
		DisplayAxisEnabled();
		DisplayAllowOutside();
		DisplayMultiplier();

		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
	}


	private void DisplayRenderingTexture()
	{
		GUIView prevTex = FindProperty("m_viewAreaManipulating").objectReferenceValue as GUIView;

		GUIView newTex = null;
		using(new FixedWidthLabel("View Area Manipulating"))
		{
			Color color = GUI.color;

			if(prevTex == null)
			{
				GUI.color = Color.red;
			}
			newTex = ObjectField<GUIView>(prevTex, true);
			GUI.color = color;
		}

		if(newTex != prevTex)
		{
			FindProperty("m_viewAreaManipulating").objectReferenceValue = newTex;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}


	private void DisplayMultiplier()
	{
		Vector2 prevValue = FindProperty("m_multiplier").vector2Value;
		Vector2 newValue = Vector2Field("Multiplier", prevValue);
		if(prevValue != newValue)
		{
			FindProperty("m_multiplier").vector2Value = newValue;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private EditorProfileKey<int, bool> ShowManipulationSettings = new EditorProfileKey<int, bool>("ShowManipulationSettings", true);

	#endregion
}