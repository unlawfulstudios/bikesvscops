using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

[CustomEditor(typeof(GUIViewText))]
public class EditorGUIViewText : EditorGUIView
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void DisplayFields()
	{
		base.DisplayFields();

     ShowTextFields = new EditorProfileKey<int, bool>("ShowTextFields", true);

    EditorGUILayout.BeginVertical("Box");
		{
			if(ShowToggleField(ShowTextFields, "Text Settings"))
			{
				displayText();
				DisplayFont();
				DisplayFontSize();
				DisplayColor();
				DisplayShow();

				EditorGUILayout.Space();
			}
		}
		EditorGUILayout.EndVertical();
	}


	private void displayText()
	{
		string prevText = TargetGUIViewText.displayText;
		string newText = TextField("Text", prevText);
		if(prevText != newText)
		{
			TargetGUIViewText.displayText = newText;
		}
	}


	private void DisplayFont()
	{
		Font prevFont = FindProperty("m_textFont").objectReferenceValue as Font;
		string prevFontName = (prevFont != null) ? prevFont.name : null;

		// Adding one as a not selected font is default.
		int prevIndexOfFont = AllFontsInProjectNames.IndexOf(prevFontName);

		Color prevColor = GUI.color;
		int newIndexOfFont = 0;
		using(new FixedWidthLabel("Font"))
		{
			if(prevIndexOfFont == -1)
			{
				prevIndexOfFont = 0;
				GUI.color = Color.red;
			}
			newIndexOfFont = Popup(prevIndexOfFont, AllFontsInProjectNames.ToArray());
		}

		GUI.color = prevColor;
		if(prevIndexOfFont != newIndexOfFont)
		{
			Font newFont = AllFontsInProjectPaths[newIndexOfFont] != InvalidFontName ?
				AssetDatabase.LoadAssetAtPath(AllFontsInProjectPaths[newIndexOfFont], typeof(Font)) as Font :
					null;
			FindProperty("m_textFont").objectReferenceValue = newFont;

			ReCalculate();
		}
	}



	private void DisplayFontSize()
	{
		int prevFontSize = FindProperty("m_fontSize").intValue;
		int newFontSize = IntField("Font Size", prevFontSize);
		if(prevFontSize != newFontSize)
		{
			FindProperty("m_fontSize").intValue = newFontSize;
			ReCalculate();
		}
	}


	private void DisplayColor()
	{
		Color prevColor = TargetGUIViewText.color;
		Color newColor = ColorField("Text Color", prevColor);

		bool applyColorButton = false;
		using(new GUIColorChange(m_defaultHammerOrange, Color.white))
		{
			if(GUILayout.Button("Default Hammer Orange Color"))
			{
				newColor = m_defaultHammerOrange;
				applyColorButton = true;
			}
		}

		if(newColor != prevColor
			|| applyColorButton)
		{
			TargetGUIViewText.color = newColor;
		}
	}

	private void DisplayShow()
	{
		using(new GUIGroupAlignment(GroupAlignment.Vertical, "BOX"))
		{
			// toggle
			TargetGUIViewText.shadow = Toggle("Shadow", TargetGUIViewText.shadow);

			if(!TargetGUIViewText.shadow)
			{
				return;
			}

			// Color
			Color prevColor = TargetGUIViewText.shadowColor;
			Color newColor = ColorField("Shadow Color", prevColor);
			if(newColor != prevColor)
			{
				TargetGUIViewText.shadowColor = newColor;
			}

			// Offset
			Vector2 prevOffset = TargetGUIViewText.shadowOffset;
			Vector2 newOffset = Vector2Field("Shadow Offset", prevOffset);
			if(newOffset != prevOffset)
			{
				TargetGUIViewText.shadowOffset = newOffset;
			}

		}
	}


	private void ReCalculate()
	{
		SerilizedTarget.ApplyModifiedProperties();
		EditorUtility.SetDirty(TargetGUIViewText);
		TargetGUIViewText.toggle = !TargetGUIViewText.toggle;
		TargetGUIViewText.toggle = !TargetGUIViewText.toggle;
		SerilizedTarget.ApplyModifiedProperties();
		MethodInfo reCalculateMethod = typeof(GUIView).GetMethod("ReCalculate", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		reCalculateMethod.Invoke(TargetGUIViewText, null);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIViewText TargetGUIViewText
	{
		get
		{
			if(this.m_targetGUIViewText == null)
			{
				this.m_targetGUIViewText = Target as GUIViewText;
			}
			return this.m_targetGUIViewText;
		}
	}


	public List<string> AllFontsInProjectNames
	{
		get
		{
			if(this.m_allFontsInProjectNames == null)
			{
				this.m_allFontsInProjectNames = new List<string>();
				//this.m_allFontsInProjectNames.Add(InvalidFontName);
				foreach(Object font in AllFontsInProject)
				{
					if(font != null)
					{
						this.m_allFontsInProjectNames.Add(font.name);
					}
					else
					{
						this.m_allFontsInProjectNames.Add(InvalidFontName);
					}
				}
			}
			return this.m_allFontsInProjectNames;
		}
	}


	public List<string> AllFontsInProjectPaths
	{
		get
		{
			if(this.m_allFontsInProjectPaths == null)
			{
				this.m_allFontsInProjectPaths = new List<string>();
				foreach(Object font in AllFontsInProject)
				{
					if(font != null)
					{
						this.m_allFontsInProjectPaths.Add(AssetDatabase.GetAssetOrScenePath(font));
					}
					else
					{
						this.m_allFontsInProjectPaths.Add(InvalidFontName);
					}
				}
			}
			return this.m_allFontsInProjectPaths;
		}
	}


	public List<Object> AllFontsInProject
	{
		get
		{
			if(this.m_allFontsInProject == null)
			{
				this.m_allFontsInProject = EditorTools.AllAssetsOfFileName("*.TTF");
				this.m_allFontsInProject.Insert(0, null);
			}
			return this.m_allFontsInProject;
		}
	}


	public string InvalidFontName
	{
		get	{ return m_invalidFontName; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIViewText m_targetGUIViewText = null;
	private EditorProfileKey<int, bool> ShowTextFields;
	private List<Object> m_allFontsInProject = null;
	private List<string> m_allFontsInProjectNames = null;
	private List<string> m_allFontsInProjectPaths = null;
	private static readonly string m_invalidFontName = "Not Selected";
	private readonly Color m_defaultHammerOrange = new Color(0.96862745098039f, 0.57647058823529f, 0.11764705882353f, 1f);

	#endregion
}