using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewRenderTextureManipulator))]
public abstract class EditorGUIViewRenderTextureManipulator : EditorGUIViewTextureManipulator
{
	#region Protected Methods

//	protected override void DisplayFields()
//	{
//		base.DisplayFields();
//
//		DisplayRenderTextureManipulationSettings();
//	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

//	private void DisplayRenderTextureManipulationSettings()
//	{
//		EditorGUILayout.BeginVertical("Box");
//		if(!ShowToggleField(ShowRenderTextureManipulationSettings, "Render-Texture Manipulation Settings"))
//		{
//			EditorGUILayout.Space();
//			EditorGUILayout.EndVertical();
//			return;
//		}
//
//		DisplayRenderingCamera();
//
//		EditorGUILayout.Space();
//		EditorGUILayout.EndVertical();
//	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	//private EditorProfileKey<int, bool> ShowRenderTextureManipulationSettings = new EditorProfileKey<int, bool>("ShowRenderTextureManipulationSettings", true);

	#endregion
}