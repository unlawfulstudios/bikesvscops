using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewBase))]
public abstract class EditorGUIViewBase : BaseEditor
{
    #region Public Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private EditorProfileKey<int, bool> ShowDebugSettings;
    private EditorProfileKey<int, bool> ShowDebugCore;


    #region Protected Properties

    protected virtual bool AllowEditingCore
    {
        get { return false; }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods

    protected void Awake()
    {
        ShowDefaultGUI = false;
        ShowDebugSettings = new EditorProfileKey<int, bool>("ShowDebugSettings", false);
        ShowDebugCore = new EditorProfileKey<int, bool>("ShowDebugCore", false);
    }

    
    protected override void DisplayFields()
    {
        base.DisplayFields();

    

        DisplayDebugFields();
    }


    protected void DisplayDebugFields()
    {
        GUIViewCore.Instance.ReCalculate();

        DisplayDebugSettings();

        DisplayCoreSettings();
    }


    protected abstract void ApplyUpdateAndRecalculateGUI();

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    private GUIViewBase TargetGUIViewBase
    {
        get
        {
            if (this.m_targetGUIViewBase == null)
            {
                this.m_targetGUIViewBase = Target as GUIViewBase;
            }
            return this.m_targetGUIViewBase;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    private void DisplayDebugSettings()
    {
        EditorGUILayout.BeginVertical("Box");
        {
            if (ShowToggleField(ShowDebugSettings, "Debug Settings"))
            {
                EditorGUILayout.BeginHorizontal();
                {
                    TargetGUIViewBase.showDebug = Toggle("Show Debug", TargetGUIViewBase.showDebug);
                    if (GUILayout.Button("Turn Off All Debug"))
                    {
                        GUIViewCore.Instance.TurnOfAllDebug();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.Space();
        }
        EditorGUILayout.EndVertical();
    }


    private void DisplayCoreSettings()
    {
        EditorGUILayout.BeginVertical("Box");
        {
            if (ShowToggleField(ShowDebugCore, "Core Settings"))
            {
                EditorGUILayout.BeginVertical();
                {
                    Vector2Field("Actual Screen Size", GUIViewCore.Instance.ScreenPixelCount);
                    EditorGUILayout.Space();
                }
                EditorGUILayout.EndVertical();
                ShowLastRectDimmed();

                // Allow adjusting the core point base if this is the core.
                if (!AllowEditingCore)
                {
                    EditorGUILayout.BeginVertical();
                    {
                        Vector2Field("GUICore PointBase", GUIViewCore.Instance.sizeInPointSpace);
                        EditorGUILayout.Space();
                    }
                    EditorGUILayout.EndVertical();
                    ShowLastRectDimmed();
                }
                else
                {
                    Vector2 newPointBase =
                        Vector2Field("Point Base", FindProperty("m_pointBase").vector2Value);
                    if (newPointBase != FindProperty("m_pointBase").vector2Value)
                    {
                        FindProperty("m_pointBase").vector2Value = newPointBase;
                        ApplyUpdateAndRecalculateGUI();
                    }
                }
                if (AllowEditingCore)
                {
                    Color currentColor = GUI.color;

                    Camera camera = FindProperty("m_observerCamera").objectReferenceValue as Camera;
                    Camera newCamera = null;

                    using (new FixedWidthLabel("Observer Camera"))
                    {
                        if (camera == null)
                        {
                            GUI.color = Color.red;
                        }

                        newCamera = ObjectField<Camera>(camera, true);
                    }
                    GUI.color = currentColor;


                    if (newCamera != camera)
                    {
                        FindProperty("m_observerCamera").objectReferenceValue = newCamera;
                        //serializedObject.ApplyModifiedProperties();
                        ApplyUpdateAndRecalculateGUI();
                    }
                }

                {
                    FloatField("Point Width", GUIViewCore.Instance.PointWidth);
                    ShowLastRectDimmed();
                }
            }
            EditorGUILayout.Space();
        }
        EditorGUILayout.EndVertical();
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private GUIViewBase m_targetGUIViewBase = null;


    #endregion
}