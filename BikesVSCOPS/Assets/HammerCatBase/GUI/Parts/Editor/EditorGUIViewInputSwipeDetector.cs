using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GUIViewInputSwipeDetector))]
public class EditorGUIViewInputSwipeDetector : EditorGUIViewTextureManipulator
{
	#region Protected Methods

	protected override void DisplayFields()
	{
		DisplayDebugFields();

		DisplayBasicSettings();

		DisplaySwipeDetectorViewSettings();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplaySwipeDetectorViewSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		if(!ShowToggleField(ShowSwipeDetectorSettings, "Swipe Detector Settings"))
		{
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			return;
		}

		DisplayAxisEnabled();

		DisplayAllowOutside();

		DisplayVelocityThreshold();

		EditorGUILayout.EndVertical();
	}

	private void DisplayVelocityThreshold()
	{
		{
			float value = FindProperty("m_cursorVelocityNeededToFastSwipe").floatValue;
			float newValue = FloatField(new GUIContent("Cursor Velocity Needed To Fast Swipe",
				"Takes into acount the total movment of the press over the time."), value);
			if(newValue != value)
			{
				FindProperty("m_cursorVelocityNeededToFastSwipe").floatValue = newValue;
				ApplyUpdateAndRecalculateGUI();
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private EditorProfileKey<int, bool> ShowSwipeDetectorSettings = new EditorProfileKey<int, bool>("ShowSwipeDetectorSettings", true);

	#endregion
}