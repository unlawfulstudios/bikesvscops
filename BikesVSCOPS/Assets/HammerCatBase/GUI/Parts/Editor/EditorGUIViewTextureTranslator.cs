using UnityEditor;

[CustomEditor(typeof(GUIViewTextureTranslator))]
public class EditorGUIViewTextureTranslator : EditorGUIViewTextureManipulator
{
	protected override void DisplayFields()
	{
		base.DisplayFields();

		DisplayTranslationSettings();
	}

	private void DisplayTranslationSettings()
	{
		using(new GUIGroupAlignment(GroupAlignment.Vertical, "Box"))
		{
			if(!ShowToggleField(ShowTextureTranslatonSettings, "Translator Settings"))
			{
				EditorGUILayout.Space();
				return;
			}

			DisplayYAxisDirection();

			EditorGUILayout.Space();
		}
	}

	protected void DisplayYAxisDirection()
	{
		bool prevValue = FindProperty("m_reverseY").boolValue;
		bool newValue = Toggle("Start on top", prevValue);
		if(prevValue != newValue)
		{
			FindProperty("m_reverseY").boolValue = newValue;
			SerilizedTarget.ApplyModifiedProperties();
		}
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private EditorProfileKey<int, bool> ShowTextureTranslatonSettings = new EditorProfileKey<int, bool>("ShowTextureTranslatonSettings", true);

	#endregion
}