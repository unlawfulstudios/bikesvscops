using UnityEditor;
using UnityEngine;
using System.Reflection;

[CustomEditor(typeof(GUIView))]
public class EditorGUIView : EditorGUIViewBase
{
    #region Public Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods
    private void OnEnable()
    {
        ShowAnchorFileds = new EditorProfileKey<int, bool>("ShowAnchorFileds", false);
        ShowViewFileds = new EditorProfileKey<int, bool>("ShowViewFileds", true);
    }

    protected override void DisplayFields()
	{
		base.DisplayFields();


          

        DisplayBasicSettings();
	}


	protected void DisplayBasicSettings()
	{
		EditorGUILayout.BeginVertical("Box");
		{
			if(ShowToggleField(ShowViewFileds, "Base Settings"))
			{
				DisplayToggle();

				DisplayPositionFields();

				DisplaySizeFields();

				DisplayLayerFields();

				DrawAnchorFields();

				DrawPinnedFields();
			}

			EditorGUILayout.Space();
		}
		EditorGUILayout.EndVertical();
	}


	protected override void ApplyUpdateAndRecalculateGUI()
	{
		if(!Application.isPlaying)
		{
			NotGatheredPinnedData = true;
		}


		SerilizedTarget.ApplyModifiedProperties();
		EditorUtility.SetDirty(TargetGUIView);
		GUIViewCore.Instance.ReCalculate();
	}


	protected void DisplayToggle()
	{
		Toggle("Toggled", TargetGUIView.toggle);
		ShowLastRectDimmed();
	}


	protected void DisplayLayerFields()
	{
		EditorGUILayout.BeginHorizontal();
		{
			float newLayer = FloatField("Layer above parent", FindProperty("m_layerAboveParent").floatValue);
			if(newLayer != FindProperty("m_layerAboveParent").floatValue)
			{
				FindProperty("m_layerAboveParent").floatValue = newLayer;
				ApplyUpdateAndRecalculateGUI();
			}

			FloatField("Layer", TargetGUIView.finalLayer);
			ShowLastRectDimmed();
		}
		EditorGUILayout.EndHorizontal();
	}


	protected void DrawAnchorFields()
	{
		if(!ShowToggleField(ShowAnchorFileds, "Anchors"))
		{
			return;
		}

			
		GUILayout.Space(25f);
		GUIViewAnchor anchor = TargetGUIView.anchor;
		GUIViewAnchor alignment = TargetGUIView.alignment;

		for(int yIt=(int)GUIViewAnchorY.Top; yIt >= (int)GUIViewAnchorY.Bottom; --yIt)
		{
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(10f);
			EditorGUILayout.BeginHorizontal();
			for(int xIt=0; xIt < (int)GUIViewAnchorX.Max; ++xIt)
			{
				string title = (xIt == (int)GUIViewAnchorX.Center && yIt == (int)GUIViewAnchorY.Top) ? "Anchor" : "";

				GUIStyle buttontype = ButtonSkin.GetStyle("Button Arrow Blank");
				
				if((int)anchor.Horizontal == xIt && (int)anchor.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Selected");
				}
				else if((int)anchor.Horizontal == xIt && (int)anchor.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up");
				}
				else if((int)anchor.Horizontal == xIt + 1 && (int)anchor.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up Right");
				}
				else if((int)anchor.Horizontal == xIt + 1 && (int)anchor.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Right");
				}
				else if((int)anchor.Horizontal == xIt + 1 && (int)anchor.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down Right");
				}
				else if((int)anchor.Horizontal == xIt && (int)anchor.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down");
				}
				else if((int)anchor.Horizontal == xIt - 1 && (int)anchor.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down Left");
				}
				else if((int)anchor.Horizontal == xIt - 1 && (int)anchor.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Left");
				}
				else if((int)anchor.Horizontal == xIt - 1 && (int)anchor.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up Left");
				}


				bool currAnchorButton = false;
				currAnchorButton = GUILayout.Toggle(currAnchorButton,
					title,
					buttontype);
				if(currAnchorButton)
				{
					TargetGUIView.anchor = new GUIViewAnchor((GUIViewAnchorX)xIt, (GUIViewAnchorY)yIt);
					//SerilizedTarget.Update();
					//GUICore.Instance.ReCalculate();
					ApplyUpdateAndRecalculateGUI();
				}
			}
			EditorGUILayout.EndHorizontal();

			//GUILayout.Space(80f);
			EditorGUILayout.BeginHorizontal();
			for(int xIt=0; xIt < (int)GUIViewAnchorX.Max; ++xIt)
			{
				string title = (xIt == (int)GUIViewAnchorX.Center && yIt == (int)GUIViewAnchorY.Top) ? "Alignment" : "";
				
				GUIStyle buttontype = ButtonSkin.GetStyle("Button Arrow Blank");
				
				if((int)alignment.Horizontal == xIt && (int)alignment.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Selected");
				}
				else if((int)alignment.Horizontal == xIt && (int)alignment.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down");
				}
				else if((int)alignment.Horizontal == xIt + 1 && (int)alignment.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down Left");
				}
				else if((int)alignment.Horizontal == xIt + 1 && (int)alignment.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Left");
				}
				else if((int)alignment.Horizontal == xIt + 1 && (int)alignment.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up Left");
				}
				else if((int)alignment.Horizontal == xIt && (int)alignment.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up");
				}
				else if((int)alignment.Horizontal == xIt - 1 && (int)alignment.Vertical == yIt - 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Up Right");
				}
				else if((int)alignment.Horizontal == xIt - 1 && (int)alignment.Vertical == yIt)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Right");
				}
				else if((int)alignment.Horizontal == xIt - 1 && (int)alignment.Vertical == yIt + 1)
				{
					buttontype = ButtonSkin.GetStyle("Button Arrow Down Right");
				}
				
				bool currAlignmentButton = false;
				currAlignmentButton = GUILayout.Toggle(currAlignmentButton,
					title,
					buttontype);
				if(currAlignmentButton)
				{
					TargetGUIView.alignment = new GUIViewAnchor((GUIViewAnchorX)xIt, (GUIViewAnchorY)yIt);
//					SerilizedTarget.Update();
//					GUICore.Instance.ReCalculate();
					ApplyUpdateAndRecalculateGUI();
				}
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndHorizontal();
		}
	}


	protected void DrawPinnedFields()
	{
		using(new FixedWidthLabel(new GUIContent("Pinned On Side", "Will extend the view's scale using the selected side as the point to extend from.")))
		{
			{
				bool newScale = Toggle("Left", FindProperty("m_pinnedToLeft").boolValue);
				if(newScale != FindProperty("m_pinnedToLeft").boolValue)
				{
					FindProperty("m_pinnedToLeft").boolValue = newScale;
					ApplyUpdateAndRecalculateGUI();
				}
			}
	
			{
				bool newScale = Toggle("Right", FindProperty("m_pinnedToRight").boolValue);
				if(newScale != FindProperty("m_pinnedToRight").boolValue)
				{
					FindProperty("m_pinnedToRight").boolValue = newScale;
					ApplyUpdateAndRecalculateGUI();
				}
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIView TargetGUIView
	{
		get
		{
			if(this.m_targetGUIView == null)
			{
				this.m_targetGUIView = Target as GUIView;
			}
			return this.m_targetGUIView;
		}
	}


	public GUISkin ButtonSkin
	{
		get
		{
			if(this.m_buttonSkin == null)
			{
				this.m_buttonSkin = EditorGUIUtility.Load("HammerCatEditor/HcEditorSkin.guiskin") as GUISkin;
			}
			return this.m_buttonSkin;
		}
	}


	private bool NotGatheredPinnedData
	{
		get
		{
			FieldInfo getNotGatheredPinnedData = typeof(GUIView).GetField("m_notGatheredPinnedData",
				BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
			return (bool)getNotGatheredPinnedData.GetValue(TargetGUIView);
		}
		set
		{
			FieldInfo setNotGatheredPinnedData = typeof(GUIView).GetField("m_notGatheredPinnedData",
				BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);
			setNotGatheredPinnedData.SetValue(TargetGUIView, (object)value);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void DisplayPositionFields()
	{
		Vector2Field("Position (positionInPointSpaceAbsolute - Alignment)", TargetGUIView.positionInPointSpace);
		ShowLastRectDimmed();

		Vector2Field("Absolute Position In Points", TargetGUIView.positionInPointSpaceAbsolute);
		ShowLastRectDimmed();

//		Vector2Field("Mid Point Anchor", TargetGUIView.midAnchorPoint);
//		ShowLastRectDimmed();

		Vector2 newPos = Vector2Field("Local Position In Points", FindProperty("m_localPositionInPoints").vector2Value);
		if(newPos != FindProperty("m_localPositionInPoints").vector2Value)
		{
			FindProperty("m_localPositionInPoints").vector2Value = newPos;
			ApplyUpdateAndRecalculateGUI();
		}
	}


	private void DisplaySizeFields()
	{
		Vector2 newSize = Vector2Field("Size In Points", FindProperty("m_sizeInPoints").vector2Value);
		if(newSize != FindProperty("m_sizeInPoints").vector2Value)
		{
			FindProperty("m_sizeInPoints").vector2Value = newSize;
			ApplyUpdateAndRecalculateGUI();
		}

		EditorGUILayout.BeginVertical();
		{
			{
				Vector2 newScale = Vector2Field("Local Scale", FindProperty("m_scale").vector2Value);
				if(newScale != FindProperty("m_scale").vector2Value)
				{
					FindProperty("m_scale").vector2Value = newScale;
					ApplyUpdateAndRecalculateGUI();
				}

				Vector2Field("Final Scale", TargetGUIView.scale);
				ShowLastRectDimmed();
			}

			using(new FixedWidthLabel(new GUIContent("Scale with Screen Width", "Will scale an image based on the diffrence of the core and screen. Primaraly used to allow core sized textures to fill the screen.")))
			{
				{
					bool newScale = Toggle("X", FindProperty("m_scaleXWithScreenWidth").boolValue);
					if(newScale != FindProperty("m_scaleXWithScreenWidth").boolValue)
					{
						FindProperty("m_scaleXWithScreenWidth").boolValue = newScale;
						ApplyUpdateAndRecalculateGUI();
					}
				}

				{
					bool newScale = Toggle("Y", FindProperty("m_scaleYWithScreenWidth").boolValue);
					if(newScale != FindProperty("m_scaleYWithScreenWidth").boolValue)
					{
						FindProperty("m_scaleYWithScreenWidth").boolValue = newScale;
						ApplyUpdateAndRecalculateGUI();
					}
				}
			}
		}
		EditorGUILayout.EndVertical();

	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIView m_targetGUIView = null;
	private EditorProfileKey<int, bool> ShowAnchorFileds ;
	private EditorProfileKey<int, bool> ShowViewFileds ;
	private GUISkin m_buttonSkin = null;

    #endregion



}