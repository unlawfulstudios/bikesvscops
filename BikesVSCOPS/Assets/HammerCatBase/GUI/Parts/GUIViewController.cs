using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GUIViewController : GUIView
{
    #region Public Properties


    private void Awake()
    {
        m_introTransitions = new List<GUIViewTransition>();
        m_exitTransitions = new List<GUIViewTransition>();
    }
 



    public GUIViewButtonEventList EventList
	{
		get	{ return this.m_eventList; }
		private set
		{
			this.m_eventList = value;
			this.m_masterEventList = null;
		}
	}


	public void Transition(GUIViewTransition.TranistionType transitionType, GUIViewController nextController)
	{
		List<GUIViewTransition> transitions = transitionType == GUIViewTransition.TranistionType.Intro ? IntroTransitions : ExitTransitions;
		Queue<GUIViewTransition> transitionQueue = new Queue<GUIViewTransition>(transitions);

		if(transitionQueue.Count == 0)
		{
			if(nextController != null && nextController != this)
			{
				if(transitionType != GUIViewTransition.TranistionType.Exit)
				{
					Debug.LogError("Only exit tranistions should have a next controller.", this);
				}
				nextController.Transition(GUIViewTransition.TranistionType.Intro, null);
			}
			else
			{
				if(transitionType == GUIViewTransition.TranistionType.Intro)
				{
					if(GUIViewCore.Instance.ActiveController != this)
					{
						GUIViewCore.Instance.ActivateController(this);
					}
				}
			}
			return;
		}


		GUIViewTransition nextTransition = transitionQueue.Dequeue();
		if(transitionType == GUIViewTransition.TranistionType.Intro)
		{
			nextTransition.Activate(transitionQueue, this);
		}
		else
		{
			nextTransition.DeActivate(transitionQueue, nextController);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void OnEnable()
	{
		recalculatesViewInformation = false;
		base.OnEnable();
	}


	protected override void ProcessToggle()
	{
		base.ProcessToggle();
		if(EventList != null)
		{
			EventList.toggle = toggle;
		}

		if(toggle && Application.isPlaying)
		{
			GUIViewInputManager.Instance.enabled = true;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private List<MethodInfo> MasterEventList
	{
		get
		{
			if(this.m_masterEventList == null)
			{
				this.m_masterEventList = new List<MethodInfo>();
				GetDerivedClassButtonEvents();
			}
			return this.m_masterEventList;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void GetDerivedClassButtonEvents()
	{
		MasterEventList.Clear();
		if(m_eventList != null)
		{
			Type eventType = m_eventList.GetType();
			MethodInfo[] derivedMethodInfos = eventType.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			foreach(MethodInfo methodInfo in derivedMethodInfos)
			{
				ParameterInfo[] parameterInfos = methodInfo.GetParameters();
				if(parameterInfos.Length == 1)
				{
					if(parameterInfos[0].ParameterType == typeof(GUIViewButton))
					{
						MasterEventList.Add(methodInfo);
					}
				}
			}
		}
	}


	private List<GUIViewTransition> IntroTransitions
	{
		get { return m_introTransitions; }
	}


	private List<GUIViewTransition> ExitTransitions
	{
		get { return m_exitTransitions; }
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private GUIViewButtonEventList m_eventList = null;
	[SerializeField]
	private List<GUIViewTransition> m_introTransitions;
	[SerializeField]
	private List<GUIViewTransition> m_exitTransitions;
	private List<MethodInfo> m_masterEventList = null;
    
	#endregion
}