using UnityEngine;
using System.Collections;

public abstract class GUIViewEffect : MonoBehaviour
{
	#region Public Methods

	public abstract bool UpdateEffect(GUIView targetForEffect, float lerpValue);

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected bool ApplyToSelf
	{
		get	{ return this.m_applyToSelf; }
	}


	protected bool ApplyToChildrenGUIViews
	{
		get	{ return this.m_applyToChildrenGUIViews; }
	}


	protected bool ApplyToChildrenGUIViewWithSelfValue
	{
		get { return this.m_applyToChildrenGUIViewWithSelfValue; }
	}

	
	protected GUIView EvaluatingGUIView
	{
		get
		{
			if(this.m_evaluatingGUIView == null)
			{
				this.m_evaluatingGUIView = GetComponent<GUIView>();
				if(this.m_evaluatingGUIView == null)
				{
					Debug.Log("Missing sibling GUI View", this);
				}
			}
			return this.m_evaluatingGUIView;
		}
	}


	protected GUIViewEffectController TargetGUIViewEffectController
	{
		get
		{
			if(this.m_targetGUIViewEffectController == null)
			{
				this.m_targetGUIViewEffectController = GetComponent<GUIViewEffectController>();
				if(this.m_targetGUIViewEffectController == null)
				{
					Debug.Log("Missing evaluating target GUI View Effect Controller", this);
				}
			}
			return this.m_targetGUIViewEffectController;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Update()
	{
		if(ApplyToSelf)
		{
			if(UpdateEffect(EvaluatingGUIView, TargetGUIViewEffectController.EvaluateLerpValue(EvaluatingGUIView)))
			{
				EvaluatingGUIView.ReCalculate();
			}
		}
		if(ApplyToChildrenGUIViews)
		{
			GUIView[] childViews = gameObject.GetComponentsInChildren<GUIView>();
			foreach(GUIView childView in childViews)
			{
				if(childView != EvaluatingGUIView)
				{
					if(UpdateEffect(childView, TargetGUIViewEffectController.EvaluateLerpValue(childView)))
					{
						childView.ReCalculate();
					}
				}
			}
		}
		if(ApplyToChildrenGUIViewWithSelfValue)
		{
			float lerpValue = TargetGUIViewEffectController.EvaluateLerpValue(EvaluatingGUIView);
			if(UpdateEffect(EvaluatingGUIView, lerpValue))
			{
				EvaluatingGUIView.ReCalculate();
			}

			GUIView[] childViews = gameObject.GetComponentsInChildren<GUIView>();
			foreach(GUIView childView in childViews)
			{
				if(childView != EvaluatingGUIView)
				{
					if(UpdateEffect(childView, lerpValue))
					{
						childView.ReCalculate();
					}
				}
			}
		}


	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	[SerializeField]
	private bool m_applyToSelf = true;
	[SerializeField]
	private bool m_applyToChildrenGUIViews = false;
	[SerializeField]
	private bool m_applyToChildrenGUIViewWithSelfValue = false;
	[SerializeField]
	private GUIViewEffectController m_targetGUIViewEffectController = null;
	private GUIView m_evaluatingGUIView = null;

	#endregion
}
