using UnityEngine;
using System.Collections;

public abstract class GUIViewEffectController : MonoBehaviour
{
	#region Public Methods

	public abstract float EvaluateLerpValue(GUIView view);

	public virtual void Reset()
	{
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected GUIView EvaluatingGUIView
	{
		get
		{
			if(this.m_evaluatingGUIView == null)
			{
				this.m_evaluatingGUIView = GetComponent<GUIView>();
				if(this.m_evaluatingGUIView == null)
				{
					Debug.LogError("Missing sibling GUI View", this);
				}
			}
			return this.m_evaluatingGUIView;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	[SerializeField]
	private GUIView m_evaluatingGUIView = null;

	#endregion
}
