using UnityEngine;
using System.Collections;

public class GUIViewEffect_Scale : GUIViewEffect
{
	#region Public Methods

	public override bool UpdateEffect(GUIView view, float lerpValue)
	{
		Vector2 newScale = Vector2.one * Mathf.Lerp(ScaleRange.x, ScaleRange.y, lerpValue);

		bool changed = newScale != view.scale;

		if(changed)
		{
			view.scale = newScale;
			view.ReCalculateNow(true);
		}

		return changed;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected Vector2 ScaleRange
	{
		get	{ return this.m_scaleRange; }
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	[MinMax]
	private Vector2 m_scaleRange = Vector2.up;

	#endregion
}
