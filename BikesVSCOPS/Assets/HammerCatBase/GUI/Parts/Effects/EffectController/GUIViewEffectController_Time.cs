using UnityEngine;
using System.Collections;

public class GUIViewEffectController_Time : GUIViewEffectController
{
	#region Public Properties

	public override float EvaluateLerpValue(GUIView view)
	{
		float currentDelta = Time.time - TimeStarted;

		return Mathf.Min(1f, Mathf.Lerp(0f, 1f, currentDelta / TotalTime));
	}


	public override void Reset()
	{
		TimeStarted = Time.time;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private float TotalTime
	{
		get { return this.m_totalTime; }
		set	{ m_totalTime = value; }
	}


	private float TimeStarted
	{
		get	{ return this.m_timeStarted; }
		set	{ m_timeStarted = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private float m_totalTime = 1f;
	// Member
	private float m_timeStarted = 0f;

	#endregion
}
