using UnityEngine;
using System.Collections;

public class GUIViewEffectController_Translation : GUIViewEffectController
{
	#region Public Properties

	public override float EvaluateLerpValue(GUIView view)
	{
		UpdateEffectRangesForScaled();

		Vector2 screenSpacePos = GUIViewCore.Instance.GetPositionInScreenSpace(view.positionInPointSpace);

		float lerpX = AxisEvaluated.x ? GetAxisLerpValue(screenSpacePos.x, EffectRangeXScaled) : 1f;
		float lerpY = AxisEvaluated.y ? GetAxisLerpValue(screenSpacePos.y, EffectRangeYScaled) : 1f;

//		if(view.name == "IMG_LEVEL3")
//		{
//			Debug.Log((lerpX * lerpY).ToString());
//		}

		return lerpX * lerpY;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private PointWithinRange EffectRangeX
	{
		get	{ return this.m_effectRangeX; }
	}


	private PointWithinRange EffectRangeY
	{
		get	{ return this.m_effectRangeY; }
	}


	private AxisToggle AxisEvaluated
	{
		get	{ return this.m_axisEvaluated; }
	}


	public PointWithinRange EffectRangeXScaled
	{
		get	{ return this.m_effectRangeXScaled; }
		set	{ m_effectRangeXScaled = value; }
	}


	public PointWithinRange EffectRangeYScaled
	{
		get	{ return this.m_effectRangeYScaled; }
		set	{ m_effectRangeYScaled = value; }
	}


	public Vector2 ScreenPixelCount
	{
		get	{ return this.m_screenPixelCount; }
		set	{ m_screenPixelCount = value; }
	}


	public Vector2 ScaledScreenSize
	{
		get	{ return this.m_scaledScreenSize; }
		set	{ m_scaledScreenSize = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private static float GetAxisLerpValue(float axisValueInScaled, PointWithinRange pointWithinRangeInScaled)
	{

		if(axisValueInScaled < pointWithinRangeInScaled.point)
		{
			float normalizedRange = pointWithinRangeInScaled.point - pointWithinRangeInScaled.range.x;
			float rangedEvaluationPoint = axisValueInScaled - pointWithinRangeInScaled.range.x;

			return Mathf.Clamp01(rangedEvaluationPoint / normalizedRange);
		}
		else if(axisValueInScaled > pointWithinRangeInScaled.point)
			{
				float normalizedRange = pointWithinRangeInScaled.range.y - pointWithinRangeInScaled.point;

				float rangedEvaluationPoint = axisValueInScaled - pointWithinRangeInScaled.point;

				return 1f - Mathf.Clamp01(rangedEvaluationPoint / normalizedRange);
			}
			else
			{
				return 1f;
			}
	}


	private void UpdateEffectRangesForScaled()
	{
		if(ScreenPixelCount != GUIViewCore.Instance.ScreenPixelCount)
		{
			ScreenPixelCount = GUIViewCore.Instance.ScreenPixelCount;

			ScaledScreenSize = new Vector2(
				ScreenPixelCount.x / GUIViewCore.Instance.sizeInPointSpace.x,
				ScreenPixelCount.y / GUIViewCore.Instance.sizeInPointSpace.y);

			Vector2 min = new Vector2(
				EffectRangeX.range.x,
				EffectRangeY.range.x);

			Vector2 max = new Vector2(
				EffectRangeX.range.y,
				EffectRangeY.range.y);

			Vector2 center = new Vector2(
				EffectRangeX.point,
				EffectRangeY.point);

			EffectRangeXScaled.range = new Vector2(min.x * ScaledScreenSize.x, max.x * ScaledScreenSize.x);
			EffectRangeXScaled.point = center.x * ScaledScreenSize.x;

			EffectRangeYScaled.range = new Vector2(min.y * ScaledScreenSize.y, max.y * ScaledScreenSize.y);
			EffectRangeYScaled.point = center.y * ScaledScreenSize.y;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private AxisToggle m_axisEvaluated = new AxisToggle();
	[SerializeField]
	private PointWithinRange m_effectRangeX = new PointWithinRange();
	[SerializeField]
	private PointWithinRange m_effectRangeY = new PointWithinRange();
	private PointWithinRange m_effectRangeXScaled = new PointWithinRange();
	private PointWithinRange m_effectRangeYScaled = new PointWithinRange();
	private Vector2 m_screenPixelCount = Vector2.zero;
	private Vector2 m_scaledScreenSize = Vector2.zero;

	#endregion
}
