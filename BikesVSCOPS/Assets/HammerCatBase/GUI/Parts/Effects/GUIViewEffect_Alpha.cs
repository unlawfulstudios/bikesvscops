using UnityEngine;

public class GUIViewEffect_Alpha : GUIViewEffect
{
	#region Public Methods

	public override bool UpdateEffect(GUIView view, float lerpValue)
	{
		GUIViewText viewText = view as GUIViewText;
		GUIViewTexture viewTexture = view as GUIViewTexture;
		if(viewTexture == null && viewText == null)
		{
			return false;
		}

		float baseAlpha = view.alpha;
		baseAlpha = Mathf.Lerp(AlphaRange.x, AlphaRange.y, lerpValue);

		view.alpha = baseAlpha;

		// This should not cause a recalculation as it is only changing color values.
		return false;
	}
	
	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected Vector2 AlphaRange
	{
		get	{ return this.m_alphaRange; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	[MinMax]
	private Vector2 m_alphaRange = Vector2.up;
	//private float 	m_lerpVal = 0.0f;

	#endregion
}
