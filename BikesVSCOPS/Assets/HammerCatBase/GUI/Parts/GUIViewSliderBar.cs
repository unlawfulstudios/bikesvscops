using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class GUIViewSliderBar : GUIViewTextureTranslator
{
	#region Members

	// Editor Exposed
	[SerializeField]
	private bool m_xAxis = true;
	[SerializeField]
	private GUIViewTexture m_bar = null;
	[SerializeField]
	private GUIViewTexture m_thumb = null;

	// Member
	private Rect m_translationalAmount;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public GUIViewTexture thumb
	{
		get
		{
			if(this.m_thumb == null)
			{
				GameObject newGO = new GameObject("thumb");
				this.m_thumb = newGO.AddComponent<GUIViewTexture>();

				this.m_thumb.transform.parent = Bar.transform;
				newGO.transform.localPosition = new Vector3(0f, 0f, 0);
				newGO.transform.localScale = Vector3.one;

				this.m_thumb.sizeInPointSpace = new Vector2(sizeInPointSpace.x * 0.1f, sizeInPointSpace.y);
				SetBarAndThumbUp(true);
			}

			return this.m_thumb;
		}
	}


	public GUIViewTexture Bar
	{
		get
		{
			if(this.m_bar == null)
			{
				GameObject newGO = new GameObject("Bar");
				this.m_bar = newGO.AddComponent<GUIViewTexture>();

				this.m_bar.transform.parent = transform;
				newGO.transform.localPosition = new Vector3(0f, 0f, 0);
				newGO.transform.localScale = Vector3.one;

				this.m_bar.sizeInPointSpace = new Vector2(sizeInPointSpace.x * 0.9f, sizeInPointSpace.y * 0.5f);
				SetBarAndThumbUp(true);
			}

			return this.m_bar;
		}
	}


	public Vector2 Range
	{
		get	{ return this.m_range; }
		set { this.m_range = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected override Rect TranslationBoundsInScreenSpace
	{
		get
		{
			Rect textureScreenRect = ViewAreaManipulating.viewGuiTexture.GetScreenRect();

			Rect barRect = Bar.viewGuiTexture.GetScreenRect();
			if(AllowAxisX)
			{
				textureScreenRect.x = barRect.x;
				textureScreenRect.width = barRect.width;
			}
			if(AllowAxisY)
			{
				textureScreenRect.y = barRect.y;
				textureScreenRect.height = barRect.height;
			}

			return textureScreenRect;
		}
	}


	protected override Rect TranslationBoundsInPointSpace
	{
		get { return m_translationalAmount; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private bool XAxis
	{
		get	{ return this.m_xAxis; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Awake()
	{
		ToggleEvent += SetBarAndThumbUp;
		SetBarAndThumbUp(toggle);
	}


	private void SetBarAndThumbUp(bool value)
	{
		if(!value)
		{
			return;
		}
		ViewAreaManipulating = this;
		AllowAxisX = XAxis;
		AllowAxisY = !XAxis;
		Target = thumb.transform;

		Bar.localPositionInPointSpace = Vector2.zero;
		Bar.anchor = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
		Bar.alignment = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
		Bar.layerAboveParent = 1f;

		thumb.localPositionInPointSpace = Vector2.zero;
		if(XAxis)
		{
			thumb.anchor = new GUIViewAnchor(GUIViewAnchorX.Left, GUIViewAnchorY.Middle);
		}
		else
		{
			thumb.anchor = new GUIViewAnchor(GUIViewAnchorX.Center, m_reverseY ? GUIViewAnchorY.Top : GUIViewAnchorY.Bottom);
		}
		thumb.alignment = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
		thumb.layerAboveParent = 2f;

		sliderFractionalAmount = Vector2.zero;

		stateGuiObjects[(int)ButtonState.Enabled].Clear();
		stateGuiObjects[(int)ButtonState.OnDown].Clear();
		stateGuiObjects[(int)ButtonState.Selected].Clear();

		stateGuiObjects[(int)ButtonState.Enabled].Add(Bar);
		stateGuiObjects[(int)ButtonState.Enabled].Add(thumb);

		stateGuiObjects[(int)ButtonState.OnDown].Add(Bar);
		stateGuiObjects[(int)ButtonState.OnDown].Add(thumb);

		if(hasHitEvent)
		{
			stateGuiObjects[(int)ButtonState.Selected].Add(Bar);
			stateGuiObjects[(int)ButtonState.Selected].Add(thumb);
		}

		this.CanManipulationOutsideArea = true;

		CalculateTranslationalAmount();
	}


	private void CalculateTranslationalAmount()
	{
		if(!Application.isPlaying)
		{
			return;
		}

		float initialValue = -1f;

		// If this has not been calculated yet, this must be the first time it is shown, so leave it at zed.
		Rect initialTranslationalValue = TranslationBoundsInPointSpace;
		if(initialTranslationalValue.width > 0f && initialTranslationalValue.height > 0f)
		{
			initialValue = sliderFractionalAmountF;
		}

		StopCoroutine("SetDefaultSettings");
		StartCoroutine("SetDefaultSettings", initialValue);
	}


	private IEnumerator SetDefaultSettings(float initialValue)
	{
		while(!hasRunInitialCalculate)
		{
			yield return new WaitForEndOfFrame();
		}

		// Make sure the bars rect is set up
		Bar.ReCalculateNow(false);

		Vector2 pointSpaceSizeEvenIfPinned = GUIViewCore.Instance.GetScreenSpaceSizeInPointSpace(
			new Vector2(Bar.rectInScreenSpace.width, Bar.rectInScreenSpace.height));

		// Evaluate the area to translate across.
		m_translationalAmount = new Rect(
			Bar.localPositionInPointSpace.x,
			Bar.localPositionInPointSpace.y,
			pointSpaceSizeEvenIfPinned.x,
			pointSpaceSizeEvenIfPinned.y);

		// If we recived a valid width/height (the core has updated at least once successfully), set the value
		if(initialValue != -1f)
		{
			sliderFractionalAmountF = initialValue;
		}
	}

	#endregion
}