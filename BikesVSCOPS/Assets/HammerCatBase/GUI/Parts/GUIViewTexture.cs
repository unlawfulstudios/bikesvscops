using UnityEngine;

[ExecuteInEditMode]
public class GUIViewTexture : GUIView
{
	#region Private Members

	[SerializeField]
	private Texture m_actualViewTexture = null;
	[SerializeField]
	private Color m_viewTextureColor = Color.gray;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public static Texture LoadDefaultTexture()
	{
		return Resources.Load("DefaultObjects/blankTexture") as Texture;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public override Color color
	{
		get	{ return this.m_viewTextureColor; }
		set
		{
			this.m_viewTextureColor = value;
			viewGuiTexture.color = value;
		}
	}


	public override float alpha
	{
		get { return this.color.a; }
		set
		{
			m_viewTextureColor.a = value;
			viewGuiTexture.color = m_viewTextureColor;
		}
	}


	public virtual Texture viewTexture
	{
		get
		{
			if(this.m_actualViewTexture == null)
			{
				if(viewGuiTexture.texture == null)
				{
					//Debug.LogWarning("Missing texture for GUIViewTexture, loading default", this);
					viewGuiTexture.texture = LoadDefaultTexture();
				}
				this.m_actualViewTexture = viewGuiTexture.texture;
			}
			return this.m_actualViewTexture;
		}
		set
		{
			if(this.m_actualViewTexture != null)
			{
				//DestroyImmediate(this.m_actualViewTexture, true);
			}
			this.m_actualViewTexture = value;
			viewGuiTexture.texture = this.m_actualViewTexture;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected override Texture debugTexture
	{
		get	{ return viewTexture; }
	}


	protected override Color debugColor
	{
		get
		{
			return new Color(
				1f,
				1f - color.b,
				1f,
				0.75f);
		}
	}


	public override Color defaultColor
	{
		get { return this.color; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void OnEnable()
	{
		viewGuiTexture.texture = viewTexture;

		base.OnEnable();
	}


	protected override void RemoveDebugTexture()
	{
		viewGuiTexture.enabled = toggle;
	}

	#endregion
}