using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public abstract class GUIViewInput : GUIView
{
	#region Public Properties

	public bool lockInput
	{
		get { return m_lockInput; }
		set { m_lockInput = value; }
	}

	public bool inputEnabled
	{
		get	{ return this.m_inputEnabled; }
		set	{ m_inputEnabled = value; }
	}

	public bool multiTouchInput
	{
		get	{ return this.m_multiTouchInput; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Defines

	protected delegate void ProcessStateUpdateMethod();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected ProcessStateUpdateMethod ProcessStateUpdate = null;


	protected CursorState inputManagerCursorState
	{
		get	{ return GUIViewInputManager.Instance.CurrentCursorState; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void LateUpdate()
	{
		base.LateUpdate();

		if(!Application.isPlaying)
		{
			return;
		}
		if(ProcessStateUpdate != null)
		{
			ProcessStateUpdate();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private bool m_multiTouchInput = false;
	private bool m_inputEnabled = true;
	private bool m_lockInput = false;

	#endregion
}