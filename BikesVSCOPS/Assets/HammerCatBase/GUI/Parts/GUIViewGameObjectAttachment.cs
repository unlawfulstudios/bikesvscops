using UnityEngine;
using HammerCat;

[ExecuteInEditMode]
public class GUIViewGameObjectAttachment : GUIView
{
	#region Public Properties

	public bool clampToScreenEdge = false;
	public bool hideIfOnScreen = false;

	public GameObject AttachedGameObject
	{
		get	{ return this.m_attachedGameObject; }
		set
		{
			if(m_attachedGameObject != value)
			{
				m_attachedGameObject = value;
				this.transform.parent = GUIViewCore.Instance.transform;
				toggle = m_attachedGameObject != null;
				if(toggle)
				{
					Update();
					ReCalculateNow(true);
				}
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void ForceUpdate()
	{
		Update();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected void Update()
	{
		UpdateConnectionToAttachedGameObject();

		if(AttachedGameObject != null)
		{
			Vector2 attachmentPointInScreenSpace = GUIViewCore.Instance.ObserverCamera.WorldToScreenPoint(AttachedGameObject.transform.position);

			Vector2 newLocalPos = GUIViewCore.Instance.GetScreenSpacePositionInPointSpace(attachmentPointInScreenSpace);

			//bool wasHidden = false;

			float halfWidth = sizeInPointSpace.x*0.5f;
			float halfHeight = sizeInPointSpace.y*0.5f;
			float xMin = halfWidth;
			float xMax = GUIViewCore.Instance.PointWidth-halfWidth;
			float yMin = halfHeight;
			float yMax = GUIViewCore.Instance.sizeInPointSpace.y-halfHeight;

			if(hideIfOnScreen == true)
			{
				if(newLocalPos.x > xMin
					&& newLocalPos.x < xMax
					&& newLocalPos.y > yMin
					&& newLocalPos.y < yMax)
				{
					if(toggle == true)
					{
						toggle = false;
					}
				}
				else if(toggle == false)
				{
					toggle = true;
				}
			}

			if(clampToScreenEdge == true)
			{
				if(newLocalPos.x < xMin)
				{
					newLocalPos.x = xMin;
				}
				else if(newLocalPos.x > xMax)
				{
					newLocalPos.x = xMax;
				}

				if(newLocalPos.y < yMin)
				{
					newLocalPos.y = yMin;
				}
				else if(newLocalPos.y > yMax)
				{
					newLocalPos.y = yMax;
				}
			}

			if(localPositionInPointSpace != newLocalPos)
			{
				localPositionInPointSpace = newLocalPos;
				ReCalculate();
			}
		}
	}


	protected override void ProcessToggle()
	{
		base.ProcessToggle();
		if(toggle)
		{
			SetBaseSettings();
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void UpdateConnectionToAttachedGameObject()
	{
//		if(!toggle)
//		{
//			// HACK: to check functionality.
//			GameObject go = GameObject.Find("Robot(User Player #0)");
//			if(go != null)
//			{
//				AttachedGameObject = go;
//			}
//		}
		if(toggle && AttachedGameObject == null)
		{
			toggle = false;
		}
	}


	private void SetBaseSettings()
	{
		if(clampToScreenEdge == true)
		{
			anchor = new GUIViewAnchor(GUIViewAnchorX.Left, GUIViewAnchorY.Bottom);
			alignment = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
		}
		else
		{
			anchor = new GUIViewAnchor(GUIViewAnchorX.Left, GUIViewAnchorY.Bottom);
			alignment = new GUIViewAnchor(GUIViewAnchorX.Left, GUIViewAnchorY.Bottom);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GameObject m_attachedGameObject = null;

	#endregion
}
