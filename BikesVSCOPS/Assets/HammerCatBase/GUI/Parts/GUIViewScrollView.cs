using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GUIViewScrollView : GUIViewTextureTranslator
{
	#region Public Properties

	public GUIView ClosetChildViewToCenter
	{
		get	{ return this.m_closetChildViewToCenter; }
		private set	{ m_closetChildViewToCenter = value; }
	}


	public PointWithinRange RangeX
	{
		get	{ return this.m_rangeX; }
		set
		{
			m_rangeX = value;
		}
	}


	public PointWithinRange RangeY
	{
		get	{ return this.m_rangeY; }
		set
		{
			m_rangeY = value;
		}
	}


	public List<GUIView> ChildViewsHeld
	{
		get
		{
			if(this.m_childViewsHeld == null)
			{
				this.m_childViewsHeld = new List<GUIView>(TargetAsGUIView.GetComponentsInChildren<GUIView>());

				List<GUIView> childrenNotBelongingToList = new List<GUIView>();
				foreach(GUIView childView in this.m_childViewsHeld)
				{
					if(childView.transform.parent != TargetAsGUIView.transform)
					{
						childrenNotBelongingToList.Add(childView);
					}
				}

				foreach(GUIView childView in childrenNotBelongingToList)
				{
					this.m_childViewsHeld.Remove(childView);
				}
			}
			return this.m_childViewsHeld;
		}
	}

	public override Transform Target
	{
		get
		{
			if(this.m_target == null)
			{
				GameObject go = new GameObject("Scrolling Target View");
				GUIView newGUIView = go.AddComponent<GUIView>();
				newGUIView.sizeInPointSpace = sizeInPointSpaceWithoutScale;
				newGUIView.anchor = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
				this.m_target = newGUIView.transform;
				
				stateGuiObjects[(int)ButtonState.Enabled].Clear();
				stateGuiObjects[(int)ButtonState.OnDown].Clear();
				stateGuiObjects[(int)ButtonState.Selected].Clear();

				stateGuiObjects[(int)ButtonState.Enabled].Add(newGUIView);
				stateGuiObjects[(int)ButtonState.OnDown].Add(newGUIView);
				stateGuiObjects[(int)ButtonState.Selected].Add(newGUIView);

				newGUIView.ReCalculate();
			}
			return this.m_target;
		}
		set	{ m_target = value;	}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	protected override void SetAdditionalSettingsPostChildren()
	{
		GetClosestViewHeldToCenterPoint();
		base.SetAdditionalSettingsPostChildren();
	}


	public GUIView GetViewFromCenterInDirection(Vector2 direction)
	{
		float closestAxisPosWithoutGoingUnder = float.PositiveInfinity;

		Vector2 currentOffsetFromCenter = TargetCenterPointBasedOnRanges - TargetAsGUIView.localPositionInPointSpace;

		direction = direction.normalized;

		GUIView closestChildFromCenterWithoutGoingOver = null;

		foreach(GUIView childView in ChildViewsHeld)
		{
			if(childView == ClosetChildViewToCenter)
			{
				continue;
			}
			float childsDistanceFromCurrentOffsetOfCenter = Vector2.Distance(childView.localPositionInPointSpace, currentOffsetFromCenter);
			Vector2 distancePerAxis = childView.localPositionInPointSpace - currentOffsetFromCenter;



			if(Mathf.Abs(direction.x) > 0f)
			{
				float distX = childsDistanceFromCurrentOffsetOfCenter * Mathf.Abs(direction.x);
				float distInDirection = distancePerAxis.x * direction.x;
				if(distX < closestAxisPosWithoutGoingUnder
					&& distX > 0f
					&& distInDirection > 0f)
				{
					closestAxisPosWithoutGoingUnder = distX;
					closestChildFromCenterWithoutGoingOver = childView;
				}
			}

			if(Mathf.Abs(direction.y) > 0f)
			{
				float distY = childsDistanceFromCurrentOffsetOfCenter * Mathf.Abs(direction.y);
				float distInDirection = distancePerAxis.y *= direction.y;
				if(distY < closestAxisPosWithoutGoingUnder
					&& distY > 0f
					&& distInDirection > 0f)
				{
					closestAxisPosWithoutGoingUnder = distY;
					closestChildFromCenterWithoutGoingOver = childView;
				}
			}
		}

		return closestChildFromCenterWithoutGoingOver;
	}


	public void AutoScrollCenterView(Vector2 direction)
	{
		GUIView closestChildFromCenterWithoutGoingOver = GetViewFromCenterInDirection(direction);

		if(closestChildFromCenterWithoutGoingOver != null)
		{
			SetCenterView(closestChildFromCenterWithoutGoingOver, false);
		}
	}


	public void SetCenterView(GUIView targetView, bool instant)
	{
		//Debug.Log("Setting center to " + targetView.name);
		if(!ChildViewsHeld.Contains(targetView))
		{
			string viewsHeld = "View Held: ";
			foreach(GUIView child in ChildViewsHeld)
			{
				viewsHeld += "\"" + child.name + "\",";
			}
			Debug.LogError("This view is not an imediate child of this scroll view target [" + viewsHeld + "]", this);


		}

		if(CorrectingPosition)
		{
			CancelInvoke("CorrectPositionCoRoutine");
		}

		CorrectedInitialPosition = TargetAsGUIView.localPositionInPointSpace;
		CorrectingPosition = true;

		CorrectedTargetPosition = targetView != null ?
			(TargetCenterPointBasedOnRanges - targetView.localPositionInPointSpace)
				: TargetCenterPointBasedOnRanges;

		StartScrollingTime = Time.fixedTime;
		DistanceToCover = Vector2.Distance(CorrectedInitialPosition, CorrectedTargetPosition);
		if(instant)
		{
			TargetAsGUIView.localPositionInPointSpace = CorrectedTargetPosition;
		}

		StartCoroutine("CorrectPositionCoRoutine");
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void ProcessToggle()
	{
		base.ProcessToggle();
		if(toggle)
		{
			SetSliderUp();
		}
	}


	protected override void ProcessManipulation(bool calcX, bool calcY)
	{
		if(CorrectingPosition)
		{
			return;
		}

		Vector2 newLocalPos = TargetAsGUIView.localPositionInPointSpace;
		Vector2 deltaInput = GUIViewCore.Instance.GetScreenSpacePositionInPointSpace(inputManagerCursorState.Delta);

		if(AllowAxisX && calcX)
		{
			newLocalPos.x =
				Mathf.Clamp(newLocalPos.x + deltaInput.x, RangeX.range.x, RangeX.range.y);
		}

		if(AllowAxisY && calcY)
		{
			newLocalPos.y =
				Mathf.Clamp(newLocalPos.y + deltaInput.y, RangeY.range.x, RangeX.range.y);
		}

		TargetAsGUIView.localPositionInPointSpace = newLocalPos;

		ReCalculateNow(true);
	}




	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private bool CorrectPositionOnUp
	{
		get	{ return this.m_correctPositionOnUp; }
	}


	public bool CorrectingPosition
	{
		get	{ return this.m_correctingPosition; }
		set
		{
//			if(m_correctingPosition == true && value == true)
//			{
//				Debug.LogWarning("Wait a minute.");
//			}
			m_correctingPosition = value;
			this.inputEnabled = !m_correctingPosition;
		}
	}


	private Vector2 CorrectedTargetPosition
	{
		get	{ return this.m_correctedTargetPosition; }
		set	{ m_correctedTargetPosition = value; }
	}


	private Vector2 CorrectedInitialPosition
	{
		get	{ return this.m_correctedInitialPosition; }
		set	{ m_correctedInitialPosition = value; }
	}


	private bool AutoScrollNormal
	{
		get	{ return this.m_autoScrollNormal; }
		set	{ m_autoScrollNormal = value; }
	}


	private float CursorVelocityNeededToFastSwipe
	{
		get	{ return this.m_cursorVelocityNeededToFastSwipe;	}
	}


	private float AutoScrollSeconds
	{
		get	{ return this.m_autoScrollNormal ? AutoScrollSecondsNormal : AutoScrollDeltaFast; }
	}


	private float AutoScrollSecondsNormal
	{
		get	{ return this.m_autoScrollSeconds_Normal; }
	}


	private float AutoScrollDeltaFast
	{
		get	{ return this.m_autoScrollSeconds_Fast; }
	}


	private Vector2 TargetCenterPointBasedOnRanges
	{
		get	{ return new Vector2(RangeX.point, RangeY.point);	}
	}


	private float DistanceToCover
	{
		get	{ return this.m_distanceToCover; }
		set	{ m_distanceToCover = value; }
	}


	private float StartScrollingTime
	{
		get	{ return this.m_startScrollingTime; }
		set	{ m_startScrollingTime = value;	}
	}


	public bool RespondToSwipesOnDisabledAxis
	{
		get	{ return this.m_respondToSwipesOnDisabledAxis; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void SetSliderUp()
	{
		ViewAreaManipulating = this;
		if(Target)
		{
			Target.parent = transform;
		}

		if(CorrectPositionOnUp)
		{
			OnFinishedEvent += CorrectPosition;
			//OnHitEvent += DebugTextLevel;
			//SetCenterView(ClosetChildViewToCenter, true);
		}
	}

//	private void DebugTextLevel(GameObject hit)
//	{
//		if(hit != null)
//		{
//			LevelNamedTexture levelNamedTexture = hit.GetComponent<LevelNamedTexture>();
//			if(levelNamedTexture != null)
//			{
//				Debug.Log(levelNamedTexture.RepresentedLevel);
//			}
//		}
//	}

	private void CorrectPosition()
	{
		AutoScrollNormal = true;

		if(ResponedToSwipeOnly())
		{
			return;
		}
		else
		{
			CursorState currentCursor = GUIViewInputManager.Instance.CurrentCursorState;

			Vector2 inputTotalVelocity = new Vector2(
			AllowAxisX ? -currentCursor.VelocityTotal.x : 0f,
			AllowAxisY ? -currentCursor.VelocityTotal.y : 0f);

			float maxAxisVelocity = Mathf.Max(Mathf.Abs(currentCursor.VelocityTotal.x), Mathf.Abs(currentCursor.VelocityTotal.y));

			if(maxAxisVelocity > CursorVelocityNeededToFastSwipe)
			{
				if(maxAxisVelocity == Mathf.Abs(currentCursor.VelocityTotal.x))
				{
					inputTotalVelocity.y = 0f;
				}
				else
				{
					inputTotalVelocity.x = 0f;
				}

				ClosetChildViewToCenter = null;
				GUIView fastSwipedView = GetViewFromCenterInDirection(inputTotalVelocity);
				if(fastSwipedView != null)
				{
					AutoScrollNormal = false;
					SetCenterView(fastSwipedView, false);

					//Debug.Log("Delta : " + currentCursor.TotalDelta + "Velocity : " + currentCursor.VelocityTotal + " Processed: " + inputTotalVelocity);
					return;
				}
				else
				{
					// Because we had reset the center view we must put it back for normal operations
					GetClosestViewHeldToCenterPoint();
				}
			}
		}


		//Debug.Log("Correcting");
		SetCenterView(ClosetChildViewToCenter, false);
	}


	private bool ResponedToSwipeOnly()
	{
		if(!RespondToSwipesOnDisabledAxis)
		{
			return false;
		}

		bool respondedToSwipeOnlyMethod = false;

		Vector2 swipedVelocity = GUIViewInputManager.Instance.CurrentCursorState.VelocityTotal;
		Vector2 swipeOutputVelocity = Vector2.zero;

		if(!AllowAxisX)
		{
			float swipeAbsVelocity = Mathf.Abs(swipedVelocity.x);
			if(swipeAbsVelocity > CursorVelocityNeededToFastSwipe)
			{
				swipeOutputVelocity.x = -swipedVelocity.x;
				AutoScrollNormal = false;
				respondedToSwipeOnlyMethod = true;
			}
		}
		if(!AllowAxisY)
		{
			float swipeAbsVelocity = Mathf.Abs(swipedVelocity.y);
			if(swipeAbsVelocity > CursorVelocityNeededToFastSwipe)
			{
				swipeOutputVelocity.y = -swipedVelocity.y;
				AutoScrollNormal = false;
				respondedToSwipeOnlyMethod = true;
			}
		}

		if(swipeOutputVelocity.x != 0f && swipeOutputVelocity.y != 0f)
		{
			if(Mathf.Abs(swipeOutputVelocity.x) > Mathf.Abs(swipeOutputVelocity.y))
			{
				swipeOutputVelocity.y = 0f;
			}
			else
			{
				swipeOutputVelocity.x = 0f;
			}
		}

		AutoScrollCenterView(swipeOutputVelocity);

		return respondedToSwipeOnlyMethod;
	}


	private IEnumerator CorrectPositionCoRoutine()
	{
		while(TargetAsGUIView.localPositionInPointSpace != CorrectedTargetPosition)
		{
			float percentLeft = 0f;

			if(AutoScrollNormal)
			{
				float distanceCovered = (Time.fixedTime - StartScrollingTime) * AutoScrollSeconds;
				percentLeft = distanceCovered / DistanceToCover;
			}
			else
			{
				float timeCovered = Time.fixedTime - StartScrollingTime;
				percentLeft = AutoScrollSeconds != 0f ? (timeCovered / AutoScrollSeconds) : 1f;
			}

			TargetAsGUIView.localPositionInPointSpace = Vector2.Lerp(CorrectedInitialPosition, CorrectedTargetPosition, percentLeft);
			ReCalculate();
			yield return new WaitForEndOfFrame();

			if(currentState != GUIViewButton.ButtonState.Enabled)
			{

				yield break;
			}
		}
		CorrectingPosition = false;
		ReCalculate();
	}


	private void GetClosestViewHeldToCenterPoint()
	{
		float closestPoint = float.PositiveInfinity;
		Vector2 currentOffsetFromCenter = TargetCenterPointBasedOnRanges - TargetAsGUIView.localPositionInPointSpace;

		ClosetChildViewToCenter = null;

		foreach(GUIView childView in ChildViewsHeld)
		{
			float dist = Vector2.Distance(childView.localPositionInPointSpace, currentOffsetFromCenter);

			if(dist < closestPoint)
			{
				closestPoint = dist;
				ClosetChildViewToCenter = childView;
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private bool m_correctPositionOnUp = true;
	[SerializeField]
	private float m_autoScrollSeconds_Normal = 600f;
	[SerializeField]
	private float m_autoScrollSeconds_Fast = 0.5f;
	[SerializeField]
	private PointWithinRange m_rangeX = new PointWithinRange();
	[SerializeField]
	private PointWithinRange m_rangeY = new PointWithinRange();
	[SerializeField]
	private float m_cursorVelocityNeededToFastSwipe = 400f;
	[SerializeField]
	private bool m_respondToSwipesOnDisabledAxis = false;
	private float m_distanceToCover = 0f;
	private float m_startScrollingTime = 0f;
	private bool m_autoScrollNormal = true;
	private bool m_correctingPosition = false;
	private Vector2 m_correctedInitialPosition = Vector2.zero;
	private Vector2 m_correctedTargetPosition = Vector2.zero;
	private List<GUIView> m_childViewsHeld = null;
	private GUIView m_closetChildViewToCenter = null;

	#endregion
}