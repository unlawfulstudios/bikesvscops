using UnityEngine;
using HammerCat;

public class GUIViewTransition_FadeInOrOut : GUIViewTransition
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	protected GUIViewTexture FullScreenDimmedBG
	{
		get
		{
			if(this.m_fullScreenDimmedBG == null)
			{
				Debug.LogError("Please attach a texture to this component.", this);
			}
			return this.m_fullScreenDimmedBG;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override System.Collections.IEnumerator Run()
	{
		FullScreenDimmedBG.toggle = true;
		float dt = 0f;
		while(FadeBackGround(
			FadeIn ? ColorFaded : ColorFull,
			FadeIn ? ColorFull : ColorFaded,
			ref dt, Duration))
		{
			yield return null;
		}

		if(!FadeIn)
		{
			FullScreenDimmedBG.toggle = false;
		}

		TransitionActivated = false;
	}


	protected bool FadeBackGround(Color startColor, Color endColor, ref float dt, float time)
	{
		if(dt < Duration)
		{
			dt += Time.deltaTime * (1f / time);

			Color prevColor = FullScreenDimmedBG.color;
			prevColor.a = Mathf.Lerp(startColor.a, endColor.a, EaseLerp.Ease(dt, EaseType.In));

			FullScreenDimmedBG.color = prevColor;

			return true;
		}
		FullScreenDimmedBG.color = endColor;
		return false;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private bool FadeIn
	{
		get	{ return this.m_fadeIn;	}
		set	{ m_fadeIn = value;	}
	}

	private static Color ColorFull
	{
		get	{ return m_colorFull; }
	}

	private static Color ColorFaded
	{
		get	{ return m_colorFaded; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private GUIViewTexture m_fullScreenDimmedBG = null;
	[SerializeField]
	private bool m_fadeIn = true;
	private static Color m_colorFull = new Color(0f, 0f, 0f, 1f);
	private static Color m_colorFaded = new Color(0f, 0f, 0f, 0f);

	#endregion
}