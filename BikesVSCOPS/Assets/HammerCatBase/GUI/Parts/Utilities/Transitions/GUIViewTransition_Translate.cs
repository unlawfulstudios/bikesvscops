using UnityEngine;
using HammerCat;

public class GUIViewTransition_Translate : GUIViewTransition
{
	#region Public Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override System.Collections.IEnumerator Run()
	{
		if(ActivatingView != null)
		{
			ActivatingView.toggle = true;
		}
		float dt = 0f;
		while(Translate(StartTranslation, EndTranslation, ref dt, Duration))
		{
			yield return null;
		}

		TransitionActivated = false;
	}


	private bool Translate(Vector2 startPos, Vector2 endPos, ref float dt, float time)
	{

		if(dt < time)
		{
			dt += Time.deltaTime * (1f / time);

			Vector2 prevPos = TranslatingView.localPositionInPointSpace;
			prevPos.x = Mathf.Lerp(startPos.x, endPos.x, EaseLerp.Ease(dt, EaseType.In));
			prevPos.y = Mathf.Lerp(startPos.y, endPos.y, EaseLerp.Ease(dt, EaseType.In));

			TranslatingView.localPositionInPointSpace = prevPos;
			TranslatingView.ReCalculate();
			return true;
		}
		TranslatingView.localPositionInPointSpace = endPos;
		TranslatingView.ReCalculate();
		return false;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIView TranslatingView
	{
		get	{ return this.m_translatingView; }
	}


	private GUIView ActivatingView
	{
		get	{ return this.m_activatingView; }
	}


	private Vector2 StartTranslation
	{
		get	{ return this.m_startTranslation; }
	}


	private Vector2 EndTranslation
	{
		get	{ return this.m_endTranslation; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private GUIView m_translatingView = null;
	[SerializeField]
	private GUIView m_activatingView = null;
	[SerializeField]
	private Vector2 m_startTranslation = Vector2.zero;
	[SerializeField]
	private Vector2 m_endTranslation = Vector2.zero;

	#endregion
}