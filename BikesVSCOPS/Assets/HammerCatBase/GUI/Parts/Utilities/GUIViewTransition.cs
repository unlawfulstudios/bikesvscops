using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public abstract class GUIViewTransition : MonoBehaviour
{
	#region Public Static Properties

	public static bool TransitionActivated
	{
		get	{ return m_transitionActivated;	}
		set	{ m_transitionActivated = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	#region Public Defines

	public enum TranistionType
	{
		Intro,
		Exit
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public float Duration
	{
		get	{ return this.m_duration; }
		protected set { m_duration = value; }
	}


//	public TranistionType Type
//	{
//		get	{ return this.m_type; }
//	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void DeActivate(Queue<GUIViewTransition> tranistions, GUIViewController nextController)
	{
		QueuedTransitions = tranistions;
		NextController = nextController;

		GUIViewInputManager.Instance.enabled = false;

		StopCoroutine("ExitControllerAndRunNext");
		StartCoroutine("ExitControllerAndRunNext", this);
	}


	public void Activate(Queue<GUIViewTransition> tranistions, GUIViewController nextController)
	{
		QueuedTransitions = tranistions;
		NextController = nextController;

		GUIViewInputManager.Instance.enabled = false;

		StopCoroutine("RunQueue");
		StartCoroutine("RunQueue", this);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Defines



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods



	protected abstract IEnumerator Run();

	/// <summary>
	/// Will activate the controller that should be on.
	/// </summary>
//	protected void SwapControllers()
//	{
//		if(Type == TranistionType.Intro)
//		{
//			GUIViewCore.Instance.ActivateController(SiblingGUIView);
//		}
//		else
//		{
//			GUIViewCore.Instance.ActivateController(NextController);
//		}
//	}


	

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

//	private GUIViewController SiblingGUIView
//	{
//		get
//		{
//			if(this.m_siblingGUIView == null)
//			{
//				this.m_siblingGUIView = gameObject.GetComponent<GUIViewController>();
//				if(this.m_siblingGUIView == null)
//				{
//					Debug.LogError("Could not find GUI View for this transition, please attach one.", this);
//				}
//			}
//			return this.m_siblingGUIView;
//		}
//	}


	private GUIViewController NextController
	{
		get	{ return this.m_nextController;	}
		set	{ m_nextController = value;	}
	}


	public Queue<GUIViewTransition> QueuedTransitions
	{
		get	{ return this.m_queuedTransitions; }
		set	{ m_queuedTransitions = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private IEnumerator RunQueue(GUIViewTransition translation)
	{
		TransitionActivated = true;

		StopCoroutine("Run");
		StartCoroutine("Run");

		while(TransitionActivated)
		{
			yield return null;
		}

		if(translation.QueuedTransitions.Count > 0)
		{
			GUIViewTransition nextTranition = translation.QueuedTransitions.Dequeue();
			nextTranition.Activate(translation.QueuedTransitions, translation.NextController);
		}
		else
		{
			GUIViewCore.Instance.ActivateController(translation.NextController);
		}
	}


	private IEnumerator ExitControllerAndRunNext(GUIViewTransition translation)
	{
		TransitionActivated = true;

		StopCoroutine("Run");
		StartCoroutine("Run");

		while(TransitionActivated)
		{
			yield return null;
		}

		if(translation.QueuedTransitions.Count > 0)
		{
			GUIViewTransition nextTranition = translation.QueuedTransitions.Dequeue();
			nextTranition.DeActivate(translation.QueuedTransitions, translation.NextController);
		}
		else
		{
			// TODO check
			GUIViewCore.Instance.ActivateController(translation.NextController);
			if(translation.NextController != null)
			{
				translation.NextController.Transition(TranistionType.Intro, null);
			}
		}
	}

//	private IEnumerator ExitPrevControllerAndRun()
//	{
//		if(GUIViewCore.Instance.ActiveController != null)
//		{
//			GUIViewCore.Instance.ActiveController.Transition(GUIViewTransition.TranistionType.Exit);
//		}
//
//		while(TransitionActivated)
//		{
//			yield return null;
//		}
//
//		StartCoroutine("Run");
//	}



	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private float m_duration = 1f;
	private GUIViewController m_nextController = null;
	private static bool m_transitionActivated = false;
	private Queue<GUIViewTransition> m_queuedTransitions = new Queue<GUIViewTransition>();

	#endregion
}