using UnityEngine;

public class CursorState
{
	#region Public Defines
	public enum PressState
	{
		None,
		Press,
		Down,
		Released,

		Total
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public PressState PressedState
	{
		get	{ return this.m_pressedState; }
		private set	{ m_pressedState = value; }
	}

	
	public Vector2 Position
	{
		get	{ return this.m_position; }
		private set
		{
			m_position.x = Mathf.Clamp(value.x, 0f, GUIViewCore.Instance.ScreenPixelCount.x);
			m_position.y = Mathf.Clamp(value.y, 0f, GUIViewCore.Instance.ScreenPixelCount.y);
		}
	}


	public Vector2 Delta
	{
		get { return this.m_delta; }
		private set	{ m_delta = value; }
	}


	public Vector2 TotalDelta
	{
		get	{ return this.m_totalDelta; }
		private set	{ m_totalDelta = value;	}
	}


	public Vector2 VelocityTotal
	{
		get	{ return this.m_velocityTotal; }
		private set	{ m_velocityTotal = value;	}
	}


	public Vector2 VelocityCurrent
	{
		get	{ return this.m_velocityCurrent; }
		private set	{ m_velocityCurrent = value;	}
	}


	public GUIViewInput ActiveInput
	{
		get	{ return this.m_activeInput; }
		private set	{ m_activeInput = value; }
	}


	public GUIViewInput InitialInput
	{
		get	{ return this.m_initialInput; }
		private set	{ m_initialInput = value; }
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public CursorState()
	{
		Position = Vector2.zero;
		Reset();
	}


	public void ApplyNewStateAndPosition(PressState newState, Vector2 newPos)
	{
		newPos.x = Mathf.Clamp(newPos.x, 0f, GUIViewCore.Instance.ScreenPixelCount.x);
		newPos.y = Mathf.Clamp(newPos.y, 0f, GUIViewCore.Instance.ScreenPixelCount.y);
		//Debug.Log(newState);
		PressState prevPressedState = PressedState;
		PressedState = newState;
		switch(newState)
		{
			case PressState.Press:
				{
					Position = PositionInitial = newPos;
					TimePressStarted = Time.fixedTime;
					Delta = TotalDelta = Vector2.zero;

					VelocityCurrent = VelocityTotal = Vector2.zero;
					break;
				}
			case PressState.Down:
			case PressState.Released:
				{
					Delta = newPos - Position;
					TotalDelta = newPos - PositionInitial;

					Position = newPos;

					float currentTimeDelta = Time.fixedDeltaTime != 0f ? (1f / Time.fixedTime) : 0f;
					VelocityCurrent = Delta * currentTimeDelta;

					float differenceOfTimeOverTotal = Time.fixedTime - TimePressStarted;
					float totalTimeDelta = differenceOfTimeOverTotal != 0f ? (1f / differenceOfTimeOverTotal) : 0f;
					VelocityTotal = TotalDelta * totalTimeDelta;

					//Debug.Log(VelocityTotal);

					break;
				}
			case PressState.None:
				{
					if(prevPressedState != PressState.None)
					{
						Reset();
					}
					break;
				}
		}
	}


	public void ApplyNewActiveInput(GUIViewInput newViewInput)
	{
		ActiveInput = newViewInput;
		
		if(PressedState == PressState.Press)
		{
			InitialInput = ActiveInput;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	public Vector2 PositionInitial
	{
		get	{ return this.m_initialCursorPosition; }
		set
		{
			m_initialCursorPosition = value;
		}
	}


	private float TimePressStarted
	{
		get	{ return this.m_timePressStarted; }
		set	{ m_timePressStarted = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Reset()
	{
		PressedState = PressState.None;
		VelocityTotal = TotalDelta = Delta = PositionInitial = Vector2.zero;
		TimePressStarted = 0f;
		InitialInput = ActiveInput = null;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private PressState m_pressedState = PressState.None;
	private Vector2 m_position = Vector2.zero;
	private Vector2 m_initialCursorPosition = Vector2.zero;
	private Vector2 m_delta = Vector2.zero;
	private Vector2 m_totalDelta = Vector2.zero;
	private Vector2 m_velocityCurrent = Vector2.zero;
	private Vector2 m_velocityTotal = Vector2.zero;
	private float m_timePressStarted = 0f;
	private GUIViewInput m_activeInput = null;
	private GUIViewInput m_initialInput = null;

	#endregion
}