using UnityEngine;

[RequireComponent(typeof(GUIView))]
public class GUIViewPointToWorldPosition : MonoBehaviour
{
	#region Public Properties

	public Vector3 WorldPosition
	{
		get
		{
			Vector3 pos = new Vector3(
				ViewToTrack.GetComponent<GUITexture>().pixelInset.x + ViewToTrack.GetComponent<GUITexture>().pixelInset.width * 0.5f,
				ViewToTrack.GetComponent<GUITexture>().pixelInset.y + ViewToTrack.GetComponent<GUITexture>().pixelInset.height * 0.5f,
				ViewToTrack.gameObject.transform.position.z);
			
			return GUIViewCore.Instance.ObserverCamera.ScreenToWorldPoint(pos);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public Vector3 GetWorldPositionOnZFromScreen(float forcedZ)
	{
		//Vector3 worldToScreen = GUIViewCore.Instance.ObserverCamera.ScreenToWorldPoint(Vector3.forward * forcedZ);
		Vector3 pos = new Vector3(
				ViewToTrack.GetComponent<GUITexture>().pixelInset.x + ViewToTrack.GetComponent<GUITexture>().pixelInset.width * 0.5f,
				ViewToTrack.GetComponent<GUITexture>().pixelInset.y + ViewToTrack.GetComponent<GUITexture>().pixelInset.height * 0.5f,
				forcedZ);

		Vector3 screenToWorld = GUIViewCore.Instance.ObserverCamera.ScreenToWorldPoint(pos);

		return screenToWorld;
	}

	public Vector3 GetWorldPositionOnZPlaneOfWorldPosition(Vector3 worldPositionToMatchZ)
	{
		Vector3 toCamera = worldPositionToMatchZ - GUIViewCore.Instance.ObserverCamera.transform.position;
		float zDistance = toCamera.magnitude;
		//Vector3 relativeVec = Vector3.Project(toCamera, GUIViewCore.Instance.ObserverCamera.transform.forward);
		//float forcedZ = relativeVec.magnitude - GUIViewCore.Instance.ObserverCamera.nearClipPlane;

		//float toCamera = startPos.z - GUIViewCore.Instance.ObserverCamera.transform.position.z;

		Vector3 pos = new Vector3(
				ViewToTrack.GetComponent<GUITexture>().pixelInset.x + ViewToTrack.GetComponent<GUITexture>().pixelInset.width * 0.5f,
				ViewToTrack.GetComponent<GUITexture>().pixelInset.y + ViewToTrack.GetComponent<GUITexture>().pixelInset.height * 0.5f,
				zDistance );

		Vector3 toWorld = GUIViewCore.Instance.ObserverCamera.ScreenToWorldPoint(pos);

		// Hack, we shouldn't have to do this. The GetWorldPositionOnZPlaneOfWorldPosition
		//	is off by positive 1 - 2 units in the z. This covers the bug.
		toWorld.z = worldPositionToMatchZ.z;

		return toWorld;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	private GUIView ViewToTrack
	{
		get
		{
			if(this.m_viewToTrack == null)
			{
				this.m_viewToTrack = GetComponent<GUIView>();
			}
			return this.m_viewToTrack;
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private GUIView m_viewToTrack = null;

	#endregion
}