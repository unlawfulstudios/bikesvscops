using UnityEngine;

[ExecuteInEditMode]
public abstract class GUIViewButtonEventList : MonoBehaviour
{
	#region Protected Properties

	public bool toggle
	{
		get { return m_toggle; }
		set
		{
			m_toggle = value;
			enabled = m_toggle;
			if(Application.isPlaying)
			{
				ProcessToggle();
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected abstract void ProcessToggle();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	protected void OnEnable()
	{
	}


	protected void OnDisable()
	{
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private bool m_toggle = false;

	#endregion
}