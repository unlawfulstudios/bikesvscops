using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(GUIView))]
public class GUIViewRadioButton : MonoBehaviour
{
	#region Private Members

	// Editor Exposed
	[SerializeField]
	private List<GUIViewButton> m_buttonList = new List<GUIViewButton>();

	// Member
	private List<int> m_nonValidButtonIndexes = new List<int>();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	#region Public Methods

	public void SetSelectedButton(string buttonName)
	{
		int buttonIndex = m_buttonList.FindIndex(delegate(GUIViewButton obj)
		{
			return obj != null && obj.name == buttonName;
		});

		SetSelectedButton(buttonIndex);
	}


	public void SetSelectedButton(int index)
	{
		if(m_buttonList.Count > index
			&& 0 <= index
			&& !m_nonValidButtonIndexes.Contains(index))
		{
			SetSelectedButton(m_buttonList[index]);
		}
		else
		{
			Debug.Log("Could not find a button with the passed in index of \"" + index + "\".", this);
		}
	}


	public void NonValidButtonIndexAdd(int index)
	{
		if(!m_nonValidButtonIndexes.Contains(index))
		{
			m_nonValidButtonIndexes.Add(index);
			m_buttonList[index].lockInput = true;
			m_buttonList[index].currentState = GUIViewButton.ButtonState.Disabled;
		}
	}


	public void NonValidButtonIndexRemove(int index)
	{
		if(m_nonValidButtonIndexes.Contains(index))
		{
			m_nonValidButtonIndexes.Remove(index);
			m_buttonList[index].lockInput = false;
		}
	}


	public void NonValidButtonIndexClear()
	{
		foreach(int markedIndex in m_nonValidButtonIndexes)
		{
			m_buttonList[markedIndex].lockInput = false;
		}
		m_nonValidButtonIndexes.Clear();
	}


	public bool NonValidButtonIndexContains(int index)
	{
		return m_nonValidButtonIndexes.Contains(index);
	}


	public int NonValidButtonIndexCount()
	{
		return m_nonValidButtonIndexes.Count;
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Awake()
	{
		foreach(GUIViewButton buttonIt in m_buttonList)
		{
			buttonIt.AddRuntimeEvent(GUIViewButton.ButtonState.OnDown, SwitchToSelected);
			buttonIt.AddRuntimeEvent(GUIViewButton.ButtonState.Selected, ButtonWasSelected);
		}
	}


	private void SetSelectedButton(GUIViewButton button)
	{
		foreach(GUIViewButton buttonIt in m_buttonList)
		{
			if(buttonIt == button)
			{
				if(button.currentState != GUIViewButton.ButtonState.Selected)
				{
					buttonIt.forceSelect = true;
				}
				buttonIt.ReCalculateNow(true);
			}
			else if(buttonIt != button && button.currentState == GUIViewButton.ButtonState.Selected)
			{
				buttonIt.forceSelect = false;
				buttonIt.ReCalculateNow(true);
			}
		}
	}


	private void ButtonWasSelected(GUIViewButton sender)
	{
		SetSelectedButton(sender);
	}


	private void SwitchToSelected(GUIViewButton sender)
	{
		sender.forceSelect = true;
	}

	#endregion
}