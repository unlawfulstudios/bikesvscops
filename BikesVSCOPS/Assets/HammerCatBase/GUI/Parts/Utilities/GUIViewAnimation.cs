using System.Reflection;
using UnityEngine;
using HammerCat;
using System.Collections.Generic;

[RequireComponent (typeof(GUIView))]
public class GUIViewAnimation : MonoBehaviour
{
	#region Public Defines

	public enum AnimationEventTimeMode
	{
		Explicit,
		Percent
	}


	public delegate void GenericAnimationEventDelegate();

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public bool toggle
	{
		get	{ return this.m_toggle; }
		set
		{
			m_toggle = value;
			if(m_toggle)
			{
				if(ResetsOnToggle)
				{
					Reset();
				}
				Update();
			}
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void Reset()
	{
		TimeAnimStarted = InvalidStartTime;
		SampleAnimation(0f);
	}


	public void SetToEnd()
	{
		if(Clip == null)
		{
			Debug.LogWarning("No clip to set");
			return;
		}

		TimeAnimStarted = Time.time - Clip.length;
		Clip.SampleAnimation(gameObject, Clip.length);
	}


	public AnimationClip Clip
	{
		get	{ return this.m_clip; }
		set { m_clip = value; }
	}


	public bool AddAnimationEvent(GenericAnimationEventDelegate animationEvent, float time, AnimationEventTimeMode mode)
	{
		float eventTime = time;
		if(mode == AnimationEventTimeMode.Percent)
		{
			if(Clip == null)
			{
				Debug.LogError("There is not clip attached to this \"GUIViewAnimation\" to base the percent on.", this);
				return false;
			}
			if(eventTime > 1f
				|| eventTime < 0f)
			{
				Debug.LogError("When using a percent specify a float between 0f and 1f", this);
				return false;
			}
			eventTime = Clip.length * time;
		}
		if(Clip != null
			&& (eventTime > Clip.length
			|| eventTime < 0f))
		{
			Debug.LogWarning("The provided time extends the time of the clip.", this);
		}

		TimedAnimationEvent newEvent = new TimedAnimationEvent(animationEvent, time);
		AllAnimationEvents.Add(newEvent);
		AllAnimationEvents.Sort((a,b) => a.time.CompareTo(b.time));
		ResortEventQueue();

		return true;
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected void Update()
	{
		if(toggle
			&& SiblingGUIView.toggle
			&& Clip != null)
		{
			// Set the time if never calculated
			if(TimeAnimStarted == InvalidStartTime)
			{
				TimeAnimStarted = Time.time;
			}

			float deltaTime = Time.time - TimeAnimStarted;


			if(Clip.wrapMode != WrapMode.Loop)
			{
				deltaTime = Mathf.Clamp(deltaTime, 0f, Clip.length);
			}

			SampleAnimation(deltaTime);

			if(Clip.wrapMode != WrapMode.Loop && deltaTime == Clip.length)
			{
				toggle = false;
			}
		}
		else if(ResetsOnAwake && TimeAnimStarted != InvalidStartTime)
		{
			// Reset it for the next time it is toggled
			TimeAnimStarted = InvalidStartTime;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Serialized Properties

	private bool AnimateOnStart
	{
		get	{ return this.m_animateOnStart;	}
	}


	private bool ResetsOnAwake
	{
		get	{ return this.m_resetsOnAwake; }
	}


	private bool ResetsOnToggle
	{
		get	{ return this.m_resetsOnToggle; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Defines

	private static readonly float InvalidStartTime = -1f;

	private class TimedAnimationEvent
	{
		public readonly GenericAnimationEventDelegate animationEvent = null;
		public readonly float time = InvalidStartTime;


		public TimedAnimationEvent(GenericAnimationEventDelegate sourceAnimationEvent, float timeEventTakesPlace)
		{
			this.animationEvent = sourceAnimationEvent;
			this.time = timeEventTakesPlace;
		}
	}

	private Queue<TimedAnimationEvent> AnimationEventQueue
	{
		get
		{
			if(this.m_animationEventQueue == null)
			{
				this.m_animationEventQueue = new Queue<TimedAnimationEvent>();
			}
			return this.m_animationEventQueue;
		}
	}


	private List<TimedAnimationEvent> AllAnimationEvents
	{
		get
		{
			if(this.m_allAnimationEvents == null)
			{
				this.m_allAnimationEvents = new List<TimedAnimationEvent>();
			}
			return this.m_allAnimationEvents;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private GUIView SiblingGUIView
	{
		get
		{
			if(this.m_siblingGUIView == null)
			{
				this.m_siblingGUIView = gameObject.GetComponent<GUIView>();
				if(this.m_siblingGUIView == null)
				{
					Debug.LogError("Failed to find sibling GUIView", this);
				}
			}
			return this.m_siblingGUIView;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

	private void Start()
	{
		if(AnimateOnStart)
		{
			toggle = true;
		}
	}


	private float TimeAnimStarted
	{
		get	{ return this.m_timeAnimStarted; }
		set
		{
			m_timeAnimStarted = value;
			if(m_timeAnimStarted != InvalidStartTime)
			{
				ResortEventQueue();
			}
		}
	}


	private void ResortEventQueue()
	{
		AnimationEventQueue.Clear();
		if(TimeAnimStarted == InvalidStartTime)
		{
			return;
		}

		float deltaTime = Time.time - TimeAnimStarted;

		foreach(TimedAnimationEvent timedEvent in AllAnimationEvents)
		{

			if(timedEvent.time > deltaTime)
			{
				AnimationEventQueue.Enqueue(timedEvent);
			}
		}
	}


	private void SampleAnimation(float time)
	{
		if(Clip == null)
		{
			return;
		}
		
		Clip.SampleAnimation(gameObject, time);

		SiblingGUIView.ReCalculateNow(true);

		if(toggle)
		{
			while(AnimationEventQueue.Count > 0
				&& time > AnimationEventQueue.Peek().time)
			{
				TimedAnimationEvent nextEvent = AnimationEventQueue.Dequeue();
				if(nextEvent.animationEvent != null)
				{
					nextEvent.animationEvent();
				}
			}
		}
	}


	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private AnimationClip m_clip = null;
	[SerializeField]
	private bool m_animateOnStart = false;
	[SerializeField]
	private bool m_resetsOnAwake = true;
	[SerializeField]
	private bool m_resetsOnToggle = true;

	// Member
	private GUIView m_siblingGUIView = null;
	private bool m_toggle = false;
	private float m_timeAnimStarted = InvalidStartTime;
	private Queue<TimedAnimationEvent> m_animationEventQueue = null;
	private List<TimedAnimationEvent> m_allAnimationEvents = null;
	//private event GenericAnimationEventDelegate AnimationEvent = null;

	#endregion
}