using UnityEngine;

public enum GUIViewAnchorX
{
	Left,
	Center,
	Right,
	
	Max
}


public enum GUIViewAnchorY
{
	Bottom,
	Middle,
	Top,

	Max
}

/// <summary>
/// Achor wrapper class to hold serialized
///		versions of the gui's anchor points.
/// </summary>
[System.Serializable]
public class GUIViewAnchor
{
	public GUIViewAnchorX Horizontal;
	public GUIViewAnchorY Vertical;


	public TextAnchor ConvertToUnityAnchor()
	{
		int convertedY = ((int)GUIViewAnchorY.Max - 1) - (int)Vertical;
		int convertedX = (int)Horizontal;
		return (TextAnchor)(convertedY * 3 + convertedX);
	}

	
	public TextAlignment ConvertToUnityAlignment()
	{
		return (TextAlignment)Horizontal;
	}


	public GUIViewAnchor(GUIViewAnchorX horizontal, GUIViewAnchorY vertical)
	{
		Horizontal = horizontal;
		Vertical = vertical;
	}


	private GUIViewAnchor()
	{
	}
}