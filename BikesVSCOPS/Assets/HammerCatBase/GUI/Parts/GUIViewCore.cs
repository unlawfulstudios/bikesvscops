using System.Collections.Generic;
using HammerCat;
using UnityEngine;

[ExecuteInEditMode]
public class GUIViewCore : MonoSingleton<GUIViewCore>, GUIViewBase
{
	#region Private Members

	// Editor Exposed
	[SerializeField]
	private Vector2 m_pointBase = new Vector2(1024f, 768f);
	[SerializeField]
	private GUIViewController m_startController = null;
	[SerializeField]
	private List<GUIViewController> m_controllers = new List<GUIViewController>();
	[SerializeField]
	private Camera m_observerCamera = null;

	// Member
	private static GUIViewController m_activeController = null;
	private GUIViewAnchor m_anchor = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
	private GUIViewAnchor m_alignment = new GUIViewAnchor(GUIViewAnchorX.Center, GUIViewAnchorY.Middle);
	private Vector2 m_screenPixelCount = Vector2.zero;
	private Rect m_screenSpaceRect = new Rect(0, 0, 1, 1);
	private float m_xScale = 0f;
	private Vector2 m_zoomScale = Vector2.one;
	private float m_pointWidth = 0f;
	private GUITexture m_coreGuiTexture = null;
	//private Camera m_sceneCamera = null;
	private Vector2 m_midAnchor = Vector2.zero;
	private bool m_hasRecalulatAtLeastOnce = false;
	private bool m_editModeEnabled = true;
	private bool m_editorDebugEnabled = true;
	private Transform m_localGUISoundTransform = null;

	// Debug
	private bool m_showDebug = false;
	private static readonly Color m_debugColor = new Color(1f, 0f, 1f, 0.25f);
	private static readonly Color m_defaultColor = new Color(0.5f, 0.5f, 0.5f, 1f);
	private static Texture m_debugTexture = null;

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Defines

	public static readonly string InvalidControllerName = "None";

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Properties

	public Vector2 localPositionInPointSpace
	{
		get	{ return Vector2.zero; }
	}


	public Vector2 positionInPointSpace
	{
		get	{ return this.localPositionInPointSpace; }
	}


	public Vector2 positionInPointSpaceAbsolute
	{
		get	{ return this.localPositionInPointSpace; }
	}

	
	public Vector2 sizeInPointSpace
	{
		get	{ return this.m_pointBase; }
	}


	public float finalLayer
	{
		get { return 0f; }
	}


	public Vector2 scale
	{
		get { return Vector2.one; }
	}


	public Vector2 midAnchorPoint
	{
		get
		{
			if(Dirty)
			{
				ReCalculate();
			}
			return this.m_midAnchor;
		}
	}


	public GUIViewAnchor anchor
	{
		get	{ return this.m_anchor; }
	}


	public GUIViewAnchor alignment
	{
		get	{ return this.m_alignment; }
	}


	public GUIViewBase viewParent
	{
		get { return null; }
	}


	public bool showDebug
	{
		get	{ return this.m_showDebug; }
		set
		{
			if(EditorDebugEnabled)
			{
				m_showDebug = value;
				if(m_showDebug)
				{
					viewGuiTexture.enabled = true;
					viewGuiTexture.texture = DebugTexture;
					viewGuiTexture.color = DebugColor;
				}
				else
				{
					viewGuiTexture.texture = null;
					viewGuiTexture.color = DefaultColor;
				}
			}
		}
	}


	public Texture DebugTexture
	{
		get
		{
			if(m_debugTexture == null)
			{
				m_debugTexture = Resources.Load("DefaultObjects/blankTexture") as Texture;
			}
			return m_debugTexture;
		}
	}


	public Color DebugColor
	{
		get	{ return m_debugColor; }
	}


	public Color DefaultColor
	{
		get { return m_defaultColor; }
	}


	public Vector2 ScreenPixelCount
	{
		get
		{
			if(Dirty)
			{
				//Debug.Log("Need to recalculate");
				ReCalculate();
			}
			return this.m_screenPixelCount;
		}
		private set	{ this.m_screenPixelCount = value; }
	}


	public Rect screenSpaceRect
	{
		get
		{
			if(Dirty)
			{
				//Debug.Log("Need to recalculate");
				ReCalculate();
			}
			return this.m_screenSpaceRect;
		}
		private set	{ this.m_screenSpaceRect = value; }
	}


	public void TurnOfAllDebug()
	{
		GUIView[] childGUIObjects = gameObject.GetComponentsInChildren<GUIView>();
		foreach(GUIView view in childGUIObjects)
		{
			view.showDebug = false;
		}
	}


	public GUIViewController ActiveController
	{
		get	{ return m_activeController; }
		private set
		{
//			if(value != null && value == m_activeController)
//			{
//				Debug.LogWarning("Activating already active controller.");
//			}
			m_activeController = value;
			if(m_activeController != null)
			{
				m_activeController.gameObject.GetComponent<GUIViewController>().toggle = true;
			}
		}
	}
	

	public float origionalPositionLeft
	{
		get	{ return 0f; }
	}


	public float origionalPositionRight
	{
		get	{ return m_pointBase.x; }
	}


	public float origionalPositionCenter
	{
		get	{ return m_pointBase.x * 0.5f; }
	}


	public bool hasRunInitialCalculate
	{
		get { return m_hasRecalulatAtLeastOnce; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Public Methods

	public void ReCalculate()
	{
		//Debug.Log("Core ReCalculate");
		transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.forward;

		ScreenPixelCount = ActualScreenPixelCount;
		screenSpaceRect = new Rect(0, 0, ScreenPixelCount.x, ScreenPixelCount.y);
		viewGuiTexture.pixelInset = screenSpaceRect;

		XScale = sizeInPointSpace.y / ScreenPixelCount.y;
		PointWidth = ScreenPixelCount.x * XScale;
		float pointWidthOfY = ScreenPixelCount.y * XScale;

		ZoomScale = new Vector2(PointWidth / sizeInPointSpace.x, pointWidthOfY / sizeInPointSpace.y);

		//Debug.Log("Pt Width: " + PointWidth + " XScale: " + XScale + " Zoom: " + ZoomScale);

		Vector2 sizeInScreenSpace = GetSizeInScreenSpace(sizeInPointSpace);
		m_midAnchor = new Vector2(PointWidth * 0.5f, sizeInScreenSpace.y * XScale * 0.5f);

		m_hasRecalulatAtLeastOnce = true;

		//Debug.Log("Anchor: " + m_midAnchor);

		//Diagnostics.Log("GUICore set up: PointBase~" + PointBase.ToString() + " ActualScreenSize~" + ScreenPixelCount.ToString() + "\n" +
		//	"XScale~" + XScale + " PointWidth~" + PointWidth, Instance);

		UpdateChildren();
	}


	public Vector2 GetScreenSpacePositionInPointSpace(Vector2 pointInScreenSpace)
	{
		if(Dirty)
		{
			ReCalculate();
		}
		Vector2 posInPointSpace = pointInScreenSpace / (1f / XScale);

		//Debug.Log("New pos in screen space: " + posInScreenSpace);
		return posInPointSpace;
	}


	public Vector2 GetPositionInScreenSpace(Vector2 pointInPointSpace)
	{
		if(Dirty)
		{
			ReCalculate();
		}
		Vector2 posInScreenSpace = pointInPointSpace * (1f / XScale);

		//Debug.Log("New pos in screen space: " + posInScreenSpace);
		return posInScreenSpace;
	}


	public Vector2 GetSizeInScreenSpace(Vector2 sizeInPointSpace)
	{
		if(Dirty)
		{
			ReCalculate();
		}

		Vector2 screenSpaceSize = new Vector2(sizeInPointSpace.x / XScale, sizeInPointSpace.y / XScale);


		//if(scaleWithScreen)
		//{

		//float YScale = sizeInPointSpace.y / ScreenPixelCount.y;
		//screenSpaceSize = new Vector2(sizeInPointSpace.x * PointWidth, sizeInPointSpace.y * PointWidth);
		//}
//		if(pinnedOnRight)
//		{
//			screenSpaceSize.x = sizeInPointSpace.x;
//		}

		return screenSpaceSize;
	}


	public Vector2 GetScreenSpaceSizeInPointSpace(Vector2 sizeInScreenSpace)
	{
		if(Dirty)
		{
			ReCalculate();
		}
		Vector2 pointSpaceSize = new Vector2(sizeInScreenSpace.x * XScale, sizeInScreenSpace.y * XScale);

		return pointSpaceSize;
	}


	public float GetFontSizeInScreenSpace(float sizeInPointSpace)
	{
		if(Dirty)
		{
			ReCalculate();
		}
		return sizeInPointSpace / XScale;
	}


	public void ActivateController(GUIViewController newActiveController)
	{
		foreach(GUIViewController controller in Controllers)
		{
			if(controller != null)
			{
				if(newActiveController == null
					|| controller.gameObject != newActiveController.gameObject)
				{
					controller.gameObject.GetComponent<GUIViewController>().toggle = false;
				}
			}
		}
		ActiveController = newActiveController != null ? newActiveController.gameObject.GetComponent<GUIViewController>() : null;
	}


	/// <summary>
	/// Switchs the controller with the first found controller with that name.
	/// </summary>
	/// <param name='name'>
	/// Name.
	/// </param>
	public void SwitchController(string name)
	{
		if(name == null
			|| name == ""
			|| name == GUIViewCore.InvalidControllerName)
		{
			ActiveController = null;
			return;
		}
		GUIViewController newController = Controllers.Find(delegate (GUIViewController controllerIt)
		{
			return controllerIt != null && controllerIt.gameObject.name == name;
		});
		if(newController != null)
		{
			//ActiveController = newController;
			if(ActiveController != null)
			{
				ActiveController.Transition(GUIViewTransition.TranistionType.Exit, newController);
			}
			else
			{
				newController.Transition(GUIViewTransition.TranistionType.Intro, newController);
			}
			return;
		}
		Debug.LogError("Could not find the controller \"" + name + "\" that was passed in.");
	}


	public AudioSource BuildLocalSound(AudioSource source, string indentifyingName)
	{
		if(!Application.isPlaying
			|| source == null)
		{
			return null;
		}

		GameObject localSound = new GameObject(indentifyingName, new System.Type[] { typeof(AudioSource) });

		localSound.transform.parent = LocalGUISoundsContainer;
		AudioSource newLocalAudioSource = localSound.GetComponent<AudioSource>();

		newLocalAudioSource.clip = source.clip;
		newLocalAudioSource.mute = source.mute;
		newLocalAudioSource.bypassEffects = source.bypassEffects;
		newLocalAudioSource.playOnAwake = source.playOnAwake;
		newLocalAudioSource.loop = source.loop;
		newLocalAudioSource.priority = source.priority;
		newLocalAudioSource.volume = source.volume;
		newLocalAudioSource.pitch = source.pitch;
		newLocalAudioSource.panStereo = source.panStereo;

		return newLocalAudioSource;
	}


	public List<GUIViewController> Controllers
	{
		get { return this.m_controllers; }
	}


	public Camera ObserverCamera
	{
		get
		{
			if(this.m_observerCamera == null)
			{
				GUIViewCoreCameraIndicator[] guiCameras = GameObject.FindObjectsOfType(typeof(GUIViewCoreCameraIndicator)) as GUIViewCoreCameraIndicator[];
				if(guiCameras.Length > 0)
				{
					this.m_observerCamera = guiCameras[0].GetComponent<Camera>();
				}
			}
			return this.m_observerCamera;
		}
	}


	public static string ScrollViewCameraLayerPrefix
	{
		get { return "HammerCat GUIViewScrollView Layer"; }
	}


	public Vector2 ZoomScale
	{
		get { return this.m_zoomScale; }
		private set	{ m_zoomScale = value; }
	}


	public bool EditModeEnabled
	{
		get { return (!Application.isPlaying && Application.isEditor && m_editModeEnabled);	}
		set { m_editModeEnabled = value; }
	}


	public bool EditorDebugEnabled
	{
		get { return Application.isEditor && m_editorDebugEnabled; }
		set { m_editorDebugEnabled = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void Init()
	{
		Diagnostics.RegisterContextForLogging(Instance, LogType.Log);
		if(viewGuiTexture != null && viewGuiTexture.texture != null)
		{
			if(!showDebug)
			{
				viewGuiTexture.texture = null;
			}
		}

#if UNITY_EDITOR
		if(EditModeEnabled)
		{
			UnityEditor.EditorApplication.update -= Update;
			UnityEditor.EditorApplication.update -= Update;
			UnityEditor.EditorApplication.update += Update;
		}
#endif
		ReCalculate();

		if(Application.isPlaying)
		{
			ActiveController = null;
			ActivateController(StartController);
			//ActiveController = StartController;
		}
		else
		{
			//ActiveController = null;
			ActivateController(StartController);
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	private Vector2 ActualScreenPixelCount
	{
		get
		{
			if(ObserverCamera != null)
			{
				return new Vector2(ObserverCamera.pixelWidth, ObserverCamera.pixelHeight);
			}
			return Vector2.one;
		}
	}


	private GUIViewController StartController
	{
		get
		{
			if(this.m_startController != null)
			{
				return this.m_startController.gameObject.GetComponent<GUIViewController>();
			}
			return this.m_startController;
		}
	}


	public float XScale
	{
		get	{ return this.m_xScale;	}
		set	{ this.m_xScale = value;	}
	}


	public float PointWidth
	{
		get	{ return this.m_pointWidth;	}
		private set	{ this.m_pointWidth = value;	}
	}


	private GUITexture viewGuiTexture
	{
		get
		{
			if(this.m_coreGuiTexture == null)
			{
				this.m_coreGuiTexture = gameObject.GetComponent<GUITexture>();
				if(this.m_coreGuiTexture == null)
				{
					this.m_coreGuiTexture = gameObject.AddComponent<GUITexture>();
				}
			}
			return this.m_coreGuiTexture;
		}
	}


	private bool Dirty
	{
		get	{ return ActualScreenPixelCount != this.m_screenPixelCount;	}
	}


	private Transform LocalGUISoundsContainer
	{
		get
		{
			if(this.m_localGUISoundTransform == null)
			{
				this.m_localGUISoundTransform = transform.Find("LocalGUISoundsContainer");

				if(this.m_localGUISoundTransform == null)
				{

					GameObject localGUISoundsContainerGO = new GameObject("LocalGUISoundsContainer");

					this.m_localGUISoundTransform = localGUISoundsContainerGO.transform;
					this.m_localGUISoundTransform.parent = transform;
					this.m_localGUISoundTransform.localScale = Vector3.one;
					this.m_localGUISoundTransform.localPosition = Vector3.zero;
					this.m_localGUISoundTransform.localRotation = Quaternion.identity;
				}
			}

			return this.m_localGUISoundTransform;
		}
	}

	/*
	private Camera SceneCamera
	{
		get
		{
			if(this.m_sceneCamera == null)
			{
				GameObject go = GameObject.Find("Main Camera");
				if(go != null)
				{
					this.m_sceneCamera = go.GetComponent<Camera>();
				}
			}
			return this.m_sceneCamera;
		}
	}*/

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Methods

//	protected override void OnEnable()
//	{
//		base.OnEnable();
//
//	}

//	private void RemoveScrollViewLayers()
//	{
//		for(int layerIt = 0; layerIt < 32; ++layerIt)
//		{
//			string layerName = LayerMask.LayerToName(layerIt);
//			if(layerName.StartsWith(ScrollViewCameraLayerPrefix))
//			{
//
//			}
//		}
//	}


	protected void OnDisable()
	{
#if UNITY_EDITOR
		if(EditModeEnabled)
		{
			UnityEditor.EditorApplication.update = null;
			UnityEditor.EditorApplication.update -= Update;
			UnityEditor.EditorApplication.update -= Update;
		}
#endif
	}


	private void Update()
	{
		//Debug.Log(ActualScreenPixelCount);
		if(enabled && Dirty && ObserverCamera != null)
		{
			ReCalculate();
		}
	}


	private void UpdateChildren()
	{
		GUIView[] childGUIObjects = gameObject.GetComponentsInChildren<GUIView>();
		foreach(GUIView view in childGUIObjects)
		{
			if(view.transform.parent == transform)
			{
				view.ReCalculate();
			}
		}
	}

	#endregion
}