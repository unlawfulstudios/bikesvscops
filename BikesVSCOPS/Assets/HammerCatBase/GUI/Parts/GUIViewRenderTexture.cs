using UnityEngine;

[ExecuteInEditMode]
public class GUIViewRenderTexture : GUIViewTexture
{
	#region Public Properties

	public override Texture viewTexture
	{
		get
		{
			if(this.m_renderTexture == null)
			{
				//if(Application.isPlaying)
				{
					//BuildRenderTexture();
				}
			}
			return this.m_renderTexture;
		}
		set
		{

		}
	}


	public Camera Observer
	{
		get { return this.m_observer; }
		set	{ m_observer = value; }
	}


	public Camera Observer2
	{
		get { return this.m_observer2; }
		set	{ m_observer2 = value; }
	}


	public void BuildRenderTexture()
	{
		//Debug.Log("Value when asked: " + rectInScreenSpace.width);
		if((int)rectInScreenSpace.width == 0
			|| (int)rectInScreenSpace.height == 0)
		{
			//Debug.Log("Recalculating");
			ReCalculate();
			return;
		}

		if(m_renderTextureBuilt == true)
		{
			if(CurrentRenderTexture != null)
			{
				SetObserverCamera();
			}

			return;
		}


		if(Application.isPlaying)
		{
			//Debug.Log("Value after: " + rectInScreenSpace.width);

			//Vector2 size = new Vector2(Mathf.Abs(sizeInPointSpace.x), Mathf.Abs(sizeInPointSpace.y));
			Vector2 size = new Vector2(rectInScreenSpace.width, rectInScreenSpace.height);
			//Debug.Log(size);


			if(CurrentRenderTexture != null
				|| viewGuiTexture.texture != null)
			{
				Debug.Log("CLEAR RENDER TEXTURE");
			}

			CurrentRenderTexture = new RenderTexture(
			(int)size.x,
			(int)size.y,
			m_depth, RenderTextureFormat);//, RenderTextureReadWrite.Default);

			CurrentRenderTexture.isPowerOfTwo = false;
			CurrentRenderTexture.filterMode = FilterMode.Trilinear;
			CurrentRenderTexture.antiAliasing = 1;

			m_renderTextureBuilt = true;
		}

		viewGuiTexture.texture = CurrentRenderTexture;

		SetObserverCamera();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	protected override void ProcessToggle()
	{
		base.ProcessToggle();

		if(toggle && Application.isPlaying)
		{
			BuildRenderTexture();
		}
		else
		{
			DestroyRenderTexture();
		}
	}

//	protected override void SetPreAdditionalSettings()
//	{
//		base.SetPreAdditionalSettings();
//		NotGatheredPinnedData = true;
//	}

	protected override void SetAdditionalSettings()
	{
		base.SetAdditionalSettings();
		if(Application.isPlaying)
		{
			BuildRenderTexture();
		}
	}


	public void SetObserverCamera()
	{
		//Debug.Log("Setting ObserverCamera to " + Observer);
		if(Observer != null)
		{
			Observer.targetTexture = CurrentRenderTexture;

			Observer.aspect = (float)CurrentRenderTexture.width / (float)CurrentRenderTexture.height;
		}

		if(Observer2 != null)
		{
			Observer2.targetTexture = CurrentRenderTexture;

			Observer2.aspect = (float)CurrentRenderTexture.width / (float)CurrentRenderTexture.height;
		}
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private int Depth
	{
		get { return this.m_depth; }
	}


	private RenderTextureFormat RenderTextureFormat
	{
		get	{ return this.m_renderTextureFormat; }
	}


	private RenderTexture CurrentRenderTexture
	{
		get	{ return this.m_renderTexture; }
		set
		{
			m_renderTexture = value;
		}
	}


	protected override void OnEnable()
	{
		//viewGuiTexture.texture = viewTexture;

		base.OnEnable();
	}


	protected void OnDisable()
	{
		DestroyRenderTexture();
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	private void DestroyRenderTexture()
	{
		m_renderTextureBuilt = false;

		if(viewGuiTexture.texture != null)
		{
			viewGuiTexture.texture = null;
		}

		if(CurrentRenderTexture != null)
		{
			CurrentRenderTexture.DiscardContents();
			CurrentRenderTexture.Release();
			CurrentRenderTexture = null;
		}

		if(Observer != null)
		{
			Observer.targetTexture = null;
		}

		if(Observer2 != null)
		{
			Observer.targetTexture = null;
		}
	}


	private void OnApplicationQuit()
	{
		DestroyRenderTexture();
	}


	private void Awake()
	{

	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	[SerializeField]
	private int m_depth = 16;
	[SerializeField]
	private RenderTextureFormat m_renderTextureFormat = RenderTextureFormat.ARGB32;
	[SerializeField]
	private Camera m_observer = null;
	private Camera m_observer2 = null;
	private RenderTexture m_renderTexture = null;
	private bool m_renderTextureBuilt = false;

	#endregion
}