using UnityEngine;
using HammerCat;

[ExecuteInEditMode]
public class GUIViewRenderTextureTranslator : GUIViewRenderTextureManipulator
{
	#region Protected Methods

	protected override void ApplyManipulation(Vector3 translation)
	{
		Target.Translate(translation);
	}

	#endregion
}