using UnityEngine;
using HammerCat;

[ExecuteInEditMode]
public abstract class GUIViewRenderTextureManipulator : GUIViewTextureManipulator
{
	#region Public Properties

	public Camera RenderingCamera
	{
		get	{ return this.m_observer; }
		set { m_observer = value; }
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Methods

	private Vector3 m_prevRayPoint = new Vector3(-Mathf.Infinity, 0f, 0f);

	protected override void ProcessManipulation(bool calcX, bool calcY)
	{
		if(RenderingCamera != null)
		{
			Vector3 targetDelta = RayInTextureDelta(
				ViewAreaManipulating.viewGuiTexture.GetScreenRect(),
				inputManagerCursorState.Position,
				inputManagerCursorState.Delta,
				ref m_prevRayPoint,
				ViewAreaManipulating.sizeInPointSpace,
				RenderingCamera,
				Target.position,
				ViewAreaManipulating.finalLayer);



			Vector3 newAdjustment = Vector3.zero;
			if(calcX)
			{
				Vector3 deltaNoY = targetDelta;
				deltaNoY.y = 0f;
				float dist = Vector3.Magnitude(deltaNoY);
				//Debug.Log(string.Format("CURSOR DELTA: {0:F3} TARGET: {1} DIST: {2}", inputManagerCursorState.Delta.x, targetDelta, dist));

				if(inputManagerCursorState.Delta.x < 0f)
				{
					dist *= -1f;
				}
				newAdjustment.x = dist * Multiplier.x;
			}

			if(calcY)
			{
				Vector3 onlyY = Vector3.up * targetDelta.y;
				float dist = Vector3.Magnitude(onlyY);
				if(inputManagerCursorState.Delta.y < 0f)
				{
					dist *= -1f;
				}
				newAdjustment.y = dist * Multiplier.y;
			}
			ApplyManipulation(newAdjustment);
		}
	}


	protected abstract void ApplyManipulation(Vector3 newAdjustment);


	protected override GameObject CheckForHit()
	{
		return RayInTextureHit(
			ViewAreaManipulating.viewGuiTexture.GetScreenRect(),
			inputManagerCursorState.Position,
			ViewAreaManipulating.sizeInPointSpace,
			RenderingCamera,
			Target,
			Mask,
				ViewAreaManipulating.finalLayer);
	}

	protected override void ResetManipulationValues()
	{
		m_prevRayPoint = new Vector3(-Mathf.Infinity, 0f, 0f);
	}

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Protected Members

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Properties

	#endregion

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	#region Private Members

	#endregion
}