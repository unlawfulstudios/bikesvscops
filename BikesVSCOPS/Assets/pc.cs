﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class pc : InGameBehaviors {

    public Cop_AI copAi { get { return this.GetComponentInParent<Cop_AI>(); } }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (!InGameEventManager.Active)
            return;
            
        if (collision.collider.CompareTag("Player"))
        {
            copAi.hittingPlayer = true;

            if (copAi.hittingPlayer && copAi.currentSpeed > 50)
            {
                  InGameManagers.Racer_Manager.Player_BikeControl.CrashTrigger(true);               
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            copAi.hittingPlayer = false;
        }
    }
}
