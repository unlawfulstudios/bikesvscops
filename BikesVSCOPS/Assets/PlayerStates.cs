﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerStates : InGameBehaviors
{

    public Node closes_Node ;
    public List<Node> sdas;

    [SerializeField]
    private float checkTime = 0.1f;

    [SerializeField]
    private float checkTimeRate = 0.1f;

    [SerializeField]
    private float Max_Range = 50;

    public bool check = false;
    public float m_distance;
    public float respawnTime = 1f;

    private Player_BikeController playerBike;

    public override void On_Awake()
    {
        playerBike = GetComponent<Player_BikeController>();
    }

    public void Check_closesNode()
    {
        if (!InGameEventManager.Active)
            return;

        closes_Node = closesNode();

        if (closes_Node != null)
            m_distance = distance;
    }

    public Node closesNode()
    {
      return InGameManagers.object_Manager.Get_Closest_Node(transform,0);        
    }

    public override void On_Start()
    {

        if ( InGameManagers.Game_Manager.current_GameMode_Type == GameMode_Type.FREERIDE_MODE)
            return;

        InvokeRepeating("Check_closesNode", checkTime, checkTimeRate);
    }
  
    public float distance
    {
        get
        {
            return Vector3.Distance(transform.position, closes_Node.transform.position);

        }

    }


    public override void On_Update()
    {
        if (!InGameEventManager.Active)
            return;

        if ( InGameManagers.Game_Manager.current_GameMode_Type == GameMode_Type.FREERIDE_MODE)
            return;

        if (!playerBike.IsCrashed)
        {
            if (distance >= Max_Range)
            {
                playerBike.IsCrashed = true;
                StartCoroutine("Respawn");

                Debug.Log("BOOOOOM");
            }
        }

    }
  
    IEnumerator Respawn()
    {
   
        yield return new WaitForSeconds(respawnTime);
        this.transform.position = closes_Node.transform.position;
        this.transform.rotation = closes_Node.transform.rotation;
        playerBike.IsCrashed = false;
    }



   
}
