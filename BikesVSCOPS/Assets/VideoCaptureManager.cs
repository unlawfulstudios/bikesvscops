﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PlacementType
{
    CamPoint,
    Player,
    Cop,
    helicopter
}
public enum CameraMovementType
{
    pan,
    rotate,
    pointToPoint
}
public enum LookAtType
{
    Player,
    Cop,
}


public class VideoCaptureManager : InGameBehaviors
{

    public VideoCaptureNode[] Cam_Nodes;
    public Camera CaptureCamera;
    public bool PanCam = false;
    public bool PanLeft = false;
    public bool PanRight = false;

    public int current_Node_moveID = -1;
    public float ZOOMAmount = 60;

    public VideoCaptureNode[] copsNode;

    public int current_Point_moveID = 0;
    public int current_PointID = 0;


    public bool rotate_Cam = false;
    public bool rotate_Left = false;
    public bool rotate_right = false;

    public float pan_Speed = 1.0f;
    public float rotation_Speed = 1.0f;

    public bool ZOOMInOnPlayer = false;
    int panIndex = 0;


    public bool LookAtPlayer = false;

    public VideoCaptureNode current_VideoCaptuerNode;

    public bool setCamOnPlayer = false;
    public bool setCamOnCop = false;

    public int copID = 0;
    public int CopCamNum = 0;
    public int PlayerCamNum = 0;
    public static VideoCaptureManager Instance;


    private void Awake()
    {
        this.enabled = record;
        if (record && Instance != this)
        {
            Instance = this;
        }
          
    }

    public void Pan_Cam(VideoCaptureNode videoCaptureNode)
    {

        if (panIndex < videoCaptureNode.points.Length)
        {
            //Move from the previous position in the array to the next over time
            CaptureCamera.transform.position = Vector3.Lerp(videoCaptureNode.points[panIndex - 1].transform.position, videoCaptureNode.points[panIndex].transform.position, pan_Speed);


            //If we've reached the destination, move to the next one
            if (CaptureCamera.transform.position == videoCaptureNode.points[panIndex].transform.position) panIndex++;
        }
    }


    public bool moveLeft = false;
    public bool moveRight = false;

    public bool NextCop = false;
    public bool lastCop = false;

    public bool Next_Cop_CamPoint = false;
    public bool last_CopCamPoint = false;

    public bool Next_PlayerCamPoint = false;
    public bool last_PlayerCamPoint = false;

    public float ZoomOFOffSet = 500;
    public bool record = false;
    private void Update()
    {
        if (!record)
            return;


        if (LookAtPlayer)
        {

            if (InGameManagers.Racer_Manager.Player_BikeControl != null)
            {
                float dist = Vector3.Distance(CaptureCamera.transform.position, InGameManagers.Racer_Manager.Player_BikeControl.transform.position);

                if (ZOOMInOnPlayer)
                {
                    if (ZOOMAmount < 115)
                        ZOOMAmount = ZoomOFOffSet / dist;
                }
                else
                {
                    ZOOMAmount = 60;
                }

                CaptureCamera.fieldOfView = ZOOMAmount;

                CaptureCamera.transform.LookAt(InGameManagers.Racer_Manager.Player_BikeControl.transform.position);
            }

        }


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            moveLeft = true;
        }
        if (moveLeft)
        {
            MoveCamToNextNode(out current_VideoCaptuerNode);
            moveLeft = false;
        }
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            moveRight = true;
        }
        if (moveRight)
        {
            MoveCamToLastNode(out current_VideoCaptuerNode);
            moveRight = false;
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            NextCop = true;
        }
        if (NextCop)
        {
            Next_CopCam();
            NextCop = false;
        }
        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            lastCop = true;
        }
        if (lastCop)
        {
            Last_CamCopCam();
            lastCop = false;
        }
        if (setCamOnCop)
        {
            if (Input.GetKeyDown(KeyCode.Keypad8))
            {
                Next_Cop_CamPoint = true;
            }
            if (Next_Cop_CamPoint)
            {
                Next_CopCamPoint();
                Next_Cop_CamPoint = false;
            }

            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                last_PlayerCamPoint = true;
            }
            if (last_PlayerCamPoint)
            {
                Last_CamCopPoint();
                last_PlayerCamPoint = false;
            }

            setCamOnPlayer = false;

        }
        if (setCamOnPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                Next_PlayerCamPoint = true;
            }
            if (Next_PlayerCamPoint)
            {
                Next_PlayerCamPoint = false;
            }

            if (Input.GetKeyDown(KeyCode.Keypad9))
            {
                Next_PlayerCamPoint = true;
            }
        }

    }

    public void MoveCamToNextNode(out VideoCaptureNode videoCaptureNode)
    {
        videoCaptureNode = current_VideoCaptuerNode;

        if (current_Node_moveID >= Cam_Nodes.Length - 1)
        {
            return;
        }
        panIndex = 0;
        current_Node_moveID++;
        Set_Cam_On_CamPoint(current_Node_moveID, current_Point_moveID);

    }
    public void MoveCamToLastNode(out VideoCaptureNode videoCaptureNode)
    {
        videoCaptureNode = current_VideoCaptuerNode;

        if (current_Node_moveID > 0)
        {
            current_Node_moveID--;

            panIndex = 0;

            Set_Cam_On_CamPoint(current_Node_moveID, current_Point_moveID);
        }


    }

    public void MoveCamToNextPoint(VideoCaptureNode videoCaptureNode)
    {
        if (current_Point_moveID < videoCaptureNode.points.Length - 1)
        {
            panIndex = current_Point_moveID;
            current_Point_moveID++;
            Set_Cam_On_CamPoint(current_Node_moveID, current_Point_moveID);
        }




    }
    public void MoveCamToLastPoint(VideoCaptureNode videoCaptureNode)
    {
        if (current_Point_moveID > 0)
        {
            current_Point_moveID--;
            panIndex = current_Point_moveID;
            Set_Cam_On_CamPoint(current_Node_moveID, current_Point_moveID);
        }



    }

    public void Reset_Cam()
    {
        current_Node_moveID = 0;
        current_Point_moveID = 0;
        Set_Cam_On_CamPoint(0, 0);
    }

    public void Pan_Cam()
    {

    }


    public void Set_Cam(VideoCaptureNode videoCaptureNode)
    {
        switch (videoCaptureNode.placementType)
        {
            case PlacementType.CamPoint:
                break;
            case PlacementType.Player:
                break;
            case PlacementType.Cop:
                break;
            default:
                break;
        }
    }
    public void Set_Cam_On_CamPoint(int NodeID, int pointID)
    {
        CaptureCamera.transform.position = Cam_Nodes[NodeID].points[pointID].transform.position;
        current_VideoCaptuerNode = Cam_Nodes[NodeID];
        CaptureCamera.transform.SetParent(Cam_Nodes[NodeID].transform);
    }
    public BikeControl DebugBike;



    public void Set_Cam_OnPlayer(int point_ID)
    {
        if (Application.isPlaying)
        {
            CaptureCamera.transform.position = InGameManagers.Racer_Manager.Player_BikeControl.bikeSetting.cameraSwitchView[point_ID].transform.position;
            CaptureCamera.transform.SetParent(InGameManagers.Racer_Manager.Player_BikeControl.bikeSetting.cameraSwitchView[point_ID].transform);
        }
        else
        {
            CaptureCamera.transform.position = DebugBike.bikeSetting.cameraSwitchView[point_ID].transform.position;
            CaptureCamera.transform.SetParent(DebugBike.bikeSetting.cameraSwitchView[point_ID].transform);
        }



    }

    public void Next_CopCam()
    {
        if (CopCamNum >= copsNode.Length - 1)
        {
            return;
        }
        copID++;
        Set_Cam_OnCop(copID, CopCamNum);
    }

    public void Last_CamCopCam()
    {

        if (CopCamNum > 0)
        {
            copID--;
            Set_Cam_OnCop(copID, CopCamNum);
        }
    }

    public void Next_CopCamPoint()
    {
        if (CopCamNum >= copsNode.Length - 1)
        {
            return;
        }
        CopCamNum++;
        Set_Cam_OnCop(copID, CopCamNum);
    }

    public void Last_CamCopPoint()
    {
        if (CopCamNum > 0)
        {
            CopCamNum--;
            Set_Cam_OnCop(copID, CopCamNum);
        }
    }


    public void Set_Cam_OnCop(int Cop_ID, int point)
    {
        CaptureCamera.transform.position = copsNode[Cop_ID].points[point].transform.position;
        CaptureCamera.transform.SetParent(copsNode[Cop_ID].points[point].transform);
    }



}
