﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class CopDistanceController : InGameBehaviors
{

    //TODO : Move to Zenject

    public float copDistance = 50;
    public bool beingChesed = false;
    public bool Hit = false;
    public Image fill_Distance;
    public GameObject[] PlayerHub;

    public float settextnum = 0;
    public float offset = 5f;
    public float timeOffest = 0.1f;
    public float copOffset = 0.07f;
    public Cop_AI close;
    private int Checkinterval = 3;
    public bool CopsInScene { get { return InGameManagers.object_Manager.All_Cops.Count > 0 ? true : false; } }

    public override void On_Update()
    {
        if (!CopsInScene)
            return;

        if (Time.frameCount % Checkinterval == 0)
        {
            trackDistance();
        }
    }

    public void StartChase()
    {
        beingChesed = true;
    }

    public void EndTarget()
    {
        beingChesed = false;
    }

    /// <summary>
    /// Track Distance of All cops
    /// </summary>
    public void trackDistance()
    {
        if (!InGameEventManager.Active)
            return;
           
        if (!Hit)
            settextnum = Mathf.Lerp(0, copDistance / offset, timeOffest);
        else
            settextnum -= copOffset * Time.deltaTime;

        close = InGameManagers.object_Manager.Get_Closest_Cop(this.transform, 0);

        if (settextnum <= 0)
        {
            settextnum = 0;
            fill_Distance.fillAmount = 0;
            foreach (GameObject obj in PlayerHub)
            {
                obj.SetActive(false);
            }

            GlobaleManagers.EventManager.Trigger_Event("PlayerBusted");
        }
        else
        {
            copDistance = Vector3.Distance(transform.position, close.transform.position);
            fill_Distance.fillAmount = settextnum;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!InGameEventManager.Active)
            return;
        if (other.CompareTag("CopCheck"))
        {
            Hit = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("CopCheck"))
        {
            Hit = false;
        }
    }


}
