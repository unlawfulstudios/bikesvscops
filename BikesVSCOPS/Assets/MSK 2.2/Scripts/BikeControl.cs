﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BikeControl : InGameBehaviors
{

    public bool isAI = false;
    public float boosttime = 2f;

    public GameController_Type controlMode() {  return  InGameManagers.Game_Manager.current_Controller_Type; }
    public BikeLights bikeLights;

    public GameObject playerCanvuas;
    public GameObject RUNText;
    public GameObject wind;
    public Transform CamObject;
    public Canvas crash_Canvas;

    public BikeWheels bikeWheels;

    public Camera switchCam;
    public Camera Main;


    public float CrashTime = 2f;
    private int Boostamount {
        get
        {
            return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value;
        }
        set
        {
            GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value = value;
        }
    }

    public float BoostFillAmount = 1;

    public bool BoostLeft
    {
        get
        {
            bool boost = Boostamount >= 0 ? true : false;

            return boost;
        }
    }

    public bool BOOST = false;

    bool slowmotion = false;

    public bool activeControl
    {
        get
        {
            if (!debug)
                return InGameEventManager.Active;
            else
                return true;
        }
    }

    public BikeSounds bikeSounds;

    public BikeParticles bikeParticles;
    private GameObject[] Particle = new GameObject[4];

    public BikeSettings bikeSetting;

    private Quaternion SteerRotation;

    [HideInInspector]
    public bool grounded = true;

    private float MotorRotation;

    public bool Crashs = false;


    [HideInInspector]
    public float steer = 0; // steering -1.0 .. 1.0
    [HideInInspector]
    public bool brake;
    private float slip = 0.0f;
    [HideInInspector]
    public bool Backward = false;

    [HideInInspector]
    public float steer2;

    public float accel = 0.0f; // accelerating -1.0 .. 1.0

    private bool shifmotor;

    [HideInInspector]
    public float curTorque = 100f;

    [HideInInspector]
    public float powerShift = 100;

    [HideInInspector]
    public bool shift;

    private float flipRotate = 0.0f;


    [HideInInspector]
    public float speed = 0.0f;

    // table of efficiency at certain RPM, in tableStep RPM increases, 1.0f is 100% efficient
    // at the given RPM, current table has 100% at around 2000RPM
    float[] efficiencyTable = { 0.6f, 0.65f, 0.7f, 0.75f, 0.8f, 0.85f, 0.9f, 1.0f, 1.0f, 0.95f, 0.80f, 0.70f, 0.60f, 0.5f, 0.45f, 0.40f, 0.36f, 0.33f, 0.30f, 0.20f, 0.10f, 0.05f };

    // the scale of the indices in table, so with 250f, 750RPM translates to efficiencyTable[3].
    float efficiencyTableStep = 250.0f;


    private float shiftDelay = 0.0f;


    private float Pitch;
    private float PitchDelay;

    private float shiftTime = 0.0f;



    public int currentGear = 0;

    [HideInInspector]
    public bool NeutralGear = true;



    [HideInInspector]
    public float motorRPM = 0.0f;

    private float wantedRPM = 0.0f;
    private float w_rotate;

    private Rigidbody myRigidbody;

    private bool shifting;

    private float Wheelie;
    private Quaternion deltaRotation1, deltaRotation2;
    public BikeAnimation bike_Animation;
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////// TouchMode (Control) ////////////////////////////////////////////////////////////////////

    [HideInInspector]
    public float accelFwd = 0.0f;
    [HideInInspector]
    public float accelBack = 0.0f;
    [HideInInspector]
    public float steerAmount = 0.0f;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private WheelComponent[] wheels;

    public UI_NitroButton nitroButton;

    public WheelCollider frontWheelCollider;

    private WheelComponent SetWheelComponent(Transform wheel, Transform axle, bool drive, float maxSteer, float pos_y)
    {

        WheelComponent result = new WheelComponent();
        GameObject wheelCol = new GameObject(wheel.name + "WheelCollider");



        wheelCol.transform.parent = transform;
        wheelCol.transform.position = wheel.position;
        wheelCol.transform.eulerAngles = transform.eulerAngles;
        pos_y = wheelCol.transform.localPosition.y;



        wheelCol.AddComponent(typeof(WheelCollider));


        result.drive = drive;
        result.wheel = wheel;
        result.axle = axle;
        result.collider = wheelCol.GetComponent<WheelCollider>();
        result.pos_y = pos_y;
        result.maxSteer = maxSteer;
        result.startPos = axle.transform.localPosition;

        return result;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void Awake()
    {
        if (bikeSetting.automaticGear) NeutralGear = false;

        if (!isAI)
        {
            crash_Canvas.gameObject.SetActive(false);
        }

        myRigidbody = transform.GetComponent<Rigidbody>();

        SteerRotation = bikeSetting.bikeSteer.localRotation;
        wheels = new WheelComponent[2];


    }

    void Start()
    {
      
        wheels[0] = SetWheelComponent(bikeWheels.WheelsBar.wheelFront, bikeWheels.WheelsBar.AxleFront, false, bikeSetting.bikeAttributes.Handling, bikeWheels.WheelsBar.AxleFront.localPosition.y);
        wheels[1] = SetWheelComponent(bikeWheels.WheelsBar.wheelBack, bikeWheels.WheelsBar.AxleBack, true, 0, bikeWheels.WheelsBar.AxleBack.localPosition.y);

        wheels[0].collider.transform.localPosition = new Vector3(0, wheels[0].collider.transform.localPosition.y, wheels[0].collider.transform.localPosition.z);
        wheels[1].collider.transform.localPosition = new Vector3(0, wheels[1].collider.transform.localPosition.y, wheels[1].collider.transform.localPosition.z);


        frontWheelCollider = wheels[0].collider;

        if(!isAI)
        wind.SetActive(false);

        foreach (WheelComponent w in wheels)
        {

            WheelCollider col = w.collider;

            col.suspensionDistance = bikeWheels.setting.Distance;
            JointSpring js = col.suspensionSpring;

            js.spring = bikeSetting.springs;
            js.damper = bikeSetting.dampers;
            col.suspensionSpring = js;


            col.radius = bikeWheels.setting.Radius;
            col.center = new Vector3(0, 0.11f, 0);

            col.mass = bikeWheels.setting.Weight;


            WheelFrictionCurve fc = col.forwardFriction;

            fc.asymptoteValue = 0.5f;
            fc.extremumSlip = 0.4f;
            fc.asymptoteSlip = 0.8f;
            fc.stiffness = bikeSetting.stiffness;
            col.forwardFriction = fc;
            fc = col.sidewaysFriction;
            fc.asymptoteValue = 0.75f;
            fc.extremumSlip = 0.2f;
            fc.asymptoteSlip = 0.5f;
            fc.stiffness = bikeSetting.stiffness;
            col.sidewaysFriction = fc;

        }
    }

    public void ShiftUp()
    {
        float now = Time.timeSinceLevelLoad;

        if (now < shiftDelay) return;


        if (currentGear < bikeSetting.gears.Length - 1)
        {

            // if (!bikeSounds.switchGear.isPlaying)
            bikeSounds.switchGear.GetComponent<AudioSource>().Play();


            if (!bikeSetting.automaticGear)
            {
                if (currentGear == 0)
                {
                    if (NeutralGear) { currentGear++; NeutralGear = false; }
                    else
                    { NeutralGear = true; }
                }
                else
                {
                    currentGear++;
                }
            }
            else
            {
                currentGear++;
            }


            shiftDelay = now + 1.0f;
            shiftTime = 1.0f;
        }
    }

    public void ShiftDown()
    {
        float now = Time.timeSinceLevelLoad;

        if (now < shiftDelay) return;

        if (currentGear > 0 || NeutralGear)
        {

            //w if (!bikeSounds.switchGear.isPlaying)
            bikeSounds.switchGear.GetComponent<AudioSource>().Play();

            if (!bikeSetting.automaticGear)
            {

                if (currentGear == 1)
                {
                    if (!NeutralGear) { currentGear--; NeutralGear = true; }
                }
                else if (currentGear == 0) { NeutralGear = false; } else { currentGear--; }
            }
            else
            {
                currentGear--;
            }


            shiftDelay = now + 0.1f;
            shiftTime = 2.0f;
        }
    }

    //public void OnCollisionEnter(Collision collision)
    //{
     
    //}



    public void SetCam(bool toggle)
    {
        if (isAI)
            return;

        Main.gameObject.SetActive(toggle);
        switchCam.gameObject.SetActive(!toggle);

        if (!toggle)
        {

            switchCam.transform.SetParent(null);
            crash_Canvas.gameObject.SetActive(true);
        }
        else
        {

            switchCam.transform.SetParent(this.transform);
            crash_Canvas.gameObject.SetActive(false);
            switchCam.transform.position = CamObject.position;
            Crashs = false;

        }

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.W))
        {

            InGameEventManager.Button_Pressed = true;

        }


        if (activeControl)
        {

            if (!bikeSetting.automaticGear)
            {
                if (Input.GetKeyDown("page up"))
                {
                    ShiftUp();


                }
                if (Input.GetKeyDown("page down"))
                {
                    ShiftDown();

                }
            }




        }




        steer2 = Mathf.LerpAngle(steer2, steer * -bikeSetting.bikeAttributes.Handling, Time.deltaTime * 10.0f);

        MotorRotation = Mathf.LerpAngle(MotorRotation, steer2 * bikeSetting.bikeAttributes.MaxTiltAngle * (Mathf.Clamp(speed / 100, 0.0f, 1.0f)), Time.deltaTime * 5.0f);

        if (bikeSetting.bikeSteer)
            bikeSetting.bikeSteer.localRotation = SteerRotation * Quaternion.Euler(0, wheels[0].collider.steerAngle, 0); // this is 90 degrees around y axis



        if (!Crashs && BoostLeft)
        {
            flipRotate = (transform.eulerAngles.z > 70 && transform.eulerAngles.z < 270) ? 180.0f : 0.0f;



            Wheelie = Mathf.Clamp(Wheelie, 0, bikeSetting.maxWheelie);
            if (shifting)
            {
                Wheelie += bikeSetting.speedWheelie * Time.deltaTime / (speed / 50);
            }
            else
            {
                Wheelie = Mathf.MoveTowards(Wheelie, 0, (bikeSetting.speedWheelie * 2) * Time.deltaTime * 1.3f);
            }



            deltaRotation1 = Quaternion.Euler(-Wheelie, 0, flipRotate - transform.localEulerAngles.z + (MotorRotation));
            deltaRotation2 = Quaternion.Euler(0, 0, flipRotate - transform.localEulerAngles.z);


            myRigidbody.MoveRotation(myRigidbody.rotation * deltaRotation2);
            bikeSetting.MainBody.localRotation = deltaRotation1;


        }
        else
        {

            bikeSetting.MainBody.localRotation = Quaternion.identity;
            Wheelie = 0;
        }


        if (switchCam != null && switchCam.gameObject.activeInHierarchy)
            switchCam.transform.LookAt(bikeSetting.MainBody);


        if (BOOST)
        {
            BoostFillAmount -= Time.deltaTime * 0.2f;
        }
        else
        {
            BoostFillAmount = 1;
        }

    }

    void FixedUpdate()
    {

        if (Loading_Manager.isLoading)
            return;

        speed = myRigidbody.velocity.magnitude * 2.7f;

        if (Crashs)
        {
            myRigidbody.constraints = RigidbodyConstraints.None;
            myRigidbody.centerOfMass = Vector3.zero;
        }
        else
        {
            myRigidbody.constraints = RigidbodyConstraints.FreezeRotationZ;
            myRigidbody.centerOfMass = bikeSetting.shiftCentre;
        }

        if (activeControl)
        {
            

            if (controlMode() == GameController_Type.pc_KeyBoard)
            {       
                accel = 0.0f;
                brake = false;

                if (!Crashs)
                {
                    steer = Mathf.MoveTowards(steer, Input.GetAxis("Horizontal"), 0.1f);
                    accel = Input.GetAxis("Vertical");
                    brake = Input.GetButton("Jump");

                }
                else
                {
                    steer = 0;
                }

            }
            if (controlMode() == GameController_Type.moblie_Touch)  
            {
                if (accelFwd != 0) { accel = accelFwd; } else { accel = accelBack; }
                steer = Mathf.MoveTowards(steer, steerAmount, 0.07f);

            }

            //if (controlMode == GameController_Type.Ai)
            //{

            //    if (accelFwd != 0) { accel = accelFwd; } else { accel = accelBack; }
            //    steer = Mathf.MoveTowards(steer, steerAmount, 0.07f);

   


        }
        else
        {

            accel = 0.0f;
            steer = 0.0f;
            brake = false;

        }

        if (accel < 0 || speed < 1.0f)
            bikeLights.brakeLights.intensity = Mathf.Lerp(bikeLights.brakeLights.intensity, 8, 0.1f);
        else
            bikeLights.brakeLights.intensity = Mathf.Lerp(bikeLights.brakeLights.intensity, 0, 0.1f);
        // handle automatic shifting

        if (bikeSetting.automaticGear && (currentGear == 1) && (accel < 0.0f))
        {
            if (speed < 1.0f)
                ShiftDown(); // reverse


        }
        else if (bikeSetting.automaticGear && (currentGear == 0) && (accel > 0.0f))
        {
            if (speed < 5.0f)
                ShiftUp(); // go from reverse to first gear

        }
        else if (bikeSetting.automaticGear && (motorRPM > bikeSetting.shiftUpRPM) && (accel > 0.0f) && speed > 10.0f && !brake)
        {
            // if (speed > 20)
            ShiftUp(); // shift up

        }
        else if (bikeSetting.automaticGear && (motorRPM < bikeSetting.shiftDownRPM) && (currentGear > 1))
        {
            ShiftDown(); // shift down
        }

        if (speed < 1.0f) Backward = true;

        if (currentGear == 0 && Backward == true)
        {
            if (speed < bikeSetting.gears[0] * -10)
                accel = -accel; // in automatic mode we need to hold arrow down for reverse
        }
        else
        {
            Backward = false;
        }
        wantedRPM = (5500.0f * accel) * 0.1f + wantedRPM * 0.9f;

        float rpm = 0.0f;
        int motorizedWheels = 0;
        bool floorContact = false;
        int currentWheel = 0;


        foreach (WheelComponent w in wheels)
        {
            WheelHit hit;
            WheelCollider col = w.collider;

            if (w.drive)
            {
                if (!NeutralGear && brake && currentGear < 2)
                {
                    rpm += accel * bikeSetting.idleRPM;

                }
                else
                {
                    if (!NeutralGear)
                    {
                        rpm += col.rpm;
                    }
                    else
                    {
                        rpm += ((bikeSetting.idleRPM * 2.0f) * accel);
                    }
                }

                motorizedWheels++;
            }


            if (Crashs)
            {
                w.collider.enabled = false;
                w.wheel.GetComponent<Collider>().enabled = true;
            }
            else
            {
                w.collider.enabled = true;
                w.wheel.GetComponent<Collider>().enabled = false;
            }


            if (brake || accel < 0.0f)
            {
                if ((accel < 0.0f) || (brake && w == wheels[1]))
                {

                    if (brake && (accel > 0.0f))
                    {
                        slip = Mathf.Lerp(slip, bikeSetting.slipBrake, accel * 0.01f);
                    }
                    else if (speed > 1.0f)
                    {
                        slip = Mathf.Lerp(slip, 1.0f, 0.002f);
                    }
                    else
                    {
                        slip = Mathf.Lerp(slip, 1.0f, 0.02f);
                    }


                    wantedRPM = 0.0f;
                    col.brakeTorque = bikeSetting.brakePower;
                    w.rotation = w_rotate;

                }
            }
            else
            {


                col.brakeTorque = accel == 0 ? col.brakeTorque = 3000 : col.brakeTorque = 0;

                slip = Mathf.Lerp(slip, 1.0f, 0.02f);

                w_rotate = w.rotation;

            }



            WheelFrictionCurve fc = col.forwardFriction;


            if (w == wheels[1])
            {

                fc.stiffness = bikeSetting.stiffness / slip;
                col.forwardFriction = fc;
                fc = col.sidewaysFriction;
                fc.stiffness = bikeSetting.stiffness / slip;
                col.sidewaysFriction = fc;
            }




            if (BOOST && (currentGear > 1))
            {
                shifting = true;
                if (powerShift == 0) { shifmotor = false; }

                powerShift = Mathf.MoveTowards(powerShift, 0.0f, Time.deltaTime * 30.0f);

                bikeSounds.nitro.volume = Mathf.Lerp(bikeSounds.nitro.volume, 1.0f, Time.deltaTime * 10.0f);

                if (!bikeSounds.nitro.isPlaying)
                {
                    bikeSounds.nitro.GetComponent<AudioSource>().Play();

                }


                curTorque = powerShift > 0 ? bikeSetting.shiftPower : bikeSetting.bikeAttributes.Acceleration;
                bikeParticles.shiftParticle1.emissionRate = Mathf.Lerp(bikeParticles.shiftParticle1.emissionRate, powerShift > 0 ? 50 : 0, Time.deltaTime * 10.0f);
                bikeParticles.shiftParticle2.emissionRate = Mathf.Lerp(bikeParticles.shiftParticle2.emissionRate, powerShift > 0 ? 50 : 0, Time.deltaTime * 10.0f);

            }
            else
            {
                shifting = false;

                if (powerShift > 20)
                {
                    shifmotor = true;
                }

                bikeSounds.nitro.volume = Mathf.MoveTowards(bikeSounds.nitro.volume, 0.0f, Time.deltaTime * 2.0f);

                if (bikeSounds.nitro.volume == 0)
                    bikeSounds.nitro.Stop();

                powerShift = Mathf.MoveTowards(powerShift, 100.0f, Time.deltaTime * 5.0f);
                curTorque = bikeSetting.bikeAttributes.Acceleration;
                bikeParticles.shiftParticle1.emissionRate = Mathf.Lerp(bikeParticles.shiftParticle1.emissionRate, 0, Time.deltaTime * 10.0f);
                bikeParticles.shiftParticle2.emissionRate = Mathf.Lerp(bikeParticles.shiftParticle2.emissionRate, 0, Time.deltaTime * 10.0f);
            }


            w.rotation = Mathf.Repeat(w.rotation + Time.deltaTime * col.rpm * 360.0f / 60.0f, 360.0f);
            w.wheel.localRotation = Quaternion.Euler(w.rotation, 0.0f, 0.0f);



            Vector3 lp = w.axle.localPosition;


            if (col.GetGroundHit(out hit) && (w == wheels[1] || (w == wheels[0] && Wheelie == 0)))
            {


                if (bikeParticles.brakeParticlePrefab)
                {
                    if (Particle[currentWheel] == null)
                    {
                        Particle[currentWheel] = Instantiate(bikeParticles.brakeParticlePrefab, w.wheel.position, Quaternion.identity) as GameObject;
                        Particle[currentWheel].name = "WheelParticle";
                        Particle[currentWheel].transform.parent = transform;
                        Particle[currentWheel].AddComponent<AudioSource>();
                        Particle[currentWheel].GetComponent<AudioSource>().spatialBlend = 1;
                        Particle[currentWheel].GetComponent<AudioSource>().rolloffMode = AudioRolloffMode.Linear;
                    }


                    var pc = Particle[currentWheel].GetComponent<ParticleSystem>();
                    bool WGrounded = false;


                    for (int i = 0; i < bikeSetting.HitGround.Length; i++)
                    {

                        if (hit.collider.CompareTag(bikeSetting.HitGround[i].tag))
                        {
                            WGrounded = bikeSetting.HitGround[i].grounded;

                            if ((brake || Mathf.Abs(hit.sidewaysSlip) > 0.5f) && speed > 1)
                            {
                                Particle[currentWheel].GetComponent<AudioSource>().clip = bikeSetting.HitGround[i].brakeSound;
                            }
                            else if (Particle[currentWheel].GetComponent<AudioSource>().clip != bikeSetting.HitGround[i].groundSound && !Particle[currentWheel].GetComponent<AudioSource>().isPlaying)
                            {

                                Particle[currentWheel].GetComponent<AudioSource>().clip = bikeSetting.HitGround[i].groundSound;
                            }

                            Particle[currentWheel].GetComponent<ParticleSystem>().startColor = bikeSetting.HitGround[i].brakeColor;

                        }


                    }




                    if (WGrounded && speed > 5 && !brake)
                    {

                        pc.enableEmission = true;

                        Particle[currentWheel].GetComponent<AudioSource>().volume = 0.5f;

                        if (!Particle[currentWheel].GetComponent<AudioSource>().isPlaying)
                            Particle[currentWheel].GetComponent<AudioSource>().Play();

                    }
                    else if ((brake || Mathf.Abs(hit.sidewaysSlip) > 0.6f) && speed > 1)
                    {

                        if ((accel < 0.0f) || ((brake || Mathf.Abs(hit.sidewaysSlip) > 0.6f) && w == wheels[1]))
                        {

                            if (!Particle[currentWheel].GetComponent<AudioSource>().isPlaying)
                                Particle[currentWheel].GetComponent<AudioSource>().Play();
                            pc.enableEmission = true;

                         
                            Particle[currentWheel].GetComponent<AudioSource>().volume = Mathf.Clamp((speed / 60), 0, 1.0f);

                        }

                    }
                    else
                    {

                        pc.enableEmission = false;
                        Particle[currentWheel].GetComponent<AudioSource>().volume = Mathf.Lerp(Particle[currentWheel].GetComponent<AudioSource>().volume, 0, Time.deltaTime * 10.0f);
                    }

                }


                lp.y -= Vector3.Dot(w.wheel.position - hit.point, transform.TransformDirection(0, 1, 0)) - (col.radius);
                lp.y = Mathf.Clamp(lp.y, w.startPos.y - bikeWheels.setting.Distance, w.startPos.y + bikeWheels.setting.Distance);


                floorContact = floorContact || (w.drive);


                if (!Crashs)
                {

                    myRigidbody.angularDrag = 10.0f;
                }
                else
                {
                    myRigidbody.angularDrag = 0.0f;


                }


                grounded = true;

                if (w.collider.GetComponent<WheelSkidmarks>())
                    w.collider.GetComponent<WheelSkidmarks>().enabled = true;

            }
            else
            {
                grounded = false;

                if (w.collider.GetComponent<WheelSkidmarks>())
                    w.collider.GetComponent<WheelSkidmarks>().enabled = false;



                if (Particle[currentWheel] != null)
                {
                    var pc = Particle[currentWheel].GetComponent<ParticleSystem>();
                    pc.enableEmission = false;
                }



                lp.y = w.startPos.y - bikeWheels.setting.Distance;

                if (!wheels[0].collider.isGrounded && !wheels[1].collider.isGrounded)
                {

                    myRigidbody.centerOfMass = new Vector3(0, 0.2f, 0);
                    myRigidbody.angularDrag = 1.0f;

                    myRigidbody.AddForce(0, -10000, 0);
                }

            }

            currentWheel++;
            w.axle.localPosition = lp;

        }


        if (motorizedWheels > 1)
        {
            rpm = rpm / motorizedWheels;
        }


        motorRPM = 0.95f * motorRPM + 0.05f * Mathf.Abs(rpm * bikeSetting.gears[currentGear]);
        if (motorRPM > 5500.0f) motorRPM = 5200.0f;


        int index = (int)(motorRPM / efficiencyTableStep);
        if (index >= efficiencyTable.Length) index = efficiencyTable.Length - 1;
        if (index < 0) index = 0;



        float newTorque = curTorque * bikeSetting.gears[currentGear] * efficiencyTable[index];

        // go set torque to the WheelsBar
        foreach (WheelComponent w in wheels)
        {
            WheelCollider col = w.collider;

            // of course, only the WheelsBar connected to the engine can get engine torque
            if (w.drive)
            {

                if (Mathf.Abs(col.rpm) > Mathf.Abs(wantedRPM))
                {

                    col.motorTorque = 0;
                }
                else
                {
                    // 
                    float curTorqueCol = col.motorTorque;
                    float currentSpeedHeld = !BOOST ? bikeSetting.bikeAttributes.MaxSpeed : bikeSetting.LimitdSpeedBoost;
                    float currentTorqueheld = !BOOST ? 1.0f : 5f;

                    if (!brake && accel != 0 && NeutralGear == false)
                    {

                        if ((speed < currentSpeedHeld && currentGear > 0) ||
                            (speed < bikeSetting.LimitBackwardSpeed && currentGear == 0))
                        {

                            col.motorTorque = curTorqueCol * 0.9f + newTorque * currentTorqueheld;
                        }
                        else
                        {
                            col.motorTorque = 0;
                            col.brakeTorque = 2000;
                        }


                    }
                    else
                    {
                        col.motorTorque = 0;

                    }
                }

            }

            float SteerAngle = Mathf.Clamp((speed) / bikeSetting.bikeAttributes.Handling, 1.0f, bikeSetting.bikeAttributes.Handling);
            col.steerAngle = steer * (w.maxSteer / SteerAngle);

        }

        // calculate pitch (keep it within reasonable bounds)
        Pitch = Mathf.Clamp(1.2f + ((motorRPM - bikeSetting.idleRPM) / (bikeSetting.shiftUpRPM - bikeSetting.idleRPM)), 1.0f, 10.0f);

        shiftTime = Mathf.MoveTowards(shiftTime, 0.0f, 0.1f);

        if (Pitch == 1 || currentGear == 0)
        {
            bikeSounds.IdleEngine.volume = Mathf.Lerp(bikeSounds.IdleEngine.volume, 1.0f, 0.1f);
            bikeSounds.LowEngine.volume = Mathf.Lerp(bikeSounds.LowEngine.volume, 0.5f, 0.1f);
            bikeSounds.HighEngine.volume = Mathf.Lerp(bikeSounds.HighEngine.volume, 0.0f, 0.1f);
        }
        else
        {

            bikeSounds.IdleEngine.volume = Mathf.Lerp(bikeSounds.IdleEngine.volume, 1.8f - Pitch, 0.1f);


            if ((Pitch > PitchDelay || accel > 0) && shiftTime == 0.0f)
            {
                bikeSounds.LowEngine.volume = Mathf.Lerp(bikeSounds.LowEngine.volume, 0.0f, 0.2f);
                bikeSounds.HighEngine.volume = Mathf.Lerp(bikeSounds.HighEngine.volume, 1.0f, 0.2f);
            }
            else
            {
                bikeSounds.LowEngine.volume = Mathf.Lerp(bikeSounds.LowEngine.volume, 1.0f, 0.2f);
                bikeSounds.HighEngine.volume = Mathf.Lerp(bikeSounds.HighEngine.volume, 0.0f, 0.2f);
            }

            bikeSounds.HighEngine.pitch = Pitch;
            bikeSounds.LowEngine.pitch = Pitch;
            PitchDelay = Pitch;

        }

    }

    public Vector3 windRotation = new Vector3(0, -180, 0);
    public void Boost()
    {
        if (!BOOST)
        {
            BOOST = true;
            wind.SetActive(true);
            wind.transform.eulerAngles = windRotation;
            StartCoroutine("BoostTime");

        }
    }

   
    public IEnumerator BoostTime()
    {
      
        yield return new WaitForSeconds(boosttime);

        BOOST = false;
        wind.SetActive(false);
        if (Boostamount >= 0)
        {
            nitroButton.interactable = true;
        }

        StopCoroutine("BoostTime");
    }


    public void CrashTrigger(bool busted)
    {
        if (!InGameEventManager.Active ||
            InGameEventManager.current_Run_EventState == Run_Event_States.End ||
            InGameEventManager.Current_endType == Result_Type.LOST)
            return;

        if (Loading_Manager.isLoading)
            return;

        if (bike_Animation != null)
            bike_Animation.Crash();

        if (!isAI)
        {
            SetCam(false);
            playerCanvuas.SetActive(false);
            Time.timeScale = 0.4f;
            RUNText.SetActive(false);
        }

        Crashs = true;



        if (busted)
            Run_Timer.timeGO = false;

        StartCoroutine(Crash(busted));
    }

    IEnumerator Crash(bool busted)
    {
        yield return new WaitForSeconds(CrashTime);
        Time.timeScale = 1;
        Set_end(busted);
        StopCoroutine("Crash");

    }

    public void Set_end(bool busted)
    {
        if (isAI)
            return;

        if (busted)
        {
            End();
        }
        else
            SetCam(true);

        playerCanvuas.SetActive(true);
    }

    void End()
    {
        SetCam(true);
        GlobaleManagers.EventManager.Trigger_Event("PlayerBusted");

    }


    public bool IsStoped()
    {
        return myRigidbody.velocity.magnitude < 0.5f ? true : false || currentGear == 0;
    }
    /////////////// Show Normal Gizmos ///////////////////////////

    void OnDrawGizmos()
    {

         if(debug)
        {

            Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

            Gizmos.matrix = rotationMatrix;
            Gizmos.color = new Color(1, 0, 0, 0.5f);

            Gizmos.DrawCube(Vector3.up / 1.6f, new Vector3(0.5f, 1.0f, 2.5f));
            Gizmos.DrawSphere(bikeSetting.shiftCentre, 0.2f);

        }

    }

#if UNITY_EDITOR
    public override void On_Reset()
    {
        bike_Animation = GetComponentInChildren<BikeAnimation>();
    }
#endif
}

