﻿using UnityEngine;
using System.Collections;

public class PoliceLights : MonoBehaviour
{
    public float time = 20;

    public GameObject RedLight;
    public GameObject BlueLight;

    private float timer = 0.0f;
    private int lightNum = 0;


    void Update()
    {
        timer = Mathf.MoveTowards(timer, 0.0f, Time.deltaTime * time);

        if (timer == 0)
        {
            lightNum++;
            if (lightNum > 12) { lightNum = 1; }
            timer = 1.0f;
        }

        if (lightNum == 1 || lightNum == 3)
        {

            RedLight.SetActive(true);

            BlueLight.SetActive(false);

        }

        if (lightNum == 5 || lightNum == 7)
        {
            BlueLight.SetActive(true);

            RedLight.SetActive(false);

        }

        if (lightNum == 2 || lightNum == 4 || lightNum == 6 || lightNum == 8)
        {

            BlueLight.SetActive(false);

            RedLight.SetActive(false);

        }

    }

}




