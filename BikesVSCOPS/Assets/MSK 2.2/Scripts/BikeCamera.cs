using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class BikeCameraSettings : ScriptableObject
{

}

public class BikeCamera : InGameBehaviors
{
    public Transform BikerMan;
    public float smooth = 0.3f;
    public float distance = 5.0f;
    public float height = 1.0f;
    public float Angle = 20;
    public List<Transform> cameraSwitchView;
    public LayerMask lineOfSightMask = 0;

    private float yVelocity = 0.0f;
    private float xVelocity = 0.0f;


    private int gearst = 0;
    private float thisAngle = -150;
    private float restTime = 0.0f;
    private Rigidbody myRigidbody;
    private Player_BikeController bikeScript;
    private IEnumerator Start()
    {


        yield return new WaitUntil(() => InGameManagers.Racer_Manager.Player_BikeControl != null);
        myRigidbody = player.GetComponent<Rigidbody>();
        bikeScript = InGameManagers.Racer_Manager.Player_BikeControl;
        cameraSwitchView = bikeScript.bikeSetting.cameraSwitchView;
        BikerMan = bikeScript.bikeSetting.bikerMan;

        myRigidbody = player.GetComponent<Rigidbody>();

    }


    public void BikeAccelBack(float amount)
    {
        bikeScript.accelBack = amount;
    }
    public void BikeSteer(float amount)
    {
        bikeScript.steerAmount = amount;
    }
   
    public void BikeHandBrake(bool HBrakeing)
    {
        reverse(HBrakeing);
    }

    public bool isBreaking = false;

    public void reverse(bool HBrakeing)
    {
        bikeScript.brake = HBrakeing;
    }

    public void BikeShift(bool Shifting)
    {        
       bikeScript.shift = Shifting;
       
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void RestBike()
    {

        if (restTime == 0)
        {
            myRigidbody.AddForce(Vector3.up * 500000);
            myRigidbody.MoveRotation(Quaternion.Euler(0, transform.eulerAngles.y, 0));
            restTime = 2.0f;
        }

    }

    void Update()
    {

        if (!player) return;

        if (bikeScript == null)
            return;



        if (restTime != 0.0f)
            restTime = Mathf.MoveTowards(restTime, 0.0f, Time.deltaTime);

        GetComponent<Camera>().fieldOfView = Mathf.Clamp(bikeScript.speed / 10.0f + 60.0f, 60, 90.0f);

        if (!bikeScript.bikeSetting.crash)
        {

                float xAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x,
               player.transform.eulerAngles.x + Angle, ref xVelocity, smooth);

                float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y,
                player.transform.eulerAngles.y, ref yVelocity, smooth);
                
                transform.eulerAngles = new Vector3(Angle, yAngle, 0.0f);

                Vector3 direction = transform.rotation * -Vector3.forward;
                float targetDistance = AdjustLineOfSight(player.transform.position + new Vector3(0, height, 0), direction);


                transform.position = player.transform.position + new Vector3(0, height, 0) + direction * targetDistance;
        }
        else
        {
            Vector3 look = BikerMan.position - transform.position;
            transform.rotation = Quaternion.LookRotation(look);
        }

    }
    float AdjustLineOfSight(Vector3 target, Vector3 direction)
    {


        RaycastHit hit;

        if (Physics.Raycast(target, direction, out hit, distance, lineOfSightMask.value))
            return hit.distance;
        else
            return distance;

    }


}
