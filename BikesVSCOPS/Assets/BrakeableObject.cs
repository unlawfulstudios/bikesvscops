﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class BrakeableObject : MonoBehaviour {

    private Rigidbody current_rigidbody() { return GetComponent<Rigidbody>(); }

    public void OnEnable()
    {
        current_rigidbody().isKinematic = true;
    }

    // TODO: Add Layer Mask 
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Cop"))
        {
            Debug.Log("Hit");
            current_rigidbody().isKinematic = false;
            current_rigidbody().AddForce(Vector3.forward * 100);
        }
    }


}
