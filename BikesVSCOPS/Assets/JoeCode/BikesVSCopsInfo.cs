﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BikesVSCopsInfo : UserProfileObject {

    public override string Name
    {
        get
        {
            return "BikesVSCopsInfo";
        }

    }
    public UserProfileKey<int, bool> HasPlayedBefore = new UserProfileKey<int, bool>("HasPlayedBefore", false);
    public UserProfileKey<int, int> Current_Bike_HolderID = new UserProfileKey<int, int>("Current_Bike_HolderIDs", 1);
    public UserProfileKey<int, int> Current_Bike_iD = new UserProfileKey<int, int>("Current_Bike_ID", 0);
    public UserProfileKey<string, string> hash = new UserProfileKey<string, string>("Hash", "NULL");
    public City current_City = new City(0);
    public RUN current_Run = new RUN(0, 0);
    public RUN next_Run = new RUN(0, 0);
    public Bike Current_Bike = new Bike(0);
    public StringData UserName = new StringData("UserName","Pilltech");
    public StringData Email = new StringData("Email", "dull@null.com");
    public IntData BoostAmount = new IntData("BoostAmount", 100);
    public IntData GetAwayTries = new IntData("GetAwayTries", 15);
    public FloatData AmountOfMoney = new FloatData("AmountOfMoney$", 1000);

    
    public override void SyncData()
    {
        BoostAmount.SyncData();
        GetAwayTries.SyncData();
        AmountOfMoney.Reset();
        base.SyncData();
    }
    public override void Reset()
    {
        BoostAmount.Reset();
        GetAwayTries.Reset();
        AmountOfMoney.Reset();
        base.Reset();
    }

    protected override bool m_DataSynced()
    {
        return BoostAmount.DataSynced && GetAwayTries.DataSynced ? true : false;
    }


}
