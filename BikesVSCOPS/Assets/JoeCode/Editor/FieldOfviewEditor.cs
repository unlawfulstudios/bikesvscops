﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FieldOrfview))]
public class FieldOfviewEditor : Editor {


    private void OnSceneGUI()
    {
        FieldOrfview fow = (FieldOrfview)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.viewRadius);
        Vector3 viewAngle_A = fow.Dir_FromAngle(-fow.viewAngle/2,false);
        Vector3 viewAngle_B = fow.Dir_FromAngle(fow.viewAngle / 2, false);

    

        Handles.DrawLine(fow.transform.position, fow.transform.transform.position + viewAngle_A * fow.viewRadius);
        Handles.DrawLine(fow.transform.position, fow.transform.transform.position + viewAngle_B * fow.viewRadius);


        Handles.color = Color.red;

            foreach (Transform visableTarget in fow.visibleTargets)
            {
                Handles.DrawLine(fow.transform.position, visableTarget.position);
            }


    }
}

[CustomEditor(typeof(VideoCaptureNode))]
public class VideoCaptureNodeEditor : Editor
{


    private void OnSceneGUI()
    {
        VideoCaptureNode videoCaptureNode = (VideoCaptureNode)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(videoCaptureNode.transform.position, Vector3.up, Vector3.forward, 360, videoCaptureNode.viewRadius);
        Vector3 viewAngle_A = videoCaptureNode.Dir_FromAngle(-videoCaptureNode.viewAngle / 2, false);
        Vector3 viewAngle_B = videoCaptureNode.Dir_FromAngle(videoCaptureNode.viewAngle / 2, false);



        Handles.DrawLine(videoCaptureNode.transform.position, videoCaptureNode.transform.transform.position + viewAngle_A * videoCaptureNode.viewRadius);
        Handles.DrawLine(videoCaptureNode.transform.position, videoCaptureNode.transform.transform.position + viewAngle_B * videoCaptureNode.viewRadius);


    }
}
