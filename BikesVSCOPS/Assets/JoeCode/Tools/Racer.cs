﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racer : InGameBehaviors
{
    public BikeControl Racer_BikeController { get { return GetComponent<BikeControl>(); } }

    public override void On_Start()
    {
        InGameManagers.object_Manager.RacerList.Add(this);
    }
  
}
