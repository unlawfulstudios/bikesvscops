﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public static class AppInfo
{
   
    public static System.Type FindType(string typeName, bool useFullName = false, bool ignoreCase = false)
    {
        if (string.IsNullOrEmpty(typeName)) return null;

        StringComparison e = (ignoreCase) ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
        if (useFullName)
        {
            foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var t in assemb.GetTypes())
                {
                    if (string.Equals(t.FullName, typeName, e)) return t;
                }
            }
        }
        else
        {
            foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var t in assemb.GetTypes())
                {
                    if (string.Equals(t.FullName, typeName, e)) return t;
                }
            }
        }
        return null;
    }
}

public static class ResourcesReturnType
{

    public static Type Value(string Name)
    {
         return AppInfo.FindType(Name); ;
    }


    private static Type m_Value;


}

