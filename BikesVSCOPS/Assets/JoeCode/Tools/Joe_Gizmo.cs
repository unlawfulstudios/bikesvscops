﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Gizmo_Type
{
    Unidentified,
    Wired_Sphere,
    Wired_Cube
}


public class Joe_Gizmo : MonoBehaviour
{

    public Color gizmo_Color;
    public Gizmo_Type current_Gizmo_Type;
    public float gizmo_Radius = 0.5f;
    public Vector3 cube_Size;

    private void OnDrawGizmos()
    {
        Gizmos.color = gizmo_Color;

        switch (current_Gizmo_Type)
        {
            case Gizmo_Type.Unidentified:
                return;

            case Gizmo_Type.Wired_Sphere:
                Gizmos.DrawWireSphere(transform.position, gizmo_Radius);
                return;
            case Gizmo_Type.Wired_Cube:
                Gizmos.DrawWireCube(transform.position, cube_Size);
                return;
        }

    }
}
