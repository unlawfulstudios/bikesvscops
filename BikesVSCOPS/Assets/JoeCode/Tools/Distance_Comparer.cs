﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Distance_Comparer : IComparer
{
    public Transform Target;
    public float DistanceAmount { get { return m_DistanceAmount; } }
    public int Compare(object x, object y)
    {
        return CheckDistance(x, y);
    }
    
    public int CheckDistance<T>(T x, T y)
    {

        Component c1 = x as Component;
        Component c2 = y as Component;


        Vector3 tPos = Target.position;
        Vector3 xPos = c1.transform.position;
        Vector3 yPos = c2.transform.position;
       

        float xDistance = (tPos - xPos).magnitude;
        float yDistance = (tPos - yPos).magnitude;

       



        if (xDistance > yDistance)
        {
            return 1;
        }
        else if (xDistance < yDistance)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

    private float m_DistanceAmount;
}
