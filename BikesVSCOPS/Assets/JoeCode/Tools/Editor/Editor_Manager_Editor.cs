﻿
using UnityEngine;
using UnityEditor;

public class Editor_Manager_Editor : ScriptableObject
{ 

    [MenuItem("Tools/Clear/ All Editor Preferences")]
    static void Clear_all_Editor()
    {
        if (EditorUtility.DisplayDialog("Delete all editor preferences.",
                "Are you sure you want to delete all the editor preferences? " +
                "This action cannot be undone.", "Yes", "No"))
        {
            Debug.Log("yes");
            EditorPrefs.DeleteAll();
        }
    }

    [MenuItem("Tools/Clear/All Player Preferences")]
    static void deleteAllExample()
    {
        if (EditorUtility.DisplayDialog("Delete all Player preferences.",
                "Are you sure you want to delete all the Player preferences? " +
                "This action cannot be undone.", "Yes", "No"))
        {
            Debug.Log("yes");
            PlayerPrefs.DeleteAll();
        }
    }
}
