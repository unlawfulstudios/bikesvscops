﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Run_Manager))]
public class RunManager_Editor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        if(GUILayout.Button("Set Start Point"))
        {
            Run_Manager.Instance.Set_Player();
        }

      
    }
}
