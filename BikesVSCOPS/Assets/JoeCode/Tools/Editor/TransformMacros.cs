﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class TransformMacros : Editor
{
    [InitializeOnLoadMethod]
    public static void InitUpdate()
    {

        EditorApplication.update += EditorUpdate;



    }

    static bool find = false;



    [MenuItem("Tools/Snap Selection To Ground/Snap #END")]
    public static void SnapToGround()
    {

        Transform[] myTransForms = Selection.transforms;

        foreach (var transform in Selection.transforms)
        {
            var hits = Physics.RaycastAll(transform.position + Vector3.up, Vector3.down, 10f);
            foreach (var hit in hits)
            {
                if (hit.collider.gameObject == transform.gameObject)
                    continue;

                transform.position = hit.point;
                break;
            }
        }



    }



    [MenuItem("Tools/Snap Selection To Ground/FindCollider")]
    public static void SetFInd()
    {
        find = true;
    }
    [MenuItem("Tools/Snap Selection To Ground/StopFindCollider")]
    public static void StopFind()
    {
        find = false;
    }

    public static void EditorUpdate()
    {


        if (find)
        {
            SnapToGround();

        }

    }

}








