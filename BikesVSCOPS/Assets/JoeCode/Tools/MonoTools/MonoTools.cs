﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class NULL : MonoBehaviour
{
    private void Awake()
    {
        Debug.LogError("NULL");

    }
}

public class CreateGAMEOBJECTINFO
{
    public string NAME;
    public bool reset;
    public string ParentName;

    public CreateGAMEOBJECTINFO(
    string _NAME,
    string _ParentName = "",
    bool _reset = false
        )
    {
        NAME = "[ "+_NAME+" ]";
        reset = _reset;
        ParentName = "[ "+_ParentName+" ]";
    }


}

[ExecuteInEditMode]
public static class MonoTools
{
    static Text text;
    static UI_Controller uI_Controller;
    static Image image;
    public static Image FindOrCreateImageObject(CreateGAMEOBJECTINFO gAMEOBJECTINFO)
    {
        Image returnImage = GrabOrAddComponent<Image>(FindOrCreateGameObject(gAMEOBJECTINFO, image)) as Image;
        return returnImage;

    }
    public static Text FindOrCreateTextObject(CreateGAMEOBJECTINFO gAMEOBJECTINFO)
    {
        return GrabOrAddComponent<Text>(FindOrCreateGameObject(gAMEOBJECTINFO, text)) as Text;

    }
    public static UI_Controller FindOrCreateUI_Controller(CreateGAMEOBJECTINFO gAMEOBJECTINFO)
    {
        return GrabOrAddComponent<UI_Controller>(FindOrCreateGameObject(gAMEOBJECTINFO, uI_Controller)) as UI_Controller;
    }
    public static GameObject FindOrCreateGameObject(CreateGAMEOBJECTINFO gAMEOBJECTINFO, params Component[] components)
    {
        GameObject foundobject = FindGameObject(gAMEOBJECTINFO.NAME);

        if (foundobject != null)
        {
            Debug.Log("MOOOPS");
            return foundobject;
        }
        else
        {
            if (string.IsNullOrEmpty(gAMEOBJECTINFO.ParentName))
            {
                return foundobject = CreateObiectInScene(gAMEOBJECTINFO, components);
            }
            else return Create_Object_InParent(gAMEOBJECTINFO, components);


        }
    }
    public static GameObject FindGameObjectANDAddComponets(string NAME, params Component[] components)
    {
        GameObject foundobject = FindGameObject(NAME);
        AddCommponetsToGameObjects(foundobject);
        return foundobject;
    }
    public static GameObject FindGameObject(string gameobjectName)
    {
        return GameObject.Find(gameobjectName);
    }
    public static GameObject CreateObiectInScene(CreateGAMEOBJECTINFO gAMEOBJECTINFO, params Component[] components)
    {
        GameObject _createdObject = new GameObject(gAMEOBJECTINFO.NAME);
        if (_createdObject && components.Length > 0)
        {
            AddCommponetsToGameObjects(_createdObject, components);
        }
        if (gAMEOBJECTINFO.reset)
        {
            ResetObjectToZed(_createdObject);
        }
        return _createdObject;
    }
    public static GameObject Create_Object_InParent(CreateGAMEOBJECTINFO gAMEOBJECTINFO, params Component[] components)
    {
        GameObject createdObject = CreateObiectInScene(gAMEOBJECTINFO, components);
        createdObject.transform.SetParent(FindGameObject(gAMEOBJECTINFO.ParentName).transform);

        return createdObject;
    }
    public static void AddCommponetsToGameObjects(GameObject gameobject, params Component[] components)
    {
        for (int i = 0; i < components.Length; i++)
        {
            if (!gameobject.GetComponent(components.GetType()))
            {
                gameobject.AddComponent(components.GetType());
            }
        }


    }
    public static Component GrabOrAddComponent<T>(GameObject gameobject)
    {
        if (gameobject.GetComponent<T>() != null)
        {
            return gameobject.GetComponent<T>() as Component;
        }
        else
        {
            return AddCommponet<T>(gameobject) as Component;
        }

    }
    public static Component AddCommponet<T>(GameObject gameObject)
    {
        gameObject.AddComponent(typeof(T));

        return gameObject.GetComponent<T>() as Component;
    }
    public static void ResetObjectToZed(GameObject gameobject)
    {
        gameobject.transform.SetPositionAndRotation(Vector3.zero, new Quaternion(0, 0, 0, 0));
    }
}
