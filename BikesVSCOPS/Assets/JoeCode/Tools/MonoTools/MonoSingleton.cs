﻿using UnityEngine;
using JS.BVSC.BVSC_Behavior;


public abstract class MonoSingleton<T> : BVSC_BehaviorBase where T : MonoSingleton<T> 
{
    #region Public Properties

    public static T Instance
    {
        get
        {
            // Instance requiered for the first time, we look for it
            if (m_instance == null)
            {
                //Debug.LogWarning("Getting Instsance of " + typeof(T).ToString());

                m_instance = GameObject.FindObjectOfType(typeof(T)) as T;


                // Object not found, we create a temporary one
                if (m_instance == null)
                {
                    //Debug.LogWarning("No instance of " + typeof(T).ToString() + " is in the game.");

                    m_instance = GameObject.FindObjectOfType(typeof(T)) as T;

                    //                    Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                    //                    m_instance = new GameObject("Temp Instance of " + typeof(T).ToString(), typeof(T)).GetComponent<T>();
                    // 
                    //                    // Problem during the creation, this should not happen
                    //                    if( m_instance == null )
                    //                    {
                    //                        Debug.LogError("Problem during the creation of " + typeof(T).ToString());
                    //                    }
                    return null;
                }


                if (!Application.isPlaying
                    && !m_instance.AllowInstancesDuringEditMode)
                {
                    Debug.LogError("This singleton was not designed to be used when the application is not playing for type \"" + typeof(T).ToString() + "\"");
                    return null;
                }
                m_instance.Init();
            }
            return m_instance;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Public Properties

    public bool AllowInstancesDuringEditMode
    {
        get { return this.m_allowInstancesDuringEditMode; }
        set { this.m_allowInstancesDuringEditMode = value; }
    }

    public bool AllowDontDistoryOnloaded
    {
        get { return this.m_AllowDontDistoryOnloaded; }
        set { this.m_AllowDontDistoryOnloaded = value;          
        }
    }
   
    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Protected Methods

    protected abstract void Init();


    public virtual void OnEnable()
    {
        //		if(name == "~Game~")
        //		{
        //			Debug.Log("Enabled, reseting " + name + " : " + typeof(T));
        //		}

        if (m_instance == null)
        {
            m_instance = this as T;


        

            m_instance.Init();
        }
        else if (m_instance != this)
        {
            DestroyImmediate(this.gameObject);
        }

        if (GameObject.FindObjectOfType(typeof(T)) as T != m_instance)
        {
            ResetInstance();
        }
       
    }


    protected virtual void OnDestroy()
    {
        //		if(name == "~Game~")
        //		{
        //			Debug.Log("Destroyed, reseting " + name + " : " + typeof(T));
        //		}

        ResetInstance();
    }


    protected void ResetInstance()
    {
        if (m_instance != null)
        {
            //			if(m_instance.name == "~Game~")
            //			{
            //				Debug.Log("Reseting " + m_instance.name + " : " + typeof(T));
            //			}
            m_instance = null;
        }
    }

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Properties

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Methods

    //	private void Awake()
    //	{
    ////		if(name == "~Game~")
    ////		{
    ////			Debug.Log("Awake, calling Init " + name + " : " + typeof(T));
    ////		}
    //
    //	}

    #endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #region Private Members

    private static T m_instance = null;

    [Header("Instnace")]

    [SerializeField]
    private bool m_allowInstancesDuringEditMode = false;

    [SerializeField]
    private bool m_AllowDontDistoryOnloaded = false;

    #endregion


    private void Awake()
    {
        if (this.m_AllowDontDistoryOnloaded)
        {
            DontDestroyOnLoad(m_instance);
        }
    }
}