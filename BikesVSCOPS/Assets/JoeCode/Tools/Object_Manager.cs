﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;



public class Object_Manager : IInitializable
{
    public  void Initialize()
    {
        Debug.Log("INTTTTTTTTTTTT");
    }

    public  List<Racer> RacerList = new List<Racer>();
    public  List<Node> Path_NodeList = new List<Node>();
    public  List<Cop_AI> All_Cops = new List<Cop_AI>();
    public  List<CheckPoint> CheckPoints = new List<CheckPoint>();


    public void AddRacer(Racer user)
    {
        RacerList.Add(user);      
    }
    public void AddNode(Node node)
    {
        Path_NodeList.Add(node);         
    }
    public void AddCops(Cop_AI cop)
    {
        All_Cops.Add(cop);
    }
    public void AddCheckPoints(CheckPoint checkPoint)
    {
        CheckPoints.Add(checkPoint);
    }

  
    public  Node Get_Closest_Node(Transform current_transform, int id)
    {
        return DistanceComparer.Closet_Componet(current_transform,Path_NodeList, id) as Node;
    }
    public  Racer Get_Closest_Racer(Transform current_transform, int id)
    {
        return DistanceComparer.Closet_Componet(current_transform,RacerList, id) as Racer;
    }
    public  Cop_AI Get_Closest_Cop(Transform current_transform, int id)
    {
        return DistanceComparer.Closet_Componet(current_transform,All_Cops, id) as Cop_AI;
    }
}
