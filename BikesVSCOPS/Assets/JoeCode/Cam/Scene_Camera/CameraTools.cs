﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraTools  {

    public static void Istant_Move(Scene_Camera camera,Vector3 moveToPoint)
    {
        camera.scene_cam.transform.position = moveToPoint;
    }
   
    public static void Lerp_To(Scene_Camera camera,Vector3 LerpToPoint,float camSpeed)
    {

        camera.transform.position = Vector3.Lerp(camera.transform.position, LerpToPoint, camSpeed);

    }


}
