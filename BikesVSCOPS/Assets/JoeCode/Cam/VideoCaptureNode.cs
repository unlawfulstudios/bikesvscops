﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoCaptureNode : MonoBehaviour
{
    public int point_ID = 0;
    public Transform[] points;
    public PlacementType placementType;
    public CameraMovementType cameraMovementType;
    public Vector3 front_SensorPostion = new Vector3(0, 0.2f, 0.5f);
    public float front_SideSensorPostion = 0.5f;
    public float front_SensorAngle = 30f;
    [Range(0, 360)]
    public float viewAngle;
    public float viewRadius;

    public bool show_Path = false;
    public Color line_Color;
    public float sphere_Size;

    void OnDrawGizmos()
    {
        if (!show_Path)
            return;

        Gizmos.color = line_Color;


        for (int i = 0; i < points.Length; i++)
        {
            Vector3 current_Node = points[i].position;
            Vector3 previous_Node = Vector3.zero;

            if (i > 0)
            {
                previous_Node = points[i - 1].position;
                Gizmos.DrawLine(previous_Node, current_Node);

            }
            else if (i == 0 && points.Length > 0)
            {
                previous_Node = points[points.Length - 1].position;
            }
            Gizmos.DrawWireSphere(current_Node, sphere_Size);
        }
    }

    public Vector3 Dir_FromAngle(float angelsInDegrees, bool angleIsgloable)
    {
        if (!angleIsgloable)
        {
            angelsInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angelsInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angelsInDegrees * Mathf.Deg2Rad));

    }

}
