﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene_CameraManagers : Scene_CameraMangerMethouds
{
    public override void Set_SceneCam(Scene_Camera _scene_cam)
    {
        scene_cam = _scene_cam;
    }
    public override void Move_SceneCam_Isnatnt(Vector3 movetoPoint)
    {
        Move_Instant(movetoPoint);
    }
    public override void Move_SceneCam_Lerp(Vector3 StartFromPoint, Vector3 movetoPoint)
    {
        MoveCam_Lerp(movetoPoint);

    }
}
