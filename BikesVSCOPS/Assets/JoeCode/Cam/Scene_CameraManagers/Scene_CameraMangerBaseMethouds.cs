﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerObjects : GlobaleManagers
{
    public Scene_Camera scene_cam;
    public float speed = 0.05f;
}
public class Scene_CameraMangerBaseMethouds : SceneManagerObjects
{
   
    protected void Move_Instant(Vector3 movetoPoint)
    {
        CameraTools.Istant_Move(scene_cam, movetoPoint);
    }
    protected void MoveCam_Lerp(Vector3 movetoPoint)
    {
        CameraTools.Lerp_To(scene_cam, movetoPoint, speed);
    }
    protected void MoveCame_Follow() { }
    public void Cam_off()
    {
        ToggoleCamera(false);
    }
    public void Cam_On()
    {
        ToggoleCamera(true);
    }
    private void ToggoleCamera(bool toogle)
    {
        scene_cam.scene_cam.gameObject.SetActive(toogle);
    }
}
public class Scene_CameraMangerMethouds : Scene_CameraMangerBaseMethouds
{
  public virtual void Set_SceneCam(Scene_Camera scene_cam) { }
  public virtual void Move_SceneCam_Isnatnt(Vector3 movetoPoint){ }
  public virtual void Move_SceneCam_Lerp(Vector3 StartFromPoint, Vector3 movetoPoint) {}

}