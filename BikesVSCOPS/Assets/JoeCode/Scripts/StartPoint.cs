﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : RacePoints {

   public Vector3 StartPosition { get { return this.transform.position;} }
   public int StatPointID;   

    protected override void Player_RaceEvent()
    {
        return;
    }
    protected override void Ai_RaceEvent()
    {
        return;
    }
}
