﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JS.BVSC.BVSC_Behavior;

public class BikeHolder : BVSC_BehaviorBase
{
    public BikeHolderCam BikeHolderCam;
    public float roationSpeed = 0.01f;
    public float roationTime = 0.01f;
    public float rotationSmothtime = 0;
    public Transform[] BikeTransfroms;



    public bool RotateLeft = false;
    public bool RotateRight = true;


    public IEnumerator Rotate_Left()
    {
        RotateLeft = true;
        yield return new WaitForSecondsRealtime(roationTime);
        RotateLeft = false;
        StartCoroutine("Rotate_Right");

    }
    public IEnumerator Rotate_Right()
    {
        RotateRight = true;
        yield return new WaitForSecondsRealtime(roationTime);
        RotateRight = false;
        StartCoroutine("Rotate_Left");

    }
    public void StartRotate()
    {
        StartCoroutine("Rotate_Left");
    }
}
