﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UI_Manager : MonoSingleton<UI_Manager>
{

    [SerializeField] private UI_Controller[] all_UI;
    public UI_Controller CurrentUIController;
    public int currentID = 0;
    public int current_BackId = 0;
    [SerializeField] private int Start_ControllerID = 0;

    
    protected override void Init()
    {
        Application.targetFrameRate = 60;
    }
    private void Start()
    {
        Change_Controller(0);
    }
    public void goBack()
    {
        if (current_BackId > 0)
            Change_Controller(current_BackId - 1);
    }

    public void Change_Controller(int switchto_id)
    {
        current_BackId = switchto_id;

        if (all_UI.Length <= 0)
            return;

        if (all_UI[switchto_id] != null)
            CurrentUIController = all_UI[switchto_id];

        if (CurrentUIController != null)
        {
            
            CurrentUIController.Set_Active_State(true);

            for (int i = 0; i < all_UI.Length; i++)
            {
        
                if (all_UI[i] != CurrentUIController)
                {
                    all_UI[i].Set_Active_State(false);
                }
            }
        }

    }
    public void Toggle_TopPanel(bool toggle)
    {
        if (Top_Panel_Controller.Instance != null)
            Top_Panel_Controller.Instance.Toggle(toggle);
    }

  
    public void SetALLOFF_DEBUGE()
    {
        for (int i = 0; i < all_UI.Length; i++)
        {
            all_UI[i].Set_Active_State(false);

        }
    }


    public void Go_TO_Instagram()
    {
        string instagram = "url:instagram://user?username=the.msy";
        Application.OpenURL(instagram);
    }
    public void Go_TO_FaceBook()
    {

    }

    #region Main Menu Goto Points
    public void GotoCarrer()
    {
        Change_Controller(1);
    }
    public void GoBackHome()
    {
        Change_Controller(0);

    }
    public void ChoseCity()
    {

        Change_Controller(2);
    }
    public void ChoseRUN()
    {
        Change_Controller(3);
    }
    public void BackTOBikes()
    {
        Change_Controller(1);
    }
    public void BackTOCity()
    {
        Change_Controller(2);
    }
    public void GoToLoad()
    {
        Change_Controller(4);
    }
    public void GoToGarage()
    {
        Change_Controller(4);
    }
    public void Close_Garage()
    {
        GoBackHome();
    }
    public void GoTo_Settings()
    {
        Change_Controller(5);
    }
    #endregion

}
