﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Garage_Bike_HolderManager : MainMenuBehaviors
{
    public float Cam_MoveSpeed = 1;
    public bool moveLeft = false;
    public bool moveRight = false;
    public Transform statsBar;
    public Text NameText;
    public float bike_Switch_Distance = 0.001f;
    public float speed = 1;
    public float rotation_Time = 0.1f;
    public float times = 0.01f;
    public float slowtime = 100;
    public float camSpeed;
    public float minSpeed = 1;
    public float smoothTime = 1;
    public Text Price_Text;

    public GameObject PuchaseButton;
    public GameObject EquipButton;

    //public string current_Name
    //{
    //    get
    //    {
    //        return Currecnt_Viewd__Bike.attributes.BikeID.ToString();
    //    }
    //}
    //public float current_Speed { get { return Currecnt_Viewd__Bike.attributes.MaxSpeed; } }
    //public float current_Handling { get { return Currecnt_Viewd__Bike.attributes.Handling; } }
    //public float current_Acceleration { get { return Currecnt_Viewd__Bike.attributes.Acceleration; } }


    //protected override void BikeHolderIniti()
    //{
    //   // moveToTransPos = campoints[0];
    //    MoveCam(0);
    //}

    //public void Move_Right()
    //{
    //    if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value < MaxBike_Amount - 1)
    //    {
    //        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value++;
    //        Set_Bike(GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value);
    //    }

    //}
    //public void Move_Left()
    //{
    //    if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value > 0)
    //    {
    //        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value--;
    //        Set_Bike(GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value);
    //    }
    //}

    //public void Set_Bike(int id)
    //{
    //    Currecnt_Viewd__Bike = GlobaleManagers.current_UserProfile.BikeList[id];

    //    if (Currecnt_Viewd__Bike != null)
    //    {  
    //       Garage_Manager.SetBike(Currecnt_Viewd__Bike);
    //    }

    //    // restart position 
    //    currentView_BikeId = 160;
    //}



    //public override void MoveCam(int id)
    //{

    //    speed = Mathf.Lerp(speed, minSpeed, smoothTime);

    //    if (id <= GlobaleManagers.current_UserProfile.BikeList.Length)
    //    {
    //      //  moveToTransPos = campoints[id];
    //    }

    //    if (moveLeft && !Mathf.Approximately(currentView_BikeId, 280))
    //        currentView_BikeId += times * speed;


    //    if (moveRight && !Mathf.Approximately(currentView_BikeId, 70))
    //        currentView_BikeId -= times * speed;

    //    if (Garage_Manager != null)
    //    {
    //        Garage_Manager.Toggle_Lock(GlobaleManagers.current_UserProfile.BikeList[id].UNLOCKED.value, canUnlock(id), id);

    //    }

    //    BikeCam.transform.position = Vector3.Lerp(BikeCam.transform.position, moveToTransPos.position, camSpeed);

    //  //  bikeTransforms[id].localRotation = new Quaternion(0, bikeTransforms[id].localRotation.y + currentView_BikeId, 0, 180);



    //}
    IEnumerator move_Left()
    {
        moveLeft = true;
        yield return new WaitForSecondsRealtime(rotation_Time);
        moveLeft = false;
        StartCoroutine("move_Right");

    }
    IEnumerator move_Right()
    {
        moveRight = true;
        yield return new WaitForSecondsRealtime(rotation_Time);
        moveRight = false;
        StartCoroutine("move_Left");

    }


    public void StartRotat()
    {
        StartCoroutine("move_Left");
    }


}

