﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class RUN : UserProfileObject
{
    public override string Name
    {
        get
        {
            return "RUNS";
        }

    }
    public int id = 0;
    public int city_id = 0;

    public UserProfileKey<string, string> RunName = null;
    public BoolData Unlocked;
    public IntData WinAmmount;

    public UserProfileBestTime RUN_BESTTIME;

    public RUN(int _id, int _city_ID)
    {
        id = _id;
        m_index = _id;
        city_id = _city_ID;

        Unlocked = new BoolData("isUnlocked" + m_index, false);
        RunName = new UserProfileKey<string, string>("Run Name " + m_index.ToString(), "RUN_" + m_index.ToString());
        WinAmmount = new IntData("Run_" + m_index + "WinAmmount", 0);

        RUN_BESTTIME = new UserProfileBestTime(m_index);
        if (RunName.Value == "RUN_1")
        {
            Unlocked.value = true;
        }
    }

    public RUN()
    {

    }
    private int m_index;


    public override void SyncData()
    {
        Unlocked.SyncData();
        WinAmmount.SyncData();
        RUN_BESTTIME.SyncData();
        base.SyncData();

    }
    public override void Reset()
    {
        Unlocked.Reset();
        WinAmmount.Reset();
        RUN_BESTTIME.Reset();
        base.Reset();

    }


    protected override bool m_DataSynced()
    {
        return Unlocked.DataSynced && WinAmmount.DataSynced
            && RUN_BESTTIME.DataSynced ? true : false;
    }


}
