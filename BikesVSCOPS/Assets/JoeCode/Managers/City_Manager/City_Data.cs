﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class City_Data
{
    
    public int cityId;
    public string City_NAme;
    public string GetAwaySceneName;
    public string FreeRideScenName;
    public Sprite CityICON;
}
