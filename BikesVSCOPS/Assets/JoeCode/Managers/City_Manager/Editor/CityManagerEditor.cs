﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Zenject;




public class CityManager : EditorMangers, IGuiRenderable
{
    private int viewIndex = 1;
    public void GuiRender()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("City_Data_Manager Editor", EditorStyles.boldLabel);

        if (City_Data_Manager != null)
        {
            if (GUILayout.Button("Show City List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = City_Data_Manager;
            }
        }
        else
        {
            Debug.Log("NOOOOO");
        }
        if (GUILayout.Button("Open City List"))
        {
            OpenItemList();
        }
        if (GUILayout.Button("New City List"))
        {
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = City_Data_Manager;
        }
        GUILayout.EndHorizontal();

        if (City_Data_Manager == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New City List", GUILayout.ExpandWidth(false)))
            {
                CreateNewItemList();
            }
            if (GUILayout.Button("Open Existing City List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }



        if (City_Data_Manager != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex > 1)
                    viewIndex--;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex < City_Data_Manager.citys.Count)
                {
                    viewIndex++;
                }
            }

            GUILayout.Space(60);
            if (City_Data_Manager.citys.Count < 3)
            {
                if (GUILayout.Button("Add Citys", GUILayout.ExpandWidth(false)))
                {
                    AddItem();
                }
            }
            if (GUILayout.Button("Delete City", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();
            if (City_Data_Manager.citys == null)
                Debug.Log("wtf");
            if (City_Data_Manager.citys.Count > 0)
            {
                GUILayout.BeginHorizontal();
                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current City", viewIndex, GUILayout.ExpandWidth(false)), 1, City_Data_Manager.citys.Count);
                //Mathf.Clamp (viewIndex, 1, inventoryItemList.itemList.Count);
                EditorGUILayout.LabelField("of   " + City_Data_Manager.citys.Count.ToString() + "  Citys", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                City_Data_Manager.citys[viewIndex - 1].City_NAme = EditorGUILayout.TextField("City Name", City_Data_Manager.citys[viewIndex - 1].City_NAme as string);
                City_Data_Manager.citys[viewIndex - 1].FreeRideScenName = EditorGUILayout.TextField("Free Ride Scen Name", City_Data_Manager.citys[viewIndex - 1].FreeRideScenName as string);
                City_Data_Manager.citys[viewIndex - 1].GetAwaySceneName = EditorGUILayout.TextField("Get AwayScene Name", City_Data_Manager.citys[viewIndex - 1].GetAwaySceneName as string);
                UserProfile.CAREER_Citys[viewIndex - 1].CityName = new UserProfileKey<string, string>("CityName" + City_Data_Manager.citys[viewIndex - 1].City_NAme, City_Data_Manager.citys[viewIndex - 1].City_NAme);
                City_Data_Manager.citys[viewIndex - 1].CityICON = EditorGUILayout.ObjectField("Item Icon", City_Data_Manager.citys[viewIndex - 1].CityICON, typeof(Sprite), false) as Sprite;

              
            }
            else
            {
                GUILayout.Label("This Inventory List is Empty.");
            }
        }
        if (GUI.changed)
        {
            EditorUtility.SetDirty(City_Data_Manager);
        }
    }

    void CreateNewItemList()
    {
        // There is no overwrite protection here!
        // There is No "Are you sure you want to overwrite your existing object?" if it exists.
        // This should probably get a string from the user to create a new name and pass it ...
        viewIndex = 1;
        City_Data_Manager = CreatCityManager.Create();
        if (City_Data_Manager)
        {
            City_Data_Manager.citys = new List<City_Data>();
            string relPath = AssetDatabase.GetAssetPath(City_Data_Manager);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }

    void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select City Data List", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            City_Data_Manager = AssetDatabase.LoadAssetAtPath(relPath, typeof(City_Data_Manager)) as City_Data_Manager;
            if (City_Data_Manager.citys == null)
                City_Data_Manager.citys = new List<City_Data>();
            if (City_Data_Manager)
            {
                EditorPrefs.SetString("Citys_ObjectPath", relPath);
            }
        }
    }

    void AddItem()
    {
        City_Data City1 = new City_Data();

        //newItem.itemName = "New Item";
        City_Data_Manager.citys.Add(City1);


        viewIndex = City_Data_Manager.citys.Count;
    }

    void DeleteItem(int index)
    {
        City_Data_Manager.citys.RemoveAt(index);
    }

}

public class CityManagerEditor : ZenjectEditorWindow
{
   

    [MenuItem("Bikes VS Cops/City Data Manager Editor")]
    public static CityManagerEditor GetOrCreateWindow()
    {
        return CustomWindoweEditorTools.GetOrCreateWindow<CityManagerEditor>() as CityManagerEditor;
    }

    public override void InstallBindings()
    {
        CityDataManagerEditorInstaller.Install(Container);

    }
  
  
}
