﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreatCityManager : ScriptableObject {

    [MenuItem("Assets/Create/City List")]
    public static City_Data_Manager Create()
    {
        City_Data_Manager asset = ScriptableObject.CreateInstance<City_Data_Manager>();

        AssetDatabase.CreateAsset(asset, "Assets/JoeCode/Managers/City_Data_Manager.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }

}
