﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[System.Serializable]
public class City : UserProfileObject
{

    public override string Name
    {
        get
        {
            return "Citys";
        }

    }

    public UserProfileKey<string, string> CityName = null;

    public BoolData UNLOCKED = new BoolData("UNLOCKED" + id, false);
    public BoolData Complete = new BoolData("Complete" + id, false);




    public UserProfileBestTime GetAwayBestTime = new UserProfileBestTime(-1);


    public RUN[] Runs =
   {
        new RUN(1,id),
        new RUN(2,id),
        new RUN(3,id),
        new RUN(4,id),
        new RUN(5,id),
        new RUN(6,id),
        new RUN(7,id),
        new RUN(8,id)

    };

    public City(int _id)
    {
        m_id =_id;
        id = m_id;
   
        CityName = new UserProfileKey<string, string>("CityName" + City_Data_Manager.citys[id].City_NAme, City_Data_Manager.citys[id].City_NAme);
        UNLOCKED = new BoolData("UNLOCKED" + id, false);
        GetAwayBestTime = new UserProfileBestTime(id);
        if (id == 0)
        {
            UNLOCKED.value = true;
        }

    }
    public City()
    {

    }
    int m_id;

    static int id;

    public override void SyncData()
    {
        GetAwayBestTime.SyncData();
        UNLOCKED.SyncData();
        Complete.SyncData();

        for (int i = 0; i < Runs.Length; i++)
        {
            Runs[i].SyncData();
        }

        base.SyncData();
    }

    public override void Reset()
    {
        GetAwayBestTime.Reset();
        UNLOCKED.Reset();
        Complete.Reset();

        for (int i = 0; i < Runs.Length; i++)
        {
            Runs[i].Reset();
        }

        base.Reset();
    }


    private bool RunsAreSynced()
    {

        for (int i = 0; i < Runs.Length; ++i)
        {
            if (Runs[i].DataSynced == false)
            {
                return false;
            }
        }

        return true;

    }

    protected override bool m_DataSynced()
    {

        return GetAwayBestTime.DataSynced ? true : false;
        //&& UNLOCKED.DataSynced
        //  && RunsAreSynced() 

    }

}
