﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerProcess : soundManagerBase
{
    int randomIndex;


    public SoundManagerProcess(SoundManagerSettings _soundsettings) : base(_soundsettings)
    {

    }

    public override void Init()
    {
        RandomizeMusic(ProjectSoundData.All_Music);
    }
    public override void Play()
    {
        if (!soundManagerSettings.musicSource.isPlaying)
            soundManagerSettings.musicSource.Play();
    }
    public override void SetClip(AudioClip clip)
    {
        soundManagerSettings.musicSource.clip = clip;
    }
    public override void PlaySingleClip(AudioClip clip)
    {
        soundManagerSettings.musicSource.Play();
    }
    public override void Stop() { }
    public override void Sound_Update()
    {

        if (startMusic)
        {
            Play();
        }

        if (triggerUISOund && !soundManagerSettings.UI_AuidoSource.isPlaying)
        {
        
            soundManagerSettings.UI_AuidoSource.Play();
            triggerUISOund = false;
        }

    }
    public void RandomizeMusic(List<AudioClip> clips)
    {
        randomIndex = Random.Range(0, clips.Count);
        soundManagerSettings.musicSource.clip = clips[randomIndex];
    }
    public override void PlayerButtonSound()
    {
        triggerUISOund = true;
    }
    public void OnDestroy()
    {
        startMusic = false;
    }
    public virtual void Pause_Sound() { }
    public virtual void Resuem_Sound() { }
}

