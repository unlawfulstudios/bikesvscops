﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class SoundManager : SoundManagerProcess
{
    public override void Pause_Sound()
    {
        soundManagerSettings.musicSource.mute = true;

        //InGameStateManager.isPaused = true;


        //for (int i = 0; i < object_Manager.All_Cops.Count; i++)
        //{
        //    object_Manager.All_Cops[i].GetComponent<AudioSource>().enabled = false;
        //}
    }
    public override void Resuem_Sound()
    {

        //InGameStateManager.isPaused = false;
        soundManagerSettings.musicSource.mute = false;

    }
    public SoundManager(SoundManagerSettings _soundsettings) : base(_soundsettings)
    {

    }
    public virtual void UpdateSoundManager() { }
}
