﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace JS.BVSC.BVSC_Behavior
{
    public class BVSC_BehaviorInjectMethouds : BVSC_BehaviorBaseMethouds
    {
        [Inject]
        public void On_Inject(
            GlobaleManagers _GlobaleManagers)
        {
            GlobaleManagers = _GlobaleManagers;
        }
    }
}