﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MainMenu_Managers : IInitializable {

    public Garage_Manager garage_Manager;
    public MainMenuSoundManager MainMenuSoundManager;


    public void Initialize()
    {
        Debug.Log("INTITTT");
    }


    public MainMenu_Managers(
        Garage_Manager _garage_Manager,
        MainMenuSoundManager _MainMenuSoundManager
        )
    {
        garage_Manager = _garage_Manager;
        MainMenuSoundManager = _MainMenuSoundManager;
    }
    // Use this for initialization

}
