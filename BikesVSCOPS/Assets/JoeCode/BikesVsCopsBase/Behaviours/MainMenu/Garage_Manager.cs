﻿using UnityEngine.UI;
using Zenject;
using UnityEngine;

public class Garage_Manager : IInitializable, ITickable
{
    public Garage_ManagerSettings _garage_ManagerSettings;

    public Garage_Manager(Garage_ManagerSettings Garage_ManagerSettings)
    {
    
        _garage_ManagerSettings = Garage_ManagerSettings;
    }
    private bool check = false;

    private int ID {
        get
        {
            return UserProfile.Instnace.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value;
        }
        set
        {
            UserProfile.Instnace.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value = value;
        }
    }
    public void Initialize()
    {
       
        _garage_ManagerSettings.BikeImage.gameObject.SetActive(true);
        UserProfile.Instnace.BikeList[0].UNLOCKED.value = true;
        UserProfile.Instnace.bikesVSCopsProfileInfo.Current_Bike =
        UserProfile.Instnace.BikeList[_garage_ManagerSettings.GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_iD.Value];
        UserProfile.Instnace.bikesVSCopsProfileInfo.current_City =
        UserProfile.Instnace.CAREER_Citys[0];
        UserProfile.Instnace.bikesVSCopsProfileInfo.current_Run = _garage_ManagerSettings.GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_City.Runs[0];
       // _garage_ManagerSettings.UlockedText.SetActive(false);

    }

   public void Tick()
    {
       if(check)
        {
           
            check = false;
        }

    }


    public void Toggle_Lock(bool toggle, bool pre_Bike, int id)
    {
        _garage_ManagerSettings.LockImage.gameObject.SetActive(!toggle);
        _garage_ManagerSettings.EquippedButton.SetActive(toggle);
     

        if (pre_Bike)
        {
            if (UserProfile.Instnace.BikeList[id].UNLOCKED.value)
            {
                _garage_ManagerSettings.buyButton.SetActive(false);
            }
            else
            {
                _garage_ManagerSettings.buyButton.SetActive(true);
            }

           // _garage_ManagerSettings.UlockedText.SetActive(false);

        }
        else
        {
            _garage_ManagerSettings.buyButton.SetActive(false);

            if (id == 0)
            {
               //_garage_ManagerSettings.UlockedText.SetActive(false);
            }
            else
            {
               // _garage_ManagerSettings.UlockedText.SetActive(true);
            }
        }

    }


    public bool canUnlock(int id)
    {
        if (id > 0)
        {
            return UserProfile.Instnace.BikeList[id - 1].UNLOCKED.value;
        }
        else
        {
            return false;
        }
    }

    public void SetBike()
    {
        Toggle_Lock(UserProfile.Instnace.BikeList[ID].UNLOCKED.value, canUnlock(ID), ID);
        Set_BikeStats(UserProfile.Instnace.BikeList[ID].attributes);
    }
    public void Set_BikeStats(BikeAttributes bikeAttributes)
    {
        _garage_ManagerSettings.BikeName_text.text = "BIKE " + bikeAttributes.BikeID.ToString();
        _garage_ManagerSettings.BikeHolderStatBar.bars[0].fillAmount = bikeAttributes.MaxSpeed / 400;
        _garage_ManagerSettings.BikeHolderStatBar.bars[1].fillAmount = bikeAttributes.Acceleration / 300;
        _garage_ManagerSettings.BikeHolderStatBar.bars[2].fillAmount = bikeAttributes.Handling / 200;
        _garage_ManagerSettings.Price_Text.text = "BUY FOR $" + bikeAttributes.purchase_Price;
    }
    


}
