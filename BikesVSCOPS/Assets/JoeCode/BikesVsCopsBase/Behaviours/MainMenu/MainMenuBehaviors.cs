﻿using Zenject;
using JS.BVSC.BVSC_Behavior;




public class MainMenuBehavioursObjects : BehaviorsProcesses
{
    public MainMenu_Managers MainMenuManagers;

}
public class MainMenuInjects : MainMenuBehavioursObjects
{
    [Inject]
    public void Construct(MainMenu_Managers _MainMenuManagers)
    {
        MainMenuManagers = _MainMenuManagers;
    }
}
public class MainMenuBehaviors: MainMenuInjects
{
    


}