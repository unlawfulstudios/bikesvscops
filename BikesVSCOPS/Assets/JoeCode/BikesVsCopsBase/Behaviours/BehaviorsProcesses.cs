﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS.BVSC.BVSC_Behavior
{
    public class BehaviorsProcesses : BehaviorsBaseMethods, IMethoudProcesseable
    {
        public override void Process_Awake()
        {
            On_Awake();
        }
        public override void Process_OnEnable()
        {
            On_Enable();
        }
        public override void Process_Start()
        {
            On_Start();
        }
        public override void Process_Update()
        {
            On_Update();
        }
        public override void Process_FixedUpdate()
        {
            On_FixedUpdate();
        }
        public override void Process_OnDestroy()
        {
            On_Destroy();
        }
        public override void Process_OnDisable()
        {
            On_Disable();
        }
#if UNITY_EDITOR
        public override void Process_OnReset()
        {
            On_Reset();
        }
#endif

    }
}