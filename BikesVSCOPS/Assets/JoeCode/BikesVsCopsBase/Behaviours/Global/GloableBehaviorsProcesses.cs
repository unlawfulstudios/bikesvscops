﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JS.BVSC.BVSC_Behavior.FireBase;
using JS.BVSC.BVSC_Behavior;

public class GloableBehaviorsProcesses : FireBase_Processes, IMethoudProcesseable
{

    public override void Process_OnEnable()
    {
        On_Enable();
    }
    public override void Process_Start()
    {
        On_Start();
    }
    public override void Process_Update()
    {
        On_Update();
    }
    public override void Process_FixedUpdate()
    {
        On_FixedUpdate();
    }
    public override void Process_OnDestroy()
    {
        On_Destroy();
    }
    public override void Process_ConnectToFireBase()
    {
        On_ConnectToFireBase();
    }

    public override void Process_ConnectedUpdate()
    {
        On_ConnectedUpdate();
    }

    public override void Process_DisConnectFromFireBase()
    {
        On_DisConnectFromFireBase();
    }

    public override void Process_OnDisable()
    {
        OnDisable();
    }
#if UNITY_EDITOR
    public override void Process_OnReset()
    {
        On_Reset();
    }
#endif

}