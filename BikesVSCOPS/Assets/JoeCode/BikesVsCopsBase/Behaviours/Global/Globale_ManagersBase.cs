﻿using UnityEngine;
using Zenject;

public class Globale_Managers_ObjectMethouds 
{
    protected virtual void ProcessSetUserProfile() { }

}
public class Managers_Object : Globale_Managers_ObjectMethouds
{
    public AppManager AppManager;
    public UserProfile current_UserProfile
    {
        get
        {
            if (m_current_UserProfile == null)
            {
                m_current_UserProfile = UserProfile.Instnace;
                return m_current_UserProfile;
            }
            else
            {
                return m_current_UserProfile;
            }
        }


    }
    private UserProfile m_current_UserProfile;
    public Bike Current_SelectedBike;
    public BikeInventoryItemList BikeInventoryItemList;
    public EventManager EventManager;
    public Scene_Manager scene_Manager;
    public Loading_Manager loading_Manager;
    public Game_Manager Game_Manager;
    public City_Data_Manager City_Data_Manager;
    public BikeHolder_CameraManager BikeHolder_CameraManager;
    public PopUpManagar PopUpManagar;
    public AuthManager authManager;
    public UserProfile_Manager UserProfile_Manager;

}
public class ManagersInjects : Managers_Object
{
    [Inject]
    public void On_Inject(
    UserProfile_Manager  _UserProfile_Manager,
     AuthManager _AuthManager,
     AppManager _AppManager,
     EventManager _EventManager,
     BikeInventoryItemList _BikeInventoryItemList,
     Scene_Manager _scene_Manager,
     Loading_Manager _Loading_Manager,
     Game_Manager _Game_Manager,
     City_Data_Manager _City_Data_Manager,
     BikeHolder_CameraManager _BikeHolder_CameraManager,
     PopUpManagar _PopUpManagar
     )
    {
        AppManager = _AppManager;
        EventManager = _EventManager;
        BikeInventoryItemList = _BikeInventoryItemList;
        scene_Manager = _scene_Manager;
        loading_Manager = _Loading_Manager;
        BikeHolder_CameraManager = _BikeHolder_CameraManager;
        Game_Manager = _Game_Manager;
        City_Data_Manager = _City_Data_Manager;
        PopUpManagar = _PopUpManagar;
        authManager = _AuthManager;
        UserProfile_Manager = _UserProfile_Manager;


    }
}
public class Globale_ManagersBase : ManagersInjects
{
    protected override void ProcessSetUserProfile()
    {
        Current_SelectedBike = current_UserProfile.BikeList[current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_iD.Value];
        current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value = current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_iD.Value;

    }

}