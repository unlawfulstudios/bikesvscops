﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;

namespace JS.BVSC.BVSC_Behavior.FireBase
{
    public class FireBaseBehaviors : FireBase_Processes
    {
        public IEnumerator Start()
        {
            yield return new WaitUntil(() => FirebaseDatabase.DefaultInstance != null);

            Process_ConnectToFireBase();
        }

    }
}
