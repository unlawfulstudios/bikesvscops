﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JS.BVSC.BVSC_Behavior.FireBase
{
    public class FireBase_Processes : FireBase_BaseMethods, IFireBaseProcesseable
    {
      
        public virtual void Process_ConnectToFireBase()
        {
            On_ConnectToFireBase();
        }

        public virtual void Process_ConnectedUpdate()
        {
            On_ConnectedUpdate();
        }

        public virtual void Process_DisConnectFromFireBase()
        {
            On_DisConnectFromFireBase();
        }


    }
}
