﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS.BVSC.BVSC_Behavior.FireBase
{

    public class FireBase_BaseMethods : MainMenuBehaviors, IFireBaseConnectable
    {
        public void On_ConnectToFireBase() { }
        public void On_ConnectedUpdate() { }
        public void On_DisConnectFromFireBase() { }
    }
}
