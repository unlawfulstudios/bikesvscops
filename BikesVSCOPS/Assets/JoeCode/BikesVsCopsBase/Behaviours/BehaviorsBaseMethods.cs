﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JS.BVSC.BVSC_Behavior;

public class BehaviorsBaseMethods : BVSC_BehaviorBase, IBVSCBehavior
{
    public virtual void On_Awake() { }
    public virtual void On_Enable() { }
    public virtual void On_Start() { }
    public virtual void On_Update() { }
    public virtual void On_LateUpdate() { }
    public virtual void On_FixedUpdate() { }
    public virtual void On_Destroy() { }
    public virtual void On_Disable() { }
# if UNITY_EDITOR
    public virtual void On_Reset()
    {
        
    }
#endif
}
