﻿using UnityEngine;

namespace JS.BVSC.BVSC_Behavior
{
    public class BVSC_BehaviorBase : BVSC_BehaviorInjectMethouds
    {
        public void OnEnable()
        {
            Process_OnEnable();
            if (donotDestroyOnLoad)
                DontDestroyOnLoad(this);

            ProcessSetUserProfile(GlobaleManagers.current_UserProfile);
        }

        public void Awake()
        {
            Process_Awake();
        }

        public void Start()
        {
            Process_Start();
        }

        public void Update()
        {
            Process_Update();
        }

        public void LateUpdate()
        {
            Process_LateUpdate();
        }

        public void FixedUpdate()
        {
            Process_FixedUpdate();
        }

        public void OnDestroy()
        {
            Process_OnDestroy();
        }

        public void OnDisable()
        {
            Process_OnDisable();

        }

#if UNITY_EDITOR
        public void Reset()
        {
            Process_OnReset();
        }




#endif
        public BVSC_BehaviorBase() { }
    }
}
