﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JS.BVSC.BVSC_Behavior
{
    public  class BVSC_BehaviorBaseMethoudProcesses : BVSC_BehaviorData
    {
        protected virtual void ProcessSetUserProfile(UserProfile m_UserProfile) { }

        public virtual void Process_OnEnable() { }
        public virtual void Process_Awake() { }
        public virtual void Process_Start() { }
        public virtual void Process_Update() { }
        public virtual void Process_LateUpdate() { }
        public virtual void Process_FixedUpdate() { }
        public virtual void Process_OnDestroy() { }
        public virtual void Process_OnDisable() { }
#if UNITY_EDITOR
        public virtual void Process_OnReset(){}
#endif

    }
}


