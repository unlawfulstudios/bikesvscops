﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHubController  {

    public PlayerHub playerHub;
    public PlayerHubController(PlayerHub _playerHub)
    {
        playerHub = _playerHub;

    }
    public void Toggle_Pause()
    {
        playerHub.PlayerUiHubController.Set_Active_State(!playerHub.PauseController.Active);
    }
    public void Toggle_PlayerHub(bool toggle)
    {
        playerHub.hubobject.gameObject.SetActive(toggle);
    }
}
