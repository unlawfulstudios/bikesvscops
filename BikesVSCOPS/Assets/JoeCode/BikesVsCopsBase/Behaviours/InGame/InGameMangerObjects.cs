﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMangerObjects : Globale_ManagersBase
{
    public PlayerHubController hubController;
    public Object_Manager object_Manager;
    public InGameEventManager inGameEventManager;
    public Racer_Manager Racer_Manager;
    public Results_Manager Results_Manager;
    public InGameStateManager InGameStateManager;
    public Run_Timer Run_Timer;
    public IngameSoundManager ingameSoundManager;
    public RaceIntroCameraManager race_Intro_Manager;



}
