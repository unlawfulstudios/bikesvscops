﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;



public class RaceIntroCameraManager : GlobaleManagers, IInitializable
{
    public RaceIntro_CamerManagerSettings CamerManagerSettings;
    public AsyncProcessor _asyncProcessor;


    public RaceIntroCameraManager(
        RaceIntro_CamerManagerSettings _CamerManagerSettings,
            AsyncProcessor asyncProcessor)
    {
        CamerManagerSettings = _CamerManagerSettings;
        _asyncProcessor = asyncProcessor;
    }
    public void Initialize()
    {
        PlaceCam(1);
    }

    public void MoveCam(RaceCams racecam)
    {

        CamerManagerSettings.raceIntroCam.transform.position = Vector3.Lerp(CamerManagerSettings.raceIntroCam.transform.position, racecam.End_Point.position, Time.deltaTime * CamerManagerSettings.transition_Speed);
        CamerManagerSettings.raceIntroCam.transform.eulerAngles = racecam.start_Point.eulerAngles;

    }
    public void PlaceCam(int camID)
    {
        CamerManagerSettings.raceIntroCam.transform.position = CamerManagerSettings.Race_Cams[camID - 1].start_Point.position;
        CamerManagerSettings.raceIntroCam.transform.rotation = CamerManagerSettings.Race_Cams[camID - 1].start_Point.rotation;
    }
    public void ActiveCam(int camID)
    {
        if (CamerManagerSettings.Race_Cams[camID] != null)
            MoveCam(CamerManagerSettings.Race_Cams[camID]);
       
    }

    public void TurnOFFALL()
    {
        CamerManagerSettings.raceIntroCam.gameObject.SetActive(false);
    }

  
    public bool Dofade = false;
  
    IEnumerator fades()
    {
        yield return new WaitForSeconds(CamerManagerSettings.fadePauseTime);
        CamerManagerSettings.fade = false;
        _asyncProcessor.StopCoroutine(fades());


    }
  

    public void FadeIN()
    {
        CamerManagerSettings.FadeImage.CrossFadeAlpha(1.0f, CamerManagerSettings.fadeTime , false);
    }
    public void FadeOut()
    {
        CamerManagerSettings.FadeImage.CrossFadeAlpha(0.0f, CamerManagerSettings.fadeTime , false);
     
    }
     

    public int fadecount = 0;
    public void RunFadeTimer()
    {
        var delta = Time.deltaTime / CamerManagerSettings._fadeCountTimeS;
       

        CamerManagerSettings.fade_Count = Mathf.RoundToInt(CamerManagerSettings._fadetotalTime);

        CamerManagerSettings._fadecurrentTime += delta;

        if (CamerManagerSettings._fadecurrentTime >= 1)
        {
            if (CamerManagerSettings._fadetotalTime - 1 <= 0)
            {
                CamerManagerSettings._fadetotalTime = 0;
            }
            else
            {
                CamerManagerSettings._fadetotalTime -= 1;
                CamerManagerSettings._fadecurrentTime = 0;
            }


        }

        if (CamerManagerSettings._fadecurrentTime.Equals(0.0f) && !CamerManagerSettings.fade)
        {
            CamerManagerSettings.fade = true;
            _asyncProcessor.StartCoroutine(fades());
            fadecount += 1;
            Debug.Log("Fade Cont :" + fadecount);
        }


        if (CamerManagerSettings._fadecurrentTime > 0.1f)
        {
            if (CamerManagerSettings.fade)
            {
                FadeIN();

            }
            else
            {
                FadeOut();
            }
        }


    }


}
