﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceCams : MonoBehaviour
{

    public int CamID = 0;



    public Color line_Color;


    public Transform start_Point;
    public Transform End_Point;

    public float sphere_Size = 0.3f;

    
    void OnDrawGizmos()
    {
        Gizmos.color = line_Color;
        if (start_Point != null)
        {
            Gizmos.DrawSphere(start_Point.transform.position, sphere_Size);
        }
        if (End_Point != null)
        {
            Gizmos.DrawSphere(End_Point.transform.position, sphere_Size);
        }
        if (End_Point != null && start_Point != null)
        {
            Gizmos.DrawLine(start_Point.transform.position, End_Point.transform.position);
        }

    }

}
