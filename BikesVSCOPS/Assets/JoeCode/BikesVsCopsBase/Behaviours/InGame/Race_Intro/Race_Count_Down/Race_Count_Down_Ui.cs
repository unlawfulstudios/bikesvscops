﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Race_Count_Down_Ui : InGameBehaviors {

    public PlayerHub playerHub;
    public Text CountDown_Text;
    public Image CountDown_CurrentImage;
    public Image CountDown_BackGroundImage;
    public GameObject CountDown_Text_Obejct;


#if UNITY_EDITOR
    private CreateGAMEOBJECTINFO countDown_Text_Info;
    private CreateGAMEOBJECTINFO countDown_BackGround_Info;
    private CreateGAMEOBJECTINFO countDown_CurrentImage_Info;

    public override void On_Reset()
    {
        countDown_Text_Info = new CreateGAMEOBJECTINFO("Ready Count Text", "Ready Count BackGround");
        CountDown_Text = MonoTools.FindOrCreateTextObject(countDown_Text_Info);
        CountDown_Text_Obejct = CountDown_Text.gameObject;

    }
#endif


}
