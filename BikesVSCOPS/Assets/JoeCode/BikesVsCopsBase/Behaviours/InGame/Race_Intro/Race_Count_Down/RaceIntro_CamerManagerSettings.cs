﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceIntro_CamerManagerSettings : InGameBehaviors {

    public RaceCams[] Race_Cams;
    public Camera raceIntroCam;
    public Race_Count_Down_Ui race_Count_Down_Ui;

    public float transition_Speed = 0.1f;



    [Header("Fade Info")]
    public Image FadeImage;
    public float fadeDuration = 0.5f;
    public float _fadetotalTime = 6.0f;
    public float _fadecurrentTime = 0;
    public float _fadeCountTimeS = 1f;
    public bool fade = false;
    public int fade_Count = 3;
    public float fadeTime = 1f;
    public float fadePauseTime = 1.1f;
    


#if UNITY_EDITOR

    private CreateGAMEOBJECTINFO faedImageinfo;
    private CreateGAMEOBJECTINFO hubobjectInfo;
    public override void On_Reset()
    {
        var Name = GameObjectName("PLAYER HUB");
        hubobjectInfo = new CreateGAMEOBJECTINFO("HUB Object", Name);

        faedImageinfo = new CreateGAMEOBJECTINFO("Fade Image", hubobjectInfo.NAME);

        Race_Cams = Resources.FindObjectsOfTypeAll<RaceCams>();
        raceIntroCam = GameObject.Find("[ RACE INTRO CAM ]").GetComponent<Camera>();
        FadeImage = MonoTools.FindOrCreateImageObject(faedImageinfo);
    }
#endif

}
