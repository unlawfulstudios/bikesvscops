﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_BikeController : Bike_Controller {

    public float BoostFillAmount = 1;
    public float boosttime = 2f;
    public GameObject wind;
    public UI_NitroButton nitroButton;
    public GameObject playerCanvuas;
    public Vector3 windRotation = new Vector3(0, -180, 0);
    public Transform CamObject;
    public Canvas crash_Canvas;
    public Camera Main;
    public GameObject RUNText;
    public Camera switchCam;
    public bool busted = false;

    public GameController_Type controlMode()
    {
        return InGameManagers.Game_Manager.current_Controller_Type;
    }
    private int Boostamount()
    {
        return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value;
    }
    public bool BoostLeft()
    {
        var boost = Boostamount() >= 0 ? true : false;
        return boost;

    }
    protected override void Init()
    {
        crash_Canvas.gameObject.SetActive(false);
        wind.SetActive(false);
    }

    protected override void On_Crash()
    {
        SetCam(false);
        playerCanvuas.SetActive(false);
        Time.timeScale = 0.4f;
        RUNText.SetActive(false);

        if (busted)
            Run_Timer.timeGO = false;
    }
    public void SetCam(bool toggle)
    {

        Main.gameObject.SetActive(toggle);
        switchCam.gameObject.SetActive(!toggle);

        if (!toggle)
        {
            switchCam.transform.SetParent(null);
            crash_Canvas.gameObject.SetActive(true);
        }
        else
        {
            switchCam.transform.SetParent(transform);
            crash_Canvas.gameObject.SetActive(false);
            switchCam.transform.position = CamObject.position;
            IsCrashed = false;
        }
    }

    private IEnumerator CrashTimer(bool busted)
    {
        yield return new WaitForSeconds(CrashTime);
        Time.timeScale = 1;
        Set_end(busted);
        StopCoroutine("Crash");
    }

    public void Set_end(bool busted)
    {
        if (busted)
            End();
        else
            SetCam(true);

        playerCanvuas.SetActive(true);
    }

    private void End()
    {
        SetCam(true);
        GlobaleManagers.EventManager.Trigger_Event("PlayerBusted");
    }

    protected override void HandelBikeMotor()
    {
        base.HandelBikeMotor();

        if (InGameManagers.Game_Manager.current_Controller_Type == GameController_Type.pc_KeyBoard)
        {
            if (Input.GetKeyDown(KeyCode.W)) InGameEventManager.Button_Pressed = true;
        }

        
        if (switchCam != null && switchCam.gameObject.activeInHierarchy)
            switchCam.transform.LookAt(bikeSetting.MainBody);


        if (BOOST)
            BoostFillAmount -= Time.deltaTime * 0.2f;
        else
            BoostFillAmount = 1;

    }

    protected override void ProcessControlls()
    {
        if (controlMode() == GameController_Type.pc_KeyBoard)
        {
            accel = 0.0f;
            brake = false;

            if (!IsCrashed)
            {
                steer = Mathf.MoveTowards(steer, Input.GetAxis("Horizontal"), 0.1f);
                accel = Input.GetAxis("Vertical");
                brake = Input.GetButton("Jump");
            }
            else
            {
                steer = 0;
            }
        }
    }

    public void Boost()
    {
        if (!BOOST)
        {
            BOOST = true;
            wind.SetActive(true);
            wind.transform.eulerAngles = windRotation;
            StartCoroutine("BoostTime");
        }
    }

    public IEnumerator BoostTime()
    {
        yield return new WaitForSeconds(boosttime);

        BOOST = false;
        wind.SetActive(false);
        if (Boostamount() >= 0) nitroButton.interactable = true;

        StopCoroutine("BoostTime");
    }





#if UNITY_EDITOR
    public override void On_Reset()
    {
        base.On_Reset();

        playerCanvuas = FindObjectOfType<PlayerHub>().gameObject;
        nitroButton = FindObjectOfType<UI_NitroButton>();
        wind = CommponetTransfrom.GetChild(4).gameObject;
        RUNText = GameObject.Find("[ Count Down Text ]");
        
        CamObject = transform.GetChild(4);
        crash_Canvas = GameObject.FindGameObjectWithTag("CrashCanvaus").GetComponent<Canvas>();
        Main = Camera.main;
        switchCam = transform.GetChild(0).GetComponent<Camera>();
    }
#endif
}
