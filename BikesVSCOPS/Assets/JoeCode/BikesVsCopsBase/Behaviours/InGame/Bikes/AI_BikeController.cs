﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_BikeController : Bike_Controller {
    
    protected override void Init()
    {
       
    }

    protected override void On_Crash()
    {
     
    }

#if UNITY_EDITOR

    public int bikeID = 0;
    public override void On_Reset()
    {
        base.On_Reset();
        bikeSetting.bikeAttributes = UserProfile.Instnace.BikeList[bikeID].attributes;

    }
#endif
}
