﻿using Zenject;

public class InGameMangersInject : InGameMangerObjects
{


    [Inject]
    public void Construct(
     PlayerHubController _hubController,
     Object_Manager _object_Manager,
     RaceIntroCameraManager _race_Intro_Manager,
     InGameEventManager _inGameEventManager,
     Racer_Manager _Racer_Manager,
     Results_Manager _Results_Manager,
     InGameStateManager _InGameStateManager,
     Run_Timer _Run_Timer,
     IngameSoundManager _ingameSoundManager )
    {
        hubController = _hubController;
        race_Intro_Manager = _race_Intro_Manager;
        object_Manager = _object_Manager;
        inGameEventManager = _inGameEventManager;
        Racer_Manager = _Racer_Manager;
        Results_Manager = _Results_Manager;
        InGameStateManager = _InGameStateManager;
        Run_Timer = _Run_Timer;
        ingameSoundManager = _ingameSoundManager;
    }



}