﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class PlayerHub : InGameBehaviors
{
    public Text speedText;
    public float player_Speed
    {
        get
        {
            if (InGameManagers.Racer_Manager.Player_BikeControl != null)
                return Mathf.RoundToInt(InGameManagers.Racer_Manager.Player_BikeControl.speed);
            else return 0;
        }
    }
    public GameObject hubobject;
    public UI_Controller PlayerUiHubController;
    public UI_Controller PauseController;
    public UI_Controller CountDownController;
    public UI_Controller ResultsManagerController;


      
    public override void On_FixedUpdate()
    {

        if (PlayerUiHubController.Active)
        {
            speedText.text = player_Speed.ToString();
        }
        else
        {
            speedText.text = "0";
            return;
        }
    }




#if UNITY_EDITOR

    [Header("Construction")]
    private CreateGAMEOBJECTINFO hubobjectInfo;
    private  CreateGAMEOBJECTINFO speedTextObjectInfo;
    private  CreateGAMEOBJECTINFO PlayerHubControllerInfo;
    private  CreateGAMEOBJECTINFO PauseUIControllerInfor;
    private  CreateGAMEOBJECTINFO CountDownUIControllerinfo;
    private  CreateGAMEOBJECTINFO ResultsManagerUIControllerinfo;


    public override void On_Reset()
    {
      
        var Name = GameObjectName("PLAYER HUB");
        this.gameObject.name = Name;
        hubobjectInfo = new CreateGAMEOBJECTINFO("HUB Object", Name);
        CountDownUIControllerinfo = new CreateGAMEOBJECTINFO("CountDownController", hubobjectInfo.NAME);
        speedTextObjectInfo = new CreateGAMEOBJECTINFO("Player Speed Text", Name);
        PlayerHubControllerInfo = new CreateGAMEOBJECTINFO("Player Hub Controller", hubobjectInfo.NAME);
        PauseUIControllerInfor = new CreateGAMEOBJECTINFO("Pause Menu", hubobjectInfo.NAME);
        ResultsManagerUIControllerinfo = new CreateGAMEOBJECTINFO("Pause Menu", hubobjectInfo.NAME);


        CountDownController = MonoTools.FindOrCreateUI_Controller(CountDownUIControllerinfo);
        PlayerUiHubController = MonoTools.FindOrCreateUI_Controller(PlayerHubControllerInfo);
        PauseController = MonoTools.FindOrCreateUI_Controller(PauseUIControllerInfor);
        hubobject = MonoTools.FindOrCreateGameObject(hubobjectInfo);
        speedText = MonoTools.FindOrCreateTextObject(speedTextObjectInfo);
        ResultsManagerController = MonoTools.FindOrCreateUI_Controller(ResultsManagerUIControllerinfo);
 
    }
#endif

}
