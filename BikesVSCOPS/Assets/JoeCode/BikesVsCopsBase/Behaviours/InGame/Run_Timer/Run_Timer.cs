﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Run_Timer : InGameManagers , IInitializable,ITickable
{

    public Run_Timer_Settings Run_Timer_Settings;
    

    public static bool timeGO = false;
    public bool active_Touch = false;

    public int count;
    public PlayerHubController playerHubController;

    public Run_Timer(
        Run_Timer_Settings _Run_Timer_Settings,
        PlayerHubController _playerHubController
        )
    {
        playerHubController = _playerHubController;
        Run_Timer_Settings = _Run_Timer_Settings;
    }

    public bool is_GetAway
    {
        get
        {
            return Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE ? true : false;
        }
    }


    public void Initialize()
    {
   
        Run_Timer_Settings.current_GameTime.ReSet_timer(Run_Timer_Settings.Race_TimePanles);
        Time.timeScale = 1;
        timeGO = true;
        ingameSoundManager.Pause_Sound();
    }


    public void Tick()
    {
        if (InGameEventManager.current_Run_EventState == Run_Event_States.inactive)
        {
            InActive_Count();
            SetActive();
            Reset_Timer();
        }
        if (InGameEventManager.current_Run_EventState == Run_Event_States.Ready)
        {
            Racer_Manager.Player_BikeControl.SetCam(true);
            Ready_Count();
            Reset_Timer();
        }
        if (InGameEventManager.current_Run_EventState == Run_Event_States.Start)
        {
            //soundManager.Instance.Play();

            if (timeGO && InGameEventManager.Active)
                run_Timer();
          

        }
        else if (InGameEventManager.current_Run_EventState == Run_Event_States.End)
        {
            ToggoleUI(false);
        }
    }

    public void SetActive()
    {
        if (active_Touch == false)
        {
            active_Touch = true;

        }
    }

    private void InActive_Count()
    {

        Run_Timer_Settings.inActive_Timer_COunt -= Time.deltaTime;

        var countid = (int)Run_Timer_Settings.inActive_Timer_COunt;

        race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text_Obejct.SetActive(false);


        ToggoleUI(false);

        if (Run_Timer_Settings.inActive_Timer_COunt < 0 && active_Touch)
        {
            Run_Timer_Settings.inActive_Timer_COunt = 0;

            EventManager.Trigger_Event("Ready");

            return;
        }

    }

 
  

    private void DegreaseTime()
    {
        float delta = Time.deltaTime / Run_Timer_Settings._CountTime;

        Run_Timer_Settings._currentTime += delta;


        count = Mathf.RoundToInt(Run_Timer_Settings._totalTime);


        if (Run_Timer_Settings._currentTime >= 1)
        {

            if (Run_Timer_Settings._totalTime - 1 <= 0)
            {
                Run_Timer_Settings._totalTime = 0;
            }
            else
            {
                Run_Timer_Settings._totalTime -= 1;
                Run_Timer_Settings._currentTime = 0;

            }


            if (Run_Timer_Settings._currentTime.Equals(0.0f))
            {
                race_Intro_Manager.PlaceCam(count);
            }

        }

        SetNumBackGround(count);

        if (count != 0)
        {
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text.text = ""+count;
            race_Intro_Manager.ActiveCam(count);
        }

        if (count <= 0)
        {
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_BackGroundImage.enabled = true;
            race_Intro_Manager.CamerManagerSettings.raceIntroCam.gameObject.SetActive(false);     
            ingameSoundManager.toggle_Sounds(true);
            Run_Timer_Settings.StartCoroutine(set());
            ingameSoundManager.Resuem_Sound();
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text_Obejct.SetActive(false);
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.gameObject.SetActive(false);
            fadout();
            SoundManager.startMusic = true;

        }
        Debug.Log(count);

    }

    IEnumerator set()
    {
 
        yield return new WaitForSeconds(1f);
      
        InGameEventManager.current_Run_EventState = Run_Event_States.Start;

    }
    public void fadout()
    {
        ToggoleUI(true);
    }

    private void Ready_Count()
    {
        race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text_Obejct.SetActive(true);
        race_Intro_Manager.CamerManagerSettings.raceIntroCam.gameObject.SetActive(true);
        Run_Timer_Settings.ready_Timer_Count -= Time.deltaTime / Run_Timer_Settings.RedayCountOfSet;
        Run_Timer_Settings.ready_Timer_Display = Run_Timer_Settings.ready_Timer_Count.ToString();
        race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.enabled = true;

        if (race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text != null)
        {
            DegreaseTime();
            race_Intro_Manager.RunFadeTimer();

        }

        if (Run_Timer_Settings.ready_Timer_Count <= 0)
        {
            Run_Timer_Settings.ready_Timer_Count = 0;
            EventManager.Trigger_Event("Start");
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_Text_Obejct.SetActive(false);
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.enabled = false;
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_BackGroundImage.enabled = false;
            race_Intro_Manager.CamerManagerSettings.raceIntroCam.gameObject.SetActive(false);
            ingameSoundManager.Resuem_Sound();
            
        
        }

    }

    public void UpdateRace_Postion(Racer racer)
    {
        if (is_GetAway)
            return;
        EndPoint.SetPositon(racer);
    }


    public void run_Timer()
    {
        Run_Timer_Settings.current_GameTime.Run_timer(Run_Timer_Settings.Race_TimePanles);
        Run_Timer_Settings.Race_TimePanles.Set_Time_Panel(Result_Type.invalid, GameMode_Type.CAREER_MODE);      
    }
 
    public void Reset_Timer()
    {

        Run_Timer_Settings.current_GameTime.ReSet_timer(Run_Timer_Settings.Race_TimePanles);
    }

    void SetNumBackGround(int SetID)
    {

        if (SetID == 3)
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.color = Color.red;
        if (SetID == 2)
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.color = Color.yellow;
        if (SetID == 1)
            race_Intro_Manager.CamerManagerSettings.race_Count_Down_Ui.CountDown_CurrentImage.color = Color.green;

    }

    public void ToggoleUI(bool toggole)
    {
        playerHubController.Toggle_PlayerHub(toggole);
    }

    IEnumerator END_RUN()
    {

        yield return new WaitForSeconds(2);
        InGameEventManager.current_Run_EventState = Run_Event_States.inactive;

    }


}
