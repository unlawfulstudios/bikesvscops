﻿namespace JS.BVSC.BVSC_Behavior.FireBase
{
    public interface IFireBaseConnectable
    {
        void On_ConnectToFireBase();
        void On_ConnectedUpdate();
        void On_DisConnectFromFireBase();
    }

}