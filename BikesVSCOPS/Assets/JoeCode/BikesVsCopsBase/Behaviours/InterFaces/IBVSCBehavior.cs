﻿namespace JS.BVSC.BVSC_Behavior
{
    public interface IBVSCBehavior
    {
        void On_Awake();
        void On_Enable();
        void On_Start();
        void On_Update();
        void On_LateUpdate();
        void On_FixedUpdate();
        void On_Destroy();
#if UNITY_EDITOR
        void On_Reset();
#endif

    }
}
