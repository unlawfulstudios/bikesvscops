﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JS.BVSC.BVSC_Behavior
{
    public interface IMethoudProcesseable
    {
        void Process_OnEnable();
        void Process_OnDisable();
        void Process_Awake();
        void Process_Start();
        void Process_Update();
        void Process_LateUpdate();
        void Process_FixedUpdate();
        void Process_OnDestroy();
#if UNITY_EDITOR
        void Process_OnReset();
#endif
    }
}
