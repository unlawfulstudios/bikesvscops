﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS.BVSC.BVSC_Behavior.FireBase
{
    public interface IFireBaseProcesseable
    {
        void Process_ConnectToFireBase();
        void Process_ConnectedUpdate();
        void Process_DisConnectFromFireBase();
    }

}
