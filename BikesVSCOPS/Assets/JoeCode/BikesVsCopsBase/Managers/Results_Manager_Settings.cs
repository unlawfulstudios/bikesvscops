﻿using UnityEngine;
using UnityEngine.UI;

public class Results_Manager_Settings : MonoBehaviour {

    public Results_TimePanels results_TimePanels;
    public Animator results_Animator;
    public Text Amount_Text;
    public Text Run_Info_text;
    public Text city_Info_text;
    public Text TriesAmountText;
}
