﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Garage_ManagerSettings : MainMenuBehaviors
{
    public Image LockImage;
    public RawImage BikeImage;
    public GameObject buyButton;
    public GameObject EquippedButton;
    public BikeHolderStatBar BikeHolderStatBar;
  //  public GameObject UlockedText;

    public Text BikeName_text;
    public Text Price_Text;

    public void Got_Help()
    {
        Application.OpenURL("https://www.bikesvscops.com/apps/help-center");
    }
    public override void On_Enable()
    {
        Debug.Log("MOOOOOOOOEnable");

        GlobaleManagers.BikeHolder_CameraManager.ON = true;
        GlobaleManagers.BikeHolder_CameraManager.bikeHolder.StartRotate();
       // MainMenuManagers.Garage_Manager.SetBike();

    }


    public override void On_Disable()
    {
        GlobaleManagers.BikeHolder_CameraManager.ON = false;
    }

}
