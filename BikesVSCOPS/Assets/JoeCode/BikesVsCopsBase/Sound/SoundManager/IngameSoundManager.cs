﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class IngameSoundManager : SoundManager
{
    public InGameManagers inGameManagers;

    [Inject]
    public void Construct(InGameManagers _inGameManagers)
    {
        inGameManagers = _inGameManagers;
    }


    public void toggle_Sounds(bool on)
    {
        if (on)
        {
            BikeSoundOn();

        }
        else
        {
            BikeSoundoff();
        }


    }
    public void disable_Sounds()
    {

        BikeSoundoff();

    }
    public void Enable_Sounds()
    {
        BikeSoundOn();
    }
    private void BikeSoundoff()
    {

        inGameManagers.Racer_Manager.Player_BikeControl.bikeSounds.disableSound();

        if (inGameManagers.Racer_Manager.Ai_BikeControl != null)
            inGameManagers.Racer_Manager.Ai_BikeControl.bikeSounds.disableSound();

    }
    private void BikeSoundOn()
    {
        inGameManagers.Racer_Manager.Ai_BikeControl.bikeSounds.enableSound();
        if (inGameManagers.Racer_Manager.Ai_BikeControl != null)
            inGameManagers.Racer_Manager.Ai_BikeControl.bikeSounds.enableSound();
    }

    public IngameSoundManager(SoundManagerSettings _soundsettings) : base(_soundsettings)
    {

    }
}
