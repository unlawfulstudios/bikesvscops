﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManagerBase : GlobaleManagers
{
    public SoundManagerSettings soundManagerSettings;
    public ProjectSoundData ProjectSoundData;

    public soundManagerBase(SoundManagerSettings ss)
    {
        soundManagerSettings = ss;
    }
    public static bool startMusic = false;
    public bool triggerUISOund = false;
    public bool HasSOund { get { return current_UserProfile.game_Settings_Manager.Has_Sound.Value; } }

    public virtual void Init() { }
    public virtual void Play() { }
    public virtual void SetClip(AudioClip clip)
    {
        // soundManagerSettings.musicSource.clip = clip;
    }
    public virtual void PlaySingleClip(AudioClip clip)
    {
        //soundManagerSettings.musicSource.Play();
    }
    public virtual void Stop() { }
    public virtual void Sound_Update() { }


    
    public virtual void RandomizeMusic(AudioClip[] clips)
    {

        //int randomIndex = Random.Range(0, clips.Length);

        //soundManagerSettings.musicSource.clip = clips[randomIndex];
    }
    public virtual void PlayerButtonSound()
    {
        //triggerUISOund = true;
    }

    



}
