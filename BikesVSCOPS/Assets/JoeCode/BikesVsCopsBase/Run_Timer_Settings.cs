﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Run_Timer_Settings : InGameBehaviors {

    public GameTimePanle Race_TimePanles;
    public GameTime current_GameTime;


   
    public float RedayCountOfSet = 2.1f;
    public float ready_Timer_Count = 4f;
    public string ready_Timer_Display;
    public float inActive_Timer_COunt = 1f;

    public float _totalTime = 3.0f;
    public float _currentTime = 0f;
    public float _CountTime = 2f;
    public float CheckPointTime = 1;
    public float checkPointRate = 1;

    public override void On_Disable()
    {
        inActive_Timer_COunt = 4;
        ready_Timer_Count = 4;
    }

#if UNITY_EDITOR
    private CreateGAMEOBJECTINFO countDown_Text_Info;
    private CreateGAMEOBJECTINFO countDown_BackGround_Info;
    private CreateGAMEOBJECTINFO countDown_CurrentImage_Info;

    public override void On_Reset()
    {
        countDown_Text_Info = new CreateGAMEOBJECTINFO("Ready Count Text", "Ready Count BackGround");
    }
#endif

}
