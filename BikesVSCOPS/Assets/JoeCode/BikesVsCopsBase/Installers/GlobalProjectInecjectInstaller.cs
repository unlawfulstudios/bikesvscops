using UnityEngine;
using Zenject;
using System;

public class GlobalProjectInecjectInstaller : MonoInstaller<GlobalProjectInecjectInstaller>
{

    [Inject]
    public StringHolder type_holder;

    public Loading_ManagerSettings lms;
    public BikeHolder BikeHolder;
    public PopUpManagerSetteings popUpManagerSetteings;
    public AuthManagerSettings AuthManagerSettings;
    public AppProcesser appProcesser;

    public override void InstallBindings()
    {
        for (int i = 0; i < type_holder.GlobalInjectTypeNames.Count; i++)
        {
            Container.BindInterfacesAndSelfTo(ResourcesReturnType.Value(type_holder.GlobalInjectTypeNames[i])).AsSingle().NonLazy();
        }
   
        Container.BindInterfacesAndSelfTo<Loading_Manager>().AsSingle().WithArguments(lms).NonLazy();
        Container.BindInterfacesAndSelfTo<AppManager>().AsSingle().WithArguments(appProcesser).NonLazy();
        Container.BindInterfacesAndSelfTo<BikeHolder_CameraManager>().AsSingle().WithArguments(BikeHolder).NonLazy();
        Container.BindInterfacesAndSelfTo<PopUpManagar>().AsSingle().WithArguments(popUpManagerSetteings).NonLazy();
        Container.BindInterfacesAndSelfTo<AuthManager>().AsSingle().WithArguments(AuthManagerSettings).NonLazy();

    }
}