using UnityEngine;
using Zenject;

public class InGameInstallers : MonoInstaller<InGameInstallers>
{
    public Run_Timer_Settings Run_Timer_Settings;
    public SoundManagerSettings SoundManagerSettings;
    public Results_Manager_Settings results_Manager_Settings;
    public PlayerHub playerHub;
    public RaceIntro_CamerManagerSettings RaceIntro_CamerManagerSettings;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<RaceIntroCameraManager>().AsSingle().WithArguments(RaceIntro_CamerManagerSettings).NonLazy();
        Container.BindInterfacesAndSelfTo<InGameStateManager>().AsSingle().NonLazy();
        Container.Bind<Racer_Manager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo <InGameEventManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<Object_Manager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<Run_Timer>().AsSingle().WithArguments(Run_Timer_Settings).NonLazy();
        Container.BindInterfacesAndSelfTo<IngameSoundManager>().AsSingle().WithArguments(SoundManagerSettings).NonLazy();
        Container.BindInterfacesAndSelfTo<Results_Manager>().AsSingle().WithArguments(results_Manager_Settings).NonLazy();
        Container.BindInterfacesAndSelfTo<InGameManagers>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerHubController>().AsSingle().WithArguments(playerHub).NonLazy();
        Container.Bind<AsyncProcessor>().FromNewComponentOnNewGameObject().AsSingle();

    }
}