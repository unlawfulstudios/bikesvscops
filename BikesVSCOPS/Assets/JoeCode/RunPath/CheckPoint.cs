﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[RequireComponent(typeof(BoxCollider))]
public class CheckPoint : InGameBehaviors {


    public void OnEnable()
    {
        this.GetComponent<BoxCollider>().enabled = true;
       
    }
    private void Start()
    {
        InGameManagers.object_Manager.CheckPoints.Add(this);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Racer>())
        {
            InGameManagers.Racer_Manager.hits += 1;
            InGameManagers.Run_Timer.UpdateRace_Postion(other.GetComponent<Racer>());

            this.GetComponent<BoxCollider>().enabled = false;
        }
    }
    
}
