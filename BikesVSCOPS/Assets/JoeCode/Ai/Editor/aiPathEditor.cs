﻿using UnityEditor;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[CustomEditor(typeof(AIPath))]
public class aiPathEditor : Editor
{
    private ArrayList sceneViews;
    SceneView sceneView { get { return SceneView.lastActiveSceneView; } }
    public bool shoWNodes = false;
    private bool fallow_Ai = false;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        AIPath AIPath_Script = (AIPath)target;

        AIPath_Script.fallowAI = EditorGUILayout.Toggle("Fallow Ai", AIPath_Script.fallowAI);

        shoWNodes = EditorGUILayout.Foldout(shoWNodes, "Sho All Nodes");

        if (shoWNodes)
            SetAIChallaner(AIPath_Script);

        if (GUI.changed)
            EditorUtility.SetDirty(this);
    }


    public void SetAIChallaner(AIPath aIPath_Script)
    {
        if (aIPath_Script.ai_Path_Type != Ai_Path_Type.Challenger)
            return;

    

        if (GUILayout.Button("Reset ALL"))
        {
            resetALL(aIPath_Script);
        }

        ShowRange(aIPath_Script);


    }

    GUIStyle style;
    public bool CanBreake = false;
    GameObject spwannode;


    bool showPosition = true;
    string status = "Show";


    public Vector3[] SetSize = new Vector3[]
    {
        new Vector3(0,0,0),
        new Vector3(25,100,100),
        new Vector3(100,100,25)

    };

    public SizeType CurrentSizeType;

    public void ShowRange(AIPath aIPath_Script)
    {
        if (aIPath_Script.nodes.Count < 0)

            return;
        style = new GUIStyle(EditorStyles.textField);

        for (int i = 0; i < aIPath_Script.nodes.Count; i++)
        {
            aIPath_Script.nodes[i].GetComponent<Node>().Show = EditorGUILayout.Foldout(aIPath_Script.nodes[i].GetComponent<Node>().Show, status + aIPath_Script.nodes[i].name);

            if (aIPath_Script.nodes[i].GetComponent<Node>().Show)
            {
                if (aIPath_Script.nodes[i].GetComponent<Node>() == aIPath_Script.Ai.GetComponent<AI_Engine>().currentNode)
                {
                    style.normal.textColor = Color.red;
                }
                else
                {
                    style.normal.textColor = Color.black;
                }

                EditorGUILayout.LabelField(aIPath_Script.nodes[i].GetComponent<Node>().name, style);

                aIPath_Script.nodes[i].GetComponent<Node>().brakeAtNode = EditorGUILayout.Toggle("Brake", aIPath_Script.nodes[i].GetComponent<Node>().brakeAtNode);

                aIPath_Script.nodes[i].GetComponent<Node>().is_CheckPoint = EditorGUILayout.Toggle("is_CheckPoint", aIPath_Script.nodes[i].GetComponent<Node>().is_CheckPoint);

                if (aIPath_Script.nodes[i].GetComponent<Node>().is_CheckPoint)
                {
                    aIPath_Script.nodes[i].GetComponent<Node>().AddCheckPoint();


                    aIPath_Script.nodes[i].GetComponent<Node>().size_Type = (SizeType)EditorGUILayout.EnumPopup("SizeType", aIPath_Script.nodes[i].GetComponent<Node>().size_Type);

                    if (aIPath_Script.nodes[i].GetComponent<Node>().size_Type == SizeType.Custom)
                    {
                        aIPath_Script.nodes[i].GetComponent<BoxCollider>().size = EditorGUILayout.Vector3Field("Collider Size:", aIPath_Script.nodes[i].GetComponent<BoxCollider>().size);
                    }
                    else
                    {
                        aIPath_Script.nodes[i].GetComponent<BoxCollider>().size = SetSize[(int)aIPath_Script.nodes[i].GetComponent<Node>().size_Type];
                    }

                }
                else
                    aIPath_Script.nodes[i].GetComponent<Node>().RemoveCheckPoint();

             

                if (GUILayout.Button("Reset"))
                {
                    aIPath_Script.nodes[i].GetComponent<Node>().slowDownspeed = 0;
                }


                if (GUILayout.Button("Set Cam"))
                {
                    aIPath_Script.fallowAI = false;

                    MoveSceneCam(aIPath_Script.nodes[i].GetComponent<Node>());
                }

            

                

                aIPath_Script.nodes[i].GetComponent<Node>().slowDownspeed = EditorGUILayout.Slider(aIPath_Script.nodes[i].GetComponent<Node>().slowDownspeed, 0.0f, 1.0f);

           

                if (aIPath_Script.nodes[i].GetComponent<Node>().brakeAtNode)
                {
                    aIPath_Script.nodes[i].GetComponent<Node>().BreakTime = EditorGUILayout.Slider(aIPath_Script.nodes[i].GetComponent<Node>().BreakTime, 0.0f, 10.0f);
                }
                else
                {
                    aIPath_Script.nodes[i].GetComponent<Node>().BreakTime = 0;
                }
              

            }


        }

    }
    public void resetALL(AIPath aIPath_Script)
    {
        if (aIPath_Script.nodes.Count < 0)
            return;


        for (int i = 0; i < aIPath_Script.nodes.Count; i++)
        {
            if (aIPath_Script.nodes[i].GetComponent<Node>().SlowDownOnApproch())
            {
                aIPath_Script.nodes[i].GetComponent<Node>().slowDownspeed = 0;
            }
        }

    }

    public void MoveSceneCam(Node node)
    {

        if (SceneView.lastActiveSceneView != null && Selection.activeTransform != null)
        {
            Selection.activeGameObject = node.gameObject;

            sceneView.LookAt(node.transform.position);
        }
    }


}
