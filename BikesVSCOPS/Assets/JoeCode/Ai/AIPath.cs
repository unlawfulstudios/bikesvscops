﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum Ai_Path_Type
{
    CopSpawnPoint,
    CopTurn,
    Challenger
}
[System.Serializable]
public class SceneViewFollower
{
#if UNITY_EDITOR
    public bool enable;
    public Vector3 positionOffset;
    public bool enableFixedRotation;
    public Vector3 fixedRotation;
    public Transform targetTransform;
    public float size;
    public bool orthographic;
    public int sceneViewIndex;

    SceneViewFollower()
    {
        enable = false;
        positionOffset = Vector3.zero;
        enableFixedRotation = false;
        fixedRotation = Vector3.zero;
        size = 5;
        orthographic = true;
        sceneViewIndex = 0;
    }
#endif
}

[ExecuteInEditMode]
public class AIPath : MonoBehaviour
{
#if UNITY_EDITOR
    public GameObject Node;

    public Color line_Color;

    public Transform Ai;


    public bool Draw_Aipath = false;
    public Ai_Path_Type ai_Path_Type = Ai_Path_Type.Challenger;

    public List<Transform> nodes = new List<Transform>();

    public bool fallowAI = false;
    public bool show_Ai_Path = false;

    public float sphere_Size = 0.3f;
    public bool on = true;
    public bool onlyInPlayMode = false;
    public SceneViewFollower[] sceneViewFollowers;
    private ArrayList sceneViews;

    void OnDrawGizmos()
    {
        if (!show_Ai_Path)
            return;

        Gizmos.color = line_Color;

        Transform[] path_transform = GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < path_transform.Length; i++)
        {
            if (path_transform[i] != transform)
            {
                nodes.Add(path_transform[i]);
            }
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            Vector3 current_Node = nodes[i].position;
            Vector3 previous_Node = Vector3.zero;

            if (i > 0)
            {
                previous_Node = nodes[i - 1].position;
                Gizmos.DrawLine(previous_Node, current_Node);
               
            }
            else if (i == 0 && nodes.Count > 0)
            {
                previous_Node = nodes[nodes.Count - 1].position;
            }
            Gizmos.DrawWireSphere(current_Node, sphere_Size);
        }
    }

    public void LateUpdate()
    {


#if UNITY_EDITOR
        if (sceneViewFollowers != null && sceneViews != null)
            {
                foreach (SceneViewFollower svf in sceneViewFollowers)
                {
                    if (svf.targetTransform == null) svf.targetTransform = transform;
                    svf.size = Mathf.Clamp(svf.size, .01f, float.PositiveInfinity);
                    svf.sceneViewIndex = Mathf.Clamp(svf.sceneViewIndex, 0, sceneViews.Count - 1);
                }
            }


        if (fallowAI)
            Follow();
#endif

    }

    void Follow()
    {
        sceneViews = UnityEditor.SceneView.sceneViews;
        if (sceneViewFollowers == null || !on || sceneViews.Count == 0) return;

        foreach (SceneViewFollower svf in sceneViewFollowers)
        {
            if (!svf.enable) continue;
            UnityEditor.SceneView sceneView = (UnityEditor.SceneView)sceneViews[svf.sceneViewIndex];
            if (sceneView != null)
            {
                if ((Application.isPlaying && onlyInPlayMode) || !onlyInPlayMode)
                {
                    sceneView.orthographic = svf.orthographic;
                    sceneView.LookAtDirect(svf.targetTransform.position + svf.positionOffset, (svf.enableFixedRotation) ? Quaternion.Euler(svf.fixedRotation) : svf.targetTransform.rotation, svf.size);
                }
            }
        }
    }
#endif
}

