﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndPoint : RacePoints
{
    public static Racer currentLead;
    public Text posText;

    public List<Racer> all_Racers;
    public float Player_MIN = 0;
    public float Player_Sec = 0;
    public float Player_MilSec = 0;

    public float Ai_MIN = 0;
    public float Ai_Sec = 0;
    public float Ai_MilSec = 0;

    public bool Finshed = false;
    public bool win = false;
    public bool lose = false;
    public static bool setText = false;

    public float checkPointCheck_Time = 1.0f;
    public float checkPointCheck_RepeatRate = 1.0f;

    public Run_Timer run_Timer;

    public List<CheckPoint> CheckPointslist;

    public bool PlayerHasfinshed = false;


    private void Awake()
    {

        InGameEventManager.Current_endType = Result_Type.invalid;
    }


    public void WIN(Collider other)
    {


        InGameManagers.ingameSoundManager.disable_Sounds();
        InGameManagers.ingameSoundManager.Pause_Sound();
        InGameManagers.Results_Manager.Player_End();
        other.GetComponent<BikeControl>().brake = true;

        if (InGameEventManager.Current_endType != Result_Type.LOST)
        {
            InGameEventManager.Current_endType = Result_Type.Win;
            InGameManagers.Results_Manager.End(true);
        }
        else
        {
            InGameManagers.Results_Manager.End(false);
        }

        InGameEventManager.current_Run_EventState = Run_Event_States.inactive;

    }

    public void Lose()
    {

        if (!InGameEventManager.Active)
            return;

        InGameEventManager.Current_endType = Result_Type.LOST;

        InGameManagers.Results_Manager.Ai_Win();

    }


    public override void Process_Update()
    {
        base.Process_Update();
    }

    public void Update()
    {
        if (setText)
        {
            Set_Text();
            setText = false;
        }

        if (PlayerHasfinshed)
        {
            //Racer_Manager.disable_Sounds();
            InGameManagers.ingameSoundManager.Pause_Sound();
        }
    }

    public static void SetPositon(Racer racer)
    {
        currentLead = racer;
        setText = true;

    }

    void Set_Text()
    {
        if (currentLead != null)
            posText.text = Racer_Manager.is_PLayer(currentLead) ? "1" : "2";

    }




    private void OnTriggerEnter(Collider other)
    {
   
        if (other.CompareTag("Player"))
        {
         
                WIN(other);

            PlayerHasfinshed = true;


            if (!other.GetComponent<BikeControl>().Crashs)
            {
                StartCoroutine("waits");
            }
                // if player is not crashing 


        }

        if (other.CompareTag("Blaze"))
        {
            Toggle(false, true);

            InGameManagers.Results_Manager.Ai_End();
            // Stop Ai Bike
            if (other.GetComponent<AI_Engine>() != null)
            {
                other.GetComponent<AI_Engine>().Finshed = true;
            }

            if (InGameEventManager.Current_endType != Result_Type.Win)
            {
                InGameEventManager.Current_endType = Result_Type.LOST;
            }

        }

    }



    public IEnumerator waits()
    {


        yield return new WaitForSeconds(0.1f);

    }
   
 
    protected override void Player_RaceEvent()
    {
        GlobaleManagers.EventManager.Trigger_Event("PlayerEnd");
    }

    protected override void Ai_RaceEvent()
    {
        if (InGameEventManager.Current_endType != Result_Type.Win)
        {
            GlobaleManagers.EventManager.Trigger_Event("AiEnd");
        }
        Lose();
    }

}
