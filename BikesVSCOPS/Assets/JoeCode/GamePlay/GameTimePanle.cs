﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class GameTimePanle : InGameBehaviors  {

    public Text Timetext;
   
    public void Set_Time_Panel(Result_Type result_Type, GameMode_Type gameMode)
    {

        Timetext.text = Results(result_Type, gameMode);

    }
    private RUN current_Run() { return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run; }
    private City currentCity() { return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_City;}
    public void Set_Time_Panel(string info)
    {

        Timetext.text = info;

    }
    public string Results(Result_Type result_Type, GameMode_Type gameMode)
    {
    

        if (gameMode == GameMode_Type.CAREER_MODE)
        {
            if(result_Type == Result_Type.Win || result_Type == Result_Type.invalid)
            {
                m_Results = GameTime.Current_Game_Time;
            }
        }
       
        if (result_Type == Result_Type.Busted)
        {
            if ( InGameManagers.Game_Manager.current_GameMode_Type == GameMode_Type.GET_AWAY_MODE)
            {
                m_Results = "YOU LASTED : " + GameTime.Current_Game_Time;


            }
            else
            {
                m_Results = GameTime.Current_Game_Time;
            }
        }
        if (result_Type == Result_Type.LOST)
            m_Results = "LOST";

        return m_Results;
    }

   
    private string m_Results = "";



#if UNITY_EDITOR
    private CreateGAMEOBJECTINFO TimeTextInfo;

    public override void On_Reset()
    {
        TimeTextInfo = new CreateGAMEOBJECTINFO("Time Display", "Time Display Panel");
     
    }
#endif

}
