﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ArrowType
{
    Left,
    Right
}
public enum GameMode_Type
{
    invalid_MODE,
    CAREER_MODE,
    RIVALRY_MODE,
    FREERIDE_MODE,
    GET_AWAY_MODE

}
public enum GameController_Type
{
    invalid,
    pc_KeyBoard,
    moblie_Touch,
}

public enum Result_Type
{
    invalid,
    Win,
    LOST,
    Busted

}

