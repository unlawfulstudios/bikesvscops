﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RUN_Events : MonoBehaviour {

    //#region Event_Listeners 
    //private UnityAction ready_Run_Listener;
    //private UnityAction start_Run_Listener;
    //private UnityAction player_End_Run_Listener;
    //private UnityAction ai_End_Run_Listener;
    //private UnityAction player_Busted_Run_Listener;
    //private UnityAction run_Restart_Listener;
    //private UnityAction Pause_Game_Listener;
    //private UnityAction wait_Game_Listener;
    //private UnityAction Resume_Game_Listener;
    //#endregion

    //public static bool isPaused = false;
    //private string[] Run_Event_Names = { "Ready", "Start", "PlayerBusted", "PlayerEnd", "AiEnd","Restart","Pause","wait", "Resume"};
    //public UI_Controller Pause_UI_Controller;
    //public UI_Controller Player_UI_Controller;
    //public Button BackButton;
    //private int CurrentTimescale = 1;

    //private void Awake()
    //{
    //    ready_Run_Listener = new UnityAction(Ready_Run_Function);
    //    start_Run_Listener = new UnityAction(Start_Run_Function);
    //    player_End_Run_Listener = new UnityAction(Player_End_Run_Function);
    //    ai_End_Run_Listener = new UnityAction(Ai_End_Run_Function);
    //    player_Busted_Run_Listener = new UnityAction(Player_Busted_Function);
    //    run_Restart_Listener = new UnityAction(Restart);
    //    Pause_Game_Listener = new UnityAction(PauseGame);
    //    wait_Game_Listener = new UnityAction(wait);
    //    Resume_Game_Listener = new UnityAction(ResumeGame);
    //}

    //private void OnEnable()
    //{
    //    Init();

    //}

    //private void OnDisable()
    //{
    //    Disable();
    //}

    //private void Ready_Run_Function()
    //{
    //    Debug.Log("Ready_Run_Function was called ");
    //    EventManager.Instance.current_Run_EventState = Run_Event_States.Ready;
  

    //}

    //private void Start_Run_Function()
    //{
    //    Debug.Log("Start_Run_Function was called ");
    //    EventManager.Instance.current_Run_EventState = Run_Event_States.Start;
    //    if(BackButton != null)
    //    BackButton.interactable = true;

    //}

    //private void Player_End_Run_Function()
    //{
       
    //    Debug.Log("Player_End_Run_Function was called ");
    //    EventManager.Instance.current_Run_EventState = Run_Event_States.End;

    //    if (EventManager.Instance.Current_endType != Result_Type.LOST)
    //    {
    //        EventManager.Instance.Current_endType = Result_Type.Win;

                    
    //        if (!UserProfile.Instance.CAREER_Citys[UserProfile.Instance.bikesVSCopsProfileInfo.current_Run.city_id].Runs
    //            [UserProfile.Instance.bikesVSCopsProfileInfo.current_Run.id+1].Unlocked.value)
    //        {
    //            Show_Complete();

    //            UserProfile.Instance.CAREER_Citys[UserProfile.Instance.bikesVSCopsProfileInfo.current_Run.city_id].Runs
    //            [UserProfile.Instance.bikesVSCopsProfileInfo.current_Run.id+1].Unlocked.value = true;
    //        }

    //    }
    //}

    //public void Show_Complete()
    //{

    //    Debug.Log("Coosadddddddddddd");
    //    //Results_Manager.Instance.Complete_RUN();
    //}
    //public void Ai_End_Run_Function()
    //{
      
    //    EventManager.Instance.Current_endType = Result_Type.LOST;
    //    //ph.Show_Results(false);
    //}

    //public void Restart()
    //{

    //    StartCoroutine(LoadYourAsyncScene());
    //    BackButton.interactable = false;
    //}

    //IEnumerator LoadYourAsyncScene()
    //{
    //    // The Application loads the Scene in the background at the same time as the current Scene.
    //    //This is particularly good for creating loading screens. You could also load the Scene by build //number.
    //    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);

    //    //Wait until the last operation fully loads to return anything
    //    while (!asyncLoad.isDone)
    //    {
          
    //        yield return null;
    //    }

        
    //}

    //public void PauseGame()
    //{
    //    Debug.Log("Pause");
    //    Toggle_Pause(true);
    //}

    //public void wait()
    //{
    
    //    Debug.Log("wait");
    //}

    //public void ResumeGame()
    //{
    //    Debug.Log("ResumeGame");
    //    Toggle_Pause(false);

    //}

    //public void Player_Busted_Function()
    //{

    //    if (EventManager.Instance.current_Run_EventState != Run_Event_States.End)
    //    {
    //        EventManager.Instance.current_Run_EventState = Run_Event_States.Busted;
    //        EventManager.Instance.Current_endType = Result_Type.LOST;
    //        Results_Manager.Instance.Player_Busted();
    //        EventManager.Instance.current_Run_EventState = Run_Event_States.End;
    //        Racer_Manager.disable_Sounds();
    //    }
    //    Time.timeScale = 1;
    //}

    //public void Toggle_Pause(bool toggle)
    //{        
    //    StartCoroutine(set(toggle));
    //    Pause_UI_Controller.Set_Active_State(toggle);
    //    Player_UI_Controller.Set_Active_State(!toggle);   
    //}

    //IEnumerator set(bool toggle)
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    isPaused = toggle;
    //    CurrentTimescale = toggle ? 0 : 1;
    //    Time.timeScale = CurrentTimescale;

    //    StopCoroutine("set");
    //}

    //void Init()
    //{              
    //    EventManager.Start_Listening(Run_Event_Names[0], ready_Run_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[1], start_Run_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[2], player_Busted_Run_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[3], player_End_Run_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[4], ai_End_Run_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[5], run_Restart_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[6], Pause_Game_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[7], wait_Game_Listener);
    //    EventManager.Start_Listening(Run_Event_Names[8], Resume_Game_Listener);
    //}

    //void Disable()
    //{
    //    EventManager.Stop_Listening(Run_Event_Names[0], ready_Run_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[1], start_Run_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[2], player_Busted_Run_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[3], player_End_Run_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[4], ai_End_Run_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[5], run_Restart_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[6], Pause_Game_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[7], wait_Game_Listener);
    //    EventManager.Stop_Listening(Run_Event_Names[8], Resume_Game_Listener);
    //}

    //private void OnDestroy()
    //{
    //    Disable();
    //}

}
