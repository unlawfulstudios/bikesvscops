﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UnityEngine.Events;
using System.Collections.Generic;


public class InGameEventManager : InGameManagers, IInitializable, ILateDisposable
{ //#region Event_Listeners 

    public static Run_Event_States current_Run_EventState;
    public static Result_Type Current_endType;
    public static bool Button_Pressed = false;
    public static bool Active
    {
        get
        {
            if (current_Run_EventState == Run_Event_States.inactive ||
                current_Run_EventState == Run_Event_States.Ready
                || current_Run_EventState == Run_Event_States.End
                || current_Run_EventState == Run_Event_States.Busted
                || current_Run_EventState == Run_Event_States.CamState
                || Current_endType == Result_Type.Busted
                || Current_endType == Result_Type.Win
                || !Button_Pressed)
            {
                return false;
            }
            else
                return true;
        }
    }

    private UnityAction ready_Run_Listener;
    private UnityAction start_Run_Listener;
    private UnityAction player_End_Run_Listener;
    private UnityAction ai_End_Run_Listener;
    private UnityAction player_Busted_Run_Listener;
    private UnityAction run_Restart_Listener;
    private UnityAction Pause_Game_Listener;
    private UnityAction wait_Game_Listener;
    private UnityAction Resume_Game_Listener;

    public static bool isPaused = false;
    private string[] Run_Event_Names = { "Ready", "Start", "PlayerBusted", "PlayerEnd", "AiEnd", "Restart", "Pause", "wait", "Resume" };
    public UI_Controller Pause_UI_Controller;
    public UI_Controller Player_UI_Controller;
    private int CurrentTimescale = 1;


    public void Initialize()
    {

        ready_Run_Listener = new UnityAction(Ready_Run_Function);
        start_Run_Listener = new UnityAction(Start_Run_Function);
        player_End_Run_Listener = new UnityAction(Player_End_Run_Function);
        ai_End_Run_Listener = new UnityAction(Ai_End_Run_Function);
        player_Busted_Run_Listener = new UnityAction(Player_Busted_Function);
        run_Restart_Listener = new UnityAction(Restart);
        Pause_Game_Listener = new UnityAction(PauseGame);
        wait_Game_Listener = new UnityAction(wait);
        Resume_Game_Listener = new UnityAction(ResumeGame);

        Init();
    }

    public void LateDispose()
    {
        Disable();
    }

    private void Ready_Run_Function()
    {
        Debug.Log("Ready_Run_Function was called ");
        current_Run_EventState = Run_Event_States.Ready;


    }

    private void Start_Run_Function()
    {
        Debug.Log("Start_Run_Function was called ");
        current_Run_EventState = Run_Event_States.Start;


    }

    private void Player_End_Run_Function()
    {

        Debug.Log("Player_End_Run_Function was called ");
        current_Run_EventState = Run_Event_States.End;

        if (Current_endType != Result_Type.LOST)
        {
            Current_endType = Result_Type.Win;


            if (!current_UserProfile.CAREER_Citys[current_UserProfile.bikesVSCopsProfileInfo.current_Run.city_id].Runs
                [current_UserProfile.bikesVSCopsProfileInfo.current_Run.id + 1].Unlocked.value)
            {
                Show_Complete();

                current_UserProfile.CAREER_Citys[current_UserProfile.bikesVSCopsProfileInfo.current_Run.city_id].Runs
                [current_UserProfile.bikesVSCopsProfileInfo.current_Run.id + 1].Unlocked.value = true;
            }

        }
    }

    public void Show_Complete()
    {

        Debug.Log("Coosadddddddddddd");
        //Results_Manager.Instance.Complete_RUN();
    }

    public void Ai_End_Run_Function()
    {
        Current_endType = Result_Type.LOST;
    }


   
    public void Restart()
    {
        Debug.Log("Restart");
    }

    public void PauseGame()
    {
        Debug.Log("Pause");
        Toggle_Pause(true);
    }

    public void wait()
    {
        Debug.Log("wait");
    }

    public void ResumeGame()
    {
        Debug.Log("ResumeGame");
        Toggle_Pause(false);

    }

    public void Player_Busted_Function()
    {

        if (current_Run_EventState != Run_Event_States.End)
        {
            current_Run_EventState = Run_Event_States.Busted;
            Current_endType = Result_Type.LOST;
            Results_Manager.Player_Busted();
            current_Run_EventState = Run_Event_States.End;
            ingameSoundManager.disable_Sounds();
        }
        Time.timeScale = 1;
    }

    public void Toggle_Pause(bool toggle)
    {
        //StartCoroutine(set(toggle));
        Pause_UI_Controller.Set_Active_State(toggle);
        Player_UI_Controller.Set_Active_State(!toggle);
    }

    IEnumerator set(bool toggle)
    {
        yield return new WaitForSeconds(0.1f);
        isPaused = toggle;
        CurrentTimescale = toggle ? 0 : 1;
        Time.timeScale = CurrentTimescale;
    }

    void Init()
    {

        EventManager.Start_Listening(Run_Event_Names[0], ready_Run_Listener);
        EventManager.Start_Listening(Run_Event_Names[1], start_Run_Listener);
        EventManager.Start_Listening(Run_Event_Names[2], player_Busted_Run_Listener);
        EventManager.Start_Listening(Run_Event_Names[3], player_End_Run_Listener);
        EventManager.Start_Listening(Run_Event_Names[4], ai_End_Run_Listener);
        EventManager.Start_Listening(Run_Event_Names[5], run_Restart_Listener);
        EventManager.Start_Listening(Run_Event_Names[6], Pause_Game_Listener);
        EventManager.Start_Listening(Run_Event_Names[7], wait_Game_Listener);
        EventManager.Start_Listening(Run_Event_Names[8], Resume_Game_Listener);
    }

    void Disable()
    {
        EventManager.Stop_Listening(Run_Event_Names[0], ready_Run_Listener);
        EventManager.Stop_Listening(Run_Event_Names[1], start_Run_Listener);
        EventManager.Stop_Listening(Run_Event_Names[2], player_Busted_Run_Listener);
        EventManager.Stop_Listening(Run_Event_Names[3], player_End_Run_Listener);
        EventManager.Stop_Listening(Run_Event_Names[4], ai_End_Run_Listener);
        EventManager.Stop_Listening(Run_Event_Names[5], run_Restart_Listener);
        EventManager.Stop_Listening(Run_Event_Names[6], Pause_Game_Listener);
        EventManager.Stop_Listening(Run_Event_Names[7], wait_Game_Listener);
        EventManager.Stop_Listening(Run_Event_Names[8], Resume_Game_Listener);
    }

  
}


