﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[System.Serializable]
public class GameTime  {


    public static string Current_Game_Time = "";
    public float time;
    public float startTime;
    public TimeSpan timeInAir;
    public static TimeSpan current_timeSpan;
    public void Run_timer(GameTimePanle timePanle)
    {

        Set_Time(timePanle, false);
   

    }
    public void ReSet_timer(GameTimePanle timePanle)
    {
        Set_Time(timePanle, true);
    }

    public void Set_Time(GameTimePanle timePanle ,bool ReSet)
    {
        if (!InGameEventManager.Active)
            return;

        timeInAir += TimeSpan.FromSeconds(Time.smoothDeltaTime);
        float m = !ReSet ? timeInAir.Minutes :00;
        float s = !ReSet ? timeInAir.Seconds :00;
        float f = !ReSet ? timeInAir.Milliseconds:00;


        var minuets = m;
        var secounds = s;
        var fraction = f;


    
        Current_Game_Time = string.Format("{0:00}:{1:00}:{2:000}", minuets, secounds, fraction);

        current_timeSpan = timeInAir;

        timePanle.Timetext.text = Current_Game_Time;
    }

 
}
