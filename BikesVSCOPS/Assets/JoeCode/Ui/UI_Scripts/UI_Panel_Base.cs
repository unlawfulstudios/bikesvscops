﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_Panel_Base : MonoBehaviour {


    //public Animator controller_Animator;
    public int ID = 0;

    public bool Has_TopPanel = false;

    public bool ActiveOnstart
    {
        get { return this.m_active; }
        set { this.m_active = value; }
    }

    public bool Active
    {
        get { return this.m_active; }
        set { this.m_active = value; }
    }


    [SerializeField]
    private bool m_active;

    [SerializeField]
    public bool m_ActiveOnstart = false;


    public virtual void Set_Active_State(bool toggle)
    {

        this.gameObject.SetActive(toggle);

     
    }
}
