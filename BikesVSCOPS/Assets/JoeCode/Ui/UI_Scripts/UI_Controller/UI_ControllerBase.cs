﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_ControllerBase : MonoBehaviour{

    //public Animator controller_Animator;
    public int ID = 0;

    public Animator Controller_Animator;


    public AnimatorUpdateMode animatorUpdateMode; 

    public bool Has_TopPanel = false;


    public bool PlayAnimation = false;

    public bool ActiveOnstart
    {
        get { return this.m_active; }
        set { this.m_active = value; }
    }

    public bool Active
    {
        get { return this.m_active; }
        set { this.m_active = value; }
    }

    [SerializeField]
    private bool m_active;

    [SerializeField]
    public bool m_ActiveOnstart = false;

    public virtual void Set_Animator_Controller()
    {
        Controller_Animator.updateMode = animatorUpdateMode;
    }

    public virtual void Play_Animator_Controller(int id) {
        
   
        Controller_Animator.Play("On");
    }

    public virtual void Set_Active_State(bool toggle) {

      
        if(this !=null)
        this.gameObject.SetActive(toggle);

        if (!Scene_Manager.in_Game())
        {
            if (toggle)
            {
                if (Has_TopPanel || UI_Manager.Instance != null)
                {

                    UI_Manager.Instance.Toggle_TopPanel(true);
                }
                else
                {
                    UI_Manager.Instance.Toggle_TopPanel(false);
                }
            }
        }

        if (PlayAnimation)
        {
            //Play_Animator_Controller(ID);
        }

    }

}
