﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollTrectSnap : MonoBehaviour
{

    public RectTransform panel;
    public Bike_Select_Button[] bttn;
    public RectTransform center;

    private float[] distance;
    private bool is_dragging = false;
    private int bttn_Distance;
    private int minButtonNum;



    private void Start()
    {
        int bttnLenght = bttn.Length;
        distance = new float[bttnLenght];

        bttn_Distance = (int)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x - 
            bttn[0].GetComponent<RectTransform>().anchoredPosition.x);


    }
    private void FixedUpdate()
    {
        
    }
    private void Update()
    {
        for (int i = 0; i < bttn.Length; i++)
        {
            distance[i] = Mathf.Abs(center.transform.position.x - bttn[i].transform.position.x);
        }

        float minDistance = Mathf.Min(distance);

        for (int a = 0; a < bttn.Length; a++)
        {
            if(minDistance == distance[a])
            {
                minButtonNum = a;
            }
        }


        if(!is_dragging)
        {
            LerpToButton(minButtonNum * -bttn_Distance);
        }
    }

    void LerpToButton(int position)
    {
        float newx = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * 10f);

        Vector2 newPosition = new Vector2(newx, panel.anchoredPosition.y);

        panel.anchoredPosition = newPosition;
    }

    public void Start_Drag()
    {
        is_dragging = true;

        Debug.Log("AAA");
    }

    public void End_Drag()
    {
        is_dragging = false;
        Debug.Log("BBB");
    }
}
