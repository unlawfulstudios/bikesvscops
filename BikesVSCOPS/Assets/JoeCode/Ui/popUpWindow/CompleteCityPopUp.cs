﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompleteCityPopUp : Info_POPUPWindow
{
    public Image current_UnlockedCity;
    public int unlockID = 0;

    protected override void PopUp_Window_Open()
    {
        base.PopUp_Window_Open();
        Set_City_Image();
    }


    public void Set_City_Image()
    {
        current_UnlockedCity.sprite = GlobaleManagers.City_Data_Manager.citys[unlockID].CityICON;
       
    }

}
