﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class Unlockedbike_Popup : Info_POPUPWindow
{
    public int bikeUnlockedId = 0;
    public RawImage currenntIamgeOfBikeUnlocked;
    public  Bike Current_Bike() { return GlobaleManagers.current_UserProfile.BikeList[bikeUnlockedId]; }
    public Text BikeName_Text;


    protected override void PopUp_Window_Open()
    {
        base.PopUp_Window_Open();

        Set_BikeName();
        Set_UnlockedBikeImage();
    }


    public void Set_UnlockedBikeImage()
    {
        currenntIamgeOfBikeUnlocked.texture = GlobaleManagers.BikeInventoryItemList.bike_Data_list[bikeUnlockedId].Icon_Iamge;
    }
    public void Set_BikeName()
    {
         BikeName_Text.text =  Current_Bike().Name;
    }
    
}
