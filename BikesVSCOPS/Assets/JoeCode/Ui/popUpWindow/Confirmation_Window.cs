﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Confirmation_Window : MonoSingleton<Confirmation_Window>
{
    static int ConfirmationId = 0;
    public GameObject ConfirmWindowObject;
    public Text ConfrimationText;

    public static int addAMount = 0;
    public static float price;
    public AddVauleType addVauleTYPE;
    protected override void Init() { ConfirmWindowObject.SetActive(false); }
   private string info;

    public void Toggle(bool toggle, int infoID, AddVauleType _addVauleTYPE, int Amount,float _price)
    {
        addVauleTYPE = _addVauleTYPE;
        if (_addVauleTYPE != AddVauleType.Bikes)
        {
            price = _price;
        }
        else
        {
            price = GlobaleManagers.current_UserProfile.BikeList[GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].attributes.purchase_Price;
        }
        ConfirmationId = infoID;
        addAMount = Amount;
        ConfirmWindowObject.SetActive(toggle);

        if (_addVauleTYPE == AddVauleType.Invalid)
            return;


        if (toggle)
        {
        
            if (_addVauleTYPE == AddVauleType.NITROUS)
            {
                info = "HITS";
                ConfrimationText.text = "ADD " + Amount.ToString() + " " + _addVauleTYPE.ToString() + " " + info;
            }
            else if(_addVauleTYPE == AddVauleType.TRIES)
            {
                info = "";
                ConfrimationText.text = "ADD " + Amount.ToString() + " " + _addVauleTYPE.ToString() + " " + info;
            }
            else if(_addVauleTYPE == AddVauleType.Bikes)
            {
                ConfrimationText.text = "PURCHASE BIKE " +  GlobaleManagers.current_UserProfile.BikeList[ GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].attributes.BikeID;
                

            }
                
        }
        else
        {

            bool canBuy = GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.AmountOfMoney.value >= price;
            if (canBuy)
            {
                if (_addVauleTYPE == AddVauleType.TRIES)
                {
                     GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.GetAwayTries.value += Amount;
                }
                else if (_addVauleTYPE == AddVauleType.NITROUS)
                {
                     GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value += Amount;
                }
                else if(_addVauleTYPE == AddVauleType.Bikes)
                {
                     GlobaleManagers.current_UserProfile.BikeList[ GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_HolderID.Value].UNLOCKED.value = true;
                }

                 GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.AmountOfMoney.value -= price;
            }
           GlobaleManagers.PopUpManagar.CLOSE();
        }
          
        

    }

}
