﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JS.BVSC.BVSC_Behavior;

public class Pop_UpWindow : BVSC_BehaviorBase {

    public override void Process_OnEnable()
    {
        PopUp_Window_Open();
    }

    protected virtual void PopUp_Window_Open(){}
    public virtual void Toggle(bool toggle) {this.gameObject.SetActive(toggle);}

}
