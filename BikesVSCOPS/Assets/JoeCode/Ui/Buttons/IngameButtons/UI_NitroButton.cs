﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_NitroButton : InGameButton
{
    public Image fillimage;
    public Text boostAmountText;
    private string boostAmountSting { get { return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value.ToString(); } }
    public GameObject BoostBackGround;

    protected override void Start()
    {
        base.Start();
        if (!InGameEventManager.Active)
            return;
        Set_Text();

    }
    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        if (!InGameEventManager.Active)
            return;


        if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value <= 0)
        {
            GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value = 0;
        }
        else
        {

            GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value -= 1;
            if (InGameManagers.Racer_Manager.Player_BikeControl)
            {
                InGameManagers.Racer_Manager.Player_BikeControl.Boost();
            }

            Set_Text();
        }
        if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value == 0)
        {
            interactable = false;
        }

    }
    public void Update()
    {
        if (InGameManagers == null)
            return;

        if (InGameManagers.Racer_Manager.Player_BikeControl == null)
            return;

        if (!InGameManagers.Racer_Manager.Player_BikeControl.BOOST)
        {
            if (fillimage.fillAmount != 1)
            {
                fillimage.fillAmount = 1;
            }
        }
        else
        {
            fillimage.fillAmount -= Mathf.Lerp(0, 1, 0.5f) * Time.deltaTime;
        }
        if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value <= 0 && fillimage.fillAmount == 0)
        {
            if (BoostBackGround.activeInHierarchy)
            {
                BoostBackGround.SetActive(false);
            }
        }
   

    }
    public void Set_Text()
    {
        boostAmountText.text = GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.BoostAmount.value == 0 ? "" : boostAmountSting;
    }


}
