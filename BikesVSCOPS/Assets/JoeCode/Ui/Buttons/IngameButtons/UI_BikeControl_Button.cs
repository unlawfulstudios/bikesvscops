﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_BikeControl_Button : InGameButton
{

    [SerializeField]
    protected BikeCamera bikeCamera;

    protected float Min = -1;
    protected float Max = 1;
    protected float zed = 0;

    protected override void Button_Press(int f)
    {
        base.Button_Press();
        Press_BUTTON();

        InGameManagers.ingameSoundManager.Resuem_Sound();

        if (isPauseButton)
            InGameManagers.ingameSoundManager.Pause_Sound();


        if (InGameEventManager.isPaused)
            InGameManagers.ingameSoundManager.Pause_Sound();
    }

    protected virtual void Press_BUTTON(){}



}
