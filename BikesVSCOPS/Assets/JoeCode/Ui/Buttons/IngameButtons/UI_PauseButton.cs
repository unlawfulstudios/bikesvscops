﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_PauseButton : InGameButton {
 


    protected override void Button_Press(int f)
    {
        InGameManagers.ingameSoundManager.toggle_Sounds(false);
        StartCoroutine(_pause());
      
    }

    IEnumerator _pause ()
    {
        InGameManagers.ingameSoundManager.toggle_Sounds(false);
        InGameManagers.ingameSoundManager.disable_Sounds();
        yield return new WaitForSeconds(0.1f);
        InGameManagers.ingameSoundManager.toggle_Sounds(false);
        InGameManagers.InGameStateManager.pause_Game();
    }
 
}
