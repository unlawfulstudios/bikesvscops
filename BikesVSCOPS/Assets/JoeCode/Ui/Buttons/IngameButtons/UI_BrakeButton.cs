﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_BrakeButton : UI_BikeControl_Button
{
    protected override void Press_BUTTON()
    {
        base.Button_Press();

        InGameManagers.Racer_Manager.Player_BikeControl.brake = true;
    }

    public override void OnButton_Up()
    {
        base.OnButton_Up();

        InGameManagers.Racer_Manager.Player_BikeControl.brake = false;
        InGameManagers.Racer_Manager.Player_BikeControl.accelBack = 0;

    }

    public override void OnButton_Down()
    {
        base.OnButton_Down();

        bikeCamera.BikeHandBrake(true);

    }
#if !UNITY_EDITOR
    public void Update()
    {

        if (bikeCamera == null && InGameManagers != null)
            return;

        if (InGameManagers.Racer_Manager.Player_BikeControl.brake && InGameManagers.Racer_Manager.Player_BikeControl.IsStoped())
        {
            InGameManagers.Racer_Manager.Player_BikeControl.brake = false;
            InGameManagers.Racer_Manager.Player_BikeControl.accelBack = -1;
        }

    }
#endif
}
