﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ResumeButton : InGameButton
{
    protected override void Button_Press(int f)
    {
        buttonAtrabutties.Button_animator.SetTrigger("UnPause");
        Time.timeScale = 1;
        StartCoroutine(Resume());
    }

    IEnumerator Resume()
    {

        yield return new WaitForSeconds(0.1f);
        InGameManagers.InGameStateManager.resume_Game();

    }
 
}
