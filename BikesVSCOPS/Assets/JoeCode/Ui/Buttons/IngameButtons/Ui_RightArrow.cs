﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_RightArrow : UI_BikeControl_Button
{
    protected override void Press_BUTTON()
    {
        base.Press_BUTTON();
        bikeCamera.BikeSteer(Max); 
    }

    public override void OnButton_Up()
    {
        base.OnButton_Up();
        bikeCamera.BikeSteer(zed);
    }

    public override void OnButton_Down()
    {
        base.OnButton_Down();

        bikeCamera.BikeSteer(Max);
    }

}
