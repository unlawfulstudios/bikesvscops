﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InGameButton : UI_Button
{
    public InGameManagers InGameManagers;

    [Inject]
    public void InjectConstruct(InGameManagers _InGameManagers)
    {
        InGameManagers = _InGameManagers;
    }

    protected override void Button_Press(int id)
    {
        base.Button_Press(id);

        if (buttonAtrabutties.canTriggerStart)
            InGameEventManager.Button_Pressed = true;

        if (if_DisableSound)
        {
            InGameManagers.ingameSoundManager.disable_Sounds();
        }
    }

    public override void PlaySound()
    {
        InGameManagers.ingameSoundManager.PlayerButtonSound();
    }

}
