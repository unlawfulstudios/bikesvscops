﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UI_RestartButton : InGameButton
{

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        Time.timeScale = 1;
        InGameManagers.ingameSoundManager.disable_Sounds();

    }
}
    
