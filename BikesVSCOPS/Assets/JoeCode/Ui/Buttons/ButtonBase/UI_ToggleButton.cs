﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ToggleButton : UI_Button {

    public int press_Amount = 0;
 
    protected override void Awake()
    {
        base.Awake();

        press_Amount = 0;
    }

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
      
        if (press_Amount < 1)
        {
            press_Amount++;
        }
        else
            press_Amount = 0;


        if (press_Amount == 1)
        {
            Activate();
        }
        else
        {
            De_Activate();         
        }

        Debug.Log("press button" + press_Amount);
    }
    protected virtual void Activate() { }
    protected virtual void De_Activate() { }

 

}
