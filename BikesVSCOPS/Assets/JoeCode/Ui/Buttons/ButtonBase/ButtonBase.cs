﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Zenject;

[System.Serializable]
public class ButtonAtrabutties
{

    public bool ChangesController = false;
    public bool isintractableAfterPress = false;
    public bool InteractableOnStart = false;
    public bool HasSound = false;
    public bool PlayAnimator = false;
    public Animator Button_animator;
    public bool LoadsAfter = false;
    public bool ChangesScene = false;
    public bool ReLoadButton;
    public bool canTriggerStart = false;
    public bool SetTimeScaleTo_0 = false;
    public bool SetTimeScaleTo_1 = false;
    public bool ShowCurrent = true;

}


public class ButtonBase : Button 
{

    public static bool button_Pressed = false;
    public int ButtonID = 0;

    public GlobaleManagers GlobaleManagers;


    [Inject]
    public void On_Inject(
     GlobaleManagers _GlobaleManagers
     )
    {
        GlobaleManagers = _GlobaleManagers;
     
    }

    public ButtonAtrabutties buttonAtrabutties;
    

    protected override void OnEnable()
    {
        base.OnEnable();

        if (this.GetType() != typeof(UI_Run_Button))
        {
            interactable = buttonAtrabutties.InteractableOnStart;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        onClick.AddListener(delegate { Button_Press(ButtonID); });
    }

    public void Set_interactable(bool toggle) {interactable = toggle; }


    protected virtual void Button_Press(int f) { Press_Button_Detection(); if (!interactable) return; }

    protected virtual void Button_Press() { Press_Button_Detection(); if (!interactable) return; }

    public virtual void OnButton_Up() { Press_Button_Detection(); }

    public virtual void OnButton_Down() { Press_Button_Detection(); }

    public virtual void PlaySound() { }

    protected virtual void Change_Controller(int controllerID) { }

    private void ButtonPress_Invoke()
    {
    }

    private void Press_Button_Detection()
    {
    }

}
