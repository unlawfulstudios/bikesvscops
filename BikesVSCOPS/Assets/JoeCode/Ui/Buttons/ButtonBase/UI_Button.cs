﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;


public class UI_Button : ButtonBase
{

  
    public bool isPauseButton = false;
    public bool if_DisableSound = false;
    public string SceneName  = "Invalid";
   
    public bool goseToMainMenu = false;

    protected override void Button_Press(int id)
    {
        if (buttonAtrabutties.isintractableAfterPress)
            Set_interactable(false);

       // if (buttonAtrabutties.HasSound)
            PlaySound();

        if (buttonAtrabutties.ChangesController)
        {
            Change_Controller(id);
        }
        if (buttonAtrabutties.LoadsAfter)
        {
           GlobaleManagers.scene_Manager.Load_Scene(SceneName, goseToMainMenu, id, buttonAtrabutties.ShowCurrent);
        }
        if (buttonAtrabutties.SetTimeScaleTo_1)
            GlobaleManagers.scene_Manager.AppTimeScale = 1;

        Debug.Log("Button_Press");
    }

  

    protected override void Change_Controller(int controllerID)
    {
        UI_Manager.Instance.Change_Controller(controllerID);
    }

  

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        OnButton_Up();
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        OnButton_Up();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);

    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        OnButton_Down();

        if (buttonAtrabutties.canTriggerStart)
            InGameEventManager.Button_Pressed = true;
    }

  
}



