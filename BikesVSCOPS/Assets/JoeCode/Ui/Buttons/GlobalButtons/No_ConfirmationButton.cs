﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class No_ConfirmationButton : Confirmation_Button
{
    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        Confirmation_Window.Instance.Toggle(false, 1, AddVauleType.Invalid, 0,0);
    }
}
