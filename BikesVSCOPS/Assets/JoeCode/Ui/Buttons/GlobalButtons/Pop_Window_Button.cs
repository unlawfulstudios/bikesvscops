﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pop_Window_Button : UI_Button {

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        GlobaleManagers.PopUpManagar.OpenInfo(f);

    }

}
