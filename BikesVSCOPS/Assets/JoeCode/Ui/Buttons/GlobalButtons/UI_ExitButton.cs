﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ExitButton : UI_Button {

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        GlobaleManagers.PopUpManagar.CLOSE();

    }

}
