﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Ui_NewUserButton :  Global_Button{

    // Use this for initialization

    protected override void Button_Press(int id)
    {
        base.Button_Press(id);

       GlobaleManagers.authManager.CreateUserAsync();
    }

  

}
