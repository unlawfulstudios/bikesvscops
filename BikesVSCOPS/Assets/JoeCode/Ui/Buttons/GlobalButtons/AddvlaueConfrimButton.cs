﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddvlaueConfrimButton : Global_Button {

    public int Amount = 0;
    public float DollarAmount = 1.00f;
    public AddVauleType current_AddVauleType = AddVauleType.Invalid;

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        Confirmation_Window.Instance.Toggle(true,1, current_AddVauleType,Amount, DollarAmount);
       
    }
}
