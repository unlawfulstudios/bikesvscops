﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yes_ConfirmationButton : Confirmation_Button
{
    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        ConFirm();
    }


    public void ConFirm()
    {
        Confirmation_Window.Instance.Toggle(false, 1, Confirmation_Window.Instance.addVauleTYPE, Confirmation_Window.addAMount,Confirmation_Window.price);
        
        GlobaleManagers.UserProfile_Manager.UpdateProfile();
    }
}
