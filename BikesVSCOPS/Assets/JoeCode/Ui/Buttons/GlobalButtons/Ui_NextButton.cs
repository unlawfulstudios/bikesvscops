﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_NextButton : UI_Button
{
    public RUN_DATA rUN_DATA;

    protected override void Awake()
    {
        base.Awake();
        buttonAtrabutties.ShowCurrent = false;
    }
    protected override void Button_Press(int f)
    {
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run = rUN_DATA.Rundata;
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.next_Run = GlobaleManagers.current_UserProfile.CAREER_Citys[0].Runs[rUN_DATA.RUNID-1];
        base.Button_Press(f);
    }

}