﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SettingButton : UI_ToggleButton {

    public GameObject OnImage;
    public GameObject OffImage;

    public bool OnAtStart = false;

    protected override void Awake()
    {

        base.Awake();

        if (OnAtStart)
            Set_On();
        else
            Set_OFF();
    }

    protected override void Activate()
    {
        Set_On();
    }
    protected override void De_Activate()
    {
        Set_OFF();
    }
    protected virtual void Set_On() { Toggle(true);}
    protected virtual void Set_OFF() { Toggle(false); }

    private void Toggle(bool toggle)
    {
        OnImage.SetActive(toggle);
        OffImage.SetActive(!toggle);
    }
	

}
