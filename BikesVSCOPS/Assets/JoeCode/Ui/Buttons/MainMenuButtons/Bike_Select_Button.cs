﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bike_Select_Button : UI_Button
{

    [SerializeField]
    public Bike current_Bike;

    public GameObject LockImage;
    public GameObject ModleView;
    public Button AmountButton;

    public Transform statsBar;
    public Text bikeNameText;
    public Text PriceText;

    public RenderTexture bike_Cam_Texture;
    public RawImage bikeImage;

    public Camera Bike_Cam;

    #region Current Bike Attributes 

    public float speed_FillAmount { get { if (current_Bike != null) { return current_Bike.attributes.MaxSpeed / 10; } else { return 0; } } }
    public float acceleration_FillAmount { get { if (current_Bike != null) { return current_Bike.attributes.Acceleration / 10; } else { return 0; } } }
    public float handling_FillAmount { get { if (current_Bike != null) { return current_Bike.attributes.Handling / 10; } else { return 0; } } }
    public float purchasePrice_FillAmount { get { if (current_Bike != null) { return current_Bike.attributes.purchase_Price / 10; } else { return 0; } } }
    public bool BikedOnLocked { get { if (current_Bike != null) { return current_Bike.UNLOCKED.value; } else { return false; } } }

    #endregion

    protected override void OnEnable()
    {
        base.OnEnable();

        if (LockImage != null)
        {
            LockImage.SetActive(!BikedOnLocked);
            ModleView.SetActive(BikedOnLocked);

        }

    }
    protected override void Awake()
    {
        base.Awake();

    }




    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        SelectBike();
    }

    public void SelectBike()
    {
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike = current_Bike;
        UI_Manager.Instance.ChoseCity();
    }



    #if (UNITY_EDITOR)
    public void SetButtON()
    {
      
        if (GlobaleManagers.current_UserProfile.BikeList.Length > 0)
        {
            if (GlobaleManagers.current_UserProfile.BikeList[ButtonID] != null)
            {
                current_Bike = GlobaleManagers.current_UserProfile.BikeList[ButtonID];

                bikeNameText.text = current_Bike.attributes.BikeID.ToString();

                if (!current_Bike.UNLOCKED.value)
                {
                    AmountButton.gameObject.SetActive(true);

                    AmountButton.GetComponentInChildren<Text>().text = " OR BUY FOR $" + current_Bike.attributes.purchase_Price;
                }
                else
                {

                    AmountButton.gameObject.SetActive(false);

                }

                statsBar.GetChild(0).GetComponent<Image>().fillAmount = speed_FillAmount;

                statsBar.GetChild(1).GetComponent<Image>().fillAmount = acceleration_FillAmount;

                statsBar.GetChild(2).GetComponent<Image>().fillAmount = handling_FillAmount;

                PriceText.text = "OR BUY FOR $" +  purchasePrice_FillAmount.ToString();

       
            
             

                LockImage.SetActive(!BikedOnLocked);

                ModleView.SetActive(BikedOnLocked);

            }

        }


    }
    #endif
}
