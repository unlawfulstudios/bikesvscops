﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garage_BottomPanelButton : UI_ToggleButton {

    public BottomPanelType bottomPanelType;

    public  Garage_BottomPanelButton otherButton;

    

    protected override void Awake()
    {
        base.Awake();
    

    }


    protected override void Activate()
    {
        if (Garage_BottomPanel.Instance.PanelOpen)
            StartCoroutine("AlradyOpen");
        else
            Garage_BottomPanel.Instance.Toggle_BottomPanel(true, bottomPanelType);
    }

    protected override void De_Activate()
    {
        Garage_BottomPanel.Instance.Toggle_BottomPanel(false, bottomPanelType);
    }

    IEnumerator AlradyOpen()
    {
        De_Activate();
        yield return new WaitForSeconds(0.5f);
        Activate();
        otherButton.press_Amount = 0;

   }

}
