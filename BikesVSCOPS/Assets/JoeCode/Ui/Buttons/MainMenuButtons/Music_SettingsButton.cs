﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_SettingsButton : SettingButton {

    protected override void Awake()
    {

        OnAtStart = GlobaleManagers.current_UserProfile.game_Settings_Manager.Has_Sound.Value;

        base.Awake();


        if(!OnAtStart)
        {
            press_Amount = 0;
        }
    }

    protected override void Set_On()
    {
        base.Set_On();

        GlobaleManagers.current_UserProfile.game_Settings_Manager.Has_Sound.Value = true;
    }
    protected override void Set_OFF()
    {
        base.Set_OFF();

        GlobaleManagers.current_UserProfile.game_Settings_Manager.Has_Sound.Value = false;
    }
}
