﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Career_UI_Button : GameMode_Buttons {

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        Set_GameMode(GameMode_Type.CAREER_MODE);
    }

   
}
