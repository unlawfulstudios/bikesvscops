﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Equip_Button : UI_Button {


    public Text equipped_text;
    public bool PopUP = false;
    public int UnlockID = 0;

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        Debug.Log("Press");
        if (!PopUP)
        {
            Debug.Log("Press");
            // set_CurrentBike(Bike_HolderManager.Instance.CurrentBike);

        }
        else
        {
            set_CurrentBike(GlobaleManagers.current_UserProfile.BikeList[UnlockID]);

            GlobaleManagers.PopUpManagar.CLOSE();
        }
        
    }

    public void Update()
    {
        if (PopUP)
            return;

        //if (UserProfile.Instance.bikesVSCopsProfileInfo.Current_Bike == Bike_HolderManager.Instance.CurrentBike)
        //{
        //    equipped_text.text = "[ EQUIPPED BIKE ]";
        //}
        //else
        //{
        //    equipped_text.text = "[ USE BIKE ]";
        //}
    }
    private void set_CurrentBike(Bike currentBike)
    {
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike = currentBike;
        GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.Current_Bike_iD.Value = currentBike.attributes.BikeID - 1;
    }
}
