﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_FreeRideButton : GameMode_Buttons
{

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);
        Set_GameMode(GameMode_Type.FREERIDE_MODE);
    }

}
