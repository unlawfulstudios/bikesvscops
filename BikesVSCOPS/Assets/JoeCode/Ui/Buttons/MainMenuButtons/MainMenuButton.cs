﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MainMenuButton : UI_Button
{

    public MainMenu_Managers MainMenu_Managers;


    [Inject]
    public void On_Inject(MainMenu_Managers _MainMenu_Managers)
    {
        MainMenu_Managers = _MainMenu_Managers;
    }


    public override void PlaySound()
    {
        MainMenu_Managers.MainMenuSoundManager.PlayerButtonSound();
     
    }
 
   
}