﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class Arrow_Button : MainMenuButton {

    public ArrowType current_ArrowType;
    public Garage_Manager Garage_Manager;

    [Inject]
    public void Construct(Garage_Manager _Garage_Manager)
    {
        Garage_Manager = _Garage_Manager;
    }

    protected override void Button_Press(int f)
    {
        base.Button_Press(f);

        if (current_ArrowType == ArrowType.Left)
        {
            GlobaleManagers.BikeHolder_CameraManager.Move_Left();
        }
        else
        {
            GlobaleManagers.BikeHolder_CameraManager.Move_Right();
        }
        
        Garage_Manager.SetBike();

    }
}
