﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacePoints : InGameBehaviors
{

    [SerializeField]
    GameObject racePoint_GameObject;

    public void Toggle(bool toggle,bool is_NPC)
    {

        if (!toggle)
        {
            if (!is_NPC)
                Player_RaceEvent();
            else
                Ai_RaceEvent();
        }
    }

    protected virtual void Player_RaceEvent() { }
    protected virtual void Ai_RaceEvent() { }


}
