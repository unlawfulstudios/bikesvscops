﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BikeSettings
{
    public bool showNormalGizmos = false;
    public GroundHits[] HitGround = new GroundHits[2];
    public Transform bikerMan;
    public List<Transform> cameraSwitchView;
    public Transform MainBody;
    public Transform bikeSteer;
    public float maxWheelie = 40.0f;
    public float speedWheelie = 30.0f;
    public float slipBrake = 3.0f;
    public float springs = 35000.0f;
    public float dampers = 4000.0f;
    public float shiftPower = 150;
    public float brakePower = 8000;

    public Vector3 shiftCentre = new Vector3(0.0f, -0.6f, 0.0f); // offset of centre of mass
    public BikeAttributes bikeAttributes;


    public float shiftDownRPM = 1500.0f; // rpm script will shift gear down
    public float shiftUpRPM = 4000.0f; // rpm script will shift gear up
    public float idleRPM = 700.0f; // idle rpm

    public float stiffness = 1.0f; // for WheelsBar, determines slip



    public bool automaticGear = true;

    public float[] gears = { -10f, 9f, 6f, 4.5f, 3f, 2.5f }; // gear ratios (index 0 is reverse)

    public float LimitBackwardSpeed = 60.0f;

    public float LimitdSpeedBoost { get { return bikeAttributes.MaxSpeed + 25; } }


    public bool crash = false;

}
