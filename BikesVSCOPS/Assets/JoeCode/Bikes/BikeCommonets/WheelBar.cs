﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WheelBar
{

    public Transform wheelFront; // connect to Front Right Wheel transform
    public Transform wheelBack; // connect to Front Left Wheel transform

    public Transform AxleFront; // connect to Back Right Wheel transform
    public Transform AxleBack; // connect to Back Left Wheel transform

}
