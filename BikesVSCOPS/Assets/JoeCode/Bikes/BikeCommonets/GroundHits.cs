﻿using UnityEngine;

[System.Serializable]
public class GroundHits
{
    public string tag = "Street";
    public bool grounded = false;
    public AudioClip brakeSound;
    public AudioClip groundSound;
    public Color brakeColor;

}