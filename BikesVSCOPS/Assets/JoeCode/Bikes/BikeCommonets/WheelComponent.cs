﻿using UnityEngine;

[System.Serializable]
public class WheelComponent
{
    public Transform wheel;
    public Transform axle;
    public WheelCollider collider;
    public Vector3 startPos;
    public float rotation = 0.0f;
    public float maxSteer;
    public bool drive;
    public float pos_y = 0.0f;
}
