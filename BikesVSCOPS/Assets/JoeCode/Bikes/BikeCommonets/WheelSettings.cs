﻿
[System.Serializable]
public class WheelSettings
{
    public float Radius = 0.3f; // the radius of the WheelsBar
    public float Weight = 1000.0f; // the weight of a wheel
    public float Distance = 0.2f;

}
