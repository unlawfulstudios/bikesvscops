﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BikeParticles
{
    public GameObject brakeParticlePrefab;
    public ParticleSystem shiftParticle1, shiftParticle2;
}
