﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BikeSounds
{
    public AudioSource IdleEngine, LowEngine, HighEngine;
    public AudioSource lowCrash;
    public AudioSource nitro;
    public AudioSource switchGear;

    public void disableSound()
    {
        IdleEngine.enabled = false;
        LowEngine.enabled = false;
        HighEngine.enabled = false;
        lowCrash.enabled = false;
        nitro.enabled = false;
        switchGear.enabled = false;
    }
    public void enableSound()
    {
        IdleEngine.enabled = true;
        LowEngine.enabled = true;
        HighEngine.enabled = true;
        lowCrash.enabled = true;
        nitro.enabled = true;
        switchGear.enabled = true;
    }

}