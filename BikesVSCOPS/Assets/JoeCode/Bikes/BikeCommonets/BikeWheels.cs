﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BikeWheels {

    public WheelBar WheelsBar;
    public WheelSettings setting;
}
