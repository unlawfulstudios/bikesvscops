﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
public static class CustomWindoweEditorTools  {

    public static EditorWindow GetOrCreateWindow<T>()
    {
        var window = EditorWindow.GetWindow(typeof(T));
        window.titleContent = new GUIContent(window.GetType().Name);
        return window ;
    }
 
}
