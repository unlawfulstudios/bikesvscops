﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Zenject;





public class BikeInventoryItemListEditor : EditorWindow
{

    public BikeInventoryItemList inventoryItemList;
    private int viewIndex = 1;

    [MenuItem("Bikes VS Cops/BikeInventory Item Editor %#e")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(BikeInventoryItemListEditor));
    }

    void OnEnable()
    {
        if (EditorPrefs.HasKey("BikeInventoryItemList"))
        {
            string objectPath = EditorPrefs.GetString("BikeInventoryItemList");
            inventoryItemList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(BikeInventoryItemList)) as BikeInventoryItemList;
        }

    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Bike Inventory Item Editor", EditorStyles.boldLabel);
        if (inventoryItemList != null)
        {
            if (GUILayout.Button("Show Item List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = inventoryItemList;
            }
        }
        if (GUILayout.Button("Open Item List"))
        {
            OpenItemList();
        }
        if (GUILayout.Button("New Item List"))
        {
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = inventoryItemList;
        }
        GUILayout.EndHorizontal();

        if (inventoryItemList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Bike Item List", GUILayout.ExpandWidth(false)))
            {
                CreateNewItemList();
            }
            if (GUILayout.Button("Open Existing Bike Item List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (inventoryItemList != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex > 1)
                    viewIndex--;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex < inventoryItemList.bike_Data_list.Count)
                {
                    viewIndex++;
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Bike", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Remove Bike", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();
            if (inventoryItemList.bike_Data_list == null)
                Debug.Log("wtf");
            if (inventoryItemList.bike_Data_list.Count > 0)
            {
                GUILayout.BeginHorizontal();
                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Bike Data ", viewIndex, GUILayout.ExpandWidth(false)), 1, inventoryItemList.bike_Data_list.Count);
                //Mathf.Clamp (viewIndex, 1, inventoryItemList.itemList.Count);
                EditorGUILayout.LabelField("of   " + inventoryItemList.bike_Data_list.Count.ToString() + "  Bikes", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();


                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.BikeID = viewIndex;
                inventoryItemList.bike_Data_list[viewIndex - 1].Icon_Iamge = EditorGUILayout.ObjectField("Icon_Iamge", inventoryItemList.bike_Data_list[viewIndex - 1].Icon_Iamge, typeof(RenderTexture), false) as RenderTexture;
                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.purchase_Price = EditorGUILayout.FloatField("Purchase_Price", inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.purchase_Price, GUILayout.ExpandWidth(false));


                GUILayout.Space(20);


                //: TODO: Change to class 
                EditorGUILayout.LabelField("----------------------------Attributes------------------------------------");

                GUILayout.Space(10);


                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.MaxSpeed = EditorGUILayout.FloatField("MaxSpeed", inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.MaxSpeed, GUILayout.ExpandWidth(false));
                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.Acceleration = EditorGUILayout.FloatField("Acceleration", inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.Acceleration, GUILayout.ExpandWidth(false));
                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.Handling = EditorGUILayout.FloatField("Handling", inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.Handling, GUILayout.ExpandWidth(false));
                inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.MaxTiltAngle = EditorGUILayout.FloatField("MaxTiltAngle", inventoryItemList.bike_Data_list[viewIndex - 1].bikeAttributes.MaxTiltAngle, GUILayout.ExpandWidth(false));
                //inventoryItemList.itemList[viewIndex - 1].isUnique = (bool)EditorGUILayout.Toggle("Unique", inventoryItemList.itemList[viewIndex - 1].isUnique, GUILayout.ExpandWidth(false));
                //inventoryItemList.itemList[viewIndex - 1].isIndestructible = (bool)EditorGUILayout.Toggle("Indestructable", inventoryItemList.itemList[viewIndex - 1].isIndestructible, GUILayout.ExpandWidth(false));
                //inventoryItemList.itemList[viewIndex - 1].isQuestItem = (bool)EditorGUILayout.Toggle("QuestItem", inventoryItemList.itemList[viewIndex - 1].isQuestItem, GUILayout.ExpandWidth(false));
                //GUILayout.EndHorizontal();

                //GUILayout.Space(10);

                //GUILayout.BeginHorizontal();
                //inventoryItemList.itemList[viewIndex - 1].isStackable = (bool)EditorGUILayout.Toggle("Stackable ", inventoryItemList.itemList[viewIndex - 1].isStackable, GUILayout.ExpandWidth(false));
                //inventoryItemList.itemList[viewIndex - 1].destroyOnUse = (bool)EditorGUILayout.Toggle("Destroy On Use", inventoryItemList.itemList[viewIndex - 1].destroyOnUse, GUILayout.ExpandWidth(false));
                //inventoryItemList.itemList[viewIndex - 1].encumbranceValue = EditorGUILayout.FloatField("Encumberance", inventoryItemList.itemList[viewIndex - 1].encumbranceValue, GUILayout.ExpandWidth(false));


                GUILayout.Space(10);

            }
            else
            {
                GUILayout.Label("This Inventory List is Empty.");
            }
        }
        if (GUI.changed)
        {
            EditorUtility.SetDirty(inventoryItemList);
        }
    }

    void CreateNewItemList()
    {
        // There is no overwrite protection here!
        // There is No "Are you sure you want to overwrite your existing object?" if it exists.
        // This should probably get a string from the user to create a new name and pass it ...
        viewIndex = 1;
        inventoryItemList = CreateBikeDataInventoryItemList.Create();
        if (inventoryItemList)
        {
            inventoryItemList.bike_Data_list = new List<Bike_Data>();
            string relPath = AssetDatabase.GetAssetPath(inventoryItemList);
            EditorPrefs.SetString("BikeInventoryItemList", relPath);
        }
    }

    void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Inventory Item List", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            inventoryItemList = AssetDatabase.LoadAssetAtPath(relPath, typeof(BikeInventoryItemList)) as BikeInventoryItemList;
            if (inventoryItemList.bike_Data_list == null)
                inventoryItemList.bike_Data_list = new List<Bike_Data>();
            if (inventoryItemList)
            {
                EditorPrefs.SetString("BikeInventoryItemList", relPath);
            }
        }
    }

    void AddItem()
    {
        Bike_Data newItem = new Bike_Data();
        inventoryItemList.bike_Data_list.Add(newItem);
        viewIndex = inventoryItemList.bike_Data_list.Count;
    }

    void DeleteItem(int index)
    {
        inventoryItemList.bike_Data_list.RemoveAt(index);
    }
}
