﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(Bike_Select_Button))]
public class BikeHolderButtonEditor : Editor
{

    public bool CanSetObject(Bike_Select_Button bikebeingChecked)
    {
        if (bikebeingChecked.statsBar == null ||
             bikebeingChecked.LockImage == null ||
             bikebeingChecked.ModleView == null ||
             bikebeingChecked.bikeNameText == null || 
             bikebeingChecked.AmountButton == null
            )
        {
            return false;
        }
        else return true;


        
       
    }
    public override void OnInspectorGUI()
    {

        base.DrawDefaultInspector();

        Bike_Select_Button bikeSelectButton = (Bike_Select_Button)target;

        if (!CanSetObject(bikeSelectButton))
        {
            bikeSelectButton.statsBar = EditorGUILayout.ObjectField("Stat Bar", bikeSelectButton.statsBar, typeof(Transform), true) as Transform;
            bikeSelectButton.LockImage = EditorGUILayout.ObjectField("LockImage Bar", bikeSelectButton.LockImage, typeof(GameObject), true) as GameObject;
            bikeSelectButton.ModleView = EditorGUILayout.ObjectField("ModleView Bar", bikeSelectButton.ModleView, typeof(GameObject), true) as GameObject;
            bikeSelectButton.bikeNameText = EditorGUILayout.ObjectField("BikeNameText ", bikeSelectButton.bikeNameText, typeof(Text), true) as Text;
            bikeSelectButton.AmountButton = EditorGUILayout.ObjectField("AmountButton ", bikeSelectButton.AmountButton, typeof(Button), true) as Button;
        }

        if (bikeSelectButton.statsBar != null)
        {
            bikeSelectButton.ButtonID = EditorGUILayout.IntField("Button Id ", bikeSelectButton.ButtonID, GUILayout.ExpandWidth(false));

            if (GUILayout.Button("Set Object"))
            {

                bikeSelectButton.SetButtON();

            }


            if (bikeSelectButton.current_Bike != null)
            {
                EditorGUILayout.LabelField("----------------------------" + bikeSelectButton.current_Bike.attributes.BikeID + "------------------------------------" as string);


                EditorGUILayout.LabelField("Unlocked :  " + bikeSelectButton.current_Bike.UNLOCKED.Value as string, GUILayout.ExpandWidth(false));
                EditorGUILayout.LabelField("Purchase_Price :  " + bikeSelectButton.current_Bike.attributes.purchase_Price as string, GUILayout.ExpandWidth(false));


                GUILayout.Space(20);


                //: TODO: Change to class 
                EditorGUILayout.LabelField("----------------------------Attributes------------------------------------");

                GUILayout.Space(10);

                EditorGUILayout.LabelField("Speed  :  " + bikeSelectButton.current_Bike.attributes.MaxSpeed as string, GUILayout.ExpandWidth(false));
                EditorGUILayout.LabelField("Acceleration  :  " + bikeSelectButton.current_Bike.attributes.Acceleration as string, GUILayout.ExpandWidth(false));
                EditorGUILayout.LabelField("Handling  :  " + bikeSelectButton.current_Bike.attributes.Handling as string, GUILayout.ExpandWidth(false));
            }
        }








        if (GUI.changed)
        {
            EditorUtility.SetDirty(bikeSelectButton);
        }



    }
}
