﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateBikeDataInventoryItemList  {



    [MenuItem("Assets/Create/Inventory Item List")]
    public static BikeInventoryItemList Create()
    {
        BikeInventoryItemList asset = ScriptableObject.CreateInstance<BikeInventoryItemList>();

        AssetDatabase.CreateAsset(asset, "Assets/InventoryItemList.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
 
}
