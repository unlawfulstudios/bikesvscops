﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class UIEditorManager : EditorWindow
{
    public int index_View = 0;
    // Add menu named "My Window" to the Window menu
    [MenuItem("Tools/UIEditorManager")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        UIEditorManager window = (UIEditorManager)EditorWindow.GetWindow(typeof(UIEditorManager));
        window.Show();
    }

    bool Toggle_Panle = false;
    bool Toggle_SplashSCreen = false;
    bool Toggle_PopWindow = false;
    void OnGUI()
    {
        UI_BUttonSetUP();

    }


    public void UI_BUttonSetUP()
    {

        if (Scene_Manager.in_Game())
            return;

        if (GUILayout.Button("Debug_SHutOFF ALL Controllers "))
        {
            UI_Manager.Instance.SetALLOFF_DEBUGE();

        }



        Toggle_SplashSCreen = EditorGUILayout.Toggle(" Tooble_SplashSCreen : ", Toggle_SplashSCreen);
        Toggle_PopWindow = EditorGUILayout.Toggle(" Toggle_PopWindow : ", Toggle_PopWindow);



 

        if (Toggle_SplashSCreen)
        {
            UI_Manager.Instance.SetALLOFF_DEBUGE();
        }
        else
        {
            if (GUILayout.Button("GoHome"))
            {
                UI_Manager.Instance.GoBackHome();
            }
            if (GUILayout.Button("Go To Garage"))
            {
                UI_Manager.Instance.GoToGarage();
            }
            if (GUILayout.Button("Pick RUN"))
            {
                UI_Manager.Instance.ChoseRUN();
            }

            if (GUILayout.Button("Pick Career"))
            {
                UI_Manager.Instance.GotoCarrer();
            }
            if (GUILayout.Button("Pick City"))
            {
                UI_Manager.Instance.ChoseCity();
            }
            if (GUILayout.Button("Pick RUN"))
            {
                UI_Manager.Instance.ChoseRUN();
            }
            if (GUILayout.Button("GoToLoad"))
            {
                UI_Manager.Instance.GoToLoad();
            }

            if (GUILayout.Button("GoBack"))
            {
                UI_Manager.Instance.goBack();
            }
            if (GUILayout.Button("Go Settings"))
            {
                UI_Manager.Instance.GoTo_Settings();
            }

            if (GUILayout.Button("Go RIVALRY MENU"))
            {
                UI_Manager.Instance.Change_Controller(7);
            }
        }

        if (GUI.changed)
            EditorUtility.SetDirty(this);
    }
}
