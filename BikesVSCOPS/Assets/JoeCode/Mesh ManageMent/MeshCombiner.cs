﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MaterialType
{
    invaled,
    Atals1
}




[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshCombiner : MonoSingleton<MeshCombiner>
{
    protected override void Init()
    {
        AllowInstancesDuringEditMode = true;
    }
# if UNITY_EDITOR
    //public Material mesh_Material_Atals1()
    //{
    //    return EidtorSceneManager.Instance.Atals1;
    //}
#endif
    public MaterialType materialType = MaterialType.invaled;

    public MeshFilter current_MeshFilter;
    public MeshRenderer current_MeshRender;
    private BoxCollider current_Collider;
    public Vector3 SIZE = new Vector3(0.5f, 0.5f, 0.5f);

    public void SetMesh()
    {
        current_MeshFilter = GetComponent<MeshFilter>();
        current_MeshRender = GetComponent<MeshRenderer>();

    }

    public void SetMat()
    {

        //if (materialType == MaterialType.Atals1)
        //{
        //    if (mesh_Material_Atals1() != null)
        //    {
        //        current_MeshRender.material = mesh_Material_Atals1();

        //    }
        //}
    }


    public void CombineMeshes()
    {
        gameObject.tag = "Obj";

        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;
        transform.
        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;

        MeshFilter[] filters = GetComponentsInChildren<MeshFilter>();
        Debug.Log(name + "is combineing massages ");

        Mesh Finel_Mesh = new Mesh();

        CombineInstance[] combiners = new CombineInstance[filters.Length];

        for (int i = 0; i < filters.Length; i++)
        {


            combiners[i].subMeshIndex = 0;
            combiners[i].mesh = filters[i].sharedMesh;
            combiners[i].transform = filters[i].transform.localToWorldMatrix;
        }

        Finel_Mesh.CombineMeshes(combiners);
        Finel_Mesh.name = "newMesh";
        current_MeshFilter.sharedMesh = Finel_Mesh;

        transform.rotation = oldRot;
        transform.position = oldPos;



        for (int i = 0; i < filters.Length; i++)
        {
            if (transform.childCount > 0)
            {
                if (transform.GetChild(i) != null && transform.GetChild(i).gameObject.activeInHierarchy)
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }
            }

            if (current_Collider == null)
                current_Collider = this.gameObject.AddComponent<BoxCollider>() as BoxCollider;

            SetMat();
            this.transform.localScale = SIZE;
            gameObject.layer = 15;
        }



    }


}
