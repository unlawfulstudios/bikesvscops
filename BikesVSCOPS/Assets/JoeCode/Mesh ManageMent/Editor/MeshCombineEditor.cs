﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshCombiner))]
public class MeshCombineEditor : Editor
{
    MeshCombiner mc;

    private void Awake()
    {
        mc = target as MeshCombiner;

        if(mc != null)
        mc.SetMesh();
    }


    public override void OnInspectorGUI()
    {

        base.DrawDefaultInspector();


        if (GUILayout.Button("Combine"))
        {
            mc.CombineMeshes();
        }
        if (GUILayout.Button("SetMat"))
        {
            mc.SetMat();
        }

    }



}
