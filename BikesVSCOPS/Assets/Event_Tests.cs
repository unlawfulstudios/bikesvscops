﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Event_Tests : MonoBehaviour {

    private UnityAction[] Run_Listener;
    private string[] Run_Event_Names = { "Ready", "Start","PlayerBusted", "PlayerEnd" ,"AiEnd"};
  

    private void Awake()
    {
        Run_Listener[0] = new UnityAction(Ready_Run_Function);
        Run_Listener[1] = new UnityAction(Start_Run_Function);
        Run_Listener[2] = new UnityAction(Player_Busted_Function);
        Run_Listener[3] = new UnityAction(Ai_End_Run_Function);
        Run_Listener[4] = new UnityAction(Player_End_Run_Function);
    }

    private void OnEnable()
    {
        Init();
    }
    private void OnDisable()
    {
        Disable();
    }
    
    private void Ready_Run_Function()
    {
        Debug.Log("Ready_Run_Function was called ");
    }
    private void Start_Run_Function()
    {
        Debug.Log("Start_Run_Function was called ");
    }
    private void Player_Busted_Function()
    {
        Debug.Log("Player Busted function was called ");
    }
    private void Player_End_Run_Function()
    {
        Debug.Log("Player End_Run_Function was called ");
    }
    private void Ai_End_Run_Function()
    {
        Debug.Log("Ai End_Run_Function was called ");
    }

    void Init()
    {

        
    }
    void Disable()
    {
    }
}
