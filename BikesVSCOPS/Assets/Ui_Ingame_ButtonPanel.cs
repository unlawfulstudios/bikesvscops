﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui_Ingame_ButtonPanel : InGameBehaviors {

    public GameObject Arrow;
    
    public bool isfinelrun = false;
    public bool isUnlockBike = false;

    public bool HidesObjects = false;

    public bool has_Complete()
    {
        return GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run.WinAmmount.value > 1 ;
    }

    public void Start()
    {
        if (GlobaleManagers.current_UserProfile.bikesVSCopsProfileInfo.current_Run.id != 7)
        {
            Arrow.SetActive(has_Complete());
        }
        else
        {
            Arrow.SetActive(false);
        }
           
    }
}
