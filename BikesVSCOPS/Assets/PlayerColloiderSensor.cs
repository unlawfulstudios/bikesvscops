﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerColloiderSensor : MonoBehaviour {


    public Cop_AI copAi { get { return this.GetComponentInParent<Cop_AI>(); } }
    
   
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            copAi.hittingPlayer = true;


            collision.gameObject.GetComponent<BikeControl>().CrashTrigger(true);

                Debug.Log("stopppppppppp");

            
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            copAi.hittingPlayer = false;
        }
    }
 
}
