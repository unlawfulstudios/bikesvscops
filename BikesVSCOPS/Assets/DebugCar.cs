﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;


public enum AiCarStates
{
    off,
    idle,
    GoingForward,
    Stoped,
    Turning
}


public class DebugCar : MonoBehaviour
{

    public Transform target_Postion;
    public Transform target_Object;
    public Transform Right_Position;
    public CarAIControl cac;

    public void Start()
    {
        MoveCare();

    }
    private void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            MoveCare();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            TurnRight();
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Stop();
        }




        // Keep the console open in debug mode.
       

    }

    public void DebugLOG()
    {

  
    }
    public void MoveCare()
    {
        cac.SetTarget(target_Postion);
    }


    public void TurnRight()
    {
        
        cac.SetTarget(Right_Position);
        cac.m_Driving = true;
        
    }
    public void Stop()
    {
        cac.SetTarget(null);
    }
}

